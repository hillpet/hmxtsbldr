/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          NvmDefs.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Define the NVM storage members
**
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_NVMDEFS_H_)
#define _NVMDEFS_H_

typedef enum NVMDEFS
{
   // Standard Version CString
   NVM_VERSION = 0,

   // CStrings
   NVM_NAME_HUMAX,               // Current STB name/model
   NVM_HMX_PATHNAME,             // Full pathname
   NVM_HMX_DEFDIR,               // Default working directory
   NVM_HMX_DIRNAME,              // File directory
   NVM_HMX_FILENAME,             // File name
   NVM_HMX_FILEEXT_TS,           // File TS  extension
   NVM_HMX_FILEEXT_NTS,          // File NTS extension
   NVM_HMX_FILEEXT_HMT,          // File HMT extension
   NVM_HMX_FILEEXT_LOG,          // File HMT extension
   NVM_HMX_EVENTNAME,            // Event name
   NVM_HMX_TIME,                 // Time stamp
   NVM_HMX_DATE,                 // Date stamp

   NVM_HMX_BATCHMODE,            // Handle batch of files
   NVM_HMX_BATCHLIST,            // List batch of files
   NVM_HMX_OVERWRITE,            // Overwrite mode, do not ask

   NVM_TS_PACKETSIZE,            // Last TS packet size
   NVM_TS_TIMECODE,              // Insert Timecodes
   NVM_TS_NORESYNC,              // No 0x47 resync on TS errors
   
   NVM_NTS_AUTOHD,               // Auto/SD/HD setting

   NUM_WNVMS
}  NVMDEFS;

#endif // !defined(_NVMDEFS_H_)
