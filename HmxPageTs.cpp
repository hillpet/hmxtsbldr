/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageTs.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for TS issues
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxPageTs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CHmxPageTs, CPropertyPage)

//
// Poor mans solution to keep the main dialog window handle here
//
static HWND    hDialogWnd;

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageTs::CHmxPageTs() : CPropertyPage(CHmxPageTs::IDD)
{
    pstHmxComm = NULL;
    pclNvm     = (CNvmStorage *) new(CNvmStorage);
    VERIFY(pclNvm);

   //{{AFX_DATA_INIT(CHmxPageTs)
   //}}AFX_DATA_INIT
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageTs::~CHmxPageTs()
{
    //
    //  Get rid of the  NVM Storage Map
    //
    if(pclNvm)
    {
        delete pclNvm;
    }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_TS_EDIT_INFILE,     clTsEditInfile);
   DDX_Control(pDX, IDC_TS_EDIT_PACKETSIZE, clTsEditPacketSize);
   DDX_Control(pDX, IDC_TS_BTN_TIMECODE,    clTsBtnTimecodes);
   DDX_Control(pDX, IDC_TS_BTN_NO_TIMECODE, clTsBtnNoTimecodes);
   DDX_Control(pDX, IDC_TS_EDIT_PESPACKETS, clTsEditPesPackets);
   DDX_Control(pDX, IDC_TS_EDIT_PESERRORS,  clTsEditPesErrors);
   DDX_Control(pDX, IDC_TS_EDIT_PSIPACKETS, clTsEditPsiPackets);
   DDX_Control(pDX, IDC_TS_EDIT_PSIERRORS,  clTsEditPsiErrors);
   //{{AFX_DATA_MAP(CHmxPageTs)
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHmxPageTs, CPropertyPage)
   ON_BN_CLICKED(IDC_TS_BTN_TIMECODE,       OnBnClickedAddTimecodes)
   ON_BN_CLICKED(IDC_TS_BTN_NO_TIMECODE,    OnBnClickedRemoveTimecodes)
   //{{AFX_MSG_MAP(CHmxPageTs)
   //}}AFX_MSG_MAP
   ON_MESSAGE(WM_SOLVER_DATA,               OnRcvWindowData)
END_MESSAGE_MAP()




//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::LoadDialog()
{
    //
    // Load the PAGE data from storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::StoreDialog()
{
    //
    // Save the PAGE data to storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::UpdateDialog()
{
   CString  clFile, clStr, clExt;

   if(pclNvm)
   {
      // -1- Event name field
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME,   &clStr);
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_TS, &clExt);
      //
      clFile.Format(_T("%s.%s"), clStr.GetBuffer(), clExt.GetBuffer());
      clTsEditInfile.SetWindowText(clFile.GetBuffer());

      if(pstHmxComm)
      {
         clStr.Format(_T("%d"), pstHmxComm->iPacketSize);
         clTsEditPacketSize.SetWindowText(clStr.GetBuffer());
      }
      // -2- 

      // -3-
   }
   UpdateData(FALSE);      // data -> dialog window

}

BOOL CHmxPageTs::OnInitDialog()
{
   LOGFONT  stLogFont;

   CPropertyPage::OnInitDialog();

   LoadDialog();
   //
   // Handle fonts
   //
   memset(&stLogFont, 0, sizeof(stLogFont));
   strcpy(stLogFont.lfFaceName, "Arial");
   //
   // Send back the window handle for this dialog, so update messages arrive here
   //
   HWND hThisWnd = this->GetSafeHwnd();
   if(hDialogWnd)
   {
      ::PostMessage(hDialogWnd, WM_SOLVER_HANDLE, (WPARAM) hThisWnd, (LPARAM) HMX_WND_TS);
   }
   UpdateData(FALSE);      // data -> dialog window
   return TRUE;  // return TRUE unless you set the focus to a control
                 // EXCEPTION: OCX Property Pages should return FALSE
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::OnOK()
{
   UpdateData(TRUE);      // data <- dialog window

   StoreDialog();

   CPropertyPage::OnOK();
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageTs::OnCancel()
{
   CPropertyPage::OnCancel();
}

//
//  Function:  RequestWndHandle
//  Purpose:   Called from the Main dialog window to save its window handle
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageTs::RequestWndHandle(HWND hWnd)
{
   //
   // Get the window handle and send it to the solver thread for communicating
   // Accept dropped files
   //
   hDialogWnd = hWnd;
}

//
//  Function:  OnRcvWindowData
//  Purpose:   Message from the SOLVER Thread: new data is in
//
//  Parms:     WPARAM = 0
//             TPARAM = 0
//  Returns:   0
//
LRESULT CHmxPageTs::OnRcvWindowData(WPARAM tWp, LPARAM tLp)
{
   pstHmxComm = (HMXCOM *) tWp;

   //
   // Check if the packetsize is 188. If so, we might need to insert timecodes
   //
   if(pstHmxComm)
   {
      if(pstHmxComm->iPacketSize == 188)
      {
         clTsBtnTimecodes.EnableWindow(TRUE);
         clTsBtnNoTimecodes.EnableWindow(FALSE);
      }
      else
      {
         clTsBtnTimecodes.EnableWindow(FALSE);
         clTsBtnNoTimecodes.EnableWindow(TRUE);
      }
   }
   else
   {
      clTsBtnTimecodes.EnableWindow(FALSE);
      clTsBtnNoTimecodes.EnableWindow(FALSE);
   }

   UpdateDialog();
   return(0);
}


//
//  Function:  OnBnClickedAddTimecodes
//  Purpose:   BTN press: Add timecodes to the SPTS file
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageTs::OnBnClickedAddTimecodes()
{
   if(pstHmxComm->hSolverWnd)
   {
      ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_TIMECODES);
   }
}

//
//  Function:  OnBnClickedRemoveTimecodes
//  Purpose:   BTN press: Remove timecodes to the SPTS file
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageTs::OnBnClickedRemoveTimecodes()
{
   if(pstHmxComm->hSolverWnd)
   {
      ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_NO_TIMECODES);
   }
}
