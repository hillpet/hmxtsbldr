/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          CvbFiles.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Misc helper functions
**                      
**
**  Entry Points:       CVB_GetTimeDateStamp()
**                      CVB_GetTimeStamp()
**                      CVB_GetDateStamp()
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       16Feb2003
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "CvbFiles.h"


//
//  Function:   CVB_GetTimeDateStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "24/12/2007 09:12:25"
//
CString CVB_GetTimeDateStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%d/%m/%Y");
    s += " ";
    s += t.Format("%H:%M:%S");

    return(s);
}

//
//  Function:   GetDateStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "24/12/2007"
//
CString CVB_GetDateStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%d/%m/%Y");

    return(s);
}

//
//  Function:   CVB_GetTimeStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "09:12:25"
//
CString CVB_GetTimeStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%H:%M:%S");
    return(s);
}


//
//  Function:   CVB_SafeMalloc
//  Purpose:    Allocate a block of memory, clear all data
//  Parms:      Size
//  Returns:    Pointer to the block
//     Note:    The safe function keeps the size at the start of the memory block
//
static size_t tAllocated = 0;

void *CVB_SafeMalloc(size_t tSize)
{
   void    *pvData = NULL;
   
   if(tSize)
   {
      tSize += 8;
      pvData = malloc(tSize);
      tAllocated += tSize;
      if(pvData) 
      {
         memset(pvData, 0, tSize);
         pvData = CVB_ulong_put((u_int8 *)pvData, (u_int32)0xdeadbeef);
         pvData = CVB_ulong_put((u_int8 *)pvData, (u_int32)tSize);
      }
   }
   return(pvData);
}

//
//  Function:   CVB_SafeFree
//  Purpose:    Free the block of memory
//  Parms:      Pointer to the block
//  Returns:    NULL
//
void *CVB_SafeFree(void *pvData)
{
   u_int32  ulSig, ulTemp;
   u_int8  *pubTemp;

   pubTemp = (u_int8 *)pvData;
   //
   if(pubTemp)
   {
      pubTemp -= 8;
      ulSig  = CVB_ulong_get(&pubTemp[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         AfxMessageBox("Malloc-Free signature error !");
         return(NULL);
      }
      ulTemp = CVB_ulong_get(&pubTemp[4], 0xffffffff, 0);
      if(ulTemp > tAllocated)
      {
         AfxMessageBox("Malloc-Free issues !");
         return(NULL);
      }
      tAllocated -= (size_t) ulTemp;
      free(pubTemp);
   }
   return(NULL);
}

//
//  Function:   CVB_SafeGetMallocSize
//  Purpose:    Get the size of a block of allocated memory
//  Parms:      Pointer to the block
//  Returns:    Size
//
u_int32 CVB_SafeGetMallocSize(void *pvData)
{
   u_int32  ulSig, ulSize=0;
   u_int8  *pubTemp;

   pubTemp = (u_int8 *)pvData;
   //
   if(pubTemp)
   {
      pubTemp -= 8;
      ulSig  = CVB_ulong_get(&pubTemp[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         AfxMessageBox("Malloc-Free signature error !");
      }
      else
      {
         ulSize = CVB_ulong_get(&pubTemp[4], 0xffffffff, 0);
      }
   }
   return(ulSize);
}

/**
 **  Name:         CVB_ascii_to_ulong
 **
 **  Description:  Converts an ascii-string into an ulong value.
 **
 **  Arguments:    string ^, value ^
 **
 **  Returns:      Address after converted value.
 **
 **/

char *CVB_ascii_to_ulong(char *pcValue, u_int32 *pulValue)
{
   u_int8 ubIndex = 0;

   //
   // Reset value
   //
   *pulValue = 0;

   while  (
            (isdigit(*pcValue))
            &&
            (ubIndex++ < 10)
          )
   {
      *pulValue *= 10;
      *pulValue += (u_int32) ((u_int32) *pcValue++ - (u_int32) '0');
   }

   return (pcValue);
}

/**
 **  Name:         CVB_ascii_to_ushort
 **
 **  Description:  Converts an ascii-string into an ushort value.
 **
 **  Arguments:    string ^, value ^
 **
 **  Returns:      Address after converted value.
 **
 **/

char *CVB_ascii_to_ushort(char *pcValue, u_int16 *usValue)
{
   u_int8 ubIndex = 0;

   //
   // Reset value
   //
   *usValue = 0;

   while  (
            (isdigit(*pcValue))
            &&
            (ubIndex++ < 5)
          )
   {
      *usValue *= 10;
      *usValue += (u_int16) (*pcValue++ - '0');
   }

   return (pcValue);
}

/**
 **  Name:         CVB_BCD_to_ulong
 **
 **  Description:  Convert BCD to u_int32
 **
 **  Arguments:    32 bits BCD
 **
 **  Returns:      u_int32
 **/
u_int32 CVB_BCD_to_ulong(u_int32 ulBcd)
{
   u_int32    ulHex, ulFactor;
   u_int8    nr;

   ulFactor = 1L;
   ulHex    = 0;

   for(nr = 0; nr < 8; nr++)
   {
      ulHex += (ulBcd & 0xf) * ulFactor;
      ulFactor *= 10;
      ulBcd >>= 4;
   }
   return (ulHex);
}

/**
 **  Name:         CVB_BCD_to_ushort
 **
 **  Description:  Convert BCD to u_int16
 **
 **  Arguments:    16 bits BCD
 **
 **  Returns:      u_int32
 **/
u_int16 CVB_BCD_to_ushort(u_int16 usBcd)
{
   u_int16   usHex, usFactor;
   u_int8    nr;

   usFactor = 1;
   usHex    = 0;

   for(nr = 0; nr < 4; nr++)
   {
      usHex += (usBcd & 0xf) * usFactor;
      usFactor *= 10;
      usBcd >>= 4;
   }
   return (usHex);
}

/**
 **  Name:         CVB_ubyte_get
 **
 **  Description:  Gets a ubyte from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int8
 **/
u_int8 CVB_ubyte_get(u_int8 *pubSrc, u_int8 ubMask, u_int8 ubShift)
{
   u_int8 ubTemp;

   ubTemp   = *pubSrc;
   ubTemp >>= ubShift;
   ubTemp  &= ubMask;

   return (ubTemp);
}

/**
 **  Name:         CVB_ubytes_put
 **
 **  Description:  Puts ubytes in a byte array
 **
 **  Arguments:    dest^, src^, nr
 **
 **  Returns:      new dest^
 **/
u_int8 *CVB_ubytes_put(u_int8 *pubDest, u_int8 *pubSrc, int iSize)
{
   return ( CVB_ubytes_get(pubDest, pubSrc, iSize) );
}

/**
 **  Name:         CVB_ubytes_get
 **
 **  Description:  Gets ubytes from a byte array
 **
 **  Arguments:    dest^, src^, nr
 **
 **  Returns:      new dest^
 **/
u_int8 *CVB_ubytes_get(u_int8 *pubDest, u_int8 *pubSrc, int iSize)
{
   int   i;

   for(i=0; i<iSize; i++)
   {
      *pubDest++ = *pubSrc++;
   }
   
   return (pubDest);
}

/**
 **  Name:         CVB_ulong_get
 **
 **  Description:  Gets a ulong from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int32
 **/
u_int32 CVB_ulong_get(u_int8 *pubSrc, u_int32 ulMask, u_int8 ubShift)
{
   u_int32 ulTemp;

   ulTemp = ((u_int32)*pubSrc       << 24) +
            ((u_int32)*(pubSrc + 1) << 16) +
            ((u_int32)*(pubSrc + 2) <<  8) +
             (u_int32)*(pubSrc + 3);
   ulTemp >>= ubShift;
   ulTemp &= ulMask;

   return (ulTemp);
}

/**
 **  Name:         CVB_ubyte_put
 **
 **  Description:  Puts a ubyte into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ubyte_put(u_int8 *ubPtr, u_int8 ubValue)
{
   *ubPtr++ = ubValue;
   return (ubPtr);
}


/**
 **  Name:         CVB_ulong_put
 **
 **  Description:  Puts a ulong into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ulong_put(u_int8 *ubPtr, u_int32 ulValue)
{
   ubPtr[0] = (u_int8)(ulValue >> 24);
   ubPtr[1] = (u_int8)(ulValue >> 16);
   ubPtr[2] = (u_int8)(ulValue >> 8);
   ubPtr[3] = (u_int8)(ulValue);
   return (ubPtr+4);
}

/**
 **  Name:         CVB_ulong_to_BCD
 **
 **  Description:  Converts a u_int32 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int32 CVB_ulong_to_BCD(u_int32 ulValue)
{
   u_int8 ubIndex,
         ubShift = 0;

   u_int32 ulBcdDigit,
         ulRet = 0;

   for (ubIndex = 0; ubIndex < 8; ubIndex++)
   {
      ulBcdDigit   = ulValue % 10;
      ulBcdDigit <<= ubShift;
      ulRet       |= ulBcdDigit;
      ulValue     /= 10;
      ubShift     += 4;
   }

   return (ulRet);
}

/**
 **  Name:         CVB_ushort_get
 **
 **  Description:  Get's ushort value out of byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      value
 **/

u_int16 CVB_ushort_get(u_int8 *ubPtr, u_int16 usMask, u_int8 ubShift)
{
   u_int16 usTemp;

   usTemp = (*ubPtr << 8) + *(ubPtr + 1);
   usTemp >>= ubShift;
   usTemp  &= usMask;

   return (usTemp);
}

/**
 **  Name:         CVB_ushort_put
 **
 **  Description:  Puts a ushort into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ushort_put(u_int8 *ubPtr, u_int16 usValue)
{
   ubPtr[0] = (u_int8) (usValue >> 8);
   ubPtr[1] = (u_int8) (usValue);
   return (ubPtr+2);
}


/**
 **  Name:         CVB_ushort_to_BCD
 **
 **  Description:  Converts a u_int16 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int16 CVB_ushort_to_BCD(u_int16 usValue)
{
   u_int8  ubIndex,
          ubShift = 0;

   u_int16 usBcdDigit,
          usRet = 0;

   for (ubIndex = 0; ubIndex < 4; ubIndex++)
   {
      usBcdDigit   = usValue % 10;
      usBcdDigit <<= ubShift;
      usRet       |= usBcdDigit;
      usValue     /= 10;
      ubShift     += 4;
   }

   return (usRet);
}

/**
 **  Name:         CVB_ushort_update
 **
 **  Description:  Updates a ushort in a byte array with an offset
 **
 **  Arguments:    array ^, offset
 **
 **  Returns:      None
 **/

void CVB_ushort_update(u_int8 *ubPtr, u_int16 usOffset)
{
   u_int16 usValue;

   //
   // Get value from array
   //
   usValue  = CVB_ushort_get(ubPtr, 0xFFFF, 0);

   //
   // Update the value
   //
   usValue += usOffset;

   //
   // Put updated value back into array
   //
   CVB_ushort_put(ubPtr, usValue);
}

/**
 **  Name:         CVB_llong_get
 **
 **  Description:  Gets a int64 (long long) from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      llong
 **/
int64 CVB_llong_get(u_int8 *pubSrc, int64 llMask, u_int8 ubShift)
{
   int64 llTemp;

   llTemp = ((int64)*pubSrc       << 56) +
            ((int64)*(pubSrc + 1) << 48) +
            ((int64)*(pubSrc + 2) << 40) +
            ((int64)*(pubSrc + 3) << 32) +
            ((int64)*(pubSrc + 4) << 24) +
            ((int64)*(pubSrc + 5) << 16) +
            ((int64)*(pubSrc + 6) <<  8) +
             (int64)*(pubSrc + 7);
   llTemp >>= ubShift;
   llTemp &= llMask;

   return (llTemp);
}

/**
 **  Name:         CVB_llong_put
 **
 **  Description:  Puts a int64 (long long) into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/
u_int8 *CVB_llong_put(u_int8 *ubPtr, int64 llValue)
{
   *ubPtr++ = (u_int8)(llValue >> 56);
   *ubPtr++ = (u_int8)(llValue >> 48);
   *ubPtr++ = (u_int8)(llValue >> 40);
   *ubPtr++ = (u_int8)(llValue >> 32);
   *ubPtr++ = (u_int8)(llValue >> 24);
   *ubPtr++ = (u_int8)(llValue >> 16);
   *ubPtr++ = (u_int8)(llValue >> 8);
   *ubPtr++ = (u_int8)(llValue);
   //
   return (ubPtr);
}

