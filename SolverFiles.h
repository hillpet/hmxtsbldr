/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:  SolverFiles.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the Solver Sudoku helper functions
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_SOLVERFILES_H_)
#define _SOLVERFILES_H_

int         SOLVER_ResolveInit      (CSolver *, int);

#endif // !defined(_SOLVERFILES_H_)
