/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          TsBldrDlg.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Implementation of the Humax 5050C TS metafile generator dialog application.
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#pragma once
#include "AutoFont.h"
#include "ReadOnlyEdit.h"
#include "afxwin.h"
#include "afxcmn.h"


//
// Main dialog class
//
class CTsBuilderDlg : public CDialog
{
public:
   // Construction
   CTsBuilderDlg(CWnd* pParent = NULL); // standard constructor
   // Dialog Data
   enum { IDD = IDD_MY5050C_DIALOG };

   void              ExitTsBuilder              (void);
   void              SolverCmdBuild             (void);
   void              SolverCmdBuildBatch        (void);
   void              SolverCmdCheck             (void);
   void              SolverCmdParse             (void);
   void              SolverLog                  (char *);
   void              StatusAddText              (char *);
   void              CreateProgressBar          (void);
   void              UpdateButtonStatus         (STBTN);
   void              ForceExtensionCompatible   (void);
   void              ForceNameCompatible        (void);

protected:
   virtual void      DoDataExchange    (CDataExchange* pDX);

private:
    CNvmStorage     *pclNvm;


protected:
   // Implementation
   HICON             m_hIcon;
   // Generated message map functions
   virtual BOOL      OnInitDialog            ();
   afx_msg void      OnSysCommand            (UINT nID, LPARAM lParam);
   afx_msg void      OnPaint                 ();
   afx_msg HCURSOR   OnQueryDragIcon         ();
   afx_msg void      OnDropFiles             (HDROP);
   afx_msg void      OnClose                 ();
   // 
   afx_msg void      OnBnClickedProperties   ();
   afx_msg void      OnBnClickedBrowseTs     ();
   afx_msg void      OnBnClickedParseTs      ();
   afx_msg void      OnBnClickedRunTest      ();
   afx_msg void      OnBnClickedBuild        ();
   afx_msg void      OnBnClickedCheckCompat  ();
   afx_msg void      OnBnClickedCheckLog     ();
   afx_msg void      OnBnClickedMyExit       ();

public:
   afx_msg LRESULT   OnRcvSolverHandle    (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverUpdate    (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverReady     (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverProgBar   (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverComm      (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverData      (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvSolverExited    (WPARAM, LPARAM);

   DECLARE_MESSAGE_MAP()

private:
   volatile BOOL     fDoExit;             // Final marker to exit
   HMXCOM           *pstHmxComm;          // Communication srtucture
   CAutoFont        *pclFontSts;          // Font handler for Status fields
   CAutoFont        *pclFontBtn;          // Font handler for status buttons
   CAutoFont        *pclFontIdx;          // Font handler for Index
   CProgressCtrl     clHmxScanProgress;   // SPTS parser progress
   CReadOnlyEdit     clHmxEditStatus;     // Used in Main DIALOG to hold misc status
   CReadOnlyEdit     clHmxEditInfile;     // Used in Main DIALOG to hold event name
   CButton           clHmxCheckLog;       // Event Logfile     on/off
   CButton           clHmxCheckCompat;    // Hmx compatibility on/off
   CReadOnlyEdit     clHmxEditRunTest;    // Run test edit field
   CReadOnlyEdit     clHmxEditRunParm;    // Run parm edit field
   CReadOnlyEdit     clHmxEditLogLevel;   // Log level edit field
   CReadOnlyEdit     clHmxBtnTs;          // Status Button TS
   CReadOnlyEdit     clHmxBtnNts;         // Status Button NTS
   CReadOnlyEdit     clHmxBtnHmt;         // Status Button HMT
};
