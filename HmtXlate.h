/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmtXlate.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Catalog translate functions
**
**  Entries:            
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       24Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef  _HMTXLATE_H_
#define  _HMTXLATE_H_

int HMT_CatelogTranslate    (HMXCOM *);


#endif  /* _HMTXLATE_H_ */
