/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:  SolverThread.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the *.cpp
**
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/
#if !defined(_SolverThread_h_)
#define _SolverThread_h_

//
// Thread global functions
//
UINT  SolverCommThread           (LPVOID);

BOOL  SolverSendUpdateToApp      (HMXCOM *, HMXMSGID, char *, int, int, int, int);

BOOL  SolverMessageToApp         (HMXCOM *, WMESG, LPVOID, int);
BOOL  SolverTextMessageToApp     (HMXCOM *, WMESG, char *);

BOOL  SolverNewMessageToApp      (HMXCOM *, WMESG, char *);
BOOL  SolverNewMessageToApp      (HMXCOM *, WMESG, char *, int);
BOOL  SolverNewMessageToApp      (HMXCOM *, WMESG, int);


#endif   // _SolverThread_h_
