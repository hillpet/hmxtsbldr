/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageTs.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for TS issues
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(AFX_HMXPAGETS_H)
#define AFX_HMXPAGETS_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AutoFont.h"
#include "ReadOnlyEdit.h"
#include "afxwin.h"
#include "afxcmn.h"


class CHmxPageTs : public CPropertyPage
{
   DECLARE_DYNCREATE(CHmxPageTs)

// Construction
public:
   CHmxPageTs();
   ~CHmxPageTs();

   void           LoadDialog     (void);
   void           StoreDialog    (void);
   void           UpdateDialog   (void);
   //
   static void    RequestWndHandle(HWND);

// Dialog Data
   //{{AFX_DATA(CHmxPageTs)
   enum { IDD = IDD_PROP_HMX_TS };
   //}}AFX_DATA


// Overrides
   // ClassWizard generate virtual function overrides
   //{{AFX_VIRTUAL(CHmxPageTs)
   public:
   virtual void OnOK();
   virtual void OnCancel();
   protected:
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
   //}}AFX_VIRTUAL

// Implementation
private:
   CNvmStorage      *pclNvm;
   HMXCOM           *pstHmxComm;          // Communication srtucture

public:
   CReadOnlyEdit     clTsEditInfile;      // Event name
   CReadOnlyEdit     clTsEditPacketSize;  // Packetsize
   CReadOnlyEdit     clTsEditPesPackets;  // PES: # of packets
   CReadOnlyEdit     clTsEditPesErrors;   //      # of errors/warnings
   CReadOnlyEdit     clTsEditPsiPackets;  // PSI: # of packets
   CReadOnlyEdit     clTsEditPsiErrors;   //      # of errors/warnings
   CButton           clTsBtnTimecodes;    // Button: Add Timecodes
   CButton           clTsBtnNoTimecodes;  // Button: Remove Timecodes
   

protected:
   // Generated message map functions
   //{{AFX_MSG(CHmxPageTs)
   virtual BOOL OnInitDialog();
   //}}AFX_MSG
   DECLARE_MESSAGE_MAP()

public:
   afx_msg void      OnBnClickedAddTimecodes    ();
   afx_msg void      OnBnClickedRemoveTimecodes ();
   afx_msg LRESULT   OnRcvWindowData            (WPARAM, LPARAM);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HMXPAGETS_H)
