/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxFiles.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Define the Hmx helper functions
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       11Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_HMXFILES_H_)
#define _HMXFILES_H_


//---------------------------------------------------------------------------------------
// HMT or Catalog structures
//---------------------------------------------------------------------------------------
typedef enum HMTCAT
{
   HMT_START            = 0x0000,   //   0 Start HMT buffer
   HMT_CHANNEL          = 0x0011,   //  17 Channel number
   HMT_TIME_START       = 0x0019,   //  25 DateTime stamp start
   HMT_TIME_END         = 0x001d,   //  29 DateTime stamp end
   HMT_PATHNAME         = 0x0021,   //  33 Linux pathname on /media/sda1/*
   HMT_EVENTNAME        = 0x0120,   // 288 Event name
   HMT_SERVICENAME      = 0x0220,   // 544 Service name
   HMT_RECORDING_OK     = 0x0242,   // 578 Recording is OKee
   HMT_ICONS1           = 0x0243,   // 579 Icons : 0x20 NEW icon
   HMT_ICONS2           = 0x0244,   // 579 Icons : 0x80 HD icon
   //
   HMT_SRVSID           = 0x02f3,   // 755 Service ID
   HMT_PATPID           = 0x02f5,   // 757 PAT
   HMT_VIDPID           = 0x02f7,   // 759 Video PID
   HMT_AUDPID           = 0x02f9,   // 761 Audio PID
   HMT_PCRPID           = 0x02fb,   // 763 PCR   PID (mostly in the Video)
   HMT_TTXPID           = 0x02fd,   // 765 Sub-Picture PID (DVB-TTX)
   HMT_UNKPID           = 0x02ff,   // 767 ??
   HMT_TIME_RESTART     = 0x0307,   // 775 Start playback time in secs
   HMT_XXXX_RESTART     = 0x030c,   // 780 Start playback time in secs
   //
   HMT_EVENTS           = 0x1000,   // 4096 Present/Following Events
   HMT_FILE_END         = 0x2000    // 8192 EOF
   // 
} HMTCAT;

#define  HMT_PATHNAME_MAX           128
#define  HMT_EVENTNAME_MAX          64
#define  HMT_SERVICENAME_MAX        32
#define  HMT_PF_EVENT_MAX           256
#define  HMT_PF_DESCR_MAX           (HMT_FILE_END-HMT_PF_EVENTS)

//
// The HMT p/f Event descriptors
//
#define  HMT_PF_NUM_EVENTS          HMT_EVENTS
#define  HMT_PF_EVENTS              HMT_EVENTS+4
// LOOP-1 start
// {
//    LOOP-2 start
//    {
//       Following 10 are event id related      //  Evnt1  Evnt2  Evnt3 (on 3 existing p/f events
#define  HMT_PF_EVT_ID              0           //  01.1d  07.1e  07.1f
#define  HMT_PF_EVT_STUFF1          2           //  00.00  00.00  00.00
#define  HMT_PF_EVT_STUFF2          4           //  00.00  00.00  00.00
#define  HMT_PF_EVT_STUFF3          6           //  00.00  00.3c  07.44
#define  HMT_PF_EVT_STUFF4          8           //  00.00  00.00  00.00
#define  HMT_PF_EVT_STUFF5          10          //  00.3c  07.08  0e.10  0x10=enable evt name/descr
#define  HMT_PF_EVT_STUFF6          12          //  04.01  04.01  04.01
#define  HMT_PF_EVT_STUFF7          14          //  02.03  02.03  09.00
#define  HMT_PF_EVT_STUFF8          16          //  00.00  00.00  00.00
#define  HMT_PF_EVT_STUFF9          18          //  00.00  00.00  00.00
#define  HMT_PF_EVT_NAME            20          //  05." -- Event 1 name -- ".00
#define  HMT_PF_EVT_FLAGS10         532         //  01.00.00.00 or 00.00.00.00
#define  HMT_PF_EVT_FLAGS11         536         //  00.xx.xx.xx or 00.00.00.00
#define  HMT_PF_EVT_SIZE1           540         //  00.00.00.b5
#define  HMT_PF_EVT_SIZE2           544         //  00.00.00.b5
#define  HMT_PF_EVT_IDX             548         //  00.00.00.01
//       LOOP-3 start
//       {
#define  HMT_PF_EVT_ISO             552         //  "dut"0
#define  HMT_PF_EVT_STUFF10         556         //  00.00.00.00
#define  HMT_PF_EVT_STUFF11         560         //  00.00.00.00
#define  HMT_PF_EVT_TEXT_LEN        564         //  Text block size
#define  HMT_PF_EVT_TEXT            566         //  Event text block
//       }  LOOP-3 end
//    }     LOOP-2 end
// }        LOOP-1 end
//
#define  HMT_PF_EVT_OVERHEAD_LEN    46          // Total size of overhead bytes in p/f event

typedef struct HMTPF
{
   u_int16  usEventId;                          // Event id
   int      iAddr;                              // Address/offset
   u_int32  ulIso;                              // ISO language descriptor
   CString *pclEventName;                       // Event Name
   u_int32  ulFlags10;                          // Some flags ?
   u_int32  ulFlags11;                          // Some offset4 ?
   CString *pclEventDescr;                      // Event descriptor
   HMTPF   *pstNext;                            // Next event
}  HMTPF;

typedef struct HMTEVT
{
   u_int32  ulNumEvents;                        // Number of p/f events 
   HMTPF   *pstPf;                              // Next Event or NULL
}  HMTEVT;

typedef struct HMTREC
{
   u_int32  ulStart1;                           // HMT_START 0000
   u_int32  ulStart2;                           // HMT_START 0004
   u_int32  ulStart3;                           // HMT_START 0008
   u_int32  ulStart4;                           // HMT_START 000c
   u_int16  usChannelNr;                        // HMT_CHANNEL
   u_int32  ulStartTime;                        // HMT_TIME_START:   00.49.df.29
   u_int32  ulEndTime;                          // HMT_TIME_END:     a4.49.df.29
   u_int8   ubRecordingOk;                      // HMT_RECORDING_OK
   u_int8   ubIconFlags1;                       // HMT_ICONS1
   u_int8   ubIconFlags2;                       // HMT_ICONS2
   u_int16  usServiceId;                        // HMT_SRVSID
   u_int16  usVideoPid;                         // HMT_VIDPID
   u_int16  usAudioPid;                         // HMT_AUDPID
   u_int16  usPcrPid;                           // HMT_PCRPID
   u_int16  usTtxPid;                           // HMT_TTXPID
   u_int16  usStartPlay;                        // HMT_TIME_RESTART
   CString *pclFilePath;                        // HMT_PATHNAME:     /media/sda1/Peter/Event_20090415_1100
   CString *pclEvent;                           // HMT_EVENTNAME:    Event
   CString *pclService;                         // HMT_SERVICENAME:  Nederland 3
   HMTEVT  *pstEvt;                             // List of P/F events
}  HMTREC;

typedef struct PIDLST
{
   int         iIndex;                 // Index in the list
   BOOL        fInUse;                 // This PID is also in the SPTS
   u_int16     usPid;                  // The PID
   u_int16     usSid;                  // The SID (if available)
   u_int32     ulPsiPackets;           //  # of PSI packets
   u_int32     ulPesPackets;           //  # of PES packets
   u_int16     usPacketSkip;           // PES Packet data to skip
   char        pcEsName[HMX_NAME_LEN]; // Name
   PIDLST     *pstNext;                // Next PID
}  PIDLST;


//---------------------------------------------------------------------------------------
// NTS or Index file structures
//---------------------------------------------------------------------------------------
typedef enum NTSIDX
{
   NTS_FRTYPE           = 0,     // [1] I=1, P=2, B=3, D=4
   NTS_OFFSET_I         = 1,     // [3] Offset I-frame 00.00.01.b3 and P/B frame 00.00.01.00
   NTS_OFFSET_SH        = 4,     // [2] Offset I-frame 00.00.01.b3 to I    frame 00.00.01.00
   NTS_FRAME_INDEX      = 6,     // [1] 0..F (skipping other PIDs)
   NTS_DATA1            = 7,     // [1] Fixed value 0x24 (SD) or 0x34 (HD)
   NTS_FILEPTR          = 8,     // [8] Points to Pict Startcode 00.00.01.00
   NTS_PTS              = 16,    // [4] PTS>>1 of last received Video Sequence Header (00.00.01.Ex)
   NTS_FRSIZE           = 20,    // [4] Size of the current frame
   NTS_TIMESTAMP        = 24,    // [4] Time stamp for timeline display and trick modes
   NTS_DATA2            = 28,    // [4] Fixed value 0x0

   NTS_IDX_SIZE         = 32     // Record length
}  NTSIDX;

typedef struct NTSREC
{
   //------------------------------------------------------------------------------------
   // NTS records Fields
   //------------------------------------------------------------------------------------
   u_int8   ubField1a;           // [1] I=1, P=2, B=3, D=4
   u_int32  ulField1b;           // [3] See comments
   u_int16  usField2a;           // [2] See comments
   u_int8   ubField2b;           // [1] 0x00..0x0f (skipping other PIDs)
   u_int8   ubField2c;           // [1] Unknown value 0x24 (SD) or 0x34 (HD)
   u_int64  llField34;           // [8] Points to Pict Startcode 00.00.01.00 (SD) 0r 00.00.01.06 (HD)
   u_int32  ulField5;            // [4] PTS>>1 of last received Video Sequence Header (00.00.01.Ex)
   u_int32  ulField6;            // [4] 00.01.06.cf, 00.00.77.40, 00.00.92.40, ..
   u_int32  ulField7;            // [4] Trick mode time stamp in mSecs
   u_int32  ulField8;            // [4] 00.00.00.00, same
   u_int32  ulField9;            // [4] HD-only : See comments
   u_int32  ulField10;           // [4] HD-only : See comments
   //------------------------------------------------------------------------------------
   // NTS Field cache data
   //------------------------------------------------------------------------------------
   u_int8   ubFrType;            // I=1, P=2, B=3, D=4
   FRTYPE   tFrType;             // Current active frame type
   u_int64  llFp;                // Filepointer to picture start
   u_int32  ulFrSize;            // Size of current frame
   //------------------------------------------------------------------------------------
   // GOP collected info         // +---------------+----------------+----------------+
   u_int8   ubFrIdx;             // |Frame index    | 0x00.....??    | 0x00.....??    |
   u_int8   ubRefX;              // |Unknown        | 0x0024         | 0x0034         |
   u_int64  llRefA;              // |A              | FP 00.00.01.B3 | FP 00.00.01.68 |
   u_int64  llRefB;              // |B              | ---            | FP 00.00.01.67 |
   u_int64  llRefPictStart;      // |Pict Start     | FP 00.00.01.00 | FP 00.00.01.06 |
   u_int64  llRefPictStartI;     // |Pict Start I   | FP 00.00.01.00 | FP 00.00.01.06 |
   //                            // +---------------+----------------+----------------+
   //------------------------------------------------------------------------------------
   u_int32  ulThisVideoPts;      // This Video PTS (msecs)
   u_int32  ulPrevVideoPts;      // Prev Video PTS (msecs)
   u_int32  ulTrickPtsStart;     // Trick mode PTS (msecs) Initial value.
   u_int32  ulTrickPts;          // Trick mode PTS (msecs)
   u_int16  usRefAI;             // RefA of the I-Frame
   u_int64  llRefAnew;           // RefA of the new frame
   u_int64  llRefBnew;           // RefB of the new frame
   u_int32  ulPtsI;              // I-frame PTS (msecs)
   u_int32  ulPts;               // Last    PTS (msecs)
   u_int32  ulDts;               // Last    DTS (msecs)
   u_int32  ulPcr;               // Last    PCR (msecs)
   u_int64  llSizeStart;         // Startpoint frame size measure
}  NTSREC;

//---------------------------------------------------------------------------------------
// TS or Single Program Transport Stream structures
//---------------------------------------------------------------------------------------
typedef struct TS_MPEG2
{
   u_int8   ubTimecode[4];
   u_int8   ubSync;
   u_int8   ubFlag1Pid[2];
   u_int8   ubFlag2Cc;
   u_int8   ubPayload[TS_MAX_PAYLOAD_SIZE];
}  TS_MPEG2;


#define TS_MAX_PACKET_SIZE       SIZEOF(TS_MPEG2)

typedef struct DLGMSG
{
   HMXMSGID    tMsgId;                 // Message ID
   char       *pcMsg;                  // Additional text
   int         iData1;                 // Additional data 1
   int         iData2;                 // Additional data 2
   int         iData3;                 // Additional data 3
}  DLGMSG;

typedef struct LOGSTC
{                                      // Start code logs
   BOOL     fNotSup;                   // Not supported
   u_int32  ulCount;                   // Counts
}  LOGSTC;


//---------------------------------------------------------------------------------------
// Generic HMX data structures
//---------------------------------------------------------------------------------------
typedef struct HMXCOM
{
   SPTSCC      tCc;                    // Parser completion code
   BOOL        fEventOk;               // We have a TS stream 
   BOOL        fHaveIframe;            // Skip all frames until the 1st I-frame
   NTSDEF      tContentType;           // Auto/SD/HD
   int         iLogPriority;           // LOG priority
   int         iHmxCompat;             // We have an event complying to the HMX structure
   BOOL        fDoLog;                 // Maintain logfile
   int         iPacketSize;            // PES Packet size
   int         iNumEs;                 // Nr of Elem.Streams
   int64       llCurrentFp;            // Current TS file pointer
   // Counters
   u_int32     ulNumFrames;            // Total number of frames (I/P/D/D)
   u_int32     ulPesPackets;           // PES Nr of MPEG2 packets
   u_int32     ulPesErrors;            // PES Nr of MPEG2 errors
   u_int32     ulPsiPackets;           // PSI Nr of MPEG2 packets
   u_int32     ulPsiErrors;            // PSI Nr of MPEG2 errors
   // Durations
   int         iTsCreate;              // TS create time
   int         iDurationStart;         // Start time from GOP
   int         iDurationEnd;           // End   time from GOP
   // File storage etc 
   int         fFileTs;                // SPTS file      open
   int         fFileNts;               // Index file     open
   int         fFileHmt;               // Catalog file   open
   int         fFileLog;               // Log file       open
   CFile      *pclFileTs;              // SPTS file      class
   CFile      *pclFileNts;             // Index file     class
   CFile      *pclFileHmt;             // Catalog file   class
   CFile      *pclFileLog;             // Log file       class
   // Dialog window handles
   HWND        hDialogWnd;             // Main dialog window handle
   HWND        hPageGenWnd;            // PropertyPage-Generic window handle
   HWND        hPageTstWnd;            // PropertyPage-TS      window handle
   HWND        hPageNtsWnd;            // PropertyPage-NTS     window handle
   HWND        hPageHmtWnd;            // PropertyPage-HMT     window handle
   HWND        hSolverWnd;             // Solver Comm dialog   window handle
   // Misc class ptrs
   CNvmStorage *pclNvm;                // NVM storage
   CSolver     *pclComm;               // Communication
   u_int8      *pubHmtRaw;             // Raw   HMT stream
   int32        lHmtSize;              // Raw   HMT stream size
   HMTREC      *pstHmtCat;             // Work  HMT struct
   u_int8      *pubHmtCat;             // Clean HMT stream
   int32        lCatSize;              // Clean HMT stream size
   u_int8      *pubNts;                // NTS stream
   int          iNtsNrRecords;         //     nr records
   NTSREC      *pstNts;                //     structure
   u_int8       ubPatVersion;          // Version of the PAT
   u_int8       ubPmtVersion;          // Version of the PMT
   PIDLST      *pstPidList;            // PID list of SPTS
   LOGSTC      *pstStCodes;            // List of startcodes
}  HMXCOM;


typedef struct MSGSCH
{
   WMESG    tWmsg;
   HMXMSGID tMsgId;
   HMXWND   tDest;   
}  MSGSCH;

typedef struct XLAT
{                                      // Non printable chars translate
   char  cIn;                          // Translate this ..
   char  cOut;                         //  ... into this
}  XLAT;

typedef struct XLCAT
{                                      // Catalog template
   u_int32  ulAddr;                                // Address
   u_int32  ulData;                                // Data
   u_int32 (*pfHndlr)(HMXCOM *, XLCAT *, HMTDIR);  // Xlate f(pstComm, pstCat)
}  XLCAT;

typedef struct STCODES
{                                      // Start code handlers
   u_int8   ubStartCodeLo;                                  // Start code 0x00.00.01.xx low
   u_int8   ubStartCodeHi;                                  // Start code 0x00.00.01.xx high
   int      (*pfHndlr)(HMXCOM *, PIDLST *, u_int8 *, int);  // Handlerfct(...)
}  STCODES;

//
// Global function
// NVM Storage 
BOOL     HMX_GetFromStorage            (CNvmStorage *, int, char *, int);
BOOL     HMX_GetFromStorage            (CNvmStorage *, int, CString *);
BOOL     HMX_GetFromStorage            (CNvmStorage *, int, int *);
void     HMX_LogEvent                  (HMXCOM *, int, char *, BOOL);
void     HMX_LogEventText              (HMXCOM *, int, char *, char *, BOOL);
void     HMX_LogEventData              (HMXCOM *, int, char *, int, BOOL);
void     HMX_LogEventHexBytes          (HMXCOM *, int, char *, u_int8 *, int, BOOL);
void     HMX_LogEventHexData           (HMXCOM *, int, char *, int, BOOL);
void     HMX_LogEventLongHexData       (HMXCOM *, int, char *, u_int64, BOOL);
void     HMX_NewFile                   (CNvmStorage *);
int      HMX_OpenFile                  (CFile *, CNvmStorage *, NVMDEFS, char);
BOOL     HMX_PutIntoStorage            (CNvmStorage *, int, char *, int);
BOOL     HMX_PutIntoStorage            (CNvmStorage *, int, CString *);
BOOL     HMX_PutIntoStorage            (CNvmStorage *, int, int);
int      HMX_ReadFile                  (CFile *, char *, int);
void     HMX_ReadHmxFile               (CNvmStorage *, char *);
int      HMX_CheckCompatibility        (HMXCOM *);
BOOL     HMX_BuildTimeDateStamp        (CString *, CString *, CString *);
void     HMX_MakeFilename              (CNvmStorage *, CString *);
void     HMX_MakePathname              (CNvmStorage *, CString *);
HMXST    HMX_SplitPathname             (CNvmStorage *);
int      HMX_WriteFile                 (CFile *, char *, int);
void     HMX_WriteHmxFile              (CNvmStorage *, char *);
//
BOOL     HMX_CheckHdtv                 (HMXCOM *);
void     HMX_UpdateStreamType          (HMXCOM *, int);
int      HMX_CopyWithTimecode          (HMXCOM *, char *, int);
int      HMX_CopyWithoutTimecode       (HMXCOM *, char *, int);
void     HMX_Init                      (HMXCOM *);
void     HMX_CloseAllFiles             (HMXCOM *);
void     HMX_CloseLogFile              (HMXCOM *);
void     HMX_CloseMetaFiles            (HMXCOM *);
void     HMX_CloseTsFile               (HMXCOM *);
int      HMX_OpenMetaFiles             (HMXCOM *, char);
int      HMX_OpenLogFile               (HMXCOM *, char);
int      HMX_OpenTsFile                (HMXCOM *, char);
SPTSCC   HMX_ParseTsFile               (HMXCOM *);
int      HMX_ReadTsFile                (HMXCOM *, char *, int);
int      HMX_WriteTsFile               (HMXCOM *, char *, int);
int      HMX_WriteLogFile              (HMXCOM *, char *, int);
//
void     HMX_SecsToDateTime            (CString *, u_int32);
u_int32  HMX_DateTimeToSecs            (CString *);
void     HMX_SecsToDate                (CString *, u_int32);
void     HMX_SecsToTime                (CString *, u_int32);
//
// TS functions
void     TS_Cleanup                    (HMXCOM *);
void     TS_DumpPidList                (HMXCOM *, PIDLST *);
void     TS_DumpStartcodes             (HMXCOM *);
u_int16  TS_GetServiceId               (HMXCOM *);
void     TS_Init                       (HMXCOM *);
void     TS_HandlePacket               (HMXCOM *, TS_MPEG2 *);
int      TS_ParseStartCodes            (HMXCOM *, PIDLST *, TS_MPEG2 *, int);
//
// HMT Catalog functions
void     HMT_Init                      (HMXCOM *);
HMTPF   *HMT_CreatePresentFollowingList(HMXCOM *);
void     HMT_DeletePresentFollowingList(HMXCOM *);
void     HMT_Cleanup                   (HMXCOM *);
BOOL     HMT_AllocateDefaultHmtData    (HMXCOM *);
u_int32  HMT_CatalogTranslate          (HMXCOM *, HMTDIR);
void     HMT_CloseHmtFile              (HMXCOM *);
HMTPF   *HMT_FindCurrentEvent          (HMXCOM *);
u_int32  HMT_GenerateHmtFile           (HMXCOM *);
int      HMT_OpenHmtFile               (HMXCOM *, char);
SPTSCC   HMT_ParseHmtFile              (HMXCOM *);
u_int32  HMT_GetNumEvents              (HMXCOM *);
HMTPF*   HMT_GetEvent                  (HMXCOM *, int);
BOOL     HMT_ReadEvents                (HMXCOM *);
int      HMT_ReadHmtFile               (HMXCOM *, char *, int);
int      HMT_WriteHmtFile              (HMXCOM *, char *, int);

//
// NTS Index functions
void     NTS_Cleanup                   (HMXCOM *);
BOOL     NTS_AddIndexRecord            (HMXCOM *);
void     NTS_CloseNtsFile              (HMXCOM *);
int      NTS_OpenNtsFile               (HMXCOM *, char);
SPTSCC   NTS_ParseNtsFile              (HMXCOM *);
int      NTS_ReadNtsFile               (HMXCOM *, char *, int);
int      NTS_WriteNtsFile              (HMXCOM *, char *, int);
BOOL     NTS_ReadNtsRecord             (HMXCOM *, u_int8 *, int);
BOOL     NTS_WriteNtsRecord            (HMXCOM *, u_int8 *, int);

#endif // !defined(_HMXFILES_H_)
