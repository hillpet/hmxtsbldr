/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageNts.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for TS index file
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxPageNts.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CHmxPageNts, CPropertyPage)

//
// Poor mans solution to keep the main dialog window handle here
//
static HWND    hDialogWnd;
//
// Local prototypes
//

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageNts::CHmxPageNts() : CPropertyPage(CHmxPageNts::IDD)
{
   iNtsRecord  = 0;
   pstHmxComm  = NULL;
   pclNvm      = (CNvmStorage *) new(CNvmStorage);
   VERIFY(pclNvm);

   pclFontMap = (CAutoFont *)  new(CAutoFont)("Courier New");
   pclFontMap->SetHeight(HMX_MAP_FONT_SIZE);

   //{{AFX_DATA_INIT(CHmxPageNts)
   //}}AFX_DATA_INIT
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageNts::~CHmxPageNts()
{
    //
    //  Get rid of the  NVM Storage Map
    //
    if(pclNvm)
    {
        delete pclNvm;
    }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_NTS_EDIT_INFILE,     clNtsEditInfile);
   DDX_Control(pDX, IDC_NTS_EDIT_BASE,       clNtsEditBase);      // Base address
   DDX_Radio(pDX,   IDC_NTS_BTN_LOC,         iNtsBtnApply);       // Radio buttons
   //                                                                IDC_NTS_BTN_LOC 0
   //                                                                IDC_NTS_BTN_ONE 1
   //                                                                IDC_NTS_BTN_ALL 2
   //
   DDX_Radio(pDX,   IDC_NTS_BTN_AUTO,        iNtsBtnAutoHd);      // Radio buttons
   //                                                                IDC_NTS_BTN_AUTO 0
   //                                                                IDC_NTS_BTN_SDTV 1
   //                                                                IDC_NTS_BTN_HDTV 2
   //
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR01,     clNtsEditAddr01);    // Address/data fields
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR02,     clNtsEditAddr02);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR03,     clNtsEditAddr03);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR04,     clNtsEditAddr04);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR05,     clNtsEditAddr05);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR06,     clNtsEditAddr06);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR07,     clNtsEditAddr07);
   DDX_Control(pDX, IDC_NTS_EDIT_ADDR08,     clNtsEditAddr08);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA01,     clNtsEditData01);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA02,     clNtsEditData02);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA03,     clNtsEditData03);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA04,     clNtsEditData04);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA05,     clNtsEditData05);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA06,     clNtsEditData06);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA07,     clNtsEditData07);
   DDX_Control(pDX, IDC_NTS_EDIT_DATA08,     clNtsEditData08);

   //{{AFX_DATA_MAP(CHmxPageNts)
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHmxPageNts, CPropertyPage)
   //{{AFX_MSG_MAP(CHmxPageNts)
   //}}AFX_MSG_MAP
   ON_BN_CLICKED(IDC_NTS_BTN_UP,             OnBnClickedUp)
   ON_BN_CLICKED(IDC_NTS_BTN_DOWN,           OnBnClickedDown)
   ON_BN_CLICKED(IDC_NTS_BTN_AND,            OnBnClickedAnd)
   ON_BN_CLICKED(IDC_NTS_BTN_OR,             OnBnClickedOr)
   ON_BN_CLICKED(IDC_NTS_BTN_AUTO,           OnBnClickedAuto)
   ON_BN_CLICKED(IDC_NTS_BTN_SDTV,           OnBnClickedAuto)
   ON_BN_CLICKED(IDC_NTS_BTN_HDTV,           OnBnClickedAuto)
   //
   ON_MESSAGE(WM_SOLVER_DATA,                OnRcvWindowData)
END_MESSAGE_MAP()




//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::LoadDialog()
{
    //
    // Load the PAGE data from storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::StoreDialog()
{
    //
    // Save the PAGE data to storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::UpdateDialog()
{
   CString  clFile, clStr, clExt;

   if(pstHmxComm && pclNvm)
   {
      if(!pstHmxComm->fFileNts)
      {
         NTS_OpenNtsFile(pstHmxComm, 'w');
         NTS_ParseNtsFile(pstHmxComm);
      }
      // -1- Event name field
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME, &clStr);
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_NTS, &clExt);
      clFile.Format(_T("%s.%s"), clStr.GetBuffer(), clExt.GetBuffer());
      clNtsEditInfile.SetWindowText(clFile.GetBuffer());

      // -2- Catalog Address/Data fields
      UpdateDump();
      //
   }
   UpdateData(FALSE);      // data -> dialog window

}

//
//  Function:   UpdateDump
//  Purpose:    Update the inspect and change fields
//  Parms:      Buffer start
//  Returns:    
//
void CHmxPageNts::UpdateDump(void)
{
   int            iIdx;
   int   iProfile;
   CString        clStr;
   u_int8         pubData[NTS_IDX_SIZE];
   u_int32        ulValue;
   CReadOnlyEdit *pclMask;
   CReadOnlyEdit *pclData;

   //
   // Check SD/HD
   //
   HMX_GetFromStorage(pclNvm, NVM_NTS_AUTOHD, &iProfile);

   //
   if( NTS_ReadNtsRecord(pstHmxComm, pubData, iNtsRecord) )
   {
      if(iProfile == NTS_DEF_HD) clStr.Format(_T("%d"), iNtsRecord/2);
      else                       clStr.Format(_T("%d"), iNtsRecord);
      clNtsEditBase.SetWindowText(clStr);
      //
      for(iIdx=0; iIdx<HMX_NTS_NUM_ADDR; iIdx++)
      {
         pclMask = GetAddrCel(iIdx);
         pclMask->SetFont(pclFontMap);
         pclMask->SetWindowText("");
         //
         ulValue = CVB_ulong_get(&pubData[iIdx*4], 0xffffffffL, 0);
         clStr.Format(_T("%08x"), ulValue);
         pclData = GetDataCel(iIdx);
         pclData->SetFont(pclFontMap);
         pclData->SetWindowText(clStr);
      }
   }
}

//
//  Function:  GetAddrCel
//  Purpose:   Retrieve a Address cell pointer
//
//  Parms:     Index 0..?
//  Returns:   Pointer to field
//
CReadOnlyEdit *CHmxPageNts::GetAddrCel(int iIdx)
{
   const CReadOnlyEdit *pclNtsAddr[HMX_HMT_NUM_ADDR] =
   {
      &clNtsEditAddr01, &clNtsEditAddr02, &clNtsEditAddr03, &clNtsEditAddr04,
      &clNtsEditAddr05, &clNtsEditAddr06, &clNtsEditAddr07, &clNtsEditAddr08
   };
   return((CReadOnlyEdit*)pclNtsAddr[iIdx]);
};

//
//  Function:  GetDataCel
//  Purpose:   Retrieve a Data cell pointer
//
//  Parms:     Index 0..?
//  Returns:   Pointer to field
//
CReadOnlyEdit *CHmxPageNts::GetDataCel(int iIdx)
{
   const CReadOnlyEdit *pclNtsData[HMX_HMT_NUM_ADDR] =
   {
      &clNtsEditData01, &clNtsEditData02, &clNtsEditData03, &clNtsEditData04,
      &clNtsEditData05, &clNtsEditData06, &clNtsEditData07, &clNtsEditData08
   };
   //
   return((CReadOnlyEdit*)pclNtsData[iIdx]);
};

BOOL CHmxPageNts::OnInitDialog()
{
   LOGFONT  stLogFont;

   CPropertyPage::OnInitDialog();

   LoadDialog();
   //
   // Handle fonts
   //
   memset(&stLogFont, 0, sizeof(stLogFont));
   strcpy(stLogFont.lfFaceName, "Arial");
   //
   // Send back the window handle for this dialog, so update messages arrive here
   //
   HWND hThisWnd = this->GetSafeHwnd();
   if(hDialogWnd)
   {
      ::PostMessage(hDialogWnd, WM_SOLVER_HANDLE, (WPARAM) hThisWnd, (LPARAM) HMX_WND_NTS);
   }
   iNtsBtnApply  = 0;
   iNtsBtnAutoHd = 0;
   //
   UpdateData(FALSE);
   return TRUE;  // return TRUE unless you set the focus to a control
                 // EXCEPTION: OCX Property Pages should return FALSE
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::OnOK()
{
   UpdateData(TRUE);

   StoreDialog();
   NTS_CloseNtsFile(pstHmxComm);

   CPropertyPage::OnOK();
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageNts::OnCancel()
{
   NTS_CloseNtsFile(pstHmxComm);
   CPropertyPage::OnCancel();
}

//
//  Function:  OnBnClickedAuto
//  Purpose:   Radio Buttons Auto/SDTV/HDTV
//
//  Parms:     
//  Returns:   
//
void CHmxPageNts::OnBnClickedAuto()
{
   UpdateData(TRUE);      // data <- dialog window
   
   HMX_PutIntoStorage(pclNvm, NVM_NTS_AUTOHD, iNtsBtnAutoHd);
   HMX_UpdateStreamType(pstHmxComm, iNtsBtnAutoHd);
   //pwjh 
   //pwjh if(pstHmxComm)
   //pwjh {
   //pwjh    switch(iNtsBtnAutoHd)
   //pwjh    {
   //pwjh       case NTS_DEF_AUTO:
   //pwjh          switch(pstHmxComm->tContentType)
   //pwjh          {
   //pwjh             default:
   //pwjh                pstHmxComm->tContentType = (NTSDEF)iNtsBtnAutoHd;
   //pwjh                break;
   //pwjh                
   //pwjh             case NTS_DEF_AUTO_SD:
   //pwjh             case NTS_DEF_AUTO_HD:
   //pwjh                // Auto mode has already found a content type: leave as-is
   //pwjh                break;
   //pwjh          }
   //pwjh          break;
   //pwjh       
   //pwjh       default:
   //pwjh       case NTS_DEF_SD:
   //pwjh       case NTS_DEF_HD:
   //pwjh          pstHmxComm->tContentType = (NTSDEF)iNtsBtnAutoHd;
   //pwjh          break;
   //pwjh    }
   //pwjh }
   UpdateDialog();
}

//
//  Function:  OnBnClickedDown
//  Purpose:   Button down 
//
//  Parms:     
//  Returns:   
//
void CHmxPageNts::OnBnClickedDown()
{
   if(iNtsRecord) iNtsRecord--;
   UpdateDialog();
}

//
//  Function:  OnBnClickedUp
//  Purpose:   Button up
//
//  Parms:     
//  Returns:   
//
void CHmxPageNts::OnBnClickedUp()
{
   if(pstHmxComm)
   {
      if(iNtsRecord < pstHmxComm->iNtsNrRecords-1) iNtsRecord++;
      UpdateDialog();
   }
}

//
//  Function:  OnBnClickedAnd
//  Purpose:   Button [And]
//
//  Parms:     
//  Returns:   
//
void CHmxPageNts::OnBnClickedAnd()
{
   int      iRec, iProfile;
   u_int8   pubData[NTS_IDX_SIZE];

   UpdateData(TRUE);      // data <- dialog window
   //
   // Check SD/HD
   //
   HMX_GetFromStorage(pclNvm, NVM_NTS_AUTOHD, &iProfile);
   //
   switch(iNtsBtnApply)
   {
      default:
      case 0:
         // Local update
         if( NTS_ReadNtsRecord(pstHmxComm, pubData, iNtsRecord) )
         {
            UpdateRecord(pstHmxComm, NTS_UPDATE_AND, pubData);
         }
         break;

      case 1:
         // Single record update
         if( NTS_ReadNtsRecord(pstHmxComm, pubData, iNtsRecord) )
         {
            UpdateRecord(pstHmxComm, NTS_UPDATE_AND, pubData);
            NTS_WriteNtsRecord(pstHmxComm, pubData, iNtsRecord);
         }
         break;

      case 2:
         // All records update
         iRec=0;
         while(iRec < pstHmxComm->iNtsNrRecords)
         {
            if( NTS_ReadNtsRecord(pstHmxComm, pubData, iRec) )
            {
               UpdateRecord(pstHmxComm, NTS_UPDATE_AND, pubData);
               NTS_WriteNtsRecord(pstHmxComm, pubData, iRec);
            }
            if(iProfile == NTS_DEF_HD) iRec += 2;
            else                       iRec += 1;
         }
         iNtsRecord  = 0;
         UpdateDump();
         break;
   }
}

//
//  Function:  OnBnClickedOr
//  Purpose:   Button [Or]
//
//  Parms:     
//  Returns:   
//
void CHmxPageNts::OnBnClickedOr()
{
   int      iRec, iProfile;
   u_int8   pubData[NTS_IDX_SIZE];

   UpdateData(TRUE);      // data <- dialog window
   //
   // Check SD/HD
   //
   HMX_GetFromStorage(pclNvm, NVM_NTS_AUTOHD, &iProfile);
   //
   switch(iNtsBtnApply)
   {
      default:
      case 0:
         // Local update
         if( NTS_ReadNtsRecord(pstHmxComm, pubData, iNtsRecord) )
         {
            UpdateRecord(pstHmxComm, NTS_UPDATE_OR, pubData);
         }
         break;

      case 1:
         // Single record update
         if( NTS_ReadNtsRecord(pstHmxComm, pubData, iNtsRecord) )
         {
            UpdateRecord(pstHmxComm, NTS_UPDATE_OR, pubData);
            NTS_WriteNtsRecord(pstHmxComm, pubData, iNtsRecord);
         }
         break;

      case 2:
         // All records update
         iRec=0;
         while(iRec < pstHmxComm->iNtsNrRecords)
         {
            if( NTS_ReadNtsRecord(pstHmxComm, pubData, iRec) )
            {
               UpdateRecord(pstHmxComm, NTS_UPDATE_OR, pubData);
               NTS_WriteNtsRecord(pstHmxComm, pubData, iRec);
            }
            if(iProfile == NTS_DEF_HD) iRec += 2;
            else                       iRec += 1;
         }
         iNtsRecord  = 0;
         UpdateDump();
         break;
   }
}

//
//  Function:  RequestWndHandle
//  Purpose:   Called from the Main dialog window to save its window handle
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageNts::RequestWndHandle(HWND hWnd)
{
   //
   // Get the window handle and send it to the solver thread for communicating
   // Accept dropped files
   //
   hDialogWnd = hWnd;
}

//
//  Function:  OnRcvWindowData
//  Purpose:   Message from the SOLVER Thread: new data is in
//
//  Parms:     WPARAM = 0
//             TPARAM = 0
//  Returns:   0
//
LRESULT CHmxPageNts::OnRcvWindowData(WPARAM tWp, LPARAM tLp)
{
   pstHmxComm = (HMXCOM *) tWp;
   UpdateDialog();
   return(0);
}

//
//  Function:  UpdateRecord
//  Purpose:   Update fields in the record
//
//  Parms:     
//             
//  Returns:   
//
void CHmxPageNts::UpdateRecord(HMXCOM *pstComm, NTSUPD tUpdate, u_int8 *pubData)
{
   int            iIdx;
   CString        clStr;
   u_int32        ulValue, ulMask;
   CReadOnlyEdit *pclMask;
   CReadOnlyEdit *pclData;

   for(iIdx=0; iIdx<HMX_NTS_NUM_ADDR; iIdx++)
   {
      pclMask = GetAddrCel(iIdx);
      pclMask->GetWindowText(clStr);
      if( clStr.GetLength() > 0)
      {
         if ( sscanf(clStr.GetBuffer(), "%x", &ulMask ) == 1 )
         {
            //
            ulValue  = CVB_ulong_get(&pubData[iIdx*4], 0xffffffffL, 0);
            switch(tUpdate)
            {
               case NTS_UPDATE_AND:
                  ulValue &= ulMask;
                  CVB_ulong_put(&pubData[iIdx*4], ulValue);
                  break;

               case NTS_UPDATE_OR:
                  ulValue |= ulMask;
                  CVB_ulong_put(&pubData[iIdx*4], ulValue);
                  break;

               default:
                  break;
            }
            clStr.Format(_T("%08x"), ulValue);
            pclData = GetDataCel(iIdx);
            pclData->SetFont(pclFontMap);
            pclData->SetWindowText(clStr);
            // Refresh mask
            clStr.Format(_T("%08x"), ulMask);
            pclMask->SetFont(pclFontMap);
            pclMask->SetWindowText(clStr);
         }
      }
   }
}



/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/
