/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          ClockApi.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Supply all clock functions
**
**  Entries:            
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef  _CLOCK_API_H__
#define  _CLOCK_API_H__

#define ES_SECONDS_PER_MINUTE    60
#define ES_SECONDS_PER_HOUR      3600
#define ES_SECONDS_PER_DAY       86400

#define ES_MILLIES_PER_SECOND    1000

// number of days between start of MJD and 1970
#define ES_MJD_1970_OFFSET       40587

//  **** completion codes ****
typedef enum _CLOCK_CC
{
   CLOCK_CC_SUCCESS = 0,       // Success
   CLOCK_CC_FAILURE,           // Failure
   CLOCK_CC_ERR_MAX_ALARM,     // Max. number of alarms was reached
   CLOCK_CC_INVALID_PARAMETER, // Invalid parameter
   CLOCK_CC_NULL_POINTER,      // NULL pointer
   CLOCK_CC_ALARM_NOT_FOUND,   // Alarm not found
   CLOCK_CC_TIME_NOT_SET,      // The system clock is not set
   CLOCK_CC_REG_ID_NOT_FOUND,  // Client registration id not found
   CLOCK_CC_ERR_MAX_CLIENT     // Too many clients
} CLOCK_CC;

typedef struct _CLOCK_COUNT_INFO
{
    u_int16 usMilliSeconds;
    u_int32 ulSeconds;
} CLOCK_COUNT_INFO;

typedef struct _CLOCK_DATE_TIME_INFO
{
    u_int8  ubWeekDay;  // value in range [0 .. 6], where 0 == Sunday
    u_int8  ubHour;
    u_int8  ubMin;
    u_int8  ubSec;
    u_int8  ubDay;
    u_int8  ubMonth;
    u_int16 usYear;
} CLOCK_DATE_TIME_INFO;

typedef enum _CLOCK_SOURCE
{
   CLOCK_SOURCE_SYSTEM,
   CLOCK_SOURCE_USER,

   MAX_NUM_CLOCK_SOURCES
} CLOCK_SOURCE;

typedef u_int16 CLOCK_REGISTRATION_ID;

typedef enum _CLOCK_NOTIFY_MASK
{
    CLOCK_NOTIFY_LOCAL_TIME_CHANGED = (1 << 0),
    CLOCK_NOTIFY_INITIAL_TIME_SET   = (1 << 1)
} CLOCK_NOTIFY_MASK;

typedef void (*CLOCK_NOTIFY_CALLBACK)(CLOCK_NOTIFY_MASK, CLOCK_SOURCE, CLOCK_COUNT_INFO *, CLOCK_COUNT_INFO *);
// where pstDeltaCount = New count - old count

typedef u_int16 CLOCK_ALARM_ID;

#define CLOCK_INVALID_ALARM_ID     0xFFFF

// callback function, used for notifications
typedef void (*CLOCK_ALARM_CALLBACK)(CLOCK_ALARM_ID, u_int32);

typedef enum _ALARM_NOTIFY_PHASE
{
   ALARM_NOTIFY_START = 0,
   ALARM_NOTIFY_STOP
} ALARM_NOTIFY_PHASE;

typedef void (*ALARM_NOTIFY_PHASE_CB_FUNC)(ALARM_NOTIFY_PHASE);

// Function prototypes
CLOCK_CC CLOCK_TimeIsSet               (void);
CLOCK_CC CLOCK_TimeSet                 (CLOCK_SOURCE, CLOCK_COUNT_INFO *);
CLOCK_CC CLOCK_TimeGet                 (CLOCK_COUNT_INFO *);
CLOCK_CC CLOCK_TimeGMTAndOffsetGet     (CLOCK_COUNT_INFO *, int32 *);
CLOCK_CC CLOCK_TimeGMTAndOffsetSet     (CLOCK_SOURCE, CLOCK_COUNT_INFO *, int32);
CLOCK_CC CLOCK_Accelerate              (u_int16);

CLOCK_CC CLOCK_RegisterAlarmNfyPhaseCallback (ALARM_NOTIFY_PHASE_CB_FUNC);

CLOCK_CC CLOCK_CallbackRegister        (CLOCK_NOTIFY_MASK, CLOCK_NOTIFY_CALLBACK, CLOCK_REGISTRATION_ID *);
CLOCK_CC CLOCK_CallbackUnregister      (CLOCK_REGISTRATION_ID);

CLOCK_CC CLOCK_AlarmCreate             (CLOCK_ALARM_ID *, CLOCK_COUNT_INFO *, CLOCK_ALARM_CALLBACK, u_int32, bool);
CLOCK_CC CLOCK_AlarmDelete             (CLOCK_ALARM_ID);
CLOCK_CC CLOCK_AlarmEnable             (CLOCK_ALARM_ID);
CLOCK_CC CLOCK_AlarmDisable            (CLOCK_ALARM_ID);
CLOCK_CC CLOCK_AlarmModify             (CLOCK_ALARM_ID, CLOCK_COUNT_INFO *, bool);

CLOCK_CC CLOCK_UtilDaysInMonthGet      (u_int8, u_int16, u_int8 *);
CLOCK_CC CLOCK_UtilWeekdayGet          (u_int8, u_int8, u_int16, u_int8 *);
CLOCK_CC CLOCK_UtilCountToDateTime     (CLOCK_COUNT_INFO *, CLOCK_DATE_TIME_INFO *);
CLOCK_CC CLOCK_UtilDateTimeToCount     (CLOCK_DATE_TIME_INFO *, CLOCK_COUNT_INFO *);

#endif  /* _CLOCK_API_H__ */
