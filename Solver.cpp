/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:  Solver.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Implementation of the Solver class, which handles the actual
**              scanning of the SPTS file
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "Solver.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//
// Module global and static data
//

BEGIN_MESSAGE_MAP(CSolver, CWnd)
   //{{AFX_MSG_MAP(CSolver)
   ON_WM_TIMER()
   //}}AFX_MSG_MAP
   // ===================
   // Private WM Messages
   // ===================
   ON_MESSAGE(WM_SOLVER_CMD,      OnSolverCmd)
   ON_MESSAGE(WM_SOLVER_LOG,      OnSolverLog)
   ON_MESSAGE(WM_SOLVER_EXIT,     OnSolverExit)

END_MESSAGE_MAP()


//
//  Function:  
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CSolver::CSolver()
{
   iSolverTimer  = 0;
   iSession      = 0;
   fDoExit       = FALSE;
}

//
//  Function:  
//  Purpose:   
//
//  Parms:     
//  Returns:
//
CSolver::~CSolver()
{
   //
   //  Get rid of the  NVM Storage Map
   //
   if(iSolverTimer)
   {
       KillTimer(iSolverTimer);
   }
}

//
//  Function:  
//  Purpose:   
//
//  Parms:     
//  Returns:
//
BOOL CSolver::InitSolver()
{
   iSession   = 0;
   iCounter   = 0;
   tCmd       = HMX_CMD_NONE;

   return(TRUE);
}

//
//  Function:  
//  Purpose:   
//
//  Parms:     
//  Returns:
//
void CSolver::OnTimer(UINT nIDEvent)
{
   iCounter++;

   CWnd::OnTimer(nIDEvent);
}

//
//  Function:  OnSolverCmd
//  Purpose:   Send cmd to the solver thread
//
//  Parms:     
//  Returns:
//
LRESULT CSolver::OnSolverCmd(WPARAM tWp, LPARAM tLp)
{
   if(iSolverTimer == 0)
   {
      iSolverTimer = SetTimer(1, 500, NULL);
      VERIFY(iSolverTimer != 0);
   }
   //
   //  Command for the worker thread
   //
   tCmd = (HMXCMD) tLp;
   iSession++;
   return(0);
}

//
//  Function:  OnSolverLog
//  Purpose:   Toggle the worker logging
//
//  Parms:     
//  Returns:
//
LRESULT CSolver::OnSolverLog(WPARAM tWp, LPARAM tLp)
{
   return(0);
}

//
//  Function:  
//  Purpose:   
//
//  Parms:     
//  Returns:
//
LRESULT CSolver::OnSolverExit(WPARAM tWp, LPARAM tLp)
{
   // void AFXAPI AfxEndThread(UINT nExitCode, BOOL bDelete = TRUE);
   // AfxEndThread(0);
   tCmd    = HMX_CMD_EXIT;
   fDoExit = TRUE;
   return(0);
}



