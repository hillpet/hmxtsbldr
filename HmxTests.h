/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxTests.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Tests header file 
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       20Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(_TEST1_H_)
#define _TEST1_H_

typedef struct HMTEST
{
   int   iAddr;                                             // HMT map address
   int   iSize;                                             // HMT field length
   BOOL (*pfHndlr)(HMXCOM *, HMTEST *, u_int8 *, u_int8 *); // f(pstComm, pstCat)

}  HMTEST;
//
// Test prototypes
//
int      HMX_RunTest             (HMXCOM *, int, int);
int      HMX_DoTest1             (HMXCOM *, int);
int      HMX_DoTest2             (HMXCOM *, int);
int      HMX_DoTest3             (HMXCOM *, int);
int      HMX_DoTest4             (HMXCOM *, int);
int      HMX_DoTest5             (HMXCOM *, int);

#endif // !defined(_TEST1_H_)
