/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          TsBldrDlg.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Implementation of the Humax 5050C TS metafile generator dialog application.
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxTsBldr.h"
#include "TsBldrDlg.h"
#include "HmxSheet.h"
#include "HmxTests.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
// Local prototypes
//
static char   *GetStatusMessage        (HMXMSGID);


/* ====== Functions separator ===========================================
void ____About_Classes____(){}
=========================================================================*/

class CAboutDlg : public CDialog
{
public:
   CAboutDlg();

// Dialog Data
   enum { IDD = IDD_ABOUTBOX };

private:
CReadOnlyEdit  clHmxAboutVersion;

protected:
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
   virtual BOOL OnInitDialog();


// Implementation
protected:
   DECLARE_MESSAGE_MAP()
public:
};

//
//  Function:  CAboutDlg
//  Purpose:   Constructor of the About... box
//
//  Parms:     
//  Returns:
//
CAboutDlg::CAboutDlg():CDialog(CAboutDlg::IDD)
{
}

BOOL CAboutDlg::OnInitDialog()
{
   CDialog::OnInitDialog();

   clHmxAboutVersion.SetWindowText(HMX_VERSION);

   return(TRUE);
}

//
//  Function:  DoDataExchange (About box)
//  Purpose:   Update the About.. variables
//
//  Parms:     
//  Returns:
//
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_HMX_ABOUT_VERSION,     clHmxAboutVersion);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


/* ====== Functions separator ===========================================
void ____Dialog_Box____(){}
=========================================================================*/

//
//  Function:  CTsBuilderDlg
//  Purpose:   Constructor of the TS-main Dialog
//
//  Parms:     
//  Returns:
//
CTsBuilderDlg::CTsBuilderDlg(CWnd* pParent):CDialog(CTsBuilderDlg::IDD, pParent)
{
   m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

   pstHmxComm = NULL;
   fDoExit    = FALSE;
   pclNvm     = (CNvmStorage *) new(CNvmStorage);
   VERIFY(pclNvm);

   // Set the fonts
   pclFontSts = (CAutoFont *)  new(CAutoFont)("Tahoma");
   pclFontBtn = (CAutoFont *)  new(CAutoFont)("Tahoma");
   pclFontIdx = (CAutoFont *)  new(CAutoFont)("Courier New");

   pclFontSts->SetHeight(HMX_STS_FONT_SIZE);
   pclFontBtn->SetHeight(HMX_BTN_FONT_SIZE);
   pclFontIdx->SetHeight(HMX_IDX_FONT_SIZE);

}

//
//  Function:  DoDataExchange
//  Purpose:   Data exchange for the main dialog variables
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_HMX_EDIT_INFILE,     clHmxEditInfile);
   DDX_Control(pDX, IDC_HMX_EDIT_STATUS,     clHmxEditStatus);
   DDX_Control(pDX, IDC_HMX_SCANPROGRESS,    clHmxScanProgress);
   DDX_Control(pDX, IDC_HMX_CHECK_LOG,       clHmxCheckLog);
   DDX_Control(pDX, IDC_HMX_CHECK_COMPAT,    clHmxCheckCompat);
   DDX_Control(pDX, IDC_HMX_EDIT_RUNTEST,    clHmxEditRunTest);
   DDX_Control(pDX, IDC_HMX_EDIT_RUNPARM,    clHmxEditRunParm);
   DDX_Control(pDX, IDC_HMX_EDIT_LOGLEVEL,   clHmxEditLogLevel);
   DDX_Control(pDX, IDC_HMX_EDIT_TS,         clHmxBtnTs);
   DDX_Control(pDX, IDC_HMX_EDIT_NTS,        clHmxBtnNts);
   DDX_Control(pDX, IDC_HMX_EDIT_HMT,        clHmxBtnHmt);
}

BEGIN_MESSAGE_MAP(CTsBuilderDlg, CDialog)
   ON_WM_SYSCOMMAND()
   ON_WM_PAINT()
   ON_WM_QUERYDRAGICON()
   ON_WM_DROPFILES()
   ON_BN_CLICKED(IDC_HMX_CHECK_COMPAT,       OnBnClickedCheckCompat)
   ON_BN_CLICKED(IDC_HMX_CHECK_LOG,          OnBnClickedCheckLog)
   ON_BN_CLICKED(IDC_HMX_BTN_PROPERTIES,     OnBnClickedProperties)
   ON_BN_CLICKED(IDC_HMX_BTN_LOADTS,         OnBnClickedBrowseTs)
   ON_BN_CLICKED(IDC_HMX_BTN_SCANTS,         OnBnClickedParseTs)
   ON_BN_CLICKED(IDC_HMX_BTN_RUNTEST,        OnBnClickedRunTest)
   ON_BN_CLICKED(IDC_HMX_BTN_BUILD,          OnBnClickedBuild)
   ON_BN_CLICKED(IDC_HMX_BTN_EXIT,           OnBnClickedMyExit)

   //}}AFX_MSG_MAP
   //
   //  Private WM Messages
   //
   ON_MESSAGE(WM_SOLVER_HANDLE,              OnRcvSolverHandle)
   ON_MESSAGE(WM_SOLVER_UPDATE,              OnRcvSolverUpdate)
   ON_MESSAGE(WM_SOLVER_READY,               OnRcvSolverReady)
   ON_MESSAGE(WM_SOLVER_PROGBAR,             OnRcvSolverProgBar)
   ON_MESSAGE(WM_SOLVER_COMM,                OnRcvSolverComm)
   ON_MESSAGE(WM_SOLVER_DATA,                OnRcvSolverData)
   ON_MESSAGE(WM_SOLVER_EXIT,                OnRcvSolverExited)
   ON_WM_CLOSE()
END_MESSAGE_MAP()

//
//  Function:  OnInitDialog
//  Purpose:   Init main TS dialog
//
//  Parms:     
//  Returns:
//
BOOL CTsBuilderDlg::OnInitDialog()
{
   CString clStr;

   CDialog::OnInitDialog();

   ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
   ASSERT(IDM_ABOUTBOX < 0xF000);

   CMenu* pSysMenu = GetSystemMenu(FALSE);
   if (pSysMenu != NULL)
   {
      CString strAboutMenu;
      strAboutMenu.LoadString(IDS_ABOUTBOX);
      if (!strAboutMenu.IsEmpty())
      {
         pSysMenu->AppendMenu(MF_SEPARATOR);
         pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
      }
   }
   //
   // Get the window handle and send it to the solver thread for communicating
   // Accept dropped files
   //
   HWND hWnd = this->GetSafeHwnd();
   AfxBeginThread(SolverCommThread, hWnd, THREAD_PRIORITY_NORMAL);
   DragAcceptFiles(TRUE);
   //
   // Set the icon for this dialog.  The framework does this automatically
   //  when the application's main window is not a dialog
   //
   SetIcon(m_hIcon, TRUE);             // Set big icon
   SetIcon(m_hIcon, FALSE);            // Set small icon

   clStr.Format(_T("(C)2008-2009 PWJH-www.patrn.nl (%s)" CRLF), HMX_VERSION);
   clHmxEditStatus.SetFont(pclFontSts);
   clHmxEditStatus.SetBackColor(WHITE);
   clHmxEditStatus.SetTextColor(BLUE);
   StatusAddText(clStr.GetBuffer());
   // Disable the SPTS functionality
   GetDlgItem(IDC_HMX_BTN_PROPERTIES)->EnableWindow(FALSE);
   GetDlgItem(IDC_HMX_BTN_SCANTS)->EnableWindow(FALSE);
   GetDlgItem(IDC_HMX_BTN_BUILD)->EnableWindow(FALSE);
   //
   CreateProgressBar();
   //
   // Go ask for the handles of the other dialog windows (Property pages)
   //
   CHmxPageGeneric::RequestWndHandle(hWnd);
   CHmxPageTs::RequestWndHandle(hWnd);
   CHmxPageNts::RequestWndHandle(hWnd);
   CHmxPageHmt::RequestWndHandle(hWnd);
   //
   return TRUE;                        // return TRUE  unless you set the focus to a control
}

//
//  Function:  CreateProgressBar
//  Purpose:   Create the progress bar
//
//  Parms:     void
//  Returns:   void
//
void CTsBuilderDlg::CreateProgressBar()
{
   clHmxScanProgress.SetRange(1, HMX_PROGRESS_RES);
   clHmxScanProgress.SetStep(1);
   clHmxScanProgress.SetPos(0);
}

//
//  Function:  ExitTsBuilder
//  Purpose:   Terminate the TS builder dialog
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::ExitTsBuilder()
{
   if(pstHmxComm)
   {
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_EXIT, (WPARAM) NULL, (LPARAM) NULL);
      }
   }
}

//
//  Function:  SolverCmdCheck
//  Purpose:   Send out CMD to the worker thread to start checking the meta file structure
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::SolverCmdCheck()
{
   if(pstHmxComm)
   {
      pstHmxComm->fDoLog = (clHmxCheckLog.GetCheck() == BST_CHECKED);
      //
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_CHECK);
      }
   }
}

//
//  Function:  SolverCmdBuild
//  Purpose:   Send out CMD to the worker thread to start building the meta file structure
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::SolverCmdBuild()
{
   if(pstHmxComm)
   {
      pstHmxComm->fDoLog = (clHmxCheckLog.GetCheck() == BST_CHECKED);
      //
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_BUILD);
      }
   }
}

//
//  Function:  SolverCmdBuildBatch
//  Purpose:   Send out CMD to the worker thread to start building the meta file structure in batch mode
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::SolverCmdBuildBatch()
{
   if(pstHmxComm)
   {
      pstHmxComm->fDoLog = (clHmxCheckLog.GetCheck() == BST_CHECKED);
      //
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_BUILD_BATCH);
      }
   }
}

//
//  Function:  SolverLog
//  Purpose:   Post message for the LOG handler
//
//  Parms:     
//  Returns:
//  NOTE:      Inactive at the moment
//
void CTsBuilderDlg::SolverLog(char *pcLogName)
{
   if(pstHmxComm)
   {
      pstHmxComm->fDoLog = (clHmxCheckLog.GetCheck() == BST_CHECKED);
      //
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_LOG, (WPARAM) pcLogName, (LPARAM) NULL);
      }
   }
}

//
//  Function:  SolverCmdParse
//  Purpose:   Send out CMD to the worker thread to start parsing the meta file structure
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::SolverCmdParse()
{
   if(pstHmxComm)
   {
      pstHmxComm->fDoLog = (clHmxCheckLog.GetCheck() == BST_CHECKED);
      if(pstHmxComm->hSolverWnd)
      {
         ::PostMessage(pstHmxComm->hSolverWnd, WM_SOLVER_CMD,  (WPARAM) NULL, (LPARAM) HMX_CMD_PARSE);
      }
   }
}

//
//  Function:  StatusAddText
//  Purpose:   Add text to the status window
//
//  Parms:     Text^
//  Returns:   void
//
void CTsBuilderDlg::StatusAddText(char *pcText)
{
   CString  clStr;
   int      iLines;

   clHmxEditStatus.GetWindowText(clStr);
   clStr += pcText;

   iLines = clHmxEditStatus.GetLineCount();

   clHmxEditStatus.SetWindowText(clStr);
   clHmxEditStatus.LineScroll(iLines);
}

//
//  Function:  UpdateButtonStatus
//  Purpose:   Update text and color of the status buttons
//
//  Parms:     BTNSTATUS
//             HMX_COMPAT_TS_EXT        0x1  SPTS has wrong extension
//             HMX_COMPAT_NTS_EXIST     0x2  NTS index   file already exists
//             HMX_COMPAT_HMT_EXIST     0x4  HMT Catalog file already exists
//
//  Returns:   void
//
void CTsBuilderDlg::UpdateButtonStatus(STBTN tButtonStatus)
{
   CString  clStr;
   int      iHmxCompat;

   if(pstHmxComm)
   {
      clHmxEditLogLevel.GetWindowText(clStr);
      sscanf(clStr.GetBuffer(), "%d", &pstHmxComm->iLogPriority);
      //
      switch(tButtonStatus)
      {
         default:
         case BTN_INIT:
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_TS, &clStr);
            clHmxBtnTs.SetWindowText(clStr);
            clHmxBtnTs.SetFont(pclFontBtn);
            clHmxBtnTs.SetBackColor(LTGRAY);
            clHmxBtnTs.SetTextColor(BLACK);
            //
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_NTS, &clStr);
            clHmxBtnNts.SetWindowText(clStr);
            clHmxBtnNts.SetFont(pclFontBtn);
            clHmxBtnNts.SetBackColor(LTGRAY);
            clHmxBtnNts.SetTextColor(BLACK);
            //
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_HMT, &clStr);
            clHmxBtnHmt.SetWindowText(clStr);
            clHmxBtnHmt.SetFont(pclFontBtn);
            clHmxBtnHmt.SetBackColor(LTGRAY);
            clHmxBtnHmt.SetTextColor(BLACK);
            break;

         case BTN_UPDATE:
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_TS, &clStr);
            clHmxBtnTs.SetWindowText(clStr);
            //
            iHmxCompat = HMX_CheckCompatibility(pstHmxComm);
            if(iHmxCompat & HMX_COMPAT_TS_EXIST)
            {
               if(iHmxCompat & HMX_COMPAT_COMPLIANT)  clHmxBtnTs.SetBackColor(GREEN);
               else
               {
                  if(iHmxCompat & HMX_COMPAT_EXT_OK)  clHmxBtnTs.SetBackColor(YELLOW);
                  else                                clHmxBtnTs.SetBackColor(RED);
               }
            }
            else                                      clHmxBtnTs.SetBackColor(LTGRAY);
            //
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_NTS, &clStr);
            clHmxBtnNts.SetWindowText(clStr);
            //
            if(iHmxCompat & HMX_COMPAT_NTS_EXIST)     
            {
               if(iHmxCompat & HMX_COMPAT_NTS_OK)     clHmxBtnNts.SetBackColor(GREEN);
               else                                   clHmxBtnNts.SetBackColor(YELLOW);
            }
            else                                      clHmxBtnNts.SetBackColor(LTGRAY);
            //
            //
            HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_HMT, &clStr);
            clHmxBtnHmt.SetWindowText(clStr);
            //
            if(iHmxCompat & HMX_COMPAT_HMT_EXIST)     
            {
               if(iHmxCompat & HMX_COMPAT_HMT_OK)     clHmxBtnHmt.SetBackColor(GREEN);
               else                                   clHmxBtnHmt.SetBackColor(YELLOW);
            }
            else                                      clHmxBtnHmt.SetBackColor(LTGRAY);
            //
            pstHmxComm->iHmxCompat = iHmxCompat;
            break;
      }
   }
}

/* ====== Functions separator ===========================================
void ____Message_Handlers____(){}
=========================================================================*/

//
//  Function:  OnSysCommand
//  Purpose:   Handle system commands
//
//  Parms:     ID, Any parm
//  Returns:   void
//
void CTsBuilderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
   if ((nID & 0xFFF0) == IDM_ABOUTBOX)
   {
      CAboutDlg dlgAbout;
      dlgAbout.DoModal();
   }
   else
   {
      CDialog::OnSysCommand(nID, lParam);
   }
}

//
//  Function:  OnPaint
//  Purpose:   If you add a minimize button to your dialog, you will need the code below
//             to draw the icon.  For MFC applications using the document/view model,
//             this is automatically done for you by the framework.
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::OnPaint() 
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }
   else
   {
      CDialog::OnPaint();
   }
}

//
//  Function:  OnQueryDragIcon
//  Purpose:   The system calls this function to obtain the cursor to display while the user drags
//             the minimized window.
//
//  Parms:     
//  Returns:
//
HCURSOR CTsBuilderDlg::OnQueryDragIcon()
{
   return static_cast<HCURSOR>(m_hIcon);
}

//
//  Function:  OnDropFiles
//  Purpose:   If you drop a file into the window, this function will handle the drop.
//
//  Parms:     HDROP
//  Returns:   void
//
void CTsBuilderDlg::OnDropFiles(HDROP hDropInfo)
{
   char    *pcPathname=NULL;
   UINT     iCh, iSize, iIdx, iCurSize=0;
   CString  clStr;

   //
   // DragQueryFile(hDrop, iFile, lpscName, iNr)
   //
   // hDrop    [in]  Identifier of the structure that contains the file names of the dropped files.
   // iFile    [in]  Index of the file to query. 
   //                If the value of this parameter is 0xFFFFFFFF, DragQueryFile returns a count of the files dropped. 
   //                If the value of this parameter is between zero and the total number of files dropped, DragQueryFile
   //                copies the file name with the corresponding value to the buffer pointed to by the lpszFile parameter.
   // lpszName [out] The address of a buffer that receives the file name of a dropped file when the function returns. 
   //                This file name is a null-terminated string. If this parameter is NULL, DragQueryFile returns the 
   //                required size, in characters, of this buffer.
   // iCh            The size, in characters, of the lpszFile buffer.
   // Return Value   A non-zero value indicates a successful call.
   //                When the function copies a file name to the buffer, the return value is a count of the characters 
   //                copied, not including the terminating null character.
   //                If the index value is 0xFFFFFFFF, the return value is a count of the dropped files. 
   //                Note that the index variable itself returns unchanged, and therefore remains 0xFFFFFFFF.
   //                If the index value is between zero and the total number of dropped files, and the lpszFile buffer 
   //                address is NULL, the return value is the required size, in characters, of the buffer, not including
   //                the terminating null character.
   //
   
   iCh = DragQueryFile(hDropInfo, 0xffffffff, NULL, 0);
   //
   if(iCh == 1)
   {
      //
      // Single file dropped: get the pathname size.
      //                      signal the background parser to check the file.
      //
      iSize = DragQueryFile(hDropInfo, 0, NULL, 0);

      if(iSize++)   //add \0
      {
         pcPathname = (char *) CVB_SafeMalloc(iSize);
         if(pcPathname)
         {
            DragQueryFile(hDropInfo, 0, pcPathname, iSize);
            //
            // Store the complete pathname into NVM
            //
            HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, pcPathname, iSize);
            //
            // Signal background parser. The NVM storage contains all details
            //                        2
            SolverCmdCheck();

            CVB_SafeFree(pcPathname);
         }
      }
   }
   else
   {
      //
      // Multiple files dropped: retrieve all pathnames
      //
      for(iIdx=0; iIdx<iCh; iIdx++)
      {
         //
         // Get the size of the <iIdx> pathname
         //
         iSize = DragQueryFile(hDropInfo, iIdx, NULL, 0);
         if(iSize++)   //add \0
         {
            if(iSize > iCurSize)
            {
               //
               // Get us a new buffer
               //
               CVB_SafeFree(pcPathname);
               pcPathname = (char *) CVB_SafeMalloc(iSize);
               iCurSize   = iSize;
            }
            if(pcPathname)
            {
               DragQueryFile(hDropInfo, iIdx, pcPathname, iSize);
               //
               // Store the complete pathname into NVM
               //
               if(iIdx) clStr += ",";
               clStr += pcPathname;
            }
         }
      }
      HMX_PutIntoStorage(pclNvm, NVM_HMX_BATCHLIST, &clStr);
      SolverCmdBuildBatch();
      CVB_SafeFree(pcPathname);
   }
}

//
//  Function:  OnBnClickedProperties
//  Purpose:   Handles the Ts-Dialog Properties button
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::OnBnClickedProperties()
{
   CWnd*       pclWnd = CWnd::GetActiveWindow();
   CHmxSheet   clHmxSheet("Humax 5050C-Properties", pclWnd, 0);

   INT_PTR iResult = clHmxSheet.DoModal();
}

//
//  Function:  OnBnClickedBrowseTs
//  Purpose:   Handle the Ts-Dialog Browse-TS button
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::OnBnClickedBrowseTs()
{
   CString     clPathname;
   CString     clFilename;
   CString     clStr;
   CFile       clFile;
   CFileStatus clSts;
   CTime       clTime;

   //
   // Turn OFF HMX Compatibility check switch
   // fDoCompat = (clHmxCheckCompat.GetCheck() == BST_CHECKED);
   //
   clHmxCheckCompat.SetCheck(FALSE);
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_DEFDIR, &clFilename);

   CFileDialog clDlg (TRUE,
                      "ts",
                      clFilename,
                      OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
                      "ts (*.ts)|*.ts|All Files (*.*)|*.*||");

   INT_PTR nResult = clDlg.DoModal ();

   switch(nResult)
   {
      case IDOK:
         clPathname = clDlg.GetPathName();
         
         //
         // Store the complete pathname into NVM
         //
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clPathname);
         //
         clFile.GetStatus(clPathname.GetBuffer(), clSts);
         clTime = clSts.m_ctime;
         pstHmxComm->iTsCreate = (int) clTime.GetTime();
         //
         clStr.Format(_T("%s-Browse TS: Date="), CVB_GetTimeDateStamp() );
         StatusAddText(clStr.GetBuffer());
         HMX_SecsToDateTime(&clStr, (u_int32) pstHmxComm->iTsCreate);
         StatusAddText(clStr.GetBuffer());
         StatusAddText(CRLF);
         //
         // Send file to parser for background parsing. The NVM storage contains all details
         //
         SolverCmdCheck();
         break;

      case IDCANCEL:
         break;
   }
}

//
//  Function:  OnBnClickedParseTs
//  Purpose:   Handle the Ts-Dialog Parse-TS button
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::OnBnClickedParseTs()
{
   SolverCmdParse();
}

//
//  Function:  OnRcvSolverHandle
//  Purpose:   Message from other windows: a window handle is received
//             so we can send messages to it..
//
//  Parms:     WPARAM = Window handle
//             TPARAM = Dialog ENUM
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverHandle(WPARAM tWp, LPARAM tLp)
{
   if(pstHmxComm)
   {
      switch(tLp)
      {
         //
         // Store the window handle in the comm structure
         //
         default:
         case HMX_WND_DLG:
            // Main dialog window handle: this is us !
            break;

         case HMX_WND_SLV:
            // Solver Comm dialog   window handle
            pstHmxComm->hSolverWnd = (HWND) tWp;
            break;

         case HMX_WND_GEN:
            // PropertyPage-Generic window handle
            pstHmxComm->hPageGenWnd = (HWND) tWp;
            SolverMessageToApp(pstHmxComm, WM_SOLVER_DATA, pstHmxComm, 0);
            break;

         case HMX_WND_TS:
            // PropertyPage-TS      window handle
            pstHmxComm->hPageTstWnd = (HWND) tWp;
            SolverMessageToApp(pstHmxComm, WM_SOLVER_DATA, pstHmxComm, 0);
            break;

         case HMX_WND_NTS:
            // PropertyPage-NTS     window handle
            pstHmxComm->hPageNtsWnd = (HWND) tWp;
            SolverMessageToApp(pstHmxComm, WM_SOLVER_DATA, pstHmxComm, 0);
            break;

         case HMX_WND_HMT:
            // PropertyPage-HMT     window handle
            pstHmxComm->hPageHmtWnd = (HWND) tWp;
            SolverMessageToApp(pstHmxComm, WM_SOLVER_DATA, pstHmxComm, 0);
            break;
      }
   }
   return(0);
}

//
//  Function:  OnRcvSolverComm
//  Purpose:   Message from the SOLVER Thread: Return the Comm structure ptr
//
//  Parms:     WPARAM = pstHmxComm
//             TPARAM = 0
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverComm(WPARAM tWp, LPARAM tLp)
{
   //
   // The solver thread is sending us the Comm structure data: 
   // Update the dialog window
   //
   if(pstHmxComm == (HMXCOM *) tWp)
   {
      UpdateButtonStatus(BTN_UPDATE);
   }
   else
   {
      pstHmxComm = (HMXCOM *) tWp;
      //
      UpdateButtonStatus(BTN_INIT);
      UpdateButtonStatus(BTN_UPDATE);
   }
   return(0);
}

//
//  Function:  OnRcvSolverData
//  Purpose:   Message from the SOLVER Thread: new data is in
//
//  Parms:     WPARAM = pstHmxComm
//             TPARAM = 0
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverData(WPARAM tWp, LPARAM tLp)
{
   CString  clStr;

   if(pstHmxComm)
   {
      GetDlgItem(IDC_HMX_BTN_PROPERTIES)->EnableWindow(TRUE);
      GetDlgItem(IDC_HMX_BTN_SCANTS)->EnableWindow(TRUE);
      GetDlgItem(IDC_HMX_BTN_BUILD)->EnableWindow(TRUE);
      //
      // Handle the new data
      //
      UpdateButtonStatus(BTN_UPDATE);
      //
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME, &clStr);
      clHmxEditInfile.SetWindowText(clStr.GetBuffer());
   }
   return(0);
}

//
//  Function:  OnRcvSolverUpdate
//  Purpose:   Message from the SOLVER Thread: Update the HMX dialog status field.
//
//  Parms:     WPARAM = Ptr to MSG Struct
//             LPARAM = Number of additional parms 
//  Returns:   0                       
//
LRESULT CTsBuilderDlg::OnRcvSolverUpdate(WPARAM tWp, LPARAM tLp)
{
   CString     clStr;
   CString     clTime;
   DLGMSG     *pstMsg;
   int         iParms;
   char       *pcText, *pcMsg;

   //
   // tWp:  Message Struct ptr (allocated)
   // tLp:  Number of additional parms
   //
   pstMsg = (DLGMSG *) tWp;
   iParms = (int) tLp;

   pcText = GetStatusMessage(pstMsg->tMsgId);
   pcMsg  = pstMsg->pcMsg;
   clTime = CVB_GetTimeDateStamp();

   switch(iParms)
   {
      case 0:
      default:
         if(pcMsg)   clStr.Format(_T("%s:%s-%s"CRLF), clTime, pcText, pcMsg);
         else        clStr.Format(_T("%s:%s"CRLF), clTime, pcText);
         break;

      case 1:
         if(pcMsg)   clStr.Format(_T("%s:%s-%s(%d)"CRLF), clTime, pcText, pcMsg, pstMsg->iData1);
         else        clStr.Format(_T("%s:%s(%d)"CRLF), clTime, pcText, pstMsg->iData1);
         break;

      case 2:
         if(pcMsg)   clStr.Format(_T("%s:%s-%s(%d, %d)"CRLF), clTime, pcText, pcMsg, pstMsg->iData1, pstMsg->iData2);
         else        clStr.Format(_T("%s:%s(%d, %d)"CRLF), clTime, pcText, pstMsg->iData1, pstMsg->iData2);
         break;

      case 3:
         if(pcMsg)   clStr.Format(_T("%s:%s-%s(%d, %d, %d)"CRLF), clTime, pcText, pcMsg, pstMsg->iData1, pstMsg->iData2, pstMsg->iData3);
         else        clStr.Format(_T("%s:%s(%d, %d, %d)"CRLF), clTime, pcText, pstMsg->iData1, pstMsg->iData2, pstMsg->iData3);
         break;
   }
   // 
   // Add this msg to the status field
   //
   StatusAddText(clStr.GetBuffer());
   //
   // Free the message memory
   //
   if(pstMsg->pcMsg) CVB_SafeFree(pstMsg->pcMsg);
   CVB_SafeFree(pstMsg);
   UpdateButtonStatus(BTN_UPDATE);
   
   return(0);
}

//
//  Function:  OnRcvSolverProgBar
//  Purpose:   Update the progress bar
//
//  Parms:     LPARAM = 0-100%
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverProgBar(WPARAM tWp, LPARAM tLp)
{
   int   iPercentage = (int) tLp;

   if(iPercentage > HMX_PROGRESS_RES) iPercentage = HMX_PROGRESS_RES;
   clHmxScanProgress.SetPos(iPercentage);

   return(0);
}

//
//  Function:  OnRcvSolverReady
//  Purpose:   Message from the SOLVER thread: Solver is ready.
//
//  Parms:     
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverReady(WPARAM tWp, LPARAM tLp)
{
   CString clStr;

   clStr.Format(_T("%s-Parser:Idle"CRLF), CVB_GetTimeDateStamp() );
   StatusAddText(clStr.GetBuffer());
   UpdateButtonStatus(BTN_UPDATE);

   return(0);
}

//
//  Function:  OnRcvSolverExited
//  Purpose:   Message from the SOLVER thread: Solver has exited
//
//  Parms:     
//  Returns:   0
//
LRESULT CTsBuilderDlg::OnRcvSolverExited(WPARAM tWp, LPARAM tLp)
{
   fDoExit = TRUE;
   return(0);
}

//
//  Function:  OnClose
//  Purpose:   Called when closing the dialog window
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnClose()
{
   ExitTsBuilder();

   Sleep(500); // allow threads to exit

   CDialog::OnClose();
}

//
//  Function:  OnBnClickedRunTest
//  Purpose:   Called when clicking the RUN TEST button
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnBnClickedRunTest()
{
   int      iTestNr=0, iTestParm=0;
   CString  clStr;

   UpdateData(TRUE);      // data <- dialog window

   
   clHmxEditRunTest.GetWindowText(clStr);
   if ( sscanf(clStr.GetBuffer(), "%d", &iTestNr ) == 1 )
   {
      clHmxEditRunParm.GetWindowText(clStr);
      sscanf(clStr.GetBuffer(), "%d", &iTestParm);
      //
      HMX_RunTest(pstHmxComm, iTestNr, iTestParm);
   }
}

//
//  Function:  OnBnClickedBuild
//  Purpose:   Handle the Ts-Dialog Build button
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnBnClickedBuild()
{
   int   iCc;
   int   iHmxCompat;
   BOOL  fDoBuild = TRUE;

   if(pstHmxComm)
   {
      iHmxCompat = pstHmxComm->iHmxCompat;
      //
      // Check the presence of the ts, nts and hmt files
      if(!(iHmxCompat & HMX_COMPAT_EXT_OK))
      {
         iCc = AfxMessageBox("SPTS has wrong extension ! Do you want to rename it to <*.ts> ?", MB_YESNOCANCEL|MB_ICONQUESTION);
         switch(iCc)
         {
            case IDCANCEL:
               // Abort here !
               fDoBuild = FALSE;
               break;

            case IDYES:
               iHmxCompat |= HMX_COMPAT_TS_RENAME;
               break;

            default:
            case IDNO:
               iHmxCompat &= ~HMX_COMPAT_TS_RENAME;
               break;
         }
      }
      if(fDoBuild && (iHmxCompat & HMX_COMPAT_NTS_EXIST))
      {
         iCc = AfxMessageBox("*.nts index file exists ! Do you want to overwrite it ?", MB_YESNOCANCEL|MB_ICONQUESTION);
         switch(iCc)
         {
            case IDCANCEL:
               // Abort here !
               fDoBuild = FALSE;
               break;

            case IDYES:
               iHmxCompat |= HMX_COMPAT_NTS_OVERWRITE;
               break;

            default:
            case IDNO:
               iHmxCompat &= ~HMX_COMPAT_NTS_OVERWRITE;
               break;
         }
      }
      if(fDoBuild && (iHmxCompat & HMX_COMPAT_HMT_EXIST))
      {
         iCc = AfxMessageBox("*.hmt catalog file exists ! Do you want to overwrite it ?", MB_YESNOCANCEL|MB_ICONQUESTION);
         switch(iCc)
         {
            case IDCANCEL:
               // Abort here !
               fDoBuild = FALSE;
               break;

            case IDYES:
               iHmxCompat |= HMX_COMPAT_HMT_OVERWRITE;
               break;

            default:
            case IDNO:
               iHmxCompat &= ~HMX_COMPAT_HMT_OVERWRITE;
               break;
         }
      }
      pstHmxComm->iHmxCompat = iHmxCompat;
      //
      // Start the build process
      //
      if(fDoBuild)
      {
         SolverCmdBuild();
      }
   }
}


//
//  Function:  OnBnClickedCheckCompat
//  Purpose:   Handle the Ts-Dialog Compatibility button
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnBnClickedCheckCompat()
{
   //
   // Check:
   // Extension is not *.ts
   // Date format in the filename is not "YYYYMMDD"
   // Time format in the filename is not "HHMM"
   //
   if( (pstHmxComm->fEventOk) && (clHmxCheckCompat.GetCheck() == BST_CHECKED) )
   {
      if( !(pstHmxComm->iHmxCompat & HMX_COMPAT_DATE_OK) )  ForceNameCompatible();
      if( !(pstHmxComm->iHmxCompat & HMX_COMPAT_TIME_OK) )  ForceNameCompatible();
      if( !(pstHmxComm->iHmxCompat & HMX_COMPAT_EXT_OK)  )  ForceExtensionCompatible();
   }
}

//
//  Function:  OnBnClickedCheckLog
//  Purpose:   Handle the Ts-Dialog Log level
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnBnClickedCheckLog()
{
   if (clHmxCheckLog.GetCheck() == BST_CHECKED)
   {
   }
   UpdateButtonStatus(BTN_UPDATE);
}

//
//  Function:  OnBnClickedMyExit
//  Purpose:   Called when closing the dialog window with the <EXIT> button
//
//  Parms:
//  Returns:
//
void CTsBuilderDlg::OnBnClickedMyExit()
{
   int   iTimeout = 1000;

   ExitTsBuilder();
   // wait for the threads to exit
   while(iTimeout)
   {
      if(fDoExit) 
      {
         iTimeout = 0;
      }
      else
      {
         Sleep(10);
         iTimeout--;
      }
   }
   OnClose();
}

//
//  Function:  ForceNameCompatible
//  Purpose:   Force the filename to be HMX compliant
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::ForceNameCompatible(void)
{
   CString  clStrOldName, clStrNewName, clDate, clTime;

   //
   // Date format is not OK: convert the file date&time stamp
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStrOldName);
   if( HMX_BuildTimeDateStamp(&clStrOldName, &clDate, &clTime) )
   {
      // 
      // We have file D&T stamp: build new pathname
      //
      HMX_PutIntoStorage(pclNvm, NVM_HMX_DATE, &clDate);
      pstHmxComm->iHmxCompat |= HMX_COMPAT_DATE_OK;
      //
      HMX_PutIntoStorage(pclNvm, NVM_HMX_TIME, &clTime);
      pstHmxComm->iHmxCompat |= HMX_COMPAT_TIME_OK;
      //
      // Rename the original SPTS file
      //
      HMX_MakePathname(pclNvm, &clStrNewName);
      HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clStrNewName);
      // Rename(old-->new)
      pstHmxComm->pclFileTs->Rename(clStrOldName.GetBuffer(), clStrNewName.GetBuffer());
      UpdateButtonStatus(BTN_UPDATE);
      //
      HMX_MakeFilename(pclNvm, &clStrNewName);
      HMX_PutIntoStorage(pclNvm, NVM_HMX_FILENAME, &clStrNewName);
      clHmxEditInfile.SetWindowText(clStrNewName.GetBuffer());
   }
}

//
//  Function:  ForceExtensionCompatible
//  Purpose:   Force the filename to be HMX compliant
//
//  Parms:     
//  Returns:
//
void CTsBuilderDlg::ForceExtensionCompatible(void)
{
   CString     clStrExt, clStrOldName, clStrNewName;

   //
   //  Extension is not *.ts: Change it into that now
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStrOldName);
   clStrExt.Format(_T("%s"), "ts"); 
   HMX_PutIntoStorage(pclNvm, NVM_HMX_FILEEXT_TS, &clStrExt);
   //
   HMX_MakePathname(pclNvm, &clStrNewName);
   HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clStrNewName);
   // Rename
   pstHmxComm->pclFileTs->Rename( clStrOldName.GetBuffer(), clStrNewName.GetBuffer() );
   UpdateButtonStatus(BTN_UPDATE);
   pstHmxComm->iHmxCompat = 0;
   pstHmxComm->iHmxCompat = HMX_CheckCompatibility(pstHmxComm);
}

/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/
//
//    HMX_MSGID_NONE = 0,             // No messages
//    HMX_MSGID_SCAN_STARTED,         // 
//    HMX_MSGID_SCAN_PACKETS,         // 
//    HMX_MSGID_SCAN_STOPPED,         // 
//    HMX_MSGID_SCAN_EXIT,            // 
//                                    // 
//    NUM_HMX_MSGID                   // Number of messages
//
static const char *pcHmxMessages[] = 
{
   "No message",
   "ERROR",
   "Scan started",
   "Scan packet",
   "Scan stopped",
   "Scan exit"
};

//
//  Function:  GetStatusMessage
//  Purpose:   Translate the MSG index to the text ptr
//
//  Parms:     HMXMSGID Status message
//  Returns:   char ptr
//
static char *GetStatusMessage(HMXMSGID tMsgIdx)
{
   if(tMsgIdx >= NUM_HMX_MSGID)
   {
      tMsgIdx = HMX_MSGID_NONE;
   }
   return((char *) pcHmxMessages[tMsgIdx] );
}




