/*  (c) Copyright:  2007
**
**  $Workfile:          NvmStorage.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Header file for the *.cpp
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 *    12Apr2009: PwjH-Removed BOOL, Added u_int16 and int
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(AFX_NVMSTORAGE_H_)
#define AFX_NVMSTORAGE_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//
// Include the NVM storage members
//
#include "NvmDefs.h"



//
// The Class for handling permanent private storage of application data in the document
//
class CNvmStorage : public CObject
{
public:
             CNvmStorage();
    virtual ~CNvmStorage();

public:
    void    NvmPut      (int, CString *);
    void    NvmPut      (int, u_int8);
    void    NvmPut      (int, u_int16);
    void    NvmPut      (int, int);
    void    NvmPut      (int, char *);
    void    NvmPut      (int, u_int8 *, int);

    BOOL    NvmGet      (int, CString *);
    BOOL    NvmGet      (int, u_int8 *);
    BOOL    NvmGet      (int, u_int16 *);
    BOOL    NvmGet      (int, int *);
    BOOL    NvmGet      (int, char *);
    BOOL    NvmGet      (int, u_int8 *, int);
    int     NvmGetSize  (int);

    void    Serialize   (CArchive &);

DECLARE_SERIAL(CNvmStorage)
};

#endif // !defined(AFX_NVMSTORAGE_H_)
