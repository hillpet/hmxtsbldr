/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageHmt.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for HMT Files
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(_AFX_HMXPAGEHMT_H_)
#define _AFX_HMXPAGEHMT_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AutoFont.h"
#include "ReadOnlyEdit.h"
#include "afxwin.h"
#include "afxcmn.h"


class CHmxPageHmt : public CPropertyPage
{
	DECLARE_DYNCREATE(CHmxPageHmt)

// Construction
public:
	CHmxPageHmt();
	~CHmxPageHmt();

   void           LoadDialog        (void);
   void           StoreDialog       (void);
   void           UpdateDialog      (void);
   void           HandleNonPrintable(const XLAT *, char *, int);
   //
   CReadOnlyEdit *GetAddrCel        (int);
   CReadOnlyEdit *GetDataCel        (int);
   void           UpdateDump        (u_int8 *);
   //
   static void    RequestWndHandle  (HWND);

// Dialog Data
	//{{AFX_DATA(CHmxPageHmt)
	enum { IDD = IDD_PROP_HMX_HMT };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CHmxPageHmt)
public:
	virtual void OnOK();
	virtual void OnCancel();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
   u_int32           ulBaseAddr;                // Dump base address
   u_int32           ulBaseStep;                // Dump base address step up/down
   CNvmStorage      *pclNvm;                    // NVM storage
   HMXCOM           *pstHmxComm;                // Communication structure
   CAutoFont        *pclFontMap;                // Font handler for Maps

public:
   CReadOnlyEdit     clHmtEditInfile;           // Event name
   CReadOnlyEdit     clHmtEditRecDuration;      // Manual override
   //                                           // Catalog:
   CReadOnlyEdit     clHmtEditPathname;         //    Event pathname
   CReadOnlyEdit     clHmtEditEventname;        //    Event   name
   CReadOnlyEdit     clHmtEditServicename;      //    Service name
   CReadOnlyEdit     clHmtEditShortDescr;       //    Short event descriptor
   CReadOnlyEdit     clHmtEditLongDescr;        //    Long  event descriptor
   // Address fields
   CReadOnlyEdit     clHmtEditBase;
   CReadOnlyEdit     clHmtEditAddr01, clHmtEditAddr02, clHmtEditAddr03, clHmtEditAddr04;
   CReadOnlyEdit     clHmtEditAddr05, clHmtEditAddr06, clHmtEditAddr07, clHmtEditAddr08;
   // Data fields
   CReadOnlyEdit     clHmtEditData01, clHmtEditData02, clHmtEditData03, clHmtEditData04;
   CReadOnlyEdit     clHmtEditData05, clHmtEditData06, clHmtEditData07, clHmtEditData08;

protected:
	// Generated message map functions
	//{{AFX_MSG(CHmxPageHmt)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
   afx_msg void      OnBnClickedUp           ();
   afx_msg void      OnBnClickedDown         ();
   afx_msg void      OnBnClickedSave         ();
	DECLARE_MESSAGE_MAP()

public:
   afx_msg LRESULT   OnRcvWindowData         (WPARAM, LPARAM);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AFX_HMXPAGEGENERIC_H_)
