/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:   SolverFiles.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Misc Solver helper functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "Solver.h"


//
// Local prototypes
//
static void          solver_LogName(BOOL);

static BOOL    fLog                = TRUE;
static BOOL    fSolving            = FALSE;

static CFile   clLogFile;

/* ====== Functions separator ===========================================
void ____SOLVER_FUNCTIONS____(){}
=========================================================================*/

//
//  Function:  SOLVER_ResolveInit
//  Purpose:   Init and start the solver
//
//  Parms:     
//  Returns:   Number of solutions
//
int SOLVER_ResolveInit(void)
{

   //SolverMessageToApp(WM_SOLVER_UPDATE, (LPVOID) 0, 0);


   fSolving     = pclSolver->fSolverStart;
   iTotalSolved = 0;

   if (pclSolver->pcLogfile)
   {
      if(clLogFile.Open(pclSolver->pcLogfile, CFile::modeNoTruncate | CFile::modeCreate | CFile::modeWrite, NULL) == 0)
      {
         fLog = FALSE;
      }
      else
      {
         clLogFile.SeekToEnd();
         solver_LogName(TRUE);
      }
   }
   else
   {
      fLog = FALSE;
   }

   //
   // do TS Scan here
   //


   //
   // Scan has finished
   //
   SolverMessageToApp(WM_SOLVER_READY, NULL, iTotalSolved);

   if (fLog)
   {
      clLogFile.Close();
   }
   return(iTotalSolved);
}


/* ====== Functions separator ===========================================
void ____SOLVER_LOCAL_FUNCTIONS____(){}
=========================================================================*/

const char *pcBars = "--------------------------------------------------------------------------------------------------------------------------"CRLF;

//
//  Function:  solver_LogName
//  Purpose:   Write to the log file
//
//  Parms:     fDoHeaderMap
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-Name"
//
//
static void solver_LogName(BOOL fDoHeader)
{
   CString clStr;

   if(fLog && fDoHeader)
   {
      clStr.Format(_T("%s-%s"CRLF), CVB_GetTimeDateStamp(), "Name");

      clLogFile.Write(pcBars, (int)(strlen(pcBars)));
      clLogFile.Write(clStr.GetBuffer(), clStr.GetLength());
      clLogFile.Write(pcBars, (int)(strlen(pcBars)));
   }
}


