/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          TsHandlers.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            TS Startcode handler functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       28Apr2009
**
 *  Revisions:
 *    $Log:   $   pwjh-1: PES: 00.00.01.CC.LL.LL.F1.F2.HL.xx.xx.xx
 *                             if(F1 & 0xc0!=0x80) -> no optional PES header
 *                                                  treat byte as ofsset to next startcode
 *                        Ignore all PES-length. Handle only PES with PUSI set.
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"

//
// Local prototypes
//
static int     ts_ParseSdStartCodes    (HMXCOM *, PIDLST *, TS_MPEG2 *, int);
static int     ts_ParseHdStartCodes    (HMXCOM *, PIDLST *, TS_MPEG2 *, int);

static int     ts_NaluAud              (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluSei              (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluSlices           (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluSps              (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluPps              (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluEndSeq           (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluEndStr           (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluFiller           (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluReserved         (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_NaluIllegal          (HMXCOM *, PIDLST *, u_int8 *, int);

static int     ts_PictureHeader        (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_SdSliceStart         (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_Reserved             (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_SequenceHeader       (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_ExtensionHeader      (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_GopStart             (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_ProgramEnd           (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_SysReserved          (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_PrivateStream1       (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_VideoStream          (HMXCOM *, PIDLST *, u_int8 *, int);
static int     ts_AudioStream          (HMXCOM *, PIDLST *, u_int8 *, int);
//
static PIDLST *ts_AddPid               (HMXCOM *, PIDLST *, u_int16);
static PIDLST *ts_BuildMapTable        (HMXCOM *, PIDLST *, u_int8 *, u_int16);
static void    ts_DumpMapTable         (HMXCOM *, u_int8 *, u_int16);
static int     ts_CheckPesHeader       (HMXCOM *, u_int8 *, int);
static void    ts_CountStartcodes      (HMXCOM *, u_int8, int);
static u_int16 ts_FindSidInMapTable    (HMXCOM *);
static int     ts_FindStartcode        (HMXCOM *, u_int8 *, int);
static PIDLST *ts_FreePidList          (HMXCOM *);
static int     ts_HandleAdaptFldCtl    (HMXCOM *, TS_MPEG2 *, u_int8, BOOL); 
static int     ts_StoreFrame           (HMXCOM *, u_int8 *, int);
static PIDLST *ts_SearchPid            (HMXCOM *, PIDLST *, u_int16);
static u_int32 ts_ExtractEscr          (HMXCOM *, u_int8 *, int);
static u_int32 ts_ExtractDts           (HMXCOM *, u_int8 *, int, int);
static u_int32 ts_ExtractPts           (HMXCOM *, u_int8 *, int, int);
static u_int32 ts_ExtractPcr           (HMXCOM *, u_int8 *);
static void    ts_InitNtsRecord        (HMXCOM *, BOOL);
static NTSDEF  ts_UpdateContentType    (HMXCOM *, NTSDEF);


//
// The HD Startcode handlers
//
const STCODES stHdStartCodes[] = 
{  // Startcode Lo   StartCode Hi   Xlate function 
   {  0x00,          0x00,          ts_NaluIllegal          },       // NALU illegal codes
   {  0x01,          0x05,          ts_NaluSlices           },       // NALU Slice 1..5
   {  0x06,          0x06,          ts_NaluSei              },       // NALU SEI
   {  0x07,          0x07,          ts_NaluSps              },       // NALU SPS
   {  0x08,          0x08,          ts_NaluPps              },       // NALU PPS
   {  0x09,          0x09,          ts_NaluAud              },       // NALU AUD
   {  0x0A,          0x0A,          ts_NaluEndSeq           },       // NALU EOSEQ
   {  0x0B,          0x0B,          ts_NaluEndStr           },       // NALU EOSTR
   {  0x0C,          0x0C,          ts_NaluFiller           },       // NALU Filler
   {  0x0D,          0x1F,          ts_NaluReserved         },       // NALU Reserved
   {  0x20,          0xAF,          ts_NaluIllegal          },       // NALU illegal codes
   {  0xB0,          0xB2,          ts_Reserved             },       // Reserved codes
   {  0xB3,          0xB3,          ts_SequenceHeader       },       // Sequence header start
   {  0xB4,          0xB4,          ts_Reserved             },       // Error sequence
   {  0xB5,          0xB5,          ts_ExtensionHeader      },       // Extension header
   {  0xB6,          0xB6,          ts_Reserved             },       // Reserved
   {  0xB7,          0xB7,          ts_Reserved             },       // Sequence end
   {  0xB8,          0xB8,          ts_GopStart             },       // Group of Pictures 
   //--- System codes -----------------------------------------------//-----------------------
   {  0xB9,          0xB9,          ts_ProgramEnd           },       // Program end
   {  0xBA,          0xBC,          ts_SysReserved,         },       // Misc 
   {  0xBD,          0xBD,          ts_PrivateStream1,      },       // Private Stream type 1
   {  0xBE,          0xBF,          ts_SysReserved,         },       // Misc 
   {  0xC0,          0xDF,          ts_AudioStream          },       // Audio start
   {  0xE0,          0xEF,          ts_VideoStream          },       // Video start
   {  0xF0,          0xFF,          ts_SysReserved          },       // Misc
};
const int iNrOfHdHandlers = sizeof(stHdStartCodes)/sizeof(STCODES);  // Nr of entries

//
// The SD Startcode handlers
//
const STCODES stSdStartCodes[] = 
{  // Startcode Lo   StartCode Hi   Xlate function 
   {  0x00,          0x00,          ts_PictureHeader        },       // Picture header
   {  0x01,          0xAF,          ts_SdSliceStart         },       // SD:Slice
   {  0xB0,          0xB2,          ts_Reserved             },       // Reserved codes
   {  0xB3,          0xB3,          ts_SequenceHeader       },       // Sequence header start
   {  0xB4,          0xB4,          ts_Reserved             },       // Error sequence
   {  0xB5,          0xB5,          ts_ExtensionHeader      },       // Extension header
   {  0xB6,          0xB6,          ts_Reserved             },       // Reserved
   {  0xB7,          0xB7,          ts_Reserved             },       // Sequence end
   {  0xB8,          0xB8,          ts_GopStart             },       // Group of Pictures 
   //--- System codes -----------------------------------------------//-----------------------
   {  0xB9,          0xB9,          ts_ProgramEnd           },       // Program end
   {  0xBA,          0xBC,          ts_SysReserved,         },       // Misc 
   {  0xBD,          0xBD,          ts_PrivateStream1,      },       // Private Stream type 1
   {  0xBE,          0xBF,          ts_SysReserved,         },       // Misc 
   {  0xC0,          0xDF,          ts_AudioStream          },       // Audio start
   {  0xE0,          0xEF,          ts_VideoStream          },       // Video start
   {  0xF0,          0xFF,          ts_SysReserved          },       // Misc
};
const int iNrOfSdHandlers = sizeof(stSdStartCodes)/sizeof(STCODES);  // Nr of entries

//
// Translate HD frames into "I/P/B/D" style frames
//
const FRTYPE tHdFrameTypes[4] = 
{
   FRAME_TYPE_I,                 // I-Frame
   FRAME_TYPE_P,                 // P-Frame
   FRAME_TYPE_B,                 // B-Frame
   FRAME_TYPE_D,                 // D-Frame
};

//
// Translate SD frames into "I/P/B/D" style frames
//
const FRTYPE tSdFrameTypes[5] = 
{
   FRAME_TYPE_NONE,              // None
   FRAME_TYPE_I,                 // I-Frame
   FRAME_TYPE_P,                 // P-Frame
   FRAME_TYPE_B,                 // B-Frame
   FRAME_TYPE_D,                 // D-Frame
};


/* ====== Functions separator ===========================================
void ___api_functions____(){}
=========================================================================*/

//
//  Function:   TS_Cleanup
//  Purpose:    Cleanup TS memory
//  Parms:      pstComm
//
//  Returns:    
//
void TS_Cleanup(HMXCOM *pstComm)
{
   if(pstComm == NULL) return;
   //
   if(pstComm->pstPidList) pstComm->pstPidList = ts_FreePidList(pstComm);
   if(pstComm->pstStCodes) pstComm->pstStCodes = (LOGSTC *) CVB_SafeFree(pstComm->pstStCodes);
   //
   pstComm->tContentType = NTS_DEF_AUTO;
}

//
//  Function:   TS_DumpPidList
//  Purpose:    Write the pid list to the log
//
//  Parms:      pstComm, PID List
//  Returns:
//
void TS_DumpPidList(HMXCOM *pstComm, PIDLST *pstPid)
{
   int   iStart;

   if(pstPid)
   {
      iStart = pstPid->iIndex;
      //
      do
      {
         HMX_LogEventHexBytes(pstComm, 0, "TS: i=", (u_int8 *) &pstPid->iIndex, 1, TRUE);
         HMX_LogEventHexData( pstComm, 0, " pid=",  pstPid->usPid,    FALSE);
         HMX_LogEventHexData( pstComm, 0, " sid=",  pstPid->usSid,    FALSE);
         HMX_LogEventData(    pstComm, 0, " use=",  pstPid->fInUse,   FALSE);
         HMX_LogEventText(    pstComm, 0, " name=", pstPid->pcEsName, FALSE);
         //
         pstPid = pstPid->pstNext;
      }
      while(pstPid->iIndex != iStart);
   }
}

//
//  Function:   TS_DumpStartcodes
//  Purpose:    Dump the startcode usage to the logfile
//  Parms:      pstComm
//
//  Returns:    
//
void TS_DumpStartcodes(HMXCOM *pstComm)
{
   int      i;
   LOGSTC  *pstStc = pstComm->pstStCodes;

   if(pstStc)
   {
      HMX_LogEvent(pstComm, 1, "TS:Startcodes", TRUE);
      for(i=0; i<256; i++)
      {
         if(pstStc->ulCount)
         {
            HMX_LogEventHexBytes(pstComm, 1, " StCde ",  (u_int8 *) &i, 1, TRUE);
            HMX_LogEventData(    pstComm, 1, "= ", pstStc->ulCount, FALSE);
            if(pstStc->fNotSup)
            {
               HMX_LogEvent(pstComm, 1, "  NOT Supported", FALSE);
            }
         }
         pstStc++;
      }
   }
   else
   {
      HMX_LogEvent(pstComm, -1, "TS:No Startcode buffer", TRUE);
   }
}

//
//  Function:  TS_GetServiceId
//  Purpose:   Search the PMT Pid in the map table.
//     Note:   The 5050c is able to extract all necessary PIDs from the Service PMT. 
//             Only problem is to isolate the PMT of the single program from the SPTS.
//  Parms:     pstComm
//
//  Returns:   usPmtPid
//
u_int16 TS_GetServiceId(HMXCOM *pstComm)
{
   u_int16  usSid;

   usSid = ts_FindSidInMapTable(pstComm);
   HMX_LogEventHexData(pstComm, 0, "TS: Service ID=", usSid, TRUE);
   return(usSid);
}

//
//  Function:   TS_Init
//  Purpose:    Init the NTS record structure
//  Parms:      pstComm
//
//  Returns:    
//
void TS_Init(HMXCOM *pstComm)
{
   // Clear those fields that are for the new parsed data
   if(pstComm->pstStCodes)
   {
      memset(pstComm->pstStCodes, 0, 256*sizeof(LOGSTC));
   }
   else pstComm->pstStCodes = (LOGSTC *) CVB_SafeMalloc(256*sizeof(LOGSTC));
   //
   pstComm->pstPidList   = ts_FreePidList(pstComm);
   pstComm->ubPatVersion = 0xff;
   pstComm->ubPmtVersion = 0xff;
   pstComm->ulPesErrors  = 0;
   pstComm->ulPesPackets = 0;
   pstComm->ulPsiPackets = 0;
   pstComm->ulPsiErrors  = 0;
   pstComm->fHaveIframe  = FALSE;
   //
   ts_InitNtsRecord(pstComm, TRUE);
}

//
//  Function:   TS_HandlePacket
//  Purpose:    SPTS State handler PAT
//
//  Parms:      pstComm, Packet^
//  Returns:    Nr of errors
//
void TS_HandlePacket(HMXCOM *pstComm, TS_MPEG2 *pstPacket)
{
   u_int16  usSize, usPid, usSect, usLast, usLen;
   u_int8   ubIdx=0, ubTblId, ubVers, ubPusi, ubStType;
   PIDLST  *pstPid;

   //-------------------------------------------------------------------------------------
   // We are doing not much here until the PAT has been acquired. 
   // From the PAT, the PrNr-PID list will be created, where all PMT PIDS of the services 
   // in this SPTS (and probably some more) should be referenced.
   //
   // At some point we need to check if this PID is both in the Program Map table and used 
   // in the stream. It is necessary to determine at some later stage what the Service ID 
   // of the main video service in this SPTS is. For this purpose, we need to look up the 
   // PID of the current TS packet and mark it as being used in the linked PID list.
   //-------------------------------------------------------------------------------------

   //-------------------------------------------------------------------------------------
   // Use the next part to set a breakpoint on the FP   
   //-------------------------------------------------------------------------------------   
   // if(pstComm->llCurrentFp == 0x1854c0) // Eendje
   if(pstComm->llCurrentFp == 0x3180) // JaneA
   {
      usSize=0;
   }
   //-------------------------------------------------------------------------------------   
   ubPusi = CVB_ubyte_get( pstPacket->ubFlag1Pid,   0x01, 6);
   usPid  = CVB_ushort_get(pstPacket->ubFlag1Pid, 0x1fff, 0);
   //
   pstPid = ts_SearchPid(pstComm, pstComm->pstPidList, usPid);
   //
   if(pstPid == NULL) 
   {
      //
      // Even if we have no PAT acquired yet, we need to keep track of the PIDs that 
      // are available in the stream. If the PAT has been acquired, with the PID list in it
      // this old list can be discarded again.
      //
      pstPid        = ts_AddPid(pstComm, pstComm->pstPidList, usPid);
      pstPid->usSid = 0xffff;
   }
   pstPid->fInUse = TRUE;
   
   #if defined(FEATURE_SKIP_PES_LENGTH)
   //
   // Skip bytes from the PES packets if we have data to ignore
   //
   if(pstPid->usPacketSkip)
   {
      if(pstPid->usPacketSkip > pstComm->iPacketSize-4)
      {
         // We have more that 1 packet to skip
         pstPid->usPacketSkip -= (pstComm->iPacketSize-4);
         return;
      }
      else
      {
         // Packetdata to skip
         ubIdx = (u_int8) pstPid->usPacketSkip;
         pstPid->usPacketSkip = 0;
      }
   }
   #endif // FEATURE_SKIP_PES_LENGTH
   

   if(ubPusi)
   {
      ubIdx = ts_HandleAdaptFldCtl(pstComm, pstPacket, ubIdx, FALSE); 
      //
      if( (usPid == 0x00) || (usPid == 0x01) || (usPid == 0x02) || (pstPid->usSid != 0xffff) )
      {
         //
         // PID = 0x00     PAT
         // PID = 0x01     CAT
         // PID = 0x02     PDIR
         // SID!= 0xFFFF   PID belongs to this service
         //
         HMX_LogEventHexBytes(pstComm, 10, "TS:PSI Packet", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
         pstComm->ulPsiPackets++;
         pstPid->ulPsiPackets++;
         //========================================================================================
         // PSI Packet
         //========================================================================================
         // This packet contains a section start.
         // Add pointerfield
         ubIdx += CVB_ubyte_get(&pstPacket->ubPayload[ubIdx], 0xff, 0) + 1;
         if(usPid == 0)
         {
            // PAT
            //
            // ubPayload[ubIdx] is the 1st byte of the PSI section
            //
            ubTblId = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx],   0xff,   0);
            usSize  = CVB_ushort_get(&pstPacket->ubPayload[ubIdx+1], 0x0fff, 0);
            usSect  = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx+6], 0xff,   0);
            usLast  = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx+7], 0xff,   0);
            //
            switch(ubTblId)
            {
               case 0x00:
                  //----------------------------
                  // Program Association section
                  //----------------------------
                  //          +------ Adaptation Field control  
                  //          |  +--- payload[0]
                  //          |  |             +-- pointer field
                  //          v  v             v
                  // 47.40.00.xx.aa.xx...xx.xx.PF.00.b0.0d.7f.fb.e3.00.00.00.01.eo.42.6e.f1.ef.cd....
                  // |  |        <- Adp.Fld ->    |  |     |     |  |     SID   PID   SID   PID
                  // |  PUSI                      |  |     |     |  Sect/Lst
                  // |                            |  |     TSID  Vers/CurNxt
                  // Sync                         |  SectLen
                  //                              TblId
                  //                              ^ubIdx  
                  //
                  ubVers = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx+5], 0x1f, 1);
                  //
                  // Skip the section header:
                  //    TblID[1], Size[2].TsId[2], Flags[1], Sect[1], Last[1]
                  //
                  ubIdx  += 8; 
                  usSize -= 5;
                  if(pstComm->ubPatVersion != ubVers)
                  {
                     // New Map table: acquire
                     HMX_LogEventData(pstComm, 0, "TS:PAT Size=", usSize, TRUE);
                     HMX_LogEventData(pstComm, 0, " Version=", ubVers, FALSE);
                     //
                     pstComm->ubPatVersion = ubVers;
                     pstComm->pstPidList   = ts_FreePidList(pstComm);
                     pstPid                = ts_AddPid(pstComm, NULL, usPid);
                     pstPid                = ts_BuildMapTable(pstComm, pstPid, &pstPacket->ubPayload[ubIdx], usSize);
                     //
                     //ts_DumpMapTable(pstComm, &pstPacket->ubPayload[ubIdx], usSize);
                  }
                  ubIdx += usSize; 
                  break;

               case 0x01:
                  //----------------------------
                  // CA section : Skip
                  //----------------------------
                  ubIdx += usSize+3; 
                  HMX_LogEvent(pstComm, 0, " PAT: CA section", TRUE);
                  break;

               case 0x02:
                  //----------------------------
                  // Program Map Section: Skip
                  //----------------------------
                  ubIdx += usSize+3; 
                  HMX_LogEvent(pstComm, 0, " PAT: Program Map section", TRUE);
                  break;

               default:
                  // Other
                  ubIdx += usSize+3; 
                  HMX_LogEvent(pstComm, 0, " PAT: Other section", TRUE);
                  break;
            }
         }
         else //PID not 0x00
         {
            ubTblId = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx],   0xff, 0);
            ubVers  = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx+5], 0x1f, 1);
            if(pstComm->ubPmtVersion != ubVers)
            {
               pstComm->ubPmtVersion = ubVers;
               HMX_LogEventData(pstComm, 10, "TS:PMT TblID=", ubTblId, TRUE);
               HMX_LogEventHexBytes(pstComm, 10, " Packet=", (u_int8 *)&pstPacket->ubSync, 32, FALSE);
               if(ubTblId==0x02)
               {
                  //-------------------------------------------------------------------------------------------------
                  // Program Map section
                  //-------------------------------------------------------------------------------------------------
                  //          +------ Adaptation Field control  
                  //          |  +--- payload[0]
                  //          |  |             +-- pointer field          PCR_PID
                  //          v  v             v                          |      
                  // 47.40.00.xx.aa.xx...xx.xx.PF.02.b0.32.00.1f.c7.00.00.e1.2d.f0.06.09.04.06.04.f3.8a.1b.e1.2d.f0.00...
                  // |  |        <- Adp.Fld ->    |  |     |     |  |           |     <---------------> |
                  // |  PUSI                      |  |     |     |  Sect/Lst    pr.info                 Stream
                  // |                            |  |     TSID  Vers/CurNxt    length                  Type
                  // Sync                         |  SectLen
                  //                              TblId
                  //                              ^ubIdx  
                  //
                  pstComm->pstHmtCat->usPcrPid = CVB_ushort_get(&pstPacket->ubPayload[ubIdx+8],  0x1fff, 0);
                  usLen                        = CVB_ushort_get(&pstPacket->ubPayload[ubIdx+10], 0x0fff, 0);
                  ubStType                     = CVB_ubyte_get( &pstPacket->ubPayload[ubIdx+usLen+12], 0xff, 0);
                  switch(ubStType)
                  {
                     case 0x01:     // 11172-2 Video
                     case 0x02:     // 13818-2 Video
                     case 0x03:     // ????
                     case 0x06:     // ????
                        //
                        // SDTV:try to set content type to SD
                        //
                        ts_UpdateContentType(pstComm, NTS_DEF_AUTO_SD);
                        HMX_LogEventData(pstComm, 0, "TS:PrMapSect:SD Video=", ubStType, TRUE);
                        break;
                  
                     case 0x1b:     // 14496-10 AVC-HD
                        //
                        // HDTV:try to set content type to HD
                        //
                        ts_UpdateContentType(pstComm, NTS_DEF_AUTO_HD);
                        HMX_LogEventData(pstComm, 0, "TS:PrMapSect:HD Video=", ubStType, TRUE);
                        break;
                  
                     default:
                        HMX_LogEventData(pstComm, -1, "TS:PrMapSect:Unknown Video=", ubStType, TRUE);
                        break;
                  }
               }
            }
         }
      }
      else // if( (usPid == 0x00) || (usPid == 0x01) || (usPid == 0x02) || (pstPid->usSid != 0xffff) )
      {
         //========================================================================================
         // PUSI and PES Packet
         //========================================================================================
         if(pstComm->ubPatVersion == 0xff)
         {
            //
            // No PAT parsed yet: Skip ALL 
            //
            TS_ParseStartCodes(pstComm, pstPid, pstPacket, (int)ubIdx);
         }
         else if(pstComm->pstHmtCat->usVideoPid == 0x000)
         {
            //
            // PAT has been parsed, but no idea what the video PID will be: Parse ALL PIDs to acquire any Video Headers
            //
            TS_ParseStartCodes(pstComm, pstPid, pstPacket, (int)ubIdx);
         }
         else if(pstComm->pstHmtCat->usVideoPid == usPid)
         {
            //
            // PAT has been parsed, video PID available: Parse ALL PIDs to acquire any Video Headers
            //
            HMX_LogEventHexData(pstComm, 1, "TS:PUSI:PID=", usPid, TRUE);
            HMX_LogEventHexBytes(pstComm, 10, "TS:PES Packet", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
            pstComm->ulPesPackets++;
            pstPid->ulPesPackets++;
            //
            // Check Video PID for PCR_PID
            //
            ts_HandleAdaptFldCtl(pstComm, pstPacket, ubIdx, TRUE); 
            //
            // PAT has been acquired correctly: start parsing the video PES only
            //
            TS_ParseStartCodes(pstComm, pstPid, pstPacket, (int)ubIdx);
         }
         else if(usPid == pstComm->pstHmtCat->usPcrPid)
         {
            //
            // We have a dedicated PCR_PID: acquire the PCR, if any
            //
            ts_HandleAdaptFldCtl(pstComm, pstPacket, ubIdx, TRUE); 
         }
         else
         {
            // HMX_LogEventHexData(pstComm, 1, "TS:PES Packet other PID=", usPid, TRUE);
         }
      }
   }
   else //No PUSI
   {
      if(pstComm->ubPatVersion != 0xff)
      {
         if( (pstComm->pstHmtCat->usVideoPid == 0x000) || (pstComm->pstHmtCat->usVideoPid == usPid) )
         {
            // PAT has been acquired correctly: start parsing the Video PES only
            // HMX_LogEventHexData(pstComm, 1, "TS:no PUSI:PID=", usPid, TRUE);
            TS_ParseStartCodes(pstComm, pstPid, pstPacket, (int)ubIdx);
         }
      }
   }
   pstComm->pstPidList = pstPid;
}

//
//  Function:   TS_ParseStartCodes
//  Purpose:    Parse the current TS package for startcodes
//  Parms:      pstComm, pstPid, pstPacket, Idx
//              ti.me.co.de.47.00.5c.12.xx.xx.xx.xx.xx.00.00.01.AA.BB.....
//                           pstPacket->ubPayload[[Idx]^   
//              (Timecode)  (Packet)   (St.code)(ID-data-->)
//
//  Returns:    Packet index, or -1 if packet skipped
//
int TS_ParseStartCodes(HMXCOM *pstComm, PIDLST *pstPid, TS_MPEG2 *pstPacket, int iIdx)
{
   if( HMX_CheckHdtv(pstComm) )
   {
      iIdx = ts_ParseHdStartCodes(pstComm, pstPid, pstPacket, iIdx);
   }
   else
   {
      iIdx = ts_ParseSdStartCodes(pstComm, pstPid, pstPacket, iIdx);
   }
   return(iIdx);
}


/* ====== Functions separator ===========================================
void ____Handler_functions____(){}
=========================================================================*/

//
//  Function:   ts_ParseHdStartCodes
//  Purpose:    Parse the current TS package for HD startcodes
//  Parms:      pstComm, pstPid, pstPacket, Idx
//              ti.me.co.de.47.00.5c.12.xx.xx.xx.xx.xx.00.00.01.AA.BB.....
//                           pstPacket->ubPayload[[Idx]^   
//              (Timecode)  (Packet)   (St.code)(ID-data-->)
//
//  Returns:    Packet index, or -1 if packet skipped
//
static int ts_ParseHdStartCodes(HMXCOM *pstComm, PIDLST *pstPid, TS_MPEG2 *pstPacket, int iIdx)
{
   BOOL     fParsing = TRUE;
   int      iNr;
   u_int32  ulSubStCode;
   u_int8   ubStCode, ubStuffing;
   u_int8  *pubPacket;
   STCODES *pstCodes;

   pubPacket = pstPacket->ubPayload;
   //
   while(fParsing)
   {
      ubStuffing  = CVB_ubyte_get(&pubPacket[iIdx], 0xff, 0);
      if(ubStuffing == 0xff) return(-1);
      //
      ulSubStCode = CVB_ulong_get(&pubPacket[iIdx], 0x00ffffffL, 8);
      if(ulSubStCode == 0x00000001L)
      {
         ubStCode = CVB_ubyte_get(&pubPacket[iIdx+3], 0xff, 0);
         HMX_LogEventHexBytes(pstComm,  1, "TS:StCde=", &ubStCode, 1, TRUE);
         HMX_LogEventHexData(pstComm,   1, " FP=", (int) pstComm->llCurrentFp, FALSE);
         HMX_LogEventHexBytes(pstComm,  1, " Idx=", (u_int8 *)&iIdx, 1, FALSE);
         HMX_LogEventHexBytes(pstComm,  1, " Data=", &pubPacket[iIdx], 32, FALSE);
         //
         // Strip away the system codes
         //
         if( ubStCode < TS_NALU_SYSTEM_CODES) ubStCode &= TS_NALU_MASK;
         //
         for(iNr=0; iNr<iNrOfHdHandlers; iNr++)
         {
            pstCodes =(STCODES *) &stHdStartCodes[iNr];
            //
            if( (ubStCode >= pstCodes->ubStartCodeLo) && (ubStCode <= pstCodes->ubStartCodeHi) )
            {
               // BP Helper code
               //if(pstComm->llCurrentFp == 0xa9ec0)
               //{
               //   HMX_LogEventHexData(pstComm,  0, "TS:Stopping here... ", (int) pstComm->llCurrentFp, TRUE);
               //}
               iIdx = pstCodes->pfHndlr(pstComm, pstPid, pubPacket, iIdx);
               ts_CountStartcodes(pstComm, ubStCode, iIdx);
               if( (iIdx == -1) || (iIdx >= pstComm->iPacketSize-4) )
               {
                  fParsing = FALSE;
               }
               else
               {
                  //
                  // It is possible that bit/byte allignment adds null bytes until the next start code !
                  //
                  //iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
                  while( (pubPacket[iIdx]==0) && (iIdx < pstComm->iPacketSize-4) )
                  {
                     ulSubStCode = CVB_ulong_get(&pubPacket[iIdx], 0x00ffffffL, 8);
                     if(ulSubStCode == 0x00000001L) break;
                     else iIdx++;
                  }
               }
               break;
            }
         }
      }
      else
      {
         fParsing = FALSE;
      }
   }
   return(iIdx);
}

//
//  Function:   ts_ParseSdStartCodes
//  Purpose:    Parse the current TS package for SD startcodes
//  Parms:      pstComm, pstPid, pstPacket, Idx
//              ti.me.co.de.47.00.5c.12.xx.xx.xx.xx.xx.00.00.01.AA.BB.....
//                           pstPacket->ubPayload[[Idx]^   
//              (Timecode)  (Packet)   (St.code)(ID-data-->)
//
//  Returns:    Packet index, or -1 if packet skipped
//
static int ts_ParseSdStartCodes(HMXCOM *pstComm, PIDLST *pstPid, TS_MPEG2 *pstPacket, int iIdx)
{
   BOOL     fParsing = TRUE;
   int      iNr;
   u_int32  ulSubStCode;
   u_int8   ubStCode, ubStuffing;
   u_int8  *pubPacket;
   STCODES *pstCodes;

   pubPacket = pstPacket->ubPayload;
   //
   while(fParsing)
   {
      ubStuffing  = CVB_ubyte_get(&pubPacket[iIdx], 0xff, 0);
      if(ubStuffing == 0xff) return(-1);
      //
      ulSubStCode = CVB_ulong_get(&pubPacket[iIdx], 0x00ffffffL, 8);
      if(ulSubStCode == 0x00000001L)
      {
         ubStCode = CVB_ubyte_get(&pubPacket[iIdx+3], 0xff, 0);
         HMX_LogEventHexBytes(pstComm,  1, "TS:StCde=", &ubStCode, 1, TRUE);
         HMX_LogEventHexData(pstComm,   1, " FP=", (int) pstComm->llCurrentFp, FALSE);
         HMX_LogEventHexBytes(pstComm,  1, " Idx=", (u_int8 *)&iIdx, 1, FALSE);
         HMX_LogEventHexBytes(pstComm,  1, " Data=", &pubPacket[iIdx], 32, FALSE);
         //
         for(iNr=0; iNr<iNrOfSdHandlers; iNr++)
         {
            pstCodes =(STCODES *) &stSdStartCodes[iNr];
            //
            if( (ubStCode >= pstCodes->ubStartCodeLo) && (ubStCode <= pstCodes->ubStartCodeHi) )
            {
               // BP Helper code
               //if(pstComm->llCurrentFp == 0xa9ec0)
               //{
               //   HMX_LogEventHexData(pstComm,  0, "TS:Stopping here... ", (int) pstComm->llCurrentFp, TRUE);
               //}
               iIdx = pstCodes->pfHndlr(pstComm, pstPid, pubPacket, iIdx);
               ts_CountStartcodes(pstComm, ubStCode, iIdx);
               if( (iIdx == -1) || (iIdx >= pstComm->iPacketSize-4) )
               {
                  fParsing = FALSE;
               }
               else
               {
                  //
                  // It is possible that bit/byte allignment adds null bytes until the next start code !
                  //
                  //iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
                  while( (pubPacket[iIdx]==0) && (iIdx < pstComm->iPacketSize-4) )
                  {
                     ulSubStCode = CVB_ulong_get(&pubPacket[iIdx], 0x00ffffffL, 8);
                     if(ulSubStCode == 0x00000001L) break;
                     else iIdx++;
                  }
               }
               break;
            }
         }
      }
      else
      {
         fParsing = FALSE;
      }
   }
   return(iIdx);
}

//
//  Function:   ts_PictureHeader  [00.00.01.00.xx.xx.xx]
//  Purpose:    Handle the picture header start code. It contains the frame type
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12......00.00.01.00.LL.LL.ff.ff.hl.xx.xx.xx....
//                            pubPacket[iIdx]^                 
//
//  Returns:    iIdx just after this start code
//
static int ts_PictureHeader(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubFrType;
   u_int64  llFp;
   NTSREC  *pstNts = pstComm->pstNts;
   
   //
   // Retrieve the next frame type
   //
   ubFrType        = CVB_ubyte_get(&pubPacket[iIdx+5], 0x0007, 3);
   pstNts->tFrType = (FRTYPE) tSdFrameTypes[ubFrType];
   //
   // Calculate frame size
   //
   llFp             = pstComm->llCurrentFp + 8 + iIdx;
   llFp            -= pstNts->llFp;
   pstNts->ulFrSize = (u_int32)llFp;
   //
   ts_StoreFrame(pstComm, pubPacket, iIdx);
   //
   // Take this point as the start of the next frame
   //
   switch(pstNts->tFrType)
   {
      case FRAME_TYPE_I:
         //
         // Init next GOP
         //
         pstNts->llRefPictStart  = pstComm->llCurrentFp + 8 + iIdx;
         pstNts->llRefPictStartI = pstNts->llRefPictStart;
         pstNts->llRefA          = pstNts->llRefAnew;
         iIdx += (4+4);
         //
         // Take this point as frame-size start reference
         //
         pstNts->llFp         = pstComm->llCurrentFp + iIdx;
         pstNts->ubFrType     = (u_int8) pstNts->tFrType;
         pstNts->ubField2b    = 0;
         pstComm->fHaveIframe = TRUE;
         break;

      default:
      case FRAME_TYPE_D:
         iIdx += (4+4);
         break;

      case FRAME_TYPE_P:
      case FRAME_TYPE_B:
         pstNts->llRefPictStart = pstComm->llCurrentFp + 8 + iIdx;
         iIdx += (4+4);
         //
         // Take this point as frame-size start reference
         //
         pstNts->llFp     = pstComm->llCurrentFp + iIdx;
         pstNts->ubFrType = (u_int8) pstNts->tFrType;
         pstNts->ubField2b++;
         iIdx++;
         break;
   }
   //
   return(iIdx);
}

//
//  Function:   ts_NaluAud
//  Purpose:    Handle the NALU AUD
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.09.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluAud(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8         ubFrType;
   u_int64        llFp;
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=AUD", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   // This PES data contains the frame type: acquire and store for later usage.
   //
   ubFrType        = CVB_ubyte_get(&pubPacket[iIdx+4], 0x7, 5);
   pstNts->tFrType = (FRTYPE) tHdFrameTypes[ubFrType];
   //
   // Calculate frame size
   // Frame size is calculated from this startpoint to the next 0x09 startpoint.
   //
   llFp             = pstComm->llCurrentFp + iIdx + 8;
   llFp            -= pstNts->llFp;
   pstNts->ulFrSize = (u_int32)llFp;
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluSei
//  Purpose:    Handle the NALU SEI
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.06.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluSei(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=SEI", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   ts_StoreFrame(pstComm, pubPacket, iIdx);
   //
   // Take this point as the start of the next frame.
   //
   switch(pstNts->tFrType)
   {
      case FRAME_TYPE_I:
         pstNts->llRefPictStart  = pstComm->llCurrentFp + 8 + iIdx;
         pstNts->llRefPictStartI = pstNts->llRefPictStart;
         pstNts->llRefA          = pstNts->llRefAnew;
         pstNts->llRefB          = pstNts->llRefBnew;
         pstComm->fHaveIframe    = TRUE;
         pstNts->ubField2b       = 0;
         break;

      default:
      case FRAME_TYPE_D:
         break;

      case FRAME_TYPE_P:
      case FRAME_TYPE_B:
         pstNts->llRefPictStart = pstComm->llCurrentFp + 8 + iIdx;
         pstNts->ubField2b++;
         break;
   }
   pstNts->ubFrType = (u_int8) pstNts->tFrType;
   pstNts->llFp     = pstComm->llCurrentFp + 8 + iIdx;
   // stop looking for more 0x06 slices.
   iIdx = pstComm->iPacketSize;
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluSlices
//  Purpose:    Handle the NALU Slices
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.01.AA.BB.....
//                                  pubPacket[iIdx]^    (05)
//
//  Returns:    New index (I hope)
//
static int ts_NaluSlices(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8         ubStCode;
   NTSREC        *pstNts = pstComm->pstNts;

   ubStCode = CVB_ubyte_get(&pubPacket[iIdx+3], TS_NALU_MASK, 0);
   HMX_LogEventHexBytes(pstComm, 5, "TS:HD-NAL=", &ubStCode, 1, TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluSPS
//  Purpose:    Handle the NALU Sequence Parameter Setting
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.07.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluSps(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=SPS", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   pstNts->llRefAnew = pstComm->llCurrentFp + 8 + iIdx;
   //pstNts->llRefA = pstComm->llCurrentFp + 8 + iIdx;
   
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluPps
//  Purpose:    Handle the NALU Picture Parameter Setting
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.08.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluPps(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=PPS", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   pstNts->llRefBnew = pstComm->llCurrentFp + 8 + iIdx;
   //pstNts->llRefB = pstComm->llCurrentFp + 8 + iIdx;
   
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluEndSeq
//  Purpose:    Handle the NALU EOSeq
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.0A.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluEndSeq(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=EOSeq", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluEndStr
//  Purpose:    Handle the NALU End of Stream
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.0B.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluEndStr(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=EOStr", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluFiller
//  Purpose:    Handle the NALU Filler
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.0C.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluFiller(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   NTSREC        *pstNts = pstComm->pstNts;

   HMX_LogEvent(pstComm, 5, "TS:HD-NALU=Filler", TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluIllegal
//  Purpose:    Handle the NALU Illegal codes
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.??.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluIllegal(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubStCode;
   NTSREC        *pstNts = pstComm->pstNts;

   ubStCode = CVB_ubyte_get(&pubPacket[iIdx+3], TS_NALU_MASK, 0);

   HMX_LogEventHexBytes(pstComm, 5, "TS:HD-NALU Illegal=", &ubStCode, 1, TRUE);
   HMX_LogEventHexBytes(pstComm, -1, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, -1, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_NaluReserved
//  Purpose:    Handle the NALU reserved
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.??.AA.BB.....
//                                  pubPacket[iIdx]^
//
//  Returns:    New index (I hope)
//
static int ts_NaluReserved(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubStCode;
   NTSREC  *pstNts = pstComm->pstNts;

   ubStCode = CVB_ubyte_get(&pubPacket[iIdx+3], TS_NALU_MASK, 0);

   HMX_LogEventHexBytes(pstComm, 5, "TS:HD-NALU Reserved=", &ubStCode, 1, TRUE);
   HMX_LogEventHexBytes(pstComm, 5, " FrType=", (u_int8 *)&pstNts->tFrType, 1, FALSE);
   HMX_LogEventHexBytes(pstComm, 5, " Packet=", &pubPacket[iIdx], 16, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_SdSliceStart
//  Purpose:    Handle the SD slice start codes
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx......00.00.01.01.AA.BB.....
//                                  pubPacket[iIdx]^       (af)
//
//
//  Returns:    New index (I hope)
//
static int ts_SdSliceStart(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{

   HMX_LogEventHexBytes(pstComm, 5, "TS:SD-Slice", &pubPacket[iIdx], 16, TRUE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_Reserved
//  Purpose:    Handle a couple of reserved codes
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.00.5c.12.00.00.01.b0.AA.BB.....
//                            pubPacket[iIdx]^  (b1)               
//
//  Returns:    -1 to abort the parsing here for now
//
static int ts_Reserved(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   //
   // Update the index to the next startcode depending the optional PES header
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_SequenceHeader [00.00.01.b3.xx.xx.xx.xx]
//  Purpose:    Handle the sequence header
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.00.5c.12.00.00.01.b3.AA.BB.....
//                       pubPacket[iIdx]^
//
//  Returns:    iIdx just after this start code
//
static int ts_SequenceHeader(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubFlags;
   NTSREC  *pstNts = pstComm->pstNts;

   // -----4-----|-----4-----|-----4-----|-----------8-----------|
   // ti.me.co.de.47.00.5c.12.00.00.01.b3.XX.XX.XX.XX.XX.XX.XX.XX.YY.YY.YY.YY.YY......
   //                        iIdx                               ^
   //                                   xxxxxxbb where bb==01|10 -> 64 bytes table follows
   //
   // Store this StCode as reference for the index file.
   //
   pstNts->llRefAnew = pstComm->llCurrentFp + 8 + iIdx;

   ubFlags = CVB_ubyte_get(&pubPacket[iIdx+11], 0x03, 0);
   if( (ubFlags==1) || (ubFlags==2) ) 
   {
      HMX_LogEventHexBytes(pstComm, 2, "TS:Seq Hdr ", &pubPacket[iIdx], 16, TRUE);
      iIdx += (4+8+64);
   }
   else
   {
      iIdx += (4+8);
   }
   return(iIdx);
}

//
//  Function:   ts_ExtensionHeader 0xB5
//  Purpose:    Handle the extension opcode
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx.xx........00.00.01.b5.AA.BB.....
//                                       pubPacket[iIdx]^
//
//  Returns:    iIdx just after this start code
//
static int ts_ExtensionHeader(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubFlag;

   //---------------------------------------------------------------------------------
   // B5: 00.00.01.b5.AB.CD.EF.GH.IJ.KL....
   //                 A = 0001 -> Fixed size    = 6
   //                     0010 -> Variable size = 5 (AB & 0x01 = 0)
   //                                           = 8 (AB & 0x01 = 1)
   //                     1000 -> Variable size = 5 (IJ & 0x40 = 0)
   //                                           = 7 (IJ & 0x40 = 1)
   //---------------------------------------------------------------------------------
   HMX_LogEventHexBytes(pstComm, 2, "TS:Ext Hdr ", &pubPacket[iIdx], 16, TRUE);
   ubFlag = CVB_ubyte_get(&pubPacket[iIdx+4], 0x0f, 4);
   //
   switch(ubFlag)
   {
      case 0x01:
         iIdx += (4+6);
         break;

      case 0x02:
         ubFlag = CVB_ubyte_get(&pubPacket[iIdx+4], 0x01, 0);
         if(ubFlag) iIdx += (4+8);
         else       iIdx += (4+5);
         break;

      case 0x08:
         ubFlag = CVB_ubyte_get(&pubPacket[iIdx+8], 0x01, 6);
         if(ubFlag) iIdx += (4+7);
         else       iIdx += (4+5);
         break;

      default:
         HMX_LogEventHexBytes(pstComm, -1, "TS:Unsupported Ext Hdr ", &pubPacket[iIdx], 16, TRUE);
         iIdx = -1;
         break;
   }
   return(iIdx);
}

//
//  Function:   ts_GopStart
//  Purpose:    Handle the 
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx.......00.00.01.b8.AA.BB.CC.DD....
//                                   pubPacket[iIdx]^
//
//  Returns:    iIdx just after this start code
//  NOTE:       The GOP carries the HRS:MIN:SECS info for this GOP
//
static int ts_GopStart(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
#if defined(FEATURE_DURATION_FROM_GOP)
   u_int16  usTime1, usTime2;
   int      iTime[3];

   usTime1 = CVB_ushort_get(&pubPacket[iIdx+4], 0xffff, 0);
   usTime2 = CVB_ushort_get(&pubPacket[iIdx+6], 0xffff, 0);

   iTime[0] = (int)(( usTime1>>10) & 0x1f);
   iTime[1] = (int)(( usTime1>>4 ) & 0x3f);
   iTime[2] = (int)(((usTime1&7) << 3) | ((usTime2>>13)&7));

   if(pstComm->iDurationStart)
   {
      pstComm->iDurationEnd = (iTime[0]*3600)+(iTime[1]*60)+iTime[2];
      HMX_LogEventData(pstComm, 5, "TS:GOP Current time = ", pstComm->iDurationEnd, TRUE);
   }
   else
   {
      pstComm->iDurationStart = (iTime[0]*3600)+(iTime[1]*60)+iTime[2];
      HMX_LogEventData(pstComm, 5, "TS:GOP Start time = ", pstComm->iDurationStart, TRUE);
   }
#endif   //defined(FEATURE_DURATION_FROM_GOP)

   iIdx += 8;

   return(iIdx);
}

//
//  Function:   ts_ProgramEnd
//  Purpose:    Handle the 
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx.xx.......00.00.01.b9.ss.ss.AA.BB.LL....
//                                      pubPacket[iIdx]^
//
//  Returns:    iIdx just after this start code
//
static int ts_ProgramEnd(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int32 ulPts, ulDts;

   pstComm->pstNts->ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 1);
   pstComm->pstNts->ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 1);
   //
   ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 90/2);
   ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 90/2);
   //
   ts_ExtractEscr(pstComm, pubPacket, iIdx);
   //
   // Sample initial trick mode PTS
   //
   if(pstComm->pstNts->ulTrickPtsStart == 0) pstComm->pstNts->ulTrickPtsStart = ulPts;
   //
   pstPid->usPacketSkip = CVB_ushort_get(&pubPacket[iIdx+4], 0xffff, 0);
   HMX_LogEventData(pstComm, 12, "TS: Program end (0xB9). Skip=", pstPid->usPacketSkip, TRUE);
   HMX_LogEventHexData(pstComm, 12, " Pts=", ulPts, FALSE);
   HMX_LogEventHexData(pstComm, 12, " Dts=", ulDts, FALSE);
   //
   //
   // Update the index to the next startcode depending the optional PES header
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_SysReserved
//  Purpose:    Handle the system reserved codes
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx.xx.......00.00.01.ba.ss.ss.AA.BB.??....
//                                      pubPacket[iIdx]^       (b?) 
//
//  Returns:    Skip packet size
//
static int ts_SysReserved(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int32 ulPts, ulDts;

   pstComm->pstNts->ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 1);
   pstComm->pstNts->ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 1);
   //
   ts_ExtractEscr(pstComm, pubPacket, iIdx);
   //
   ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 90/2);
   ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 90/2);
   //
   // Sample initial trick mode PTS
   //
   if(pstComm->pstNts->ulTrickPtsStart == 0) pstComm->pstNts->ulTrickPtsStart = ulPts;
   //
   pstPid->usPacketSkip = CVB_ushort_get(&pubPacket[iIdx+4], 0xffff, 0);
   HMX_LogEventData(pstComm, 12, "TS:Unsupported. Skip=", pstPid->usPacketSkip, TRUE);
   HMX_LogEventHexData(pstComm, 12, " Pts=", ulPts, FALSE);
   HMX_LogEventHexData(pstComm, 12, " Dts=", ulDts, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_PrivateStream1
//  Purpose:    Handle the private stream ID 0xBD
//  Parms:      pstComm, pubPacket, Index into packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.40.5c.12.xx.xx.xx.......00.00.01.bd.ss.ss.xx.yy.LL.....
//                                      pubPacket[iIdx]^
//
//  Returns:    iIdx just after this start code
//
static int ts_PrivateStream1(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int32 ulPts, ulDts;

   pstComm->pstNts->ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 1);
   pstComm->pstNts->ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 1);
   //
   ts_ExtractEscr(pstComm, pubPacket, iIdx);
   //
   ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 90/2);
   ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 90/2);
   //
   // Sample initial trick mode PTS
   //
   if(pstComm->pstNts->ulTrickPtsStart == 0) pstComm->pstNts->ulTrickPtsStart = ulPts;
   //
   pstPid->usPacketSkip = CVB_ushort_get(&pubPacket[iIdx+4], 0xffff, 0);
   // HMX_LogEvent(pstComm, -1, "TS:Unsupported Private Stream (0xBD)", TRUE);
   HMX_LogEventData(pstComm, 12, "TS:Private Stream1 (0xBD). Skip=", pstPid->usPacketSkip, TRUE);
   HMX_LogEventHexData(pstComm, 12, " Pts=", ulPts, FALSE);
   HMX_LogEventHexData(pstComm, 12, " Dts=", ulDts, FALSE);
   //
   // Update the index to the next startcode depending the optional PES header
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_VideoStream
//  Purpose:    Handle the Video Stream Header (0xE0..0xEF)
//  Parms:      pstComm, pubPacket, Index into packet
//              ti.me.co.de.47.40.5c.12.xx.xx.xx............00.00.01.e0.00.00.ff.ff.LL..
//                                           pubPacket[iIdx]^       (ef)
//              (Timecode)  (Packet)                       (St.code)(ID-data-->)
//
//  Returns:    iIdx just after this start code
//
static int ts_VideoStream(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubSize;

#if defined(FEATURE_DURATION_FROM_PTS)
   u_int32 ulPts, ulDts;

   // ti.me.co.de.47.xx.yy.zz.00.00.01.e0.00.00.ff.ff.LL.xx.xx.xx
   //                xx.yy = PID      (ef)
   // SS.aa.bb......zz SS=size header
   //
   pstComm->pstNts->ulPrevVideoPts = pstComm->pstNts->ulThisVideoPts;
   pstComm->pstNts->ulThisVideoPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 1);

   // HMX_LogEventHexData(pstComm, 1, "TS:Frame=", pstComm->pstNts->ubFrType, TRUE);
   // HMX_LogEventHexData(pstComm, 1, " Video stream Pts=", pstComm->pstNts->ulThisVideoPts, FALSE);
   // HMX_LogEventHexData(pstComm, 1, " DeltaPts=", pstComm->pstNts->ulThisVideoPts-pstComm->pstNts->ulPrevVideoPts, FALSE);
   //
   pstComm->pstNts->ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 1);
   pstComm->pstNts->ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 1);
   //
   ts_ExtractEscr(pstComm, pubPacket, iIdx);
   //
   ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 90/2);
   ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 90/2);
   //
   // Sample initial trick mode PTS
   //
   if(pstComm->pstNts->ulTrickPtsStart == 0) pstComm->pstNts->ulTrickPtsStart = ulPts;
   pstComm->pstNts->ulTrickPts = ulPts;
   HMX_LogEventHexData(pstComm, 2, "TS:Video stream Pts=", ulPts, TRUE);
   HMX_LogEventHexData(pstComm, 2, " Dts=", ulDts, FALSE);
   HMX_LogEventHexData(pstComm, 2, " Delta=", ulPts-ulDts, FALSE);

   if(pstComm->iDurationStart)
   {
      pstComm->iDurationEnd = (int) ulPts/1000;
      HMX_LogEventData(pstComm, 10, "TS:Video:Current time=", pstComm->iDurationEnd, TRUE);
   }
   else
   {
      pstComm->iDurationStart = (int) ulPts/1000;
      HMX_LogEventData(pstComm, 10, "TS:Video:Start time=", pstComm->iDurationStart, TRUE);
   }
#endif   //defined(FEATURE_DURATION_FROM_PTS)
   //
   HMX_LogEvent(pstComm, 10, "TS:Video: Plug in PID.", TRUE);
   HMX_LogEventHexBytes(pstComm, 10, "TS:Video ", &pubPacket[iIdx], 16, TRUE);
   pstComm->pstHmtCat->usVideoPid = pstPid->usPid;
   //pwjh pstComm->pstHmtCat->usPcrPid   = CVB_ushort_get(&pubPacket[5],      0x1fff, 0);
   ubSize                         = CVB_ubyte_get(&pubPacket[iIdx+8],  0xff,   0);
   strcpy(pstPid->pcEsName, "Video");
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

//
//  Function:   ts_AudioStream
//  Purpose:    Handle the Audio Stream Header (0xC0..0xDF)
//  Parms:      pstComm, pubPacket, Index into packet
//              ti.me.co.de.47.40.5c.12.xx.xx.xx......00.00.01.c0.00.00.ff.ff.LL.xx.xx.xx.
//                                     pubPacket[iIdx]^       (df)
//              (Timecode)  (Packet)                 (St.code)(ID-data-->)
//
//  Returns:    iIdx just after this start code
//
static int ts_AudioStream(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubSize;
   u_int32 ulPts, ulDts;

   // ti.me.co.de.47.xx.yy.zz.00.00.01.c0.00.00.ff.ff.LL.xx.xx.xx
   //                xx.yy = PID      (df)
   // SS.aa.bb......zz SS=size header
   //
   ulPts = ts_ExtractPts(pstComm, pubPacket, iIdx, 90/2);
   ulDts = ts_ExtractDts(pstComm, pubPacket, iIdx, 90/2);
   //
   ts_ExtractEscr(pstComm, pubPacket, iIdx);
   //
   HMX_LogEventHexData(pstComm, 2, "TS:Audio stream Pts=", ulPts, TRUE);
   HMX_LogEventHexData(pstComm, 2, " Dts=", ulDts, FALSE);
   //
   HMX_LogEvent(pstComm, 2, "TS:Audio: Plug in PID.", TRUE);
   HMX_LogEventHexBytes(pstComm, 2, "TS:Audio ", &pubPacket[iIdx], 16, TRUE);
   pstComm->pstHmtCat->usAudioPid = CVB_ushort_get(&pubPacket[5],      0x1fff, 0);
   ubSize                         = CVB_ubyte_get(&pubPacket[iIdx+8],  0xff,   0);
   strcpy(pstPid->pcEsName, "Audio");
   //
   // iIdx = ts_CheckPesHeader(pstComm, pubPacket, iIdx);
   iIdx = ts_FindStartcode(pstComm, pubPacket, iIdx);
   return(iIdx);
}

/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/

//
//  Function:   ts_AddPid
//  Purpose:    Add the PID to the linked list.
//
//  Parms:      Comm, PIDlist, PID
//  Returns:    PIDlist of this PID
//
static PIDLST *ts_AddPid(HMXCOM *pstComm, PIDLST *pstPid, u_int16 usPid)
{
   BOOL     fFound;
   int      iStart;
   static   int iMaxIndex=1;
   PIDLST  *pstNew;

   if(pstPid == NULL)
   {
      pstPid = (PIDLST *) CVB_SafeMalloc(sizeof(PIDLST));
      iMaxIndex       = 1;
      pstPid->iIndex  = iMaxIndex;
      pstPid->usPid   = usPid;
      pstPid->pstNext = pstPid;
      // HMX_LogEventData(pstComm, 0, "TS:Add PID:", usPid, TRUE);
   }
   else
   {
      fFound = FALSE;
      iStart = pstPid->iIndex;
      //
      while(!fFound)
      {
         if(pstPid->usPid == usPid)
         {
            // Found
            fFound = TRUE;
         }
         else
         {
            if(pstPid->pstNext->iIndex == iStart)
            {
               // List exhausted: add this PID
               pstNew = (PIDLST *) CVB_SafeMalloc(sizeof(PIDLST));
               pstNew->iIndex  = ++iMaxIndex;
               pstNew->usPid   = usPid;
               pstNew->pstNext = pstPid->pstNext;
               pstPid->pstNext = pstNew;
               pstPid          = pstNew;
               fFound = TRUE;
            }
            else
            {
               pstPid = pstPid->pstNext;
            }
         }
      }
   }
   switch(usPid)
   {
      case 0:
         strcpy(pstPid->pcEsName, "PAT");
         break;
         
      case 1:
         strcpy(pstPid->pcEsName, "CAT");
         break;
         
      case 2:
         strcpy(pstPid->pcEsName, "TSDT");
         break;
         
      default:
         strcpy(pstPid->pcEsName, "---");
         break;
   }
   return(pstPid);
}
//
//  Function:   ts_BuildMapTable
//  Purpose:    Build the PMT with PIDs and SIDs entries, so we are able to find the
//              Service ID (Program Nr) for the catalog.
//
//  Parms:      pstComm, PAT Data, len
//  Returns:    
//
static PIDLST *ts_BuildMapTable(HMXCOM *pstComm, PIDLST *pstPid, u_int8 *pubData, u_int16 usLen)
{
   u_int16  usSid, usPid;

   while(usLen > 0)
   {
      usSid = CVB_ushort_get(pubData,   0xffff, 0);
      usPid = CVB_ushort_get(pubData+2, 0x1fff, 0);
      // HMX_LogEventData(pstComm, 5, "   PrNr:", usSid, TRUE);
      // HMX_LogEventData(pstComm, 5, " Pid:",    usPid, FALSE);
      //
      pstPid        = ts_AddPid(pstComm, pstPid, usPid);
      pstPid->usSid = usSid;
      //
      pubData += 4;
      usLen   -= 4;
   }
   return(pstPid);
}

//
//  Function:   ts_DumpMapTable
//  Purpose:    Write the map table to the log
//
//  Parms:      pstComm, Data, len
//  Returns:
//
static void ts_DumpMapTable(HMXCOM *pstComm, u_int8 *pubData, u_int16 usLen)
{
   u_int16  usPrNr, usPid;


   HMX_LogEvent(pstComm, 5, "TS:Map Table section:", TRUE);
   //
   while(usLen > 0)
   {
      usPrNr = CVB_ushort_get(pubData,   0xffff, 0);
      usPid  = CVB_ushort_get(pubData+2, 0x1fff, 0);
      HMX_LogEventData(pstComm, 5, "   PrNr:", usPrNr, TRUE);
      HMX_LogEventData(pstComm, 5, " Pid:",    usPid, FALSE);
      pubData += 4;
      usLen   -= 4;
   }
}

//
//  Function:   ts_CheckPesHeader
//  Purpose:    Check if this payload PES has an optional PES header. If not, use
//              the PesHdr length as "pointer_field"
//  Parms:      pstComm, payload^, idx
//
//  Returns:    New iIdx
//
static int ts_CheckPesHeader(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubPesHdrFlag, ubPesHdrLen;

   ubPesHdrFlag = CVB_ubyte_get(&pubPacket[iIdx+6], 0xff, 0);
   if( (ubPesHdrFlag & 0xc0) == 0x80)
   {
      // we have the optional PES header: skip all PES header bytes
      ubPesHdrLen = CVB_ubyte_get(&pubPacket[iIdx+8], 0xff, 0);
      iIdx += ubPesHdrLen+9;
   }
   else
   {
      // this PES does not have an optional PES header. Assume this byte as "pointer_field" to
      // the next startcode
      iIdx += ubPesHdrFlag+6;
   }
   return(iIdx);
}

//
//  Function:   ts_CountStartcodes
//  Purpose:    Count the startcode usage
//  Parms:      pstComm, startcode 0..255, index after parse
//
//  Returns:    
//
static void ts_CountStartcodes(HMXCOM *pstComm, u_int8 ubStCode, int iIdx)
{
   LOGSTC  *pstStc = pstComm->pstStCodes;

   if(pstStc)
   {
      pstStc += ubStCode;
      pstStc->ulCount++;
   }
}

//
//  Function:   ts_ExtractEscr
//  Purpose:    Extract the ES Clock Ref from a system packet
//  Parms:      pstComm, pubPacket, Index into packet
//              ti.me.co.de.47.00.5c.12.00.00.01.b9.00.00.ff.Xf.LL.xx.xx.xx.
//     pubPacket^                      iIdx     (ff)         
//              (Timecode)  (Packet)    (St.code)(ID-data-->)
//
//  Returns:    ESCR in mSecs
//  Note:       
//
static u_int32 ts_ExtractEscr(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx)
{
   u_int8   ubEscr; 
   u_int32  ulEscr=0;

   ubEscr = CVB_ubyte_get(&pubPacket[iIdx+7], 0x01, 5);
   if(ubEscr)
   {
      //       byte-14    byte-15   byte-16   byte-17   byte-18    Shift Mask        
      //       xxxx.aaa1.bbbb.bbbb.bbbb.bbb1.cccc.cccc.cccc.ccc1 
      //  Dts1 0000.1110.0000.0000.0000.0000.0000.0000            << 4  0x0e00.0000 
      //  Dts2           1111.1111.1111.1110.0000.0000.0000.0000  >> 3  0x1fff.c000 
      //  Dts3           0000.0000.0000.0000.1111.1111.1111.1111  >> 2  0x0000.3fff 
      //  -->
      //  Dts1 1110.0000.0000.0000.0000.0000.0000.0000
      //  Dts2 0001.1111.1111.1111.1100.0000.0000.0000
      //  Dts3 0000.0000.0000.0000.0011.1111.1111.1111
      //
      //
      HMX_LogEventData(pstComm, 5, "TS:Escr found", ubEscr, TRUE);
   }
   return(ulEscr);
}

//
//  Function:   ts_ExtractDts
//  Purpose:    Extract the Decode TS from a system packet
//  Parms:      pstComm, pubPacket, Index into packet, user divide
//              ti.me.co.de.47.00.5c.12.00.00.01.b9.00.00.ff.Xf.LL.xx.xx.xx.
//     pubPacket^                      iIdx     (ff)         PTS marker
//              (Timecode)  (Packet)    (St.code)(ID-data-->)
//
//  Returns:    DTS in mSecs
//  Note:       The raw value has dropped the LSB to fit the PTS into 32 bits !
//              To obtain the time in (m)secs, scale by 90kHz/2 !
//
static u_int32 ts_ExtractDts(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx, int iDivide)
{
   u_int8   ubFlags; 
   u_int32  ulDts1=0, ulDts2, ulDts3;

   ubFlags = CVB_ubyte_get(&pubPacket[iIdx+7], 0x03, 6);
   switch(ubFlags)
   {
      case 0x3:
         //       byte-14    byte-15   byte-16   byte-17   byte-18    Shift Mask        
         //       xxxx.aaa1.bbbb.bbbb.bbbb.bbb1.cccc.cccc.cccc.ccc1 
         //  Dts1 0000.1110.0000.0000.0000.0000.0000.0000            << 4  0x0e00.0000 
         //  Dts2           1111.1111.1111.1110.0000.0000.0000.0000  >> 3  0x1fff.c000 
         //  Dts3           0000.0000.0000.0000.1111.1111.1111.1111  >> 2  0x0000.3fff 
         //  -->
         //  Dts1 1110.0000.0000.0000.0000.0000.0000.0000
         //  Dts2 0001.1111.1111.1111.1100.0000.0000.0000
         //  Dts3 0000.0000.0000.0000.0011.1111.1111.1111
         //
         ulDts1 = CVB_ulong_get(&pubPacket[iIdx+14],  0x0e000000, 0);
         ulDts1 <<= 4;
         ulDts2 = CVB_ulong_get(&pubPacket[iIdx+15], 0x1fffc000, 3);
         ulDts3 = CVB_ulong_get(&pubPacket[iIdx+15], 0x00003fff, 2); // drop lsb here !
         
         ulDts1 |= ulDts2;
         ulDts1 |= ulDts3;
         ulDts1 /= iDivide;        // User scale
         //
         HMX_LogEventData(pstComm, 5, "TS:Dts[", ubFlags, TRUE);
         HMX_LogEventData(pstComm, 5, "](mSec)=", ulDts1, FALSE);
         break;

      default:
      case 0x0:
      case 0x1:
      case 0x2:
         HMX_LogEventHexBytes(pstComm, 5, "TS:No Dts in this package", &pubPacket[iIdx], 16, TRUE);
         break;
   }
   return(ulDts1);
}

//
//  Function:   ts_ExtractPts
//  Purpose:    Extract the Presentation TS from a system packet
//  Parms:      pstComm, pubPacket, Index into packet, Divider
//              ti.me.co.de.47.00.5c.12.00.00.01.b9.00.00.ff.Xf.LL.xx.xx.xx.
//     pubPacket^                      iIdx     (ff)         PTS marker
//              (Timecode)  (Packet)    (St.code)(ID-data-->)
//
//  Returns:    PTS in mSecs, scaled by used
//  Note:       The raw value has dropped the LSB to fit the PTS into 32 bits !
//              To obtain the time in (m)secs, scale by 90kHz/2 !
//
static u_int32 ts_ExtractPts(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx, int iDivide)
{
   u_int8   ubFlags; 
   u_int32  ulPts1=0, ulPts2, ulPts3;

   ubFlags = CVB_ubyte_get(&pubPacket[iIdx+7], 0x03, 6);
   switch(ubFlags)
   {
      case 0x2:
      case 0x3:
         //       byte-9    byte-10   byte-11   byte-12   byte-13    Shift Mask        
         //       xxxx.aaa1.bbbb.bbbb.bbbb.bbb1.cccc.cccc.cccc.ccc1 
         //  Pts1 0000.1110.0000.0000.0000.0000.0000.0000            << 4  0x0e00.0000 
         //  Pts2           1111.1111.1111.1110.0000.0000.0000.0000  >> 3  0x1fff.c000 
         //  Pts3           0000.0000.0000.0000.1111.1111.1111.1111  >> 2  0x0000.3fff 
         //  -->
         //  Pts1 1110.0000.0000.0000.0000.0000.0000.0000
         //  Pts2 0001.1111.1111.1111.1100.0000.0000.0000
         //  Pts3 0000.0000.0000.0000.0011.1111.1111.1111
         //
         ulPts1 = CVB_ulong_get(&pubPacket[iIdx+9],  0x0e000000, 0);
         ulPts1 <<= 4;
         ulPts2 = CVB_ulong_get(&pubPacket[iIdx+10], 0x1fffc000, 3);
         ulPts3 = CVB_ulong_get(&pubPacket[iIdx+10], 0x00003fff, 2); // drop lsb here !
         
         ulPts1 |= ulPts2;
         ulPts1 |= ulPts3;
         ulPts1 /= iDivide;        // User scale
         //
         HMX_LogEventData(pstComm, 5, "TS:Pts[", ubFlags, TRUE);
         HMX_LogEventData(pstComm, 5, "](mSec)=", ulPts1, FALSE);
         break;

      default:
      case 0x0:
      case 0x1:
         HMX_LogEventHexBytes(pstComm, 5, "TS:Pts unsupported flags", &pubPacket[iIdx], 16, TRUE);
         break;
   }
   return(ulPts1);
}

//
//  Function:   ts_ExtractPcr
//  Purpose:    Extract the PCR from a system packet
//  Parms:      pstComm, pubPacket
//              ti.me.co.de.47.00.5c.12.AA.xx.xx.xx.xx.xx.xx.xx.xx.xx.xx.xx
//                                pubPacket^
//                                AA=Adapt Field length
//
//  Returns:    PCR in mSecs
//  Note:       The PCR (and/or OPCR) aree signalled in de Adaptation field:
//                   pcr_base       33 bits
//                   reserved       6
//                   pcr_extension  9
//
static u_int32 ts_ExtractPcr(HMXCOM *pstComm, u_int8 *pubPacket)
{
   u_int8   ubFlags; 
   u_int32  ulPcr, ulPcrBase, ulPcrExt=0;

   ubFlags = CVB_ubyte_get(&pubPacket[0], 0x01, 4);
   if(ubFlags)
   {
      ulPcrBase   = CVB_ulong_get(&pubPacket[1], 0xffffffff, 0);
      ulPcrBase <<= 1;
      ulPcr       = CVB_ulong_get(&pubPacket[3], 0x00000001, 31);
      ulPcrBase  |= ulPcr;
      ulPcrExt    = CVB_ulong_get(&pubPacket[3], 0x000001ff, 0);
      //
      HMX_LogEventData(pstComm, 5, "TS:PcrBase=", ulPcrBase, TRUE);
      HMX_LogEventData(pstComm, 5, " PcrExt=", ulPcrExt, FALSE);
      
   }
   else  HMX_LogEvent(pstComm, 5, "TS:No PCR", TRUE);
   //
   ubFlags = CVB_ubyte_get(&pubPacket[0], 0x01, 3);
   if(ubFlags)
   {
      ulPcrBase   = CVB_ulong_get(&pubPacket[1], 0xffffffff, 0);
      ulPcrBase <<= 1;
      ulPcr       = CVB_ulong_get(&pubPacket[3], 0x00000001, 31);
      ulPcrBase  |= ulPcr;
      ulPcrExt    = CVB_ulong_get(&pubPacket[3], 0x000001ff, 0);
      //
      HMX_LogEventData(pstComm, 5, "TS:OPcrBase=", ulPcrBase, TRUE);
      HMX_LogEventData(pstComm, 5, " OPcrExt=", ulPcrExt, FALSE);
      
   }
   else  HMX_LogEvent(pstComm, 5, "TS:No OPCR", TRUE);
   return(ulPcrExt);
}

//
//  Function:   ts_FindStartcode
//  Purpose:    Find the NEXT start of a startcode 00.00.01 from here to the end of the packet
//  Parms:      pstComm, payload^, idx
//
//  Returns:    New iIdx to the startcode
//
static int ts_FindStartcode(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx)
{
   u_int32  ulSubStCode;
   int      iMaxIdx = pstComm->iPacketSize-4;

   while(++iIdx < iMaxIdx)
   {
      //
      // It is possible that bit/byte allignment adds null bytes until the next start code !
      //
      ulSubStCode = CVB_ulong_get(&pubPacket[iIdx], 0x00ffffffL, 8);
      if(ulSubStCode == 0x00000001L) break;
   }   
   if(iIdx >= iMaxIdx) iIdx = -1;
   return(iIdx);
}

//
//  Function:   ts_FindSidInMapTable
//  Purpose:    Go look if there is a match in PIDs and PAT entries, so we are able to find the
//              Service ID (Program Nr) for the catalog.
//
//  Parms:      pstComm, pstPid
//  Returns:    usSid
//
static u_int16 ts_FindSidInMapTable(HMXCOM *pstComm)
{
   u_int16  usSid=0;
   PIDLST  *pstPid;
   int      iCount=0, iStart;

   pstPid = pstComm->pstPidList;
   //
   if(pstPid)
   {
      iStart = pstPid->iIndex;
      //
      // The Map table looks like:
      //    usPid    usSid    fInUse   Name
      //   =================================
      //    0x005c   0xffff   1        Video
      //    0x1e40   0x023b   0        ---
      //    0x0578   0x2714   0        ---
      //    0x0514   0x2713   1        ---   <--- Found in the SPTS and the PAT !
      //     ~~       ~~      ~         ~
      //    0x0000   0x0000   1        NIT
      //    0x005d   0xffff   1        Audio
      //    0x00fe   0xffff   1        ---
      //    0x138b   0xffff   1        ---
      //
      do
      {
         if( (pstPid->fInUse) && (pstPid->usSid != 0xffff) && (pstPid->usSid != 0x0000))
         {
            usSid = pstPid->usSid;
            HMX_LogEventData(pstComm, 5, "   Sid:",  pstPid->usSid, TRUE);
            HMX_LogEventHexData(pstComm, 5, " Pid:", pstPid->usPid, FALSE);
            iCount++;
         }
         pstPid = pstPid->pstNext;
      }
      while(pstPid->iIndex != iStart);
   }
   //
   // Check if there is more than 1 service in the SPTS that is listed in the PAT. 
   // This should not be the case: report the event.
   //
   if(iCount ==0) HMX_LogEvent(pstComm,     -1, "TS:NO service entry in PAT",  TRUE);
   if(iCount > 1) HMX_LogEventData(pstComm, -1, "TS:Multiple service entries in PAT:",  iCount, TRUE);
   //
   return(usSid);
}

//
//  Function:   ts_FreePidList
//  Purpose:    Free the PID list
//
//  Parms:      pstComm
//  Returns:    NULL
//
static PIDLST *ts_FreePidList(HMXCOM *pstComm)
{
   PIDLST  *pstPid = pstComm->pstPidList;
   PIDLST  *pstTmp;
   PIDLST  *pstStr;

   if(pstPid)
   {
      pstStr = pstPid;
      pstPid = pstPid->pstNext;
      while(pstPid != pstStr)
      {
         pstTmp = pstPid;
         pstPid = pstPid->pstNext;
         CVB_SafeFree(pstTmp);
      }
      CVB_SafeFree(pstStr);
  }
  return(NULL);
}

//
//  Function:   ts_HandleAdaptFldCtl
//  Purpose:    Check if this payload PES has an Adaptation field.
//              If so, return the size of it to jump over it. 
//              If fPCR is set, extract the PCR from the field also.
//  Parms:      pstComm, Packet ptr, idx, DoPcr
//
//  Returns:    New iIdx
//
static int ts_HandleAdaptFldCtl(HMXCOM *pstComm, TS_MPEG2 *pstPacket, u_int8 ubIdx, BOOL fPcr) 
{
   u_int8   ubAfc;

   //
   // Handle Adaptation field 
   //
   ubAfc = CVB_ubyte_get(&pstPacket->ubFlag2Cc, 0x03, 4);
   //
   switch(ubAfc)
   {
      case 1:
         // No adaptation field
         //HMX_LogEventHexBytes(pstComm, 10, "TS:PUSI AFC=1", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
         break;

      default:
         HMX_LogEvent(pstComm, -1, "TS:Unknown AFC", TRUE);
         HMX_LogEventHexBytes(pstComm, 10, "TS:PUSI AFC=??", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
         return(ubIdx);

      case 2:
         // Adaptation field, no payload: skip packet
         HMX_LogEventHexBytes(pstComm, 10, "TS:PUSI AFC=2", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
         if(fPcr) ts_ExtractPcr(pstComm, &pstPacket->ubPayload[1]);
         return(ubIdx);

      case 3:
         // Adaptation field, and payload
         // Add Adaptationfield length
         //HMX_LogEventHexBytes(pstComm, 10, "TS:PUSI AFC=3", (u_int8 *)&pstPacket->ubSync, 32, TRUE);
         if(fPcr) ts_ExtractPcr(pstComm, &pstPacket->ubPayload[1]);
         ubIdx += CVB_ubyte_get(&pstPacket->ubPayload[0], 0xff, 0) + 1;
         break;
   }
   return(ubIdx);
}

//
//  Function:   ts_StoreFrame
//  Purpose:    Handle the frame we just finished acquiring.
//  Parms:      pstComm, Packet^, Idx
//              ti.me.co.de.47.40.5c.12......00.00.01.zz.xx.xx.xx.xx.xx.xx.xx.xx.xx....
//                            pubPacket[iIdx]^        zz=00 for SD         
//                                                       06 for HD
//  Returns:
//
static int ts_StoreFrame(HMXCOM *pstComm, u_int8 *pubPacket, int iIdx)
{
   NTSREC  *pstNts = pstComm->pstNts;
   
   //
   // pstNts->ubFrType is the previous frame type, that we need to store, if we have one, 
   // and start with the new one.
   //
   if(pstNts->ubFrType)
   {
      if(pstComm->fHaveIframe)
      {
         //
         // New frame, and we had a previous one to save. Check if we're SD or HD and store
         // the previous frame accordingly.
         //
         if( HMX_CheckHdtv(pstComm) )
         {
            //
            // HD Content
            //
            switch(pstNts->ubFrType)
            {
               case FRAME_TYPE_I:
                  pstNts->ubField1a  = pstNts->ubFrType;
                  pstNts->ulPtsI     = pstNts->ulPts;
                  pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefB);
                  pstNts->usField2a  = (u_int16)  pstNts->ulField1b;
                  pstNts->ubField2c  = 0x34;
                  pstNts->llField34  = pstNts->llFp;
                  pstNts->ulField5   = pstNts->ulPrevVideoPts;
                  pstNts->ulField6   = pstNts->ulFrSize;
                  pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
                  pstNts->ulField9   = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
                  pstNts->ulField8   = 0;
                  pstNts->ulField10  = (u_int32) (pstNts->llRefB - pstNts->llRefA);
                  //
                  pstNts->usRefAI    =  pstNts->usField2a;
                  break;
         
               default:
                  pstNts->ubField1a  = pstNts->ubFrType;
                  pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefB);
                  pstNts->usField2a  =            pstNts->usRefAI;
                  pstNts->ubField2c  = 0x34;
                  pstNts->llField34  = pstNts->llFp;
                  pstNts->ulField5   = pstNts->ulPrevVideoPts;
                  pstNts->ulField6   = pstNts->ulFrSize;
                  pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
                  pstNts->ulField8   = 0;
                  pstNts->ulField9   = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
                  pstNts->ulField10  = (u_int32) (pstNts->llRefB - pstNts->llRefA);
                  break;
            }
         }
         else
         {
            //
            // SD Content
            //
            switch(pstNts->ubFrType)
            {
               case FRAME_TYPE_I:
                  pstNts->ubField1a  = pstNts->ubFrType;
                  pstNts->ulPtsI     = pstNts->ulPts;
                  pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart  - pstNts->llRefA);
                  pstNts->usField2a  = (u_int16) (pstNts->llRefPictStartI - pstNts->llRefA);
                  pstNts->ubField2c  = 0x24;
                  pstNts->llField34  = pstNts->llFp;
                  pstNts->ulField5   = pstNts->ulThisVideoPts;
                  pstNts->ulField6   = pstNts->ulFrSize;
                  pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
                  pstNts->ulField8   = 0;
                  break;
         
               default:
                  pstNts->ubField1a  = pstNts->ubFrType;
                  pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
                  pstNts->usField2a  = (u_int16) (pstNts->llRefPictStartI - pstNts->llRefA);
                  pstNts->ubField2c  = 0x24;
                  pstNts->llField34  = pstNts->llFp;
                  pstNts->ulField5   = pstNts->ulThisVideoPts;
                  pstNts->ulField6   = pstNts->ulFrSize;
                  pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
                  pstNts->ulField8   = 0;
                  break;
            }
         }
         //pwjh switch(pstComm->tContentType)
         //pwjh {
         //pwjh    case NTS_DEF_AUTO:
         //pwjh    case NTS_DEF_SD:
         //pwjh    case NTS_DEF_AUTO_SD:
         //pwjh       //
         //pwjh       // SD Content
         //pwjh       //
         //pwjh       switch(pstNts->ubFrType)
         //pwjh       {
         //pwjh          case FRAME_TYPE_I:
         //pwjh             pstNts->ubField1a  = pstNts->ubFrType;
         //pwjh             pstNts->ulPtsI     = pstNts->ulPts;
         //pwjh             pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart  - pstNts->llRefA);
         //pwjh             pstNts->usField2a  = (u_int16) (pstNts->llRefPictStartI - pstNts->llRefA);
         //pwjh             pstNts->ubField2c  = 0x24;
         //pwjh             pstNts->llField34  = pstNts->llFp;
         //pwjh             pstNts->ulField5   = pstNts->ulThisVideoPts;
         //pwjh             pstNts->ulField6   = pstNts->ulFrSize;
         //pwjh             pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
         //pwjh             pstNts->ulField8   = 0;
         //pwjh             break;
         //pwjh 
         //pwjh          default:
         //pwjh             pstNts->ubField1a  = pstNts->ubFrType;
         //pwjh             pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
         //pwjh             pstNts->usField2a  = (u_int16) (pstNts->llRefPictStartI - pstNts->llRefA);
         //pwjh             pstNts->ubField2c  = 0x24;
         //pwjh             pstNts->llField34  = pstNts->llFp;
         //pwjh             pstNts->ulField5   = pstNts->ulThisVideoPts;
         //pwjh             pstNts->ulField6   = pstNts->ulFrSize;
         //pwjh             pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
         //pwjh             pstNts->ulField8   = 0;
         //pwjh             break;
         //pwjh       }
         //pwjh       break;
         //pwjh 
         //pwjh    case NTS_DEF_HD:
         //pwjh    case NTS_DEF_AUTO_HD:
         //pwjh       //
         //pwjh       // HD Content
         //pwjh       //
         //pwjh       switch(pstNts->ubFrType)
         //pwjh       {
         //pwjh          case FRAME_TYPE_I:
         //pwjh             pstNts->ubField1a  = pstNts->ubFrType;
         //pwjh             pstNts->ulPtsI     = pstNts->ulPts;
         //pwjh             pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefB);
         //pwjh             pstNts->usField2a  = (u_int16)  pstNts->ulField1b;
         //pwjh             pstNts->ubField2c  = 0x34;
         //pwjh             pstNts->llField34  = pstNts->llFp;
         //pwjh             pstNts->ulField5   = pstNts->ulPrevVideoPts;
         //pwjh             pstNts->ulField6   = pstNts->ulFrSize;
         //pwjh             pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
         //pwjh             pstNts->ulField9   = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
         //pwjh             pstNts->ulField8   = 0;
         //pwjh             pstNts->ulField10  = (u_int32) (pstNts->llRefB - pstNts->llRefA);
         //pwjh             //
         //pwjh             pstNts->usRefAI    =  pstNts->usField2a;
         //pwjh             break;
         //pwjh 
         //pwjh          default:
         //pwjh             pstNts->ubField1a  = pstNts->ubFrType;
         //pwjh             pstNts->ulField1b  = (u_int32) (pstNts->llRefPictStart - pstNts->llRefB);
         //pwjh             pstNts->usField2a  =            pstNts->usRefAI;
         //pwjh             pstNts->ubField2c  = 0x34;
         //pwjh             pstNts->llField34  = pstNts->llFp;
         //pwjh             pstNts->ulField5   = pstNts->ulPrevVideoPts;
         //pwjh             pstNts->ulField6   = pstNts->ulFrSize;
         //pwjh             pstNts->ulField7   = pstNts->ulTrickPts - pstNts->ulTrickPtsStart + 0x600;
         //pwjh             pstNts->ulField8   = 0;
         //pwjh             pstNts->ulField9   = (u_int32) (pstNts->llRefPictStart - pstNts->llRefA);
         //pwjh             pstNts->ulField10  = (u_int32) (pstNts->llRefB - pstNts->llRefA);
         //pwjh             break;
         //pwjh       }
         //pwjh       break;
         //pwjh }
         HMX_LogEventHexBytes(pstComm, 2, "TS:", &pubPacket[iIdx], 16, TRUE);
         HMX_LogEventData(    pstComm, 2, " Frame=", (int)pstNts->ubField1a, FALSE);
         HMX_LogEventHexData( pstComm, 2, " Size=", pstNts->ulField6, FALSE);
         HMX_LogEventData(    pstComm, 2, " Fld1b=", pstNts->ulField1b, FALSE);
         HMX_LogEventData(    pstComm, 2, " Fld2a=", pstNts->usField2a, FALSE);
         //
         NTS_AddIndexRecord(pstComm);
      }
   }
   return(iIdx);
}

//
//  Function:   ts_InitNtsRecord
//  Purpose:    Set the NTS record structure to init values.
//  Parms:      pstComm
//
//  Returns:    
//
static void ts_InitNtsRecord(HMXCOM *pstComm, BOOL fResetAll)
{
   NTSREC  *pstNts = pstComm->pstNts;

   if(pstNts)
   {
      if(fResetAll)
      {
         memset(pstNts, 0, sizeof(NTSREC));
      }
   }
}

//
//  Function:   ts_SearchPid
//  Purpose:    Search a PID in the linked list from the Map table
//
//  Parms:      Comm, PID list, PID
//  Returns:    PIDlist of this PID
//
static PIDLST *ts_SearchPid(HMXCOM *pstComm, PIDLST *pstPid, u_int16 usPid)
{
   BOOL     fSearching=TRUE;
   int      iStart;

   if(pstPid)
   {
      iStart = pstPid->iIndex;
      //
      while(fSearching)
      {
         if(pstPid->usPid == usPid)
         {
            // Found
            fSearching = FALSE;
         }
         else
         {
            if(pstPid->pstNext->iIndex == iStart)
            {
               fSearching = FALSE;
               pstPid     = NULL;
            }
            else
            {
               pstPid = pstPid->pstNext;
            }
         }
      }
   }
   return(pstPid);
}

//
//  Function:   ts_UpdateContentType
//  Purpose:    Update the content type if required
//
//  Parms:      Comm, content type
//  Returns:    Actual content type
//
static NTSDEF ts_UpdateContentType(HMXCOM *pstComm, NTSDEF tDef)
{
   switch(pstComm->tContentType)
   {
      case NTS_DEF_SD:
      case NTS_DEF_HD:
         // User has fixed the content type to SD or HD: leave it that way
         break;
         
      default:
      case NTS_DEF_AUTO_SD:
      case NTS_DEF_AUTO_HD:
         // User has selected Auto: set to current content type
         pstComm->tContentType = tDef;
         break;
   }
   return(pstComm->tContentType);
}