/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmtXlate.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            HMT Catalog translate functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       24Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "ClockApi.h"

//
// Local prototypes
//
static u_int32 hmt_GetDefaultTimestamp  (HMXCOM *, int);
//
static u_int32 hmt_InsStart             (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsChannelNr         (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsStarttime         (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsEndtime           (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsPathname          (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsEventname         (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsServicename       (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsMiscValues        (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsRecordingOk       (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsIcons1            (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsIcons2            (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsPidList           (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsPfCount           (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsPfEvents          (HMXCOM *, XLCAT *, HMTDIR);
static u_int32 hmt_InsTimeRestart       (HMXCOM *, XLCAT *, HMTDIR);

//
// The Catalog template file
//
XLCAT stHmxTranslateList[] = 
{  // Address                 Data              Xlate function 
   {  HMT_START,              0,                hmt_InsStart            },          // We need some kind of header
   {  HMT_CHANNEL,            0,                hmt_InsChannelNr        },          // We need a channel number
   {  HMT_TIME_START,         0,                hmt_InsStarttime        },          // DateTime stamp start
   {  HMT_TIME_END,           0,                hmt_InsEndtime          },          // DateTime stamp end
   {  HMT_PATHNAME,           0,                hmt_InsPathname         },          // Linux pathname on /media/sda1/*
   {  HMT_EVENTNAME,          0,                hmt_InsEventname        },          // Event name
   {  HMT_SERVICENAME,        0,                hmt_InsServicename      },          // Service name
   {  HMT_RECORDING_OK,       0,                hmt_InsRecordingOk      },          // The recording has completed OKee
   {  HMT_ICONS1,             0,                hmt_InsIcons1           },          // Icon bits 1
   {  HMT_ICONS2,             0,                hmt_InsIcons2           },          // Icon bits 2
   {  HMT_PATPID,             0,                hmt_InsPidList          },          // Start of the PID list
   {  HMT_TIME_RESTART,       0,                hmt_InsTimeRestart      },          // Playback restart time
   {  HMT_EVENTS,             0,                hmt_InsPfCount          },          // P/F event descriptor count
   {  HMT_PF_EVENTS,          0,                hmt_InsPfEvents         },          // P/F event descriptors
};
const int iNrHmxXlate = sizeof(stHmxTranslateList)/sizeof(XLCAT);                   // Nr of entries

/* ====== Functions separator ===========================================
void ___api_functions____(){}
=========================================================================*/

//
//  Function:  HMT_CatalogTranslate
//  Purpose:   Translate acquired values into the HMT catalog buffer
//  Parms:     pstComm, dir Cat-->Stream or vv.
//
//  Returns:   Size of the pubHmtCat buffer
//
u_int32 HMT_CatalogTranslate(HMXCOM *pstComm, HMTDIR tDir)
{
   int      iNr;
   u_int32  ulMaxSize=0, ulSize=0;
   XLCAT   *pstXlat;

   for(iNr=0; iNr<iNrHmxXlate; iNr++)
   {
      pstXlat = &stHmxTranslateList[iNr];
      ulSize  = pstXlat->pfHndlr(pstComm, pstXlat, tDir);
      if(ulSize > ulMaxSize) ulMaxSize = ulSize;
   }
   return(ulMaxSize);
}


/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/

//
//  Function:  hmt_GetDefaultTimestamp
//  Purpose:   Get a better timestamp than 01 Jan 1970
//  Parms:     pstComm, 0(start) or 1 (end)
//
//  Returns:   ulSecs
//
static u_int32 hmt_GetDefaultTimestamp(HMXCOM *pstComm, int iEnd)
{
   u_int32  ulSecs, ulTemp;
   int      iDuration;

   //
   // There are various ways to obtain the duration of this recording:
   // 1. Parse the GOP starts and determine the start/final end time
   // 2. Check the PTS packets in the stream
   // 3. Count the number of I/P/B frames and multiply by 20 msecs.
   //
   if(iEnd)
   {
      iDuration = (pstComm->iDurationEnd - pstComm->iDurationStart);
      if(iDuration > 0)
      {
         // GOP way
         ulSecs = (u_int32)(pstComm->iTsCreate + iDuration);
      }
      else
      {
         // No GOPs in the stream: use packet count
         ulTemp = (pstComm->ulNumFrames * 20) / 1000;
         ulSecs = (u_int32)(pstComm->iTsCreate + ulTemp);
      }
   }
   else
   {
      ulSecs = (u_int32)pstComm->iTsCreate;
   }
   return(ulSecs);
}

//
//  Function:  hmt_InsStart
//  Purpose:   Insert a fictive start nr
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsStart(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         pubData = CVB_ulong_put(pubData, pstComm->pstHmtCat->ulStart1);
         pubData = CVB_ulong_put(pubData, pstComm->pstHmtCat->ulStart2);
         pubData = CVB_ulong_put(pubData, pstComm->pstHmtCat->ulStart3);
         pubData = CVB_ulong_put(pubData, pstComm->pstHmtCat->ulStart4);
         ulAddr += 16;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ulStart1 = CVB_ulong_get(pubData+0,  0xffffffffL, 0);
         pstComm->pstHmtCat->ulStart2 = CVB_ulong_get(pubData+4,  0xffffffffL, 0);
         pstComm->pstHmtCat->ulStart3 = CVB_ulong_get(pubData+8,  0xffffffffL, 0);
         pstComm->pstHmtCat->ulStart4 = CVB_ulong_get(pubData+12, 0xffffffffL, 0);
         ulAddr += 16;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsChannelNr
//  Purpose:   Insert a fictive channel number
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsChannelNr(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         CVB_ushort_put(pubData, pstComm->pstHmtCat->usChannelNr);
         ulAddr += 2;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->usChannelNr = CVB_ushort_get(pubData, 0xffffL, 0);
         ulAddr += 2;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsStarttime
//  Purpose:   
//  Parms:     pstComm, pubData
//             ulStartTime   HMT_TIME_START:   00.49.df.29
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsStarttime(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr, ulSecsStart, ulSecsEnd;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         //
         ulSecsStart = pstComm->pstHmtCat->ulStartTime;
         if(ulSecsStart == 0)
         {
            ulSecsStart = hmt_GetDefaultTimestamp(pstComm, 0);
            ulSecsEnd   = hmt_GetDefaultTimestamp(pstComm, 1);
            //
            pstComm->pstHmtCat->ulStartTime = ulSecsStart;
            pstComm->iDurationStart         = ulSecsStart;
            pstComm->pstHmtCat->ulEndTime   = ulSecsEnd;
            pstComm->iDurationEnd           = ulSecsEnd;
         }
         //
         CVB_ulong_put(pubData, ulSecsStart);
         ulAddr += 4;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ulStartTime = CVB_ulong_get(pubData, 0xffffffffL, 0);
         ulAddr += 4;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsEndtime
//  Purpose:    
//  Parms:     pstComm, pubData
//             ulEndTime   HMT_TIME_END:   00.49.df.29
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsEndtime(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr, ulSecsEnd;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         ulSecsEnd   = pstComm->pstHmtCat->ulEndTime;
         if(ulSecsEnd == 0)
         {
            ulSecsEnd = hmt_GetDefaultTimestamp(pstComm, 1);
            pstComm->pstHmtCat->ulEndTime = ulSecsEnd;
            pstComm->iDurationEnd         = ulSecsEnd;
         }
         //
         CVB_ulong_put(pubData, ulSecsEnd);
         ulAddr += 4;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ulEndTime = CVB_ulong_get(pubData, 0xffffffffL, 0);
         ulAddr += 4;
         break;
   }
   return(ulAddr);
}

//
//  Function:  hmt_InsPathname
//  Purpose:   
//  Parms:     pstComm, pubData
//             pcFilePath[HMT_PATHNAME_MAX]  HMT_PATHNAME:     /media/sda1/Peter/Event_20090415_1100
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsPathname(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr, ulSize;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         ulSize = pstComm->pstHmtCat->pclFilePath->GetLength();
         memcpy(pubData, pstComm->pstHmtCat->pclFilePath->GetBuffer(), ulSize);
         ulAddr += ulSize;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->pclFilePath->Format(_T("%s"), pubData);
         break;
   }
   return(ulAddr);
}

//
//  Function:  hmt_InsEventname
//  Purpose:   
//  Parms:     pstComm, pubData
//             pcEvent[HMT_EVENTNAME_MAX]   HMT_EVENTNAME:    Event
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsEventname(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int32  ulSize;
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         ulSize = pstComm->pstHmtCat->pclEvent->GetLength();
         memcpy(pubData, pstComm->pstHmtCat->pclEvent->GetBuffer(), ulSize);
         ulAddr += ulSize;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->pclEvent->Format(_T("%s"), pubData);
         break;
   }
   return(ulAddr);
}

//
//  Function:   hmt_InsServicename
//  Purpose:    
//  Parms:     pstComm, pubData
//             pcService[HMT_SERVICENAME_MAX]   HMT_SERVICENAME:  Nederland 3
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsServicename(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr, ulSize;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         ulSize = pstComm->pstHmtCat->pclService->GetLength();
         memcpy(pubData, pstComm->pstHmtCat->pclService->GetBuffer(), ulSize);
         ulAddr += ulSize;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->pclService->Format(_T("%s"), pubData);
         break;
   }
   return(ulAddr);
}

//
//  Function:  hmt_InsRecordingOk
//  Purpose:   Insert a fictive Recording OKee marker
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsRecordingOk(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8              *pubData;
   u_int32              ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         CVB_ubyte_put(pubData, pstComm->pstHmtCat->ubRecordingOk);
         ulAddr += 1;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ubRecordingOk = CVB_ubyte_get(pubData, 0xff, 0);
         ulAddr += 1;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsIcons1
//  Purpose:   Insert the Icon bit(s)
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsIcons1(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         CVB_ubyte_put(pubData, pstComm->pstHmtCat->ubIconFlags1);
         ulAddr += 1;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ubIconFlags1 = CVB_ubyte_get(pubData, 0xff, 0);
         ulAddr += 1;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsIcons2
//  Purpose:   Insert the Icon bit(s)
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsIcons2(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         // Update the HD/SD icon
         if( HMX_CheckHdtv(pstComm) ) pstComm->pstHmtCat->ubIconFlags2 |= HMT_ICON_HD;
         else                         pstComm->pstHmtCat->ubIconFlags2 &=~HMT_ICON_HD;
         //
         CVB_ubyte_put(pubData, pstComm->pstHmtCat->ubIconFlags2);
         ulAddr += 1;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->ubIconFlags2 = CVB_ubyte_get(pubData, 0xff, 0);
         ulAddr += 1;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsMiscValues
//  Purpose:    
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsMiscValues(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         break;

      case HMT_STREAM_TO_CAT:
         break;
   }
   return(0);
}

//
//  Function:  hmt_InsPidList
//  Purpose:    
//  Parms:     pstComm, pubData
//                            HMT_PATPID:
//             usServiceId    HMT_SRVSID <--- Needs to be filled out !
//             usVideoPid     HMT_VIDPID <--- Other PIDs will be acquired from the SPTS
//             usAudioPid     HMT_AUDPID
//             usPcrPid       HMT_PCRPID
//             usTtxPid       HMT_TTXPID
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsPidList(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         CVB_ushort_put(&pstComm->pubHmtCat[HMT_SRVSID], pstComm->pstHmtCat->usServiceId);
         // CVB_ushort_put(&pstComm->pubHmtCat[HMT_VIDPID], pstComm->pstHmtCat->usVideoPid);
         // CVB_ushort_put(&pstComm->pubHmtCat[HMT_AUDPID], pstComm->pstHmtCat->usAudioPid);
         // CVB_ushort_put(&pstComm->pubHmtCat[HMT_PCRPID], pstComm->pstHmtCat->usPcrPid);
         // CVB_ushort_put(&pstComm->pubHmtCat[HMT_TTXPID], pstComm->pstHmtCat->usTtxPid);
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->usServiceId = CVB_ushort_get(&pstComm->pubHmtCat[HMT_SRVSID], 0x1fff, 0);
         // pstComm->pstHmtCat->usVideoPid  = CVB_ushort_get(&pstComm->pubHmtCat[HMT_VIDPID], 0x1fff, 0);
         // pstComm->pstHmtCat->usAudioPid  = CVB_ushort_get(&pstComm->pubHmtCat[HMT_AUDPID], 0x1fff, 0);
         // pstComm->pstHmtCat->usPcrPid    = CVB_ushort_get(&pstComm->pubHmtCat[HMT_PCRPID], 0x1fff, 0);
         // pstComm->pstHmtCat->usTtxPid    = CVB_ushort_get(&pstComm->pubHmtCat[HMT_TTXPID], 0x1fff, 0);
         break;
   }
   return(0);
}

//
//  Function:  hmt_InsTimeRestart
//  Purpose:   Playback restart time in secs
//  Parms:     pstComm, pubData
//
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsTimeRestart(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         CVB_ushort_put(pubData, pstComm->pstHmtCat->usStartPlay);
         ulAddr += 2;
         break;

      case HMT_STREAM_TO_CAT:
         pstComm->pstHmtCat->usStartPlay = CVB_ushort_get(pubData, 0xffff, 0);
         ulAddr += 2;
         break;
   }

   return(ulAddr);
}

//
//  Function:  hmt_InsPfCount
//  Purpose:   Handle the number of P/F Events
//
//  Parms:     pstComm, Buffer
//       
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsPfCount(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int8  *pubData;
   u_int32  ulAddr, ulNr;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         ulNr = pstComm->pstHmtCat->pstEvt->ulNumEvents;
         //
         CVB_ulong_put(pubData, ulNr);
         ulAddr += 4;
         break;

      case HMT_STREAM_TO_CAT:
         ulNr = CVB_ulong_get(pubData, 0xffffffffL, 0);
         pstComm->pstHmtCat->pstEvt->ulNumEvents = ulNr;
         if(ulNr == 0)
         {
            HMX_LogEvent(pstComm, -1, "HMT: No Now/Next events in the stream !", TRUE);
            break;
         }
         break;
   }
   return(ulAddr);
}

//
//  Function:  hmt_InsPfEvents
//  Purpose:   Handle the list of P/F events
//
//  Parms:     pstComm, Buffer
//       
//  Returns:   End address of the buffer
//
static u_int32 hmt_InsPfEvents(HMXCOM *pstComm, XLCAT *pstXlate, HMTDIR tDir)
{
   u_int32  ulSizeDesc, ulNr, x;
   u_int32  ulSizeName, ulSizeText;
   u_int8  *pubData, *pubDataStart;
   u_int32  ulAddr;
   HMTPF   *pstPf;

   ulAddr  = pstXlate->ulAddr;
   pubData = &(pstComm->pubHmtCat[ulAddr]);
   pstPf   = pstComm->pstHmtCat->pstEvt->pstPf;
   
   //
   switch(tDir)
   {
      case HMT_CAT_TO_STREAM:
         if(pstPf==NULL)
         {
            HMX_LogEvent(pstComm, -1, "HMT: Now/Next event list empty !", TRUE);
            break;
         }
         ulNr         = pstComm->pstHmtCat->pstEvt->ulNumEvents;
         pubDataStart = pubData;
         //
         // Put out all linked events
         //
         for(x=0; x<ulNr; x++)
         {
            if(pstPf)
            {
               ulSizeName = pstPf->pclEventName->GetLength()+1;                        // Size of the event name
               ulSizeText = pstPf->pclEventDescr->GetLength()+1;                       // Size of the descriptor text
               ulSizeDesc = ulSizeText + 22;                                           // Size of the descriptor block 
                                                                                       //
               CVB_ushort_put(&pubData[HMT_PF_EVT_ID],     pstPf->usEventId);          // The Event ID
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF1], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF2], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF3], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF4], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF5], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF6], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF7], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF8], 0);                         // 00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_STUFF9], 0);                         // 00.00
               CVB_ubytes_put(&pubData[HMT_PF_EVT_NAME],                               //
                             (u_int8 *)pstPf->pclEventName->GetBuffer(), ulSizeName);  // The Event name
               CVB_ulong_put(&pubData[HMT_PF_EVT_FLAGS10], 0);                         // 00.00.00.00
               CVB_ulong_put(&pubData[HMT_PF_EVT_FLAGS11], 0);                         // 00.00.00.00
               CVB_ulong_put(&pubData[HMT_PF_EVT_SIZE1],   ulSizeDesc);                // The Event Description size (twice)
               CVB_ulong_put(&pubData[HMT_PF_EVT_SIZE2],   ulSizeDesc);                //
               CVB_ulong_put(&pubData[HMT_PF_EVT_IDX],     x+1);                       // Event index
               CVB_ulong_put(&pubData[HMT_PF_EVT_ISO],     pstPf->ulIso);              // The Event ISO "dut"0
               CVB_ulong_put(&pubData[HMT_PF_EVT_STUFF10], 0);                         // 00.00.00.00
               CVB_ulong_put(&pubData[HMT_PF_EVT_STUFF11], 0);                         // 00.00.00.00
               CVB_ushort_put(&pubData[HMT_PF_EVT_TEXT_LEN], ulSizeText);              // Event descr text size
               CVB_ubytes_put(&pubData[HMT_PF_EVT_TEXT],                               //
                             (u_int8 *)pstPf->pclEventDescr->GetBuffer(), ulSizeText); //
               //
               pstPf    = pstPf->pstNext;
               pubData += ulSizeText+HMT_PF_EVT_TEXT;
            }
            else
            {
               HMX_LogEvent(pstComm, -1, "HMT: Now/Next event list mismatch !", TRUE);
            }
         }
         ulAddr += (u_int32) (pubData-pubDataStart);
         break;

      case HMT_STREAM_TO_CAT:
         HMT_ReadEvents(pstComm);
         break;
   }
   return(ulAddr);
}

