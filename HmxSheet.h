/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxSheet.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Setup the property pages for the dialogs header file
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(AFX_HMXSHEET_H)
#define AFX_HMXSHEET_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "HmxPageGeneric.h"
#include "HmxPageTs.h"
#include "HmxPageHmt.h"
#include "HmxPageNts.h"


class CHmxSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CHmxSheet)

// Construction
public:
	CHmxSheet(UINT nIDCaption,    CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CHmxSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
    CHmxPageGeneric     m_HmxPageGeneric;
    CHmxPageTs          m_HmxPageTs;
    CHmxPageHmt         m_HmxPageHmt;
    CHmxPageNts         m_HmxPageNts;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHmxSheet)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHmxSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CHmxSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HMXSHEET_H)
