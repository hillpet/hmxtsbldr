/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageGeneric.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for generic issues
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxPageGeneric.h"
#include "ClockApi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CHmxPageGeneric, CPropertyPage)



//
// keep the main dialog window handle here
//
static HWND hDialogWnd;


//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageGeneric::CHmxPageGeneric() : CPropertyPage(CHmxPageGeneric::IDD)
{
   iNumEs     = 1;
   iIdxEs     = 1;
   pstHmxComm = NULL;
   pclNvm     = (CNvmStorage *) new(CNvmStorage);
   VERIFY(pclNvm);

   //{{AFX_DATA_INIT(CHmxPageGeneric)
   //}}AFX_DATA_INIT
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageGeneric::~CHmxPageGeneric()
{
    //
    //  Get rid of the  NVM Storage Map
    //
    if(pclNvm)
    {
        delete pclNvm;
    }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_GEN_EDIT_INFILE,     clGenEditInfile);
   DDX_Control(pDX, IDC_GEN_EDIT_IDX,        clGenEditIndex);
   DDX_Control(pDX, IDC_GEN_SPIN_IDX,        clGenSpinButton);
   DDX_Control(pDX, IDC_GEN_EDIT_PID,        clGenEditPid);
   DDX_Control(pDX, IDC_GEN_EDIT_TS_PACKETS, clGenEditPsiPackets);
   DDX_Control(pDX, IDC_GEN_EDIT_PES_PACKETS,clGenEditPesPackets);
   DDX_Control(pDX, IDC_GEN_EDIT_PES_NAME,   clGenEditPesName);
   DDX_Control(pDX, IDC_GEN_EDIT_REC_START,  clGenEditRecStart);
   DDX_Control(pDX, IDC_GEN_EDIT_REC_END,    clGenEditRecEnd);
   //{{AFX_DATA_MAP(CHmxPageGeneric)
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHmxPageGeneric, CPropertyPage)
   //{{AFX_MSG_MAP(CHmxPageGeneric)
   //}}AFX_MSG_MAP
   ON_MESSAGE(WM_SOLVER_DATA,                OnRcvWindowData)
   ON_NOTIFY(UDN_DELTAPOS, IDC_GEN_SPIN_IDX, OnBnSpinEsIndex)
END_MESSAGE_MAP()




//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::LoadDialog()
{
    //
    // Load the PAGE data from storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::StoreDialog()
{
    //
    // Save the PAGE data to storage
    //
}

//
//  Function:   UpdateDialog
//  Purpose:    Update all the fields in this window
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::UpdateDialog()
{
   CString  clStr;
   int      iIdx;
   PIDLST  *pstPidList;
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   if(pstHmxComm && pclNvm)
   {
      //-----------------------------------------------------------------------
      // -1- Event name field
      //-----------------------------------------------------------------------
      HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME, &clStr);
      clGenEditInfile.SetWindowText(clStr.GetBuffer());

      //-----------------------------------------------------------------------
      // -2- ES list
      //-----------------------------------------------------------------------
      if( iNumEs != pstHmxComm->iNumEs)
      {
         iNumEs = pstHmxComm->iNumEs;
         clGenSpinButton.SetRange32(1, iNumEs);
      }
      //
      if(iIdxEs > iNumEs) iIdx = iNumEs;
      iIdx = iIdxEs;
      pstPidList = pstHmxComm->pstPidList;
      if(iIdx && pstPidList)
      {
         // Show the PES data of the selected ES index (Video/Audio/TTX/??)
         while(--iIdx) pstPidList = pstPidList->pstNext;
         {
            clStr.Format(_T("%d"), iIdxEs);
            clGenEditIndex.SetWindowText(clStr);
            //
            clStr.Format(_T("0x%X"), pstPidList->usPid);
            clGenEditPid.SetWindowText(clStr);
            //
            clStr.Format(_T("%d"), pstPidList->ulPsiPackets);
            clGenEditPsiPackets.SetWindowText(clStr);
            //
            clStr.Format(_T("%d"), pstPidList->ulPesPackets);
            clGenEditPesPackets.SetWindowText(clStr);
            //
            clStr.Format(_T("%s"), pstPidList->pcEsName);
            clGenEditPesName.SetWindowText(clStr);
            //
            stCount.ulSeconds = pstHmxComm->pstHmtCat->ulStartTime;
            CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
            clStr.Format(_T("%02d-%02d-%04d  %02d:%02d:%02d"), 
                           stDateTime.ubDay,
                           stDateTime.ubMonth,
                           stDateTime.usYear+1900,
                           stDateTime.ubHour,
                           stDateTime.ubMin,
                           stDateTime.ubSec);
            clGenEditRecStart.SetWindowText(clStr);
            //
            stCount.ulSeconds = pstHmxComm->pstHmtCat->ulEndTime;
            CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
            clStr.Format(_T("%02d-%02d-%04d  %02d:%02d:%02d"), 
                           stDateTime.ubDay,
                           stDateTime.ubMonth,
                           stDateTime.usYear+1900,
                           stDateTime.ubHour,
                           stDateTime.ubMin,
                           stDateTime.ubSec);
            clGenEditRecEnd.SetWindowText(clStr);
         }
      }
      //-----------------------------------------------------------------------
      // -3-
      //-----------------------------------------------------------------------
   }
   UpdateData(FALSE);      // data -> dialog window

}

//
//  Function:   OnInitDialog
//  Purpose:    Init upon window entry
//  Parms:      
//  Returns:    
//
BOOL CHmxPageGeneric::OnInitDialog()
{
   LOGFONT  stLogFont;

   CPropertyPage::OnInitDialog();

   LoadDialog();
   CreateIndexSpin();
   //clGenEditIndex.SetFont(pclFontIdx);
   //
   // Handle fonts
   //
   memset(&stLogFont, 0, sizeof(stLogFont));
   strcpy(stLogFont.lfFaceName, "Arial");
   //
   // Send back the window handle for this dialog, so update messages arrive here
   //
   HWND hThisWnd = this->GetSafeHwnd();
   if(hDialogWnd)
   {
      ::PostMessage(hDialogWnd, WM_SOLVER_HANDLE, (WPARAM) hThisWnd, (LPARAM) HMX_WND_GEN);
   }
   //
   // Update the dialog fields first
   //
   UpdateDialog();
   return TRUE;  // return TRUE unless you set the focus to a control
                 // EXCEPTION: OCX Property Pages should return FALSE
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::OnOK()
{
   UpdateData(TRUE);

   StoreDialog();

   CPropertyPage::OnOK();
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::OnCancel()
{
   CPropertyPage::OnCancel();
}

//
//  Function:   OnBnSpinEsIndex
//  Purpose:    Spin button index up/dn
//  Parms:      
//  Returns:    
//
void CHmxPageGeneric::OnBnSpinEsIndex(NMHDR *pNMHDR, LRESULT *pResult)
{
   LPNMUPDOWN  pUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
   // 
   // Update dialog on spin buttons up/dn
   //
   switch(pUpDown->iDelta)
   {
      case 1:
         if(iIdxEs < iNumEs) iIdxEs++;
         break;

      case -1:
         if(iIdxEs > 1) iIdxEs--;
         break;

      default:
         break;
   }
   UpdateDialog();
   *pResult = 0;
}

//
//  Function:  CreateSpinIndex
//  Purpose:   Create the spin index for the ES selection
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageGeneric::CreateIndexSpin()
{
   clGenSpinButton.SetRange32(1, iNumEs);
   clGenSpinButton.SetPos(1);
}

//
//  Function:  RequestWndHandle
//  Purpose:   Called from the Main dialog window to save its window handle
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageGeneric::RequestWndHandle(HWND hWnd)
{
   //
   // Get the window handle and send it to the solver thread for communicating
   // Accept dropped files
   //
   hDialogWnd = hWnd;
}

//
//  Function:  OnRcvWindowData
//  Purpose:   Message from the SOLVER Thread: new data is in
//
//  Parms:     WPARAM = pstComm struct
//             TPARAM = 0
//  Returns:   0
//
LRESULT CHmxPageGeneric::OnRcvWindowData(WPARAM tWp, LPARAM tLp)
{
   pstHmxComm = (HMXCOM *) tWp;
   UpdateDialog();
   return(0);
}

/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/

