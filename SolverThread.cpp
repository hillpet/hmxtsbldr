/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:  SolverThread.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Aux thread
**
**  Entries:    SolverThread()
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"

//
// Local prototypes
//
static UINT    SolverWorkThread              (LPVOID);
//
static int     solver_CmdAddTimecodes        (HMXCOM *);
static int     solver_CmdBuildFiles          (HMXCOM *);
static int     solver_CmdBuildFilesInBatch   (HMXCOM *);
static int     solver_CmdCheckTs             (HMXCOM *);
static int     solver_CountBatchEntries      (HMXCOM *, CString*);
static int     solver_GetBatchEntry          (HMXCOM *, CString *, int, char *, int);
static int     solver_CountElementaryStreams (PIDLST *);
static int     solver_CmdParseTs             (HMXCOM *);
static int     solver_CmdRemoveTimecodes     (HMXCOM *);
static int     solver_InsertTimecodes        (HMXCOM *);
static int     solver_DeleteTimecodes        (HMXCOM *);
static HWND    solver_ScheduleMessage        (HMXCOM *, WMESG, HMXMSGID, int *);


//
//  Function:   Entrypoint for the Solver work Thread
//  Purpose:    Startup the Solver thread
//
//  Parms:      
//  Returns:    Not until program exit
//
UINT SolverWorkThread(LPVOID parameter)
{
   int      iThisSession = 0;
   HMXCMD   tCmd         = HMX_CMD_NONE;
   
   HMXCOM  *pstComm      = (HMXCOM *) parameter;
   CSolver *pclSolver    = pstComm->pclComm;

   for(;;)
   {
      switch(tCmd)
      {
         default:
         case HMX_CMD_NONE:
         Sleep(500);
            break;

         case HMX_CMD_PARSE:
            iThisSession = pclSolver->iSession;
            solver_CmdParseTs(pstComm);
            break;

         case HMX_CMD_CHECK:
            iThisSession = pclSolver->iSession;
            solver_CmdCheckTs(pstComm);
            break;

         case HMX_CMD_BUILD:
            iThisSession = pclSolver->iSession;
            solver_CmdBuildFiles(pstComm);
            break;

         case HMX_CMD_BUILD_BATCH:
            iThisSession = pclSolver->iSession;
            solver_CmdBuildFilesInBatch(pstComm);
            break;

         case HMX_CMD_TIMECODES:
            iThisSession = pclSolver->iSession;
            solver_CmdAddTimecodes(pstComm);
            break;

         case HMX_CMD_NO_TIMECODES:
            iThisSession = pclSolver->iSession;
            solver_CmdRemoveTimecodes(pstComm);
            break;

         case HMX_CMD_STOP:
            break;

         case HMX_CMD_EXIT:
            AfxEndThread(0);
            break;
      }
      // We have finished or we got interrupted
      // Got a new session started ?
      //
      if( iThisSession != pclSolver->iSession)
      {
         //
         // Start a new solver session
         //
         tCmd = pclSolver->tCmd;
      }
      else
      {
         tCmd = HMX_CMD_NONE;
      }
   }
   return(0);
}

//
//  Function:   Entrypoint for the Solver Communication Thread
//  Purpose:    Startup the Solver thread by the application's MainFrame :
//              AfxBeginThread(SolverThread, hWnd, THREAD_PRIORITY_NORMAL);
//
//  Parms:      Handle of the Application main dialog window. 
//              The Comm and Worker threads use this handle to send messages.
//
//  Returns:    Not until program exit
//
UINT SolverCommThread(LPVOID parameter)
{
   MSG          stMsg;
   HWND         hWnd;
   HMXCOM       stHmxComm;
   CWnd        *pclWnd;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CFile       *pclFile;


   //hWnd  = (HWND) parameter;
   //
   // Save the main dialog window handle, we need it to send messages to the host thread.
   // Get the client window, we need that to spin off the new solver thread.
   //
   pclWnd = CWnd::FromHandle((HWND) parameter);

   //
   // Both threads share the solver and NVM classes.
   //
   memset(&stHmxComm, 0, sizeof(HMXCOM));

   pclSolver = (CSolver *) new(CSolver);
   VERIFY(pclSolver);

   pclNvm = (CNvmStorage *) new(CNvmStorage);
   VERIFY(pclNvm);

   //
   // Get the file classes for SPTS, Index, Log and Catalog
   //
   pclFile = (CFile *) new(CFile);
   VERIFY(pclFile);
   stHmxComm.pclFileTs  = pclFile;
                        
   pclFile = (CFile *) new(CFile);
   VERIFY(pclFile);
   stHmxComm.pclFileNts = pclFile;

   pclFile = (CFile *) new(CFile);
   VERIFY(pclFile);
   stHmxComm.pclFileHmt = pclFile;

   pclFile = (CFile *) new(CFile);
   VERIFY(pclFile);
   stHmxComm.pclFileLog = pclFile;

   stHmxComm.pclComm = pclSolver;
   stHmxComm.pclNvm  = pclNvm;
   //
   // Store the Main dialog window handle
   //
   stHmxComm.hDialogWnd = (HWND) parameter;

   pclSolver->InitSolver();
   pclSolver->Create(NULL, "TS-Scanner", WS_CHILD | WS_VISIBLE, CRect(0, 0, 20, 20), pclWnd, 1234);
   //
   // Now we need to obtain a handle to this Solver window, and pass this back
   // to the dialog windows, so they are able to send us messages.
   //
   hWnd = pclSolver->GetSafeHwnd();
   stHmxComm.hSolverWnd = hWnd;
   if(hWnd)
   {
      //
      // Start the worker thread
      //
      AfxBeginThread(SolverWorkThread, &stHmxComm, THREAD_PRIORITY_NORMAL);
      SolverMessageToApp(&stHmxComm, WM_SOLVER_COMM, &stHmxComm, 0);
   }
   else
   {
      AfxMessageBox("Problems with TS-Scanner Apps handle !");
   }


   //=======================================================================
   // Sit here and await Solver Class Messages until we exit the program
   //
   while(!pclSolver->fDoExit && GetMessage(&stMsg, NULL, 0, 0) )
   {
      TranslateMessage(&stMsg);
      DispatchMessage(&stMsg);
   }
   //=======================================================================
   //
   // Free the Class memory
   //
   SolverMessageToApp(&stHmxComm, WM_SOLVER_EXIT, NULL, 0);

   return 0;
}

//
//  Function:   SolverMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//
//  Parms:      HmxComm, Message ID, pvParm, iParm
//  Returns:    TRUE if OKee
//
BOOL SolverMessageToApp(HMXCOM *pstComm, WMESG tMsg, LPVOID pvParm1, int iParm2)
{
   BOOL     fCc = FALSE; 
   HWND     hWnd;
   int      iIndex = 0;

   do
   {
      hWnd = solver_ScheduleMessage(pstComm, tMsg, HMX_MSGID_NONE, &iIndex);
      if(hWnd)
      {
         //
         // we have a destination for this message
         //
         ::PostMessage(hWnd, tMsg, (WPARAM) pvParm1, (LPARAM) iParm2);
         fCc = TRUE;
      }
   }
   while(iIndex != -1);
   return(fCc);
}

//
//  Function:   SolverNewMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//
//  Parms:      HmxComm, Message ID, Message text
//  Returns:    TRUE if OKee
//  NOTE:       Receiver has to release the message buffer memory
//
BOOL SolverNewMessageToApp(HMXCOM *pstComm, WMESG tMsg, char *pcMsg)
{
   CString s;
   BOOL     fCc = FALSE; 
   HWND     hWnd;
   int      iIndex = 0;

   do
   {
      hWnd = solver_ScheduleMessage(pstComm, tMsg, HMX_MSGID_NONE, &iIndex);
      if(hWnd)
      {
         //
         // we have a destination for this message
         //
         s  = CVB_GetTimeDateStamp();
         s += ':';
         s += pcMsg;

         char *pcParm = (char *)CVB_SafeMalloc(s.GetLength());
         if(pcParm)
         {
            //
            // Put out a message to the main thread to signal something.
            // Main thread needs to free the memory.
            //
            strncpy(pcParm, s.GetBuffer(0), s.GetLength());
            ::PostMessage(hWnd, tMsg, (WPARAM) pcParm, (LPARAM) 0);
            fCc = TRUE;
         }
      }
   }
   while(iIndex != -1);
   return(fCc);

}

//
//  Function:   SolverNewMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//              Add the time and date stamp plus the parameter to the message
//
//  Parms:      HmxComm, Message ID, Message text, message parms
//  Returns:    TRUE if OKee
//  NOTE:       Receiver has to release the message buffer memory
//
BOOL SolverNewMessageToApp(HMXCOM *pstComm, WMESG tMsg, char *pcMsg, int iParm1)
{
   CString  clStr1, clStr2;
   BOOL     fCc = FALSE; 
   HWND     hWnd;
   int      iIndex = 0;

   do
   {
      hWnd = solver_ScheduleMessage(pstComm, tMsg, HMX_MSGID_NONE, &iIndex);
      if(hWnd)
      {
         clStr1  = CVB_GetTimeDateStamp();
         clStr1 += ':';
         clStr1 += pcMsg;

         clStr2.Format(_T(" %d"), iParm1);

         clStr1 += clStr2;

         char *pcParm = (char *)CVB_SafeMalloc(clStr1.GetLength()+1);
         if(pcParm)
         {
            //
            // Put out a message to the main thread to signal something.
            // Main thread needs to free the memory.
            //
            strcpy(pcParm, clStr1.GetBuffer());
            ::PostMessage(hWnd, tMsg, (WPARAM) pcParm, (LPARAM) 0);
            fCc = TRUE;
         }
      }
   }
   while(iIndex != -1);
   return(fCc);
}

//
//  Function:  SolverSendUpdateToApp
//  Purpose:   Send msg WM_SOLVER_UPDATE to App
//
//  Parms:     HmxComm, Msg ID, Msg addendum, Nr of additional parms, parm1, parm2, parm3 
//  Returns:   TRUE if OKee
//  NOTE:      App must free the message body !
//
BOOL SolverSendUpdateToApp(HMXCOM *pstComm, HMXMSGID tMsgId, char *pcMsg, int iNr, int iData1, int iData2, int iData3)
{
   BOOL     fCc = FALSE; 
   DLGMSG  *pstMsg;
   HWND     hWnd;
   int      iIndex = 0;
   char    *pcMsgCopy;

   do
   {
      hWnd = solver_ScheduleMessage(pstComm, WM_SOLVER_UPDATE, tMsgId, &iIndex);
      if(hWnd)
      {
         //
         // we have a destination for this message
         //
         pstMsg    = (DLGMSG *) CVB_SafeMalloc(sizeof(DLGMSG));
         pcMsgCopy = (char *)   CVB_SafeMalloc(strlen(pcMsg)+1);
         //
         if(pstMsg)
         {
            strcpy(pcMsgCopy, pcMsg);
            //
            pstMsg->tMsgId = tMsgId;
            pstMsg->pcMsg  = pcMsgCopy;
            pstMsg->iData1 = iData1;
            pstMsg->iData2 = iData2;
            pstMsg->iData3 = iData3;
            //
            ::PostMessage(hWnd, WM_SOLVER_UPDATE, (WPARAM) pstMsg, (LPARAM) iNr);
            fCc = TRUE;
         }
      }
   }
   while(iIndex != -1);
   return(fCc);
}
//
//  Function:   SolverTextMessageToApp
//  Purpose:    Send a text message to the host application (MainFrame)
//              Do NOT add the time and date stamp to the message
//
//  Parms:      HMXCOM, Message ID, Message text
//  Returns:    TRUE if OKee
//  NOTE:       Receiver has to release both the message buffer memory
//
BOOL SolverTextMessageToApp(HMXCOM *pstComm, WMESG tMsg, char *pcMsg)
{
   char *pcParm = (char *) CVB_SafeMalloc(strlen(pcMsg)+1);
   BOOL     fCc = FALSE; 
   HWND     hWnd;
   int      iIndex = 0;

   do
   {
      hWnd = solver_ScheduleMessage(pstComm, tMsg, HMX_MSGID_NONE, &iIndex);
      if(hWnd)
      {
         if(pcParm)
         {
            //
            // Put out a message to the main thread to signal something.
            // Main thread needs to free the memory.
            //
            strcpy(pcParm, pcMsg);
            ::PostMessage(hWnd, tMsg, (WPARAM) pcParm, (LPARAM) 0);
            fCc = TRUE;
         }
      }
   }
   while(iIndex != -1);
   return(fCc);
}

/* ====== Functions separator ===========================================
void ____Cmd_functions____(){}
=========================================================================*/
//
//  Function:  solver_CmdAddTimecodes
//  Purpose:   Insert a (dummy) timecode into the selected SPTS
//             Note: Called upon selecting the <Add Timecodes> TS Property page dialog button
//                   We need to copy the TS file in order to insert timecodes
//  Parms:     HMXCOM struct
//  Returns:   Session or 0=error
//
static int solver_CmdAddTimecodes(HMXCOM *pstComm)
{
   int          iCurrentSession;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CString      clStr;

   if(!pstComm->fEventOk)
   {
      // No event has been selected
      AfxMessageBox("No valid SPTS has been selected !");
      return(0);
   }
   pclSolver       = pstComm->pclComm;
   pclNvm          = pstComm->pclNvm;
   iCurrentSession = pclSolver->iSession;
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Add timecodes : ", 1, iCurrentSession, 0, 0);
   //
   // Cleanup previous checks
   // Go Copy/Concatenate timecades and packets
   // Make the new situation permanent
   // Copy has finished
   //
   TS_Cleanup(pstComm);
   HMT_Cleanup(pstComm);
   NTS_Cleanup(pstComm);
   //
   HMX_Init(pstComm);
   solver_InsertTimecodes(pstComm);
   //
   solver_CmdCheckTs(pstComm);
   //
   HMX_LogEvent(pstComm, 0, "Add timecodes ready.", TRUE);
   //
   SolverMessageToApp(pstComm, WM_SOLVER_DATA, pstComm, 0);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(iCurrentSession);
}

//
//  Function:  solver_CmdBuildFilesInBatch
//  Purpose:   Take all collected info and build all meta files
//             Note: Called upon selecting the <BUILD> dialog button
//
//  Parms:     HMXCOM struct
//  Returns:   Session or 0=error
//
static int solver_CmdBuildFilesInBatch(HMXCOM *pstComm)
{
   int          iCurrentSession, iNr, iSize, iIdx;
   char        *pcDest;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CString      clStr;

   pclSolver       = pstComm->pclComm;
   pclNvm          = pstComm->pclNvm;
   iCurrentSession = pclSolver->iSession;
   //
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Building files in batch : ", 1, iCurrentSession, 0, 0);
   //
   // Get all pathnames from the list and feed them into the builder
   //
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_BATCHLIST, &clStr);
   iSize = clStr.GetLength();
   if(iSize)
   {
      pstComm->fEventOk = TRUE;
      pcDest = (char *) CVB_SafeMalloc(iSize);
      iNr    = solver_CountBatchEntries(pstComm, &clStr);
      for(iIdx=1; iIdx<=iNr; iIdx++)
      {
         solver_GetBatchEntry(pstComm, &clStr, iIdx, pcDest, iSize);
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, pcDest, (int)strlen(pcDest)+1);
         SolverMessageToApp(pstComm, WM_SOLVER_DATA, pstComm, 0);
         solver_CmdCheckTs(pstComm);
         solver_CmdBuildFiles(pstComm);
      }
   }
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);
   
   return(iCurrentSession);
}

//
//  Function:  solver_CmdBuildFiles
//  Purpose:   Take all collected info and build the meta files
//             Note: Called upon selecting the <BUILD> dialog button
//
//  Parms:     HMXCOM struct
//  Returns:   Session or 0=error
//
static int solver_CmdBuildFiles(HMXCOM *pstComm)
{
   int          iCurrentSession;
   u_int32      ulSize;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CString      clStr;

   if(!pstComm->fEventOk)
   {
      // No event has been selected
      AfxMessageBox("No valid SPTS has been selected !");
      return(0);
   }
   pclSolver       = pstComm->pclComm;
   pclNvm          = pstComm->pclNvm;
   iCurrentSession = pclSolver->iSession;
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Building files : ", 1, iCurrentSession, 0, 0);
   //
   HMX_Init(pstComm);
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStr);
   //
   HMX_OpenLogFile(pstComm, 'a');
   //
   HMX_LogEvent(pstComm, 0, "Building starting :", TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), TRUE);
   //
   // 1. Check if the files are all opened, and check the extensions : 
   //       *.ts 
   //       *.nts 
   //       *.hmt
   //
      HMX_OpenTsFile(pstComm, 'r');
   //
   // Rename TSEXT to *.ts if extension is different
   // Check which files to build
   //

   //
   // 2. Build *.nts
   //
   if(pstComm->iHmxCompat & HMX_COMPAT_NTS_EXIST)
   {
      if(pstComm->iHmxCompat & HMX_COMPAT_NTS_OVERWRITE)
      {
         // File exists, but may be overwritten
         NTS_OpenNtsFile(pstComm, 'c');
      }
   }
   else
   {
      // File does not yet exist
      NTS_OpenNtsFile(pstComm, 'c');
   }
   HMX_ParseTsFile(pstComm);
   //
   HMX_SecsToDateTime(&clStr, (u_int32) pstComm->iDurationStart);
   HMX_LogEvent(pstComm, 0, "Event start=", TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), FALSE);
   
   HMX_SecsToDateTime(&clStr, (u_int32) pstComm->iDurationEnd);
   HMX_LogEvent(pstComm, 0, "Event end  =", TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), FALSE);

   //
   // 3. Build *.hmt
   //
   if(pstComm->iHmxCompat & HMX_COMPAT_HMT_EXIST)
   {
      if(pstComm->iHmxCompat & HMX_COMPAT_HMT_OVERWRITE)
      {
         // File exists but can be overwritten
         HMT_OpenHmtFile(pstComm, 'c');
         ulSize = HMT_GenerateHmtFile(pstComm);
         HMT_WriteHmtFile(pstComm, (char *)pstComm->pubHmtCat, (int) ulSize);
      }
   }
   else
   {
      HMT_OpenHmtFile(pstComm, 'c');
      ulSize = HMT_GenerateHmtFile(pstComm);
      HMT_WriteHmtFile(pstComm, (char *)pstComm->pubHmtCat, (int) ulSize);
   }
   pstComm->iNumEs = solver_CountElementaryStreams(pstComm->pstPidList);
   TS_DumpStartcodes(pstComm);
   //
   //
   // Build has finished
   //
   HMX_LogEvent(pstComm, 0, "Building ready.", TRUE);
   HMX_CloseAllFiles(pstComm);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(iCurrentSession);
}

//
//  Function:  solver_CmdCheckTs
//  Purpose:   Check the meta file structure
//             Note: Called upon selecting the <BROWSE TS> dialog button
//  Parms:     HMXCOM struct
//  Returns:   
//
static int solver_CmdCheckTs(HMXCOM *pstComm)
{
   HMXST        tCc;
   CNvmStorage *pclNvm;
   CString      clStr;

   pclNvm = pstComm->pclNvm;

   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Checking meta files : ", 0, 0, 0, 0);

   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStr);
   //
   // Cleanup previous checks
   // If needed, close previously opened files
   //
   TS_Cleanup(pstComm);
   HMT_Cleanup(pstComm);
   NTS_Cleanup(pstComm);
   //
   HMX_Init(pstComm);
   //
   // 1. Get the SPTS pathname (Dialog Open or Drag&drop)
   //    Split the path into its components and store it for generic retrieval
   //
   tCc = HMX_SplitPathname(pclNvm);
   //
   // 1. Open all files applicable for the HMX meta file structure
   //
   HMX_OpenLogFile(pstComm, 'a');
   HMX_LogEvent(pstComm, 0, "Checking SPTS:", TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), TRUE);
   HMX_OpenMetaFiles(pstComm, 'r');
   //
   // 2. Parse the Catalog file
   //
   HMT_ParseHmtFile(pstComm);
   //
   // 3. Parse the Index file
   //
   NTS_ParseNtsFile(pstComm);
   //
   // For now: OKee
   //
   HMX_CloseAllFiles(pstComm);
   pstComm->fEventOk = TRUE;

   SolverMessageToApp(pstComm, WM_SOLVER_DATA, pstComm, 0);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(0);
}

//
//  Function:  solver_CmdParseTs
//  Purpose:   Init and start the TS scan
//             Note: Called upon selecting the <PARSE TS> dialog button
//
//  Parms:     HMXCOM struct
//  Returns:   Session or 0=error
//
static int solver_CmdParseTs(HMXCOM *pstComm)

{
   int          iCurrentSession;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CString      clStr;

   if(!pstComm->fEventOk)
   {
      // No event has been selected
      AfxMessageBox("No valid SPTS has been selected !");
      return(0);
   }
   pclSolver       = pstComm->pclComm;
   pclNvm          = pstComm->pclNvm;
   iCurrentSession = pclSolver->iSession;
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Parse SPTS : ", 1, iCurrentSession, 0, 0);
   //
   HMX_Init(pstComm);
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStr);
   //
   HMX_OpenLogFile(pstComm, 'a');
   HMX_OpenTsFile(pstComm, 'r');
   HMX_LogEvent(pstComm, 0, "Parse starting :", TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), TRUE);
   //
   // Start SPTS Scan here until ready or terminated
   // Parse the SPTS file
   //
   HMX_ParseTsFile(pstComm);
   HMT_GenerateHmtFile(pstComm);
   //
   pstComm->iNumEs = solver_CountElementaryStreams(pstComm->pstPidList);
   TS_DumpStartcodes(pstComm);
   //
   // Scan has finished
   //
   HMX_LogEvent(pstComm, 0, "Parser ready.", TRUE);
   HMX_CloseAllFiles(pstComm);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(iCurrentSession);
}

//
//  Function:  solver_CmdRemoveTimecodes
//  Purpose:   Delete the timecodes from the selected SPTS
//  Parms:     HMXCOM struct
//  Returns:   Session or 0=error
//
static int solver_CmdRemoveTimecodes(HMXCOM *pstComm)
{
   int          iCurrentSession;
   CSolver     *pclSolver;
   CNvmStorage *pclNvm;
   CString      clStr;

   if(!pstComm->fEventOk)
   {
      // No event has been selected
      AfxMessageBox("No valid SPTS has been selected !");
      return(0);
   }
   pclSolver       = pstComm->pclComm;
   pclNvm          = pstComm->pclNvm;
   iCurrentSession = pclSolver->iSession;
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Remove timecodes : ", 1, iCurrentSession, 0, 0);
   //
   // Cleanup previous checks
   // Go Copy/delete timecades and packets
   // Make the new situation permanent
   // Copy has finished
   //
   TS_Cleanup(pstComm);
   HMT_Cleanup(pstComm);
   NTS_Cleanup(pstComm);
   //
   HMX_Init(pstComm);
   solver_DeleteTimecodes(pstComm);
   //
   solver_CmdCheckTs(pstComm);
   //
   HMX_LogEvent(pstComm, 0, "Delete timecodes ready.", TRUE);
   //
   SolverMessageToApp(pstComm, WM_SOLVER_DATA, pstComm, 0);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(iCurrentSession);
}


/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/

//
// Message scheduler
//
const MSGSCH stMsgScheduler[] = 
{
//----------------------------------------------------------------------------------------
//    WMESG                   HMXMSGID                   HMXWND
//    WM_SOLVER_xxx           Message ID                 Destination window
//----------------------------------------------------------------------------------------
   {  WM_SOLVER_UPDATE,       HMX_MSGID_SCAN_STARTED,    HMX_WND_DLG             },
   {  WM_SOLVER_UPDATE,       HMX_MSGID_SCAN_PACKETS,    HMX_WND_DLG             },
   {  WM_SOLVER_UPDATE,       HMX_MSGID_SCAN_STOPPED,    HMX_WND_DLG             },
   {  WM_SOLVER_UPDATE,       HMX_MSGID_SCAN_ERROR,      HMX_WND_DLG             },
   //
   {  WM_SOLVER_PROGBAR,      HMX_MSGID_NONE,            HMX_WND_DLG             },
   {  WM_SOLVER_HANDLE,       HMX_MSGID_NONE,            HMX_WND_DLG             },
   {  WM_SOLVER_READY,        HMX_MSGID_NONE,            HMX_WND_DLG             },
   {  WM_SOLVER_COMM,         HMX_MSGID_NONE,            HMX_WND_DLG             },
   //
   {  WM_SOLVER_DATA,         HMX_MSGID_NONE,            HMX_WND_DLG             },
   {  WM_SOLVER_DATA,         HMX_MSGID_NONE,            HMX_WND_GEN             },
   {  WM_SOLVER_DATA,         HMX_MSGID_NONE,            HMX_WND_TS              },
   {  WM_SOLVER_DATA,         HMX_MSGID_NONE,            HMX_WND_NTS             },
   {  WM_SOLVER_DATA,         HMX_MSGID_NONE,            HMX_WND_HMT             },
   //
   {  WM_SOLVER_EXIT,         HMX_MSGID_NONE,            HMX_WND_DLG             },
   // END
   {  WM_SOLVER_NONE,         HMX_MSGID_NONE,            HMX_WND_TERM            }
};

//
//  Function:  solver_InsertTimecodes
//  Purpose:   Insert timecodes into the SPTS
//
//  Parms:     Comm structure
//  Returns:   0 = error
//
static int solver_InsertTimecodes(HMXCOM *pstComm)
{
   int         iIdx=1, iCompat;
   CString     clOldPath, clNewPath;
   CFileStatus clFstat;
   CNvmStorage *pclNvm;

   pclNvm = pstComm->pclNvm;

   iCompat = HMX_CheckCompatibility(pstComm);
   if( !(iCompat & HMX_COMPAT_COMPLIANT) )
   {
      AfxMessageBox("SPTS Filename format is incompatible !\nUse the <Create Compatibility> option first.");
      return(0);
   }
   //
   // Procedure:
   // 
   // 1. Rename the selected SPTS file : *.ts  --> *.ts1 or higher
   // 2. Save the new EXT
   // 3. Create the original SPTS name again as destination
   // 4. Copy concatenate the timecode + packets to the new file
   // 5  Split the new file back up into the components
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME,   &clOldPath);
   //
   // Rename the existing SPTS file : *.ts01 or higher number if exists
   //
   do
   {
      clNewPath.Format(_T("%s%02d"), clOldPath.GetBuffer(), iIdx++);
      //
      if(!CFile::GetStatus(clNewPath.GetBuffer(), clFstat))
      {
         // File does not yet exists: rename existing SPTS to that name
         pstComm->pclFileTs->Rename(clOldPath.GetBuffer(), clNewPath.GetBuffer());
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clNewPath);
         HMX_SplitPathname(pclNvm);
         //
         HMX_CopyWithTimecode(pstComm, clOldPath.GetBuffer(), 188);
         //
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clOldPath);
         HMX_SplitPathname(pclNvm);
         //
         pstComm->iPacketSize = 192;
         iIdx=0;
      }
   }
   while(iIdx>0);
   return(1);
}

//
//  Function:  solver_DeleteTimecodes
//  Purpose:   Delete timecodes from the SPTS
//
//  Parms:     Comm structure
//  Returns:   0 = error
//
static int solver_DeleteTimecodes(HMXCOM *pstComm)
{
   int         iIdx=1, iCompat;
   CString     clOldPath, clNewPath;
   CFileStatus clFstat;
   CNvmStorage *pclNvm;

   pclNvm = pstComm->pclNvm;

   iCompat = HMX_CheckCompatibility(pstComm);
   if( !(iCompat & HMX_COMPAT_COMPLIANT) )
   {
      AfxMessageBox("SPTS Filename format is incompatible !\nUse the <Create Compatibility> option first.");
      return(0);
   }
   //
   // Procedure:
   // 
   // 1. Rename the selected SPTS file : *.ts  --> *.ts1 or higher
   // 2. Save the new EXT
   // 3. Create the original SPTS name again as destination
   // 4. Copy packets without the timecode to the new file
   // 5  Split the new file back up into the components
   //
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clOldPath);
   //
   // Rename the existing SPTS file : *.ts01 or higher number if exists
   //
   do
   {
      clNewPath.Format(_T("%s%02d"), clOldPath.GetBuffer(), iIdx++);
      //
      if(!CFile::GetStatus(clNewPath.GetBuffer(), clFstat))
      {
         // File does not yet exists: rename existing SPTS to that name
         pstComm->pclFileTs->Rename(clOldPath.GetBuffer(), clNewPath.GetBuffer());
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clNewPath);
         HMX_SplitPathname(pclNvm);
         //
         HMX_CopyWithoutTimecode(pstComm, clOldPath.GetBuffer(), 192);
         //
         HMX_PutIntoStorage(pclNvm, NVM_HMX_PATHNAME, &clOldPath);
         HMX_SplitPathname(pclNvm);
         //
         pstComm->iPacketSize = 188;
         iIdx=0;
      }
   }
   while(iIdx>0);
   return(1);
}
//
//  Function:  solver_CountElementaryStreams
//  Purpose:   Count the total number of different PES types in the SPTS
//
//  Parms:     
//  Returns:   Number
//  PilList:      iIndex
//                iPid
//                dwTsPackets
//                dwPesPackets
//                pcEsName[HMX_NAME_LEN]
//               *pstNext
//      
static int solver_CountElementaryStreams(PIDLST *pstPidList)
{
   int   iTotal = 0;
   int   iStart;

   if(pstPidList)
   {
      iStart = pstPidList->iIndex;
      do
      {
         iTotal++;
         pstPidList = pstPidList->pstNext;
      }
      while(pstPidList->iIndex != iStart);
   }
   return(iTotal);
}

//
//  Function:  solver_ScheduleMessage
//  Purpose:   Reschedule the incoming message to the correct destination window
//
//  Parms:     pstComm, WMESG, HMXMSGID tMsgId, Index
//  Returns:   Dest window handle
//  Updates:   Index. Will be -1 on end of list.
//
static HWND solver_ScheduleMessage(HMXCOM *pstComm, WMESG tWmsg, HMXMSGID tMsgId, int *piIndex)
{
   int      iIndex = *piIndex;
   BOOL     fFoundOne = FALSE;
   HMXWND   tDest;   
   HWND     hWnd = 0;

   while( !fFoundOne)
   {
      if( (stMsgScheduler[iIndex].tWmsg == tWmsg) && (stMsgScheduler[iIndex].tMsgId == tMsgId) )
      {
         //
         // This message needs to go out: get the right window handle to send the message to
         //
         tDest = stMsgScheduler[iIndex].tDest;
         switch(tDest)
         {
            //
            // Get the window handle from the comm structure
            //
            default:
            case HMX_WND_TERM:
               iIndex = -1;
               break;

            case HMX_WND_DLG:
               // Main dialog window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hDialogWnd;
               break;

            case HMX_WND_SLV:
               // Solver Comm dialog   window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hSolverWnd;
               break;

            case HMX_WND_GEN:
               // PropertyPage-Generic window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hPageGenWnd;
               break;

            case HMX_WND_TS:
               // PropertyPage-TS      window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hPageTstWnd;
               break;

            case HMX_WND_NTS:
               // PropertyPage-NTS     window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hPageNtsWnd;
               break;

            case HMX_WND_HMT:
               // PropertyPage-HMT     window handle
               iIndex++;
               fFoundOne = TRUE;
               hWnd      = pstComm->hPageHmtWnd;
               break;
         }
      }
      else
      {
         if( stMsgScheduler[iIndex].tDest == HMX_WND_TERM)
         {
            fFoundOne = TRUE;
            iIndex    = -1;
         }
         else  
         {
            iIndex++;
         }
      }
   }
   *piIndex = iIndex;
   return(hWnd);
}


//
//  Function:  solver_CountBatchEntries
//  Purpose:   Count the number of pathnames in the comma separated buffer
//
//  Parms:     pstComm, CString *
//  Returns:   Number of pathnames
//
static int solver_CountBatchEntries(HMXCOM *pstComm, CString *pclStr)
{
   char *pcBuffer;
   int iSize;
   int iNr=0;

   pcBuffer = pclStr->GetBuffer();
   iSize    = pclStr->GetLength()+1;
   
   while(iSize--)
   {
      if( (*pcBuffer == ',') || (*pcBuffer == 0) ) iNr++;
      pcBuffer++;
   }
   return(iNr);
}

//
//  Function:  solver_GetBatchEntry
//  Purpose:   Get one string from the comma separated buffer
//
//  Parms:     pstComm, 
//  Returns:   Size
//
static int solver_GetBatchEntry(HMXCOM *pstComm, CString *pclStr, int iIdx, char *pcDest, int iLength)
{
   char *pcBuffer;
   int   iSize = 0, iNr = 1;

   pcBuffer = pclStr->GetBuffer();
   iSize    = pclStr->GetLength();
   
   while(iSize)
   {
      if(iNr == iIdx) break;
      if(*pcBuffer++ == ',') iNr++;
      iSize--;
   }
   if(iSize)
   {
      iSize = 0;
      while(iLength--)
      {
         if( (*pcBuffer == ',') || (*pcBuffer == 0) ) 
         {
            *pcDest = 0;
            break;
         }
         *pcDest++ = *pcBuffer++;
         iSize++;
      }
   }
   return(iSize);
}
