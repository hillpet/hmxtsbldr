/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxTsBldr.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Defines the class behaviors for the application.
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#pragma once

#ifndef __AFXWIN_H__
   #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"      // main symbols


//
// Command line options
//
class CMyCommandLineInfo : public CCommandLineInfo
{
// Attributes
public:
   BOOL     fCmdLineB;           // /b       Batch mode
   BOOL     fCmdLineH;           // /h       Help text
   BOOL     fCmdLineO;           // /o       Overwrite mode
   BOOL     fCmdLineR;           // /r       Autoresync mode
   BOOL     fCmdLineP;           // /p=xxx   Dir name
   CString  clStrCmdLineP;       //          Directory
   //
   BOOL     fSuccess;            // all switches ok
   //
   CString  clStrCmdLineAll;     // Commandline if no parms

// Construction
public:
   CMyCommandLineInfo(void);
   //
   BOOL     IsHelpMode()      {  return(fCmdLineH);      };
   BOOL     IsBatchMode()     {  return(fCmdLineB);      };
   BOOL     IsOverwriteMode() {  return(fCmdLineO);      };
   BOOL     IsAutoResyncMode(){  return(fCmdLineR);      };
   BOOL     HasDefaultDir()   {  return(fCmdLineP);      };
   CString *GetDefaultDir()   {  return(&clStrCmdLineP); };

// Overrides
public:
   virtual void ParseParam( LPCTSTR, BOOL, BOOL);
};

//
// CMy5050CApp:
// See 5050C.cpp for the implementation of this class
//

class CMyHmxTsApp : public CWinApp
{
public:
   CMyHmxTsApp();

   CMyCommandLineInfo clCmdInfo;
   CNvmStorage       *pclNvm;

// Overrides
public:
   virtual BOOL InitInstance();

// Implementation

   DECLARE_MESSAGE_MAP()
};

extern CMyHmxTsApp theApp;