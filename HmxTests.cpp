/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxTests.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Contains test 1 routine
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       20Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "ClockApi.h"
#include "HmxTests.h"


//
// Prototypes
//
static u_int32 hmx_DumpTimestamps      (HMXCOM *, CString *, u_int8 *, u_int32);
static int     hmx_MergeNtsField       (HMXCOM *, int);
static BOOL    hmx_ReadNtsRecord       (HMXCOM *, CFile *, u_int8 *);
static BOOL    hmx_WriteNtsRecord      (HMXCOM *, CFile *, u_int8 *);
static int     hmx_CheckNtsFields      (HMXCOM *, u_int8 *, u_int8 *);
static int     hmx_CompareHmts         (HMXCOM *, int);

static BOOL    hmx_HmtCompareGenericPid(HMXCOM *, HMTEST *, u_int8 *, u_int8 *, char *);
static BOOL    hmx_HmtCompareGeneric   (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
//
static BOOL    hmx_HmtCompareStart     (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareHeader    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareStartTime (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareEndTime   (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePathname  (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareService   (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareChName    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareFlags1    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareFlags2    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareSid       (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidXx1    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidV      (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidA      (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidPcr    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidXx2    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePidXx3    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareNrXx1     (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareNrXx2     (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareNrXx3     (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtComparePlayback  (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareNrEvents  (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);
static BOOL    hmx_HmtCompareEvents    (HMXCOM *, HMTEST *, u_int8 *, u_int8 *);

const HMTEST stHmtTestVectors[] = 
{
   {  0x0000,   4,   hmx_HmtCompareStart              },
   {  0x0004,  21,   hmx_HmtCompareHeader             },
   {  0x0019,   4,   hmx_HmtCompareStartTime          },
   {  0x001D,   4,   hmx_HmtCompareEndTime            },
   {  0x0021, 128,   hmx_HmtComparePathname           },
   {  0x0120, 128,   hmx_HmtCompareService            },
   {  0x0220,  32,   hmx_HmtCompareChName             },
   {  0x0240,   4,   hmx_HmtCompareFlags1             },
   {  0x02e9,  10,   hmx_HmtCompareFlags2             },
   {  0x02f3,   2,   hmx_HmtCompareSid                },
   {  0x02f5,   2,   hmx_HmtComparePidXx1             },
   {  0x02f7,   2,   hmx_HmtComparePidV               },
   {  0x02f9,   2,   hmx_HmtComparePidA               },
   {  0x02fb,   2,   hmx_HmtComparePidPcr             },
   {  0x02fd,   2,   hmx_HmtComparePidXx2             },
   {  0x02ff,   2,   hmx_HmtComparePidXx3             },
   {  0x0301,   2,   hmx_HmtCompareNrXx1              },
   {  0x0303,   2,   hmx_HmtCompareNrXx2              },
   {  0x0305,   4,   hmx_HmtCompareNrXx3              },
   {  0x0309,   8,   hmx_HmtComparePlayback           },
   {  0x1000,   4,   hmx_HmtCompareNrEvents           },
   {  0x1004,   0,   hmx_HmtCompareEvents             },
   {  -1,       0,   NULL                             }
};

/* ====== Functions separator ===========================================
void ____Test_functions____(){}
=========================================================================*/

//
//  Function:  HMX_RunTest
//  Purpose:   Run one of the tests
//
//  Parms:     Comm structure, test nr
//  Returns:   0 = error
//
int HMX_RunTest(HMXCOM *pstComm, int iTest, int iParm)
{
   int   iCc = 0;

   if(pstComm)
   {
      switch(iTest)
      {
         default:
         case 0:
            break;

         case 1:
            iCc = HMX_DoTest1(pstComm, iParm);
            break;

         case 2:
            iCc = HMX_DoTest2(pstComm, iParm);
            break;

         case 3:
            iCc = HMX_DoTest3(pstComm, iParm);
            break;

         case 4:
            iCc = HMX_DoTest4(pstComm, iParm);
            break;

         case 5:
            iCc = HMX_DoTest5(pstComm, iParm);
            break;

      }
   }
   return(iCc);
}

//
//  Function:  HMX_DoTest1
//  Purpose:   Run test 1
//
//  Parms:     Comm structure, parameter
//  Returns:   0 = error
//
int HMX_DoTest1(HMXCOM *pstComm, int iParm)
{
   CNvmStorage *pclNvm;
   CString      clPathname, clStr;
   u_int8      *pubHmtRaw;     

   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Running Test 1:Dump timestamps", 1, iParm, 0, 0);

   stDateTime.ubDay    = 10;
   stDateTime.ubMonth  =  4;
   stDateTime.usYear   = 2009-1900;
   stDateTime.ubHour   = 11;
   stDateTime.ubMin    = 12;
   stDateTime.ubSec    = 0;
   CLOCK_UtilDateTimeToCount(&stDateTime, &stCount);
   clStr.Format(_T("   20090410_1112  timestamp = %08X"), stCount.ulSeconds);

   //
   // Cleanup previous checks
   //
   HMX_Init(pstComm);
   //
   // Get the SPTS pathname (Dialog Open or Drag&drop)
   // Split the path into its components and store it for generic retrieval
   //
   pclNvm = pstComm->pclNvm;
   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clPathname);
   HMX_SplitPathname(pclNvm);
   HMX_OpenLogFile(pstComm, 'a');
   HMX_LogEvent(pstComm, 0, "Test 1: Dump HMT header for time stamp detection.", TRUE);
   HMX_LogEvent(pstComm, 0, clPathname.GetBuffer(), TRUE);
   HMX_LogEvent(pstComm, 0, clStr.GetBuffer(), TRUE);
   //
   HMT_OpenHmtFile(pstComm, 'r');
   if( !pstComm->fFileHmt )
   {
      // no HMT file could be opened : we are not HMX compliant
      HMX_LogEvent(pstComm, 0, "   HMT file not found !", TRUE);
   }
   else
   {
      //
      // Read the HMT Catalog file
      //
      HMT_ParseHmtFile(pstComm);
      pubHmtRaw = pstComm->pubHmtRaw;
      if(pubHmtRaw)
      {
         //
         // HMT file is read into pubHmt. Do the tests
         //
         HMX_LogEvent(pstComm, 0, "   Time stamp ", TRUE);
         hmx_DumpTimestamps(pstComm, &clStr, pubHmtRaw, HMT_TIME_START);
         HMX_LogEventText(pstComm, 0, "Start:", clStr.GetBuffer(), TRUE);
         hmx_DumpTimestamps(pstComm, &clStr, pubHmtRaw, HMT_TIME_END);
         HMX_LogEventText(pstComm, 0, "End:", clStr.GetBuffer(), TRUE);
      }
   }
   HMT_CloseHmtFile(pstComm);
   HMX_CloseLogFile(pstComm);
   //
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);
   return(1);
}

//
//  Function:  HMX_DoTest2
//  Purpose:   Run test 2: dump comma separated NTS fields
//
//  Parms:     Comm structure, parameter
//  Returns:   0 = error
//
int HMX_DoTest2(HMXCOM *pstComm, int iParm)
{
   int         iNtsRecord=0, iProfile;
   CString     clStr;

   u_int8      pubData[NTS_IDX_SIZE];
   u_int8      ubFrm;
   u_int32     ulPts, ulTs;

   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Running Test 2:Dump comma separated NTS records", 1, iParm, 0, 0);
   HMX_LogEvent(pstComm, 0, "TEST: Number 2 started.", TRUE);
   //
   // Check SD/HD
   //
   HMX_GetFromStorage(pstComm->pclNvm, NVM_NTS_AUTOHD, &iProfile);
   HMX_OpenLogFile(pstComm, 'a');
   NTS_OpenNtsFile(pstComm, 'r');

   clStr.Format(_T(",Idx, PTS(Hex), PTS(Msecs), Time(Hex), Time(Msec)"));
   HMX_LogEventText(pstComm, 0, " ",  clStr.GetBuffer(), TRUE);

   while( NTS_ReadNtsRecord(pstComm, pubData, iNtsRecord) )
   {
      ubFrm = CVB_ubyte_get( &pubData[0],  0xff,        0);
      ulPts = CVB_ulong_get( &pubData[16], 0xffffffffL, 0);
      ulTs  = CVB_ulong_get( &pubData[24], 0xffffffffL, 0);

      clStr.Format(_T(",%d, %x, %d, %x, %d"), ubFrm, ulPts, ulPts/45, ulTs, ulTs);
      HMX_LogEventText(pstComm, 0, " ",  clStr.GetBuffer(), TRUE);
      //
      if(iProfile == NTS_DEF_HD) iNtsRecord += 2;
      else                       iNtsRecord += 1;
   }
   NTS_CloseNtsFile(pstComm);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);
   HMX_CloseLogFile(pstComm);

   return(1);
}

//
//  Function:  HMX_DoTest3
//  Purpose:   Run test 3
//             Put out <FrameType> <Delta Bytes_1_2> <Delta PTS> relative to last FrameType 1
//  Parms:     Comm structure, parameter
//  Returns:   0 = error
//
int HMX_DoTest3(HMXCOM *pstComm, int iParm)
{
   int         iNtsRecord=0;
   CString     clStr;

   u_int8      pubData[NTS_IDX_SIZE];
   u_int8      ubFrm;
   u_int16     usXxx, usXxI=0;
   u_int32     ulPts, ulPIa=0, ulPIb=0;

   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Running Test 3:Dump PTS-delta's per frame", 1, iParm, 0, 0);
   HMX_LogEvent(pstComm, 0, "TEST: Number 3 started.", TRUE);
   HMX_OpenLogFile(pstComm, 'a');
   NTS_OpenNtsFile(pstComm, 'r');

   while( NTS_ReadNtsRecord(pstComm, pubData, iNtsRecord) )
   {
      ubFrm = CVB_ubyte_get( &pubData[0],  0xff,        0);
      usXxx = CVB_ushort_get(&pubData[1],  0xffff,      0);
      ulPts = CVB_ulong_get( &pubData[12], 0xffffffffL, 0)/45; //90kHz already >> 1

      switch((FRTYPE)ubFrm)
      {
         case FRAME_TYPE_I:
            usXxI = usXxx;
            ulPIb = ulPIa;
            ulPIa = ulPts;
            HMX_LogEventData(pstComm, 0, "Fr=", ubFrm, TRUE);
            break;

         default:
         case FRAME_TYPE_D:
            break;

         case FRAME_TYPE_P:
         case FRAME_TYPE_B:
            HMX_LogEventData(pstComm, 0, "   ", ubFrm, TRUE);
            if(ulPts >ulPIb) clStr.Format(_T("X=%4d  DTS=%5d"), usXxx-usXxI, ulPts-ulPIb);
            else             clStr.Format(_T("X=%4d  DTS=-----"), usXxx-usXxI);
            HMX_LogEventText(pstComm, 0, "  ",  clStr.GetBuffer(), FALSE);
            break;
      }
      iNtsRecord++;
   }
   NTS_CloseNtsFile(pstComm);
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);
   HMX_CloseLogFile(pstComm);

   return(1);
}

//
//  Function:  HMX_DoTest4
//  Purpose:   Run test 4
//             Merge one original NTS field with one existing NTS field
//  Parms:     Comm structure, parameter
//  Returns:   0 = error
//
int HMX_DoTest4(HMXCOM *pstComm, int iParm)
{
   int   iCc;
   
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Running Test 4:Check/Merge NTS records/fields", 1, iParm, 0, 0);
   
   HMX_LogEvent(pstComm, 0, "TEST: Number 4 (check/merge NTS field) started.", TRUE);
   //
   switch(pstComm->tContentType)
   {
      case NTS_DEF_AUTO:
         iCc = AfxMessageBox("Content type is still unknown ! Assume SDTV ?", MB_YESNOCANCEL|MB_ICONQUESTION);
         //
         switch(iCc)
         {
            case IDCANCEL:
               // Abort here !
               return(0);

            case IDYES:
               pstComm->tContentType = NTS_DEF_AUTO_SD;
               break;

            default:
            case IDNO:
               pstComm->tContentType = NTS_DEF_AUTO_HD;
               break;
         }
         break;
         
      case NTS_DEF_SD:
      case NTS_DEF_AUTO_SD:
      case NTS_DEF_HD:
      case NTS_DEF_AUTO_HD:
         break;
   }
   
   hmx_MergeNtsField(pstComm, iParm);
   
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(1);
}

//
//  Function:  HMX_DoTest5
//  Purpose:   Run test 5
//             Verify delta's between 2 HMT files
//  Parms:     Comm structure, parameter
//  Returns:   0 = error
//
int HMX_DoTest5(HMXCOM *pstComm, int iParm)
{   
   SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_STARTED, "Running Test 5:Compare HMT's", 1, iParm, 0, 0);
   
   HMX_LogEvent(pstComm, 0, "TEST: Number 5 (Compare HMT's) started.", TRUE);
   //
   hmx_CompareHmts(pstComm, iParm);
   
   SolverMessageToApp(pstComm, WM_SOLVER_READY, NULL, 0);

   return(1);
}


/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/

//
//  Function:  hmx_DumpTimestamps
//  Purpose:   Retrieve time stamp from a byte stream
//
//  Parms:     Comm, CString^, buffer, offset
//  Returns:   u_int32
//
static u_int32 hmx_DumpTimestamps(HMXCOM *pstComm, CString *pclStr, u_int8 *pubData, u_int32 ulOffset)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = CVB_ulong_get(&pubData[ulOffset], 0xffffffffL, 0);
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   pclStr->Format(_T("%02d-%02d-%04d  %02d:%02d:%02d"), 
                           stDateTime.ubDay,
                           stDateTime.ubMonth,
                           stDateTime.usYear+1900,
                           stDateTime.ubHour,
                           stDateTime.ubMin,
                           stDateTime.ubSec);
   return(0);
}

//
//  Function:  hmx_MergeNtsField
//  Purpose:   Merge one original NTS field with one existing NTS field
//
//  Parms:     Comm structure, field # (0 = check all fields)
//  Returns:   0 = error
//
static int hmx_MergeNtsField(HMXCOM *pstComm, int iField)
{
   int         iCc, iErrors=0;
   int         iIdx=1;
   BOOL        fInSync;
   //
   CString     clStrNew, clStrOri, clStrGen;
   CString     clDir, clEvt, clDate, clTime;
   CFile       clFileNew, clFileGen, clFileOri;
   CFileStatus clFstat;
   //
   u_int8      pubOriData[NTS_IDX_SIZE*2];
   u_int8      pubGenData[NTS_IDX_SIZE*2];
   //
   u_int64     llLastFpOri = 0;
   u_int64     llLastFpGen = 0;
   //
   u_int32     ulData;
   u_int16     usData;
   u_int8      ubData;

   //
   // Open LOG file
   // Open NTS original    (O)
   // Open NTS generated   (G)
   // Open NTS new         (N)
   //
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DIRNAME,    &clDir);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_EVENTNAME,  &clEvt);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DATE,       &clDate);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_TIME,       &clTime);
   
   clStrOri.Format(_T("%s\\%s_%s_%s.nts.ori"),   clDir.GetBuffer(), clEvt.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer() );
   clStrNew.Format(_T("%s\\%s_%s_%s.nts"),       clDir.GetBuffer(), clEvt.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer() );
   clStrGen.Format(_T("%s\\%s_%s_%s.nts"),       clDir.GetBuffer(), clEvt.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer() );
   
   //
   // Rename the existing NTS file : *.nts.001 or higher number if exists
   //
   if(iField)
   {
      do
      {
         clStrGen.Format(_T("%s\\%s_%s_%s.nts.%03d"), clDir.GetBuffer(), clEvt.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer(), iIdx++);
         //
         if(!CFile::GetStatus(clStrGen.GetBuffer(), clFstat))
         {
            // File does not yet exists: rename existing generated NTS to that name
            clFileGen.Rename(clStrNew.GetBuffer(), clStrGen.GetBuffer());
            iIdx=0;
         }
      }
      while(iIdx);
      //
      iCc = clFileNew.Open(clStrNew.GetBuffer(), CFile::modeReadWrite | CFile::modeCreate, NULL);
      if(iCc==0)
      {
         // Problems
         HMX_LogEventText(pstComm, -1, "TEST:Problems creating ",  clStrNew.GetBuffer(), TRUE);
         return(0);
      }
   }
   //
   // Open all files:   ori: *.nts.ori : original HUMAX NTS file as template
   //                   gen: *.nts.001 : generated NTS file to copy
   //                   new: *.nts     : new file as result of the merge  
   //
   HMX_OpenLogFile(pstComm, 'a');
   //
   iCc = clFileOri.Open(clStrOri.GetBuffer(), CFile::modeRead, NULL);
   if(iCc==0)
   {
      // Problems
      HMX_LogEventText(pstComm, -1, "TEST:Problems opening ",  clStrOri.GetBuffer(), TRUE);
      HMX_CloseLogFile(pstComm);
      return(0);
   }
   iCc = clFileGen.Open(clStrGen.GetBuffer(), CFile::modeRead, NULL);
   if(iCc==0)
   {
      // Problems
      HMX_LogEventText(pstComm, -1, "TEST:Problems opening ",  clStrGen.GetBuffer(), TRUE);
      HMX_CloseLogFile(pstComm);
      return(0);
   }
   //
   // While(ReadRecord(O)
   //    Read(G)
   //    Sync(O.llFp, G.llFp)
   //    Merge(N.field, O.field)
   //    Write(N.Record
   //
   HMX_LogEventText(pstComm, 0, "TEST:Starting check on ",  clStrNew.GetBuffer(), TRUE);
   //
   while( hmx_ReadNtsRecord(pstComm, &clFileGen, pubGenData) )
   {
      //
      // Keep both index file records in sync using the Filepointers
      //
      llLastFpGen = CVB_llong_get(&pubGenData[8], 0xffffffffffffffff, 0);
      while(llLastFpGen > llLastFpOri)
      {
         //
         // Keep skipping records until we are in sync with the generated NTS record
         //
         if( hmx_ReadNtsRecord(pstComm, &clFileOri, pubOriData) )
         {
            llLastFpOri = CVB_llong_get(&pubOriData[8], 0xffffffffffffffff, 0);
         }
         else
         {
            HMX_LogEventText(pstComm, -1, "TEST:No more records in ",  clStrOri.GetBuffer(), TRUE);
            break;
         }
      }
      fInSync = (llLastFpGen == llLastFpOri);
      
      if(fInSync)
      {
         switch(iField)
         {
            case 0:
               //
               // Check all fields
               //
               iErrors += hmx_CheckNtsFields(pstComm, pubOriData, pubGenData);
               break;
               
            case 1:
               // Field-1a
               ubData = CVB_ubyte_get(&pubOriData[0], 0xFF, 0);
               CVB_ubyte_put(&pubGenData[0], ubData);
               break;
               
            case 2:
               // Field-1b
               ubData = CVB_ubyte_get(&pubGenData[0], 0xFF,       0);
               ulData = CVB_ulong_get(&pubOriData[0], 0x00FFFFFF, 8);
               ulData |= (u_int32)(ubData << 24);
               CVB_ulong_put(&pubGenData[0], ulData);
               break;
               
            case 3:
               // Field-2a
               usData = CVB_ushort_get(&pubOriData[4], 0xFFFF, 0);
               CVB_ushort_put(&pubGenData[4], usData);
               break;
               
            case 4:
               // Field-2b
               ubData = CVB_ubyte_get(&pubOriData[6], 0xFF, 0);
               CVB_ubyte_put(&pubGenData[6], ubData);
               break;
               
            case 5:
               // Field-2c
               ubData = CVB_ubyte_get(&pubOriData[7], 0xFF, 0);
               CVB_ubyte_put(&pubGenData[7], ubData);
               break;
               
            case 6:
               // FP is sync'ed
               break;
               
            case 7:
               // Field-5
               ulData = CVB_ulong_get(&pubOriData[16], 0xFFFFFFFF, 0);
               CVB_ulong_put(&pubGenData[16], ulData);
               break;
               
            case 8:
               // Field-6
               ulData = CVB_ulong_get(&pubOriData[20], 0xFFFFFFFF, 0);
               CVB_ulong_put(&pubGenData[20], ulData);
               break;
               
            case 9:
               // Field-7
               ulData = CVB_ulong_get(&pubOriData[24], 0xFFFFFFFF, 0);
               CVB_ulong_put(&pubGenData[24], ulData);
               break;
               
            case 10:
               // Field-8: Always 0
               break;
               
            case 11:
               // Field-9
               ulData = CVB_ulong_get(&pubOriData[32], 0xFFFFFFFF, 0);
               CVB_ulong_put(&pubGenData[32], ulData);
               break;
               
            case 12:
               // Field-10
               ulData = CVB_ulong_get(&pubOriData[36], 0xFFFFFFFF, 0);
               CVB_ulong_put(&pubGenData[36], ulData);
               break;
         }
         //
         // Write the merged data to the new NTS file
         //
         if(iField) hmx_WriteNtsRecord(pstComm, &clFileNew, pubGenData);
      }
      else
      {
         HMX_LogEventLongHexData(pstComm, 0, "TEST:FPs Out of sync: Gen=",  llLastFpGen, TRUE);
         HMX_LogEventLongHexData(pstComm, 0, " Ori=",  llLastFpOri, FALSE);
      }
   }
   clFileOri.Close();
   clFileGen.Close();
   //
   if(iField)  clFileNew.Close();
   else        HMX_LogEventData(pstComm, 0, "TEST:End check errors=",  iErrors, TRUE);
   //
   HMX_CloseLogFile(pstComm);

   return(1);
}


//
//  Function:  hmx_ReadNtsRecord
//  Purpose:   Read one nts record (depending SD/HD)
//
//  Parms:     Comm structure, File^, Buffer^
//  Returns:   TRUE = OKee
//
static BOOL hmx_ReadNtsRecord(HMXCOM *pstComm, CFile *pclFile, u_int8 *pubData)
{
   int   iSize;

   //
   // Get SD/HD
   //
   //pwjh int iProfile
   //pwjh HMX_GetFromStorage(pstComm->pclNvm, NVM_NTS_AUTOHD, &iProfile);
   //pwjh switch(iProfile)
   //
   memset(pubData, 0, 2*NTS_IDX_SIZE);
   //
   if( HMX_CheckHdtv(pstComm) ) iSize = NTS_IDX_SIZE*2;
   else                         iSize = NTS_IDX_SIZE; 
   //pwjh 
   //pwjh switch(pstComm->tContentType)
   //pwjh {
   //pwjh    case NTS_DEF_AUTO:
   //pwjh    case NTS_DEF_SD:
   //pwjh    case NTS_DEF_AUTO_SD:
   //pwjh       iSize = NTS_IDX_SIZE;
   //pwjh       break;
   //pwjh 
   //pwjh    case NTS_DEF_HD:
   //pwjh    case NTS_DEF_AUTO_HD:
   //pwjh       iSize = NTS_IDX_SIZE*2;
   //pwjh       break;
   //pwjh }
   return( (HMX_ReadFile(pclFile, (char *)pubData, iSize ) == iSize) );
}

//
//  Function:  hmx_WriteNtsRecord
//  Purpose:   Write one nts record (depending SD/HD)
//
//  Parms:     Comm structure, File^, Buffer^
//  Returns:   TRUE = OKee
//
static BOOL hmx_WriteNtsRecord(HMXCOM *pstComm, CFile *pclFile, u_int8 *pubData)
{
   int   iSize;

   //
   // Get SD/HD
   //
   //pwjh int iProfile
   //pwjh HMX_GetFromStorage(pstComm->pclNvm, NVM_NTS_AUTOHD, &iProfile);
   //pwjh switch(iProfile)
   //
   if( HMX_CheckHdtv(pstComm) ) iSize = NTS_IDX_SIZE*2;
   else                         iSize = NTS_IDX_SIZE; 
   //pwjh switch(pstComm->tContentType)
   //pwjh {
   //pwjh    case NTS_DEF_AUTO:
   //pwjh    case NTS_DEF_SD:
   //pwjh    case NTS_DEF_AUTO_SD:
   //pwjh       iSize = NTS_IDX_SIZE;
   //pwjh       break;
   //pwjh 
   //pwjh    case NTS_DEF_HD:
   //pwjh    case NTS_DEF_AUTO_HD:
   //pwjh       iSize = NTS_IDX_SIZE*2;
   //pwjh       break;
   //pwjh }
   return( (HMX_WriteFile(pclFile, (char *)pubData, iSize ) == iSize) );
}

//
//  Function:  hmx_CheckNtsFields
//  Purpose:   Check diffs between the current 2 nts records
//
//  Parms:     Comm structure, BufferOri^, BufferGen^
//  Returns:   Number of errors
//
//  Note:
//             Field Size  Field-ID Offset   Description
//             ----------------------------------------------------------------
//             1     1     Field1a     0     Frame type I, P, B, 
//             2     3     Field1b     1     Offset A
//             3     2     Field2a     4     Offset B
//             4     1     Field2b     6     
//             5     1     Field2c     7
//             6     8     Field34     8
//             7     4     Field5      16    File Pointer
//             8     4     Field6      20    PTS
//             9     4     Field7      24    Frame size
//             10    4     Field8      28    Timestamp trick modes
//             11    4     Field9      32    Pict Start offset
//             12    4     Field10     36    Pict I start offset
//             
//
static int hmx_CheckNtsFields(HMXCOM *pstComm, u_int8 *pubOriData, u_int8 *pubGenData)
{
   u_int8   ubDataOri=0, ubDataGen=0;
   u_int16  usDataOri=0, usDataGen=0;
   u_int32  ulDataOri=0, ulDataGen=0;
   u_int64  llFp;
   int      iField;
   int      iErrors=0;
   
   llFp = CVB_llong_get(&pubGenData[16], 0xFFFFFFFFFFFFFFFF, 0);
   //
   for(iField=1; iField<=12; iField++)
   {
      switch(iField)
      {
         case 1:
            ubDataOri = CVB_ubyte_get(&pubOriData[0], 0xFF, 0);
            ubDataGen = CVB_ubyte_get(&pubGenData[0], 0xFF, 0);
            if(ubDataOri != ubDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-1a  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ubDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ubDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 2:
            ulDataOri = CVB_ulong_get(&pubOriData[0], 0x00FFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[0], 0x00FFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-1b  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 3:
            usDataOri = CVB_ushort_get(&pubOriData[4], 0xFFFF, 0);
            usDataGen = CVB_ushort_get(&pubGenData[4], 0xFFFF, 0);
            if(usDataOri != usDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-2a  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  usDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", usDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 4:
            ubDataOri = CVB_ubyte_get(&pubOriData[6], 0xFF, 0);
            ubDataGen = CVB_ubyte_get(&pubGenData[6], 0xFF, 0);
            if(ubDataOri != ubDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-2b  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ubDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ubDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 5:
            ubDataOri = CVB_ubyte_get(&pubOriData[7], 0xFF, 0);
            ubDataGen = CVB_ubyte_get(&pubGenData[7], 0xFF, 0);
            if(ubDataOri != ubDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-2c  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ubDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ubDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 6:
            // FPs are always in sync
            break;
            
         case 7:
            ulDataOri = CVB_ulong_get(&pubOriData[16], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[16], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-5   FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 8:
            ulDataOri = CVB_ulong_get(&pubOriData[20], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[20], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-6   FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 9:
            ulDataOri = CVB_ulong_get(&pubOriData[24], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[24], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-7   FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 10:
            ulDataOri = CVB_ulong_get(&pubOriData[28], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[28], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-8   FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 11:
            ulDataOri = CVB_ulong_get(&pubOriData[32], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[32], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-9   FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
            
         case 12:
            ulDataOri = CVB_ulong_get(&pubOriData[36], 0xFFFFFFFF, 0);
            ulDataGen = CVB_ulong_get(&pubGenData[36], 0xFFFFFFFF, 0);
            if(ulDataOri != ulDataGen)
            {
               HMX_LogEventLongHexData(pstComm, 0, "TEST:Field-10  FP=",  llFp, TRUE);
               HMX_LogEventHexData(pstComm, 0, " Gen=",  ulDataGen, FALSE);
               HMX_LogEventHexData(pstComm, 0, " Ori=", ulDataOri, FALSE);
               iErrors++;
            }
            break;
      }
   }
   return(iErrors);
}

//
//  Function:  hmx_CompareHmts
//  Purpose:   Compare two HMT files 
//
//  Parms:     Comm structure, par not used
//  Returns:   0 = error
//
static int hmx_CompareHmts(HMXCOM *pstComm, int iParm)
{
   int         iCc, iIdx, iErrors=0;
   int         iSizeOri;
   CString     clStrOri;
   CString     clDir, clEvt, clDate, clTime;
   CFile       clFileOri;
   u_int8     *pubOri;
   HMTEST     *pstTst;

   if(!pstComm->fEventOk)
   {
      // No event has been selected
      AfxMessageBox("No valid SPTS has been selected !");
      return(0);
   }
   //
   // Open LOG file
   // Open HMT original    (O)
   // Open HMT generated   (G)
   //
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DIRNAME,    &clDir);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_EVENTNAME,  &clEvt);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DATE,       &clDate);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_TIME,       &clTime);
   
   clStrOri.Format(_T("%s\\%s_%s_%s.hmt.ori"), clDir.GetBuffer(), clEvt.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer() );
   //
   // Open all files:   ori: *.hmt.ori : original HUMAX HMT file as template
   //                   gen: *.hmt     : tool generated HMT file to verify
   //
   HMX_OpenLogFile(pstComm, 'a');
   HMT_OpenHmtFile(pstComm, 'a');
   //
   iCc = clFileOri.Open(clStrOri.GetBuffer(), CFile::modeRead, NULL);
   if(iCc==0)
   {
      // Problems
      HMX_LogEventText(pstComm, -1, "TEST:Problems opening ",  clStrOri.GetBuffer(), TRUE);
      HMX_CloseLogFile(pstComm);
      return(0);
   }
   //
   // Read Original HMT file
   //
   iSizeOri = (int) clFileOri.GetLength();
   pubOri   = (u_int8 *) CVB_SafeMalloc(iSizeOri);
   HMX_ReadFile(&clFileOri, (char *)pubOri, iSizeOri);
   //
   // Verify item by item all important fields
   //
   iIdx = 0;
   //
   while(stHmtTestVectors[iIdx].pfHndlr)
   {
      pstTst   = (HMTEST *)&stHmtTestVectors[iIdx];
      iErrors += pstTst->pfHndlr(pstComm, (HMTEST *)pstTst, pubOri, pstComm->pubHmtCat);
      iIdx++;
   };
   clFileOri.Close();
   HMT_CloseHmtFile(pstComm);
   HMX_LogEventData(pstComm, 0, "TEST:End check errors=",  iErrors, TRUE);
   HMX_CloseLogFile(pstComm);

   return(0);
}

/* ====== Functions separator ===========================================
void ____Hmt_test_functions____(){}
=========================================================================*/

//
//  Function:  hmx_HmtCompareGeneric
//  Purpose:   Compare generic bytes
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareGeneric(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Generic data:      Adr= ", iAddr, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Ori= ", &pubOri[iAddr], iSize, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Gen= ", &pubGen[iAddr], iSize, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareGenericPid
//  Purpose:   Compare various pids
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^, PID text^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareGenericPid(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen, char *pcPid)
{
   BOOL     fCc = TRUE;
   int      iAddr;
   u_int16  usDataOri, usDataGen;
   
   iAddr = pstTst->iAddr;

   usDataOri = CVB_ushort_get( &pubOri[iAddr], 0xffff, 0);
   usDataGen = CVB_ushort_get( &pubGen[iAddr], 0xffff, 0);
   //
   if(usDataOri != usDataGen)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:PID                Adr= ", iAddr, TRUE);
      HMX_LogEventHexData(pstComm, 0,  "                                      Ori= ", usDataOri, TRUE);
      HMX_LogEventText(   pstComm, 0,  " : ",  pcPid, FALSE);
      HMX_LogEventHexData(pstComm, 0,  "                                      Gen= ", usDataGen, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareStart
//  Purpose:   Compare Start index
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareStart(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iDataOri, iDataGen;
   
   iAddr = pstTst->iAddr;

   iDataOri = CVB_ulong_get( &pubOri[iAddr], 0xffffffff, 0);
   iDataGen = CVB_ulong_get( &pubGen[iAddr], 0xffffffff, 0);
   //
   if(iDataOri != iDataGen)
   {
      //                               012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData(pstComm, 0, "TEST:HMT Check-ERR:Start index        Adr= ", iAddr, TRUE);
      HMX_LogEventHexData(pstComm, 0, "                                      Ori= ", iDataOri, TRUE);
      HMX_LogEventHexData(pstComm, 0, "                                      Gen= ", iDataGen, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareHeader
//  Purpose:   Compare header
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareHeader(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Headr              Adr= ", iAddr, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Ori= ", &pubOri[iAddr], iSize, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Gen= ", &pubGen[iAddr], iSize, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareStartTime
//  Purpose:   Compare start times
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareStartTime(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL     fCc = TRUE;
   CString  clStrOri, clStrGen;
   int      iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      hmx_DumpTimestamps(pstComm, &clStrOri, pubOri, iAddr);
      hmx_DumpTimestamps(pstComm, &clStrGen, pubGen, iAddr);
      //
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Starttime          Adr= ", iAddr, TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Ori= ", clStrOri.GetBuffer(), TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Gen= ", clStrGen.GetBuffer(), TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareEndTime
//  Purpose:   Compare end times
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareEndTime(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL     fCc = TRUE;
   CString  clStrOri, clStrGen;
   int      iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      hmx_DumpTimestamps(pstComm, &clStrOri, pubOri, iAddr);
      hmx_DumpTimestamps(pstComm, &clStrGen, pubGen, iAddr);
      //
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Endtime            Adr= ", iAddr, TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Ori= ", clStrOri.GetBuffer(), TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Gen= ", clStrGen.GetBuffer(), TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtComparePathname
//  Purpose:   Compare Linux pathnames
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePathname(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Pathname           Adr= ", iAddr, TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Ori= ", (char *)&pubOri[iAddr], TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Gen= ", (char *)&pubGen[iAddr], TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareService
//  Purpose:   Compare Service name
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareService(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Service name       Adr= ", iAddr, TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Ori= ", (char *)&pubOri[iAddr], TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Gen= ", (char *)&pubGen[iAddr], TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareChName
//  Purpose:   Compare channel name
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareChName(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Channel name       Adr= ", iAddr, TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Ori= ", (char *)&pubOri[iAddr], TRUE);
      HMX_LogEventText(    pstComm, 0, "                                      Gen= ", (char *)&pubGen[iAddr], TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareFlags1
//  Purpose:   Compare various flags
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareFlags1(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGeneric(pstComm, pstTst, pubOri, pubGen) );
}

//
//  Function:  hmx_HmtCompareFlags2
//  Purpose:   Compare variuous more flags
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareFlags2(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGeneric(pstComm, pstTst, pubOri, pubGen) );
}

//
//  Function:  hmx_HmtCompareSid
//  Purpose:   Compare SID
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareSid(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "Service-ID") );
}

//
//  Function:  hmx_HmtComparePidXx1
//  Purpose:   Compare PID Unknown 1
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidXx1(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "XX-1") );
}

//
//  Function:  hmx_HmtComparePidV
//  Purpose:   Compare video PID
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidV(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "Video") );
}

//
//  Function:  hmx_HmtComparePidA
//  Purpose:   Compare Audio PID
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidA(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "Audio") );
}

//
//  Function:  hmx_HmtComparePidPcr
//  Purpose:   Compare PCR PID
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidPcr(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "PCR") );
}

//
//  Function:  hmx_HmtComparePidXx2
//  Purpose:   Compare unknown PID 2
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidXx2(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "XX-2") );
}

//
//  Function:  hmx_HmtComparePidXx3
//  Purpose:   Compare Unknown PID 3
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePidXx3(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGenericPid(pstComm, pstTst, pubOri, pubGen, "XX-3") );
}

//
//  Function:  hmx_HmtCompareNrXx1
//  Purpose:   Compare Unknown data 1
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareNrXx1(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGeneric(pstComm, pstTst, pubOri, pubGen) );
}

//
//  Function:  hmx_HmtCompareNrXx2
//  Purpose:   Compare unknown data 2
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareNrXx2(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGeneric(pstComm, pstTst, pubOri, pubGen) );
}

//
//  Function:  hmx_HmtCompareNrXx3
//  Purpose:   Compare unknown data 3
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareNrXx3(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   return( hmx_HmtCompareGeneric(pstComm, pstTst, pubOri, pubGen) );
}

//
//  Function:  hmx_HmtComparePlayback
//  Purpose:   Compare playback stamp
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtComparePlayback(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iSize;

   iAddr = pstTst->iAddr;
   iSize = pstTst->iSize;
   
   if(memcmp(&pubOri[iAddr], &pubGen[iAddr], iSize) != 0)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Playback position  Adr= ", iAddr, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Ori= ", &pubOri[iAddr], iSize, TRUE);
      HMX_LogEventHexBytes(pstComm, 0, "                                      Gen= ", &pubGen[iAddr], iSize, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareNrEvents
//  Purpose:   Compare number of events
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareNrEvents(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL  fCc = TRUE;
   int   iAddr, iDataOri, iDataGen;
   
   iAddr = pstTst->iAddr;

   iDataOri = CVB_ulong_get( &pubOri[iAddr], 0xffffffff, 0);
   iDataGen = CVB_ulong_get( &pubGen[iAddr], 0xffffffff, 0);
   //
   if(iDataOri != iDataGen)
   {
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-ERR:Number of events   Adr= ", iAddr, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                                      Ori= ",  iDataOri, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                                      Gen= ",  iDataGen, TRUE);
      fCc = FALSE;
   }
   return(fCc);
}

//
//  Function:  hmx_HmtCompareEvents
//  Purpose:   Compare the stored events
//
//  Parms:     Comm structure, Test struct, Ori buffer^, Gen buffer^
//  Returns:   0 = error
//
static BOOL hmx_HmtCompareEvents(HMXCOM *pstComm, HMTEST *pstTst, u_int8 *pubOri, u_int8 *pubGen)
{
   BOOL     fCc = TRUE;
   int      iEvent, iNumEventsOri, iNumEventsGen;
   HMTREC  *pstHmtCat;
   u_int8  *pubHmtRaw;
   u_int8  *pubHmtCat;
   HMTPF   *pstPf;

   pstHmtCat = pstComm->pstHmtCat;
   pubHmtCat = pstComm->pubHmtCat;
   pubHmtRaw = pstComm->pubHmtRaw;
   //
   // Retrieve all Generated PF events
   //
   iNumEventsGen = HMT_GetNumEvents(pstComm);
   for(iEvent=1; iEvent<=iNumEventsGen; iEvent++)
   {
      pstPf = HMT_GetEvent(pstComm, iEvent);
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-   :Event              Adr= ", pstPf->iAddr, TRUE);
      HMX_LogEventData(    pstComm, 0, "                   Event              Gen= ", iEvent, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                   Event Flags10           ", pstPf->ulFlags10, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                   Event Flags11           ", pstPf->ulFlags11, TRUE);
      HMX_LogEventText(    pstComm, 0, "                   Event name              ", pstPf->pclEventName->GetBuffer(), TRUE);
      HMX_LogEventText(    pstComm, 0, "                   Event Description       ", pstPf->pclEventDescr->GetBuffer(), TRUE);
   }
   //
   // Select ORI HMT buffers
   //
   pstComm->pstHmtCat = NULL;    // Create new
   pstComm->pubHmtRaw = NULL;    // Create new
   pstComm->pubHmtCat = pubOri;
   HMT_Init(pstComm);
   HMT_ReadEvents(pstComm);
   iNumEventsOri = HMT_GetNumEvents(pstComm);
   for(iEvent=1; iEvent<=iNumEventsOri; iEvent++)
   {
      pstPf = HMT_GetEvent(pstComm, iEvent);
      //                                012345678901234567890123456789012345678901234567890
      HMX_LogEventHexData( pstComm, 0, "TEST:HMT Check-   :Event              Adr= ", pstPf->iAddr, TRUE);
      HMX_LogEventData(    pstComm, 0, "                   Event              Ori= ", iEvent, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                   Event Flags10           ", pstPf->ulFlags10, TRUE);
      HMX_LogEventHexData( pstComm, 0, "                   Event Flags11           ", pstPf->ulFlags11, TRUE);
      HMX_LogEventText(    pstComm, 0, "                   Event name              ", pstPf->pclEventName->GetBuffer(), TRUE);
      HMX_LogEventText(    pstComm, 0, "                   Event Description       ", pstPf->pclEventDescr->GetBuffer(), TRUE);
   }
   //
   // Warning if number of events do not match
   //
   if(iNumEventsGen != iNumEventsOri)
   {
      //                            012345678901234567890123456789012345678901234567890
      HMX_LogEventData(pstComm, 0, "TEST:HMT Check-ERR:Events different   Gen= ", iNumEventsGen, TRUE);
      HMX_LogEventData(pstComm, 0, " Ori= ", iNumEventsOri, FALSE);
   }
   //
   // Release original PF events again, and restore generated events
   //
   HMT_Cleanup(pstComm);
   pstComm->pstHmtCat = pstHmtCat;
   pstComm->pubHmtCat = pubHmtCat;
   pstComm->pubHmtRaw = pubHmtRaw;
   //
   return(fCc);
}

