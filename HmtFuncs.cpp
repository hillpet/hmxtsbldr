/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmtFuncs.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            HMT Catalog file functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "ClockApi.h"

//
// Local prototypes
//
static int     hmt_AddLinebreaks             (u_int8 *, int, int, int);
static BOOL    hmt_AllocateHmtStorage        (HMXCOM *);
static void    hmt_CatalogDefaults           (HMXCOM *);
static int     hmt_UnchainEvent              (HMXCOM *, CString *, u_int8 *, u_int8 *);
static int     hmt_CountIsoDescriptors       (u_int8 *, int);
static int     hmt_DiscardIsoDescriptors     (u_int8 *, int);
static void    hmt_Init                      (HMXCOM *);
static BOOL    hmt_InsertIntoBuffer          (u_int8 *, u_int8 *, int);
static int     hmt_RemoveChar                (char *, char, char);
static int     hmt_RemoveLinebreaks          (u_int8 *, int);
static u_int8 *hmt_SkipData                  (u_int8 *, u_int32, u_int32);


const u_int8  *pubCrLf = (u_int8 *) CRLF;
//
const char     pcDefaultDescr[] =
               {
                  "There was no content description for this event, so "
                  "it does not have now/next information to display. "
                  "Use the 5050c-TsBuilder tool to add or modify the "
                  "event description for this content. The tool can "
                  "be downloaded from www.patrn.nl."
               };


/* ====== Functions separator ===========================================
void ___api_functions____(){}
=========================================================================*/

//
//  Function:   HMT_CreatePresentFollowingList
//  Purpose:    Create member of the p/f linked list
//  Parms:      pstComm
//
//  Returns:    New pstPf if not allocated yet
//
HMTPF *HMT_CreatePresentFollowingList(HMXCOM *pstComm)
{
   HMTPF *pstPf;

   pstPf = (HMTPF *) CVB_SafeMalloc(sizeof(HMTPF));
   pstPf->usEventId     = 0xbeef;
   pstPf->ulIso         = 0x64757400;     //"dut".00
   pstPf->pclEventName  = new(CString);
   pstPf->pclEventDescr = new(CString);
   
   return(pstPf);
}

//
//  Function:   HMT_DeletePresentFollowingList
//  Purpose:    Delete the p/f linked list
//  Parms:      pstComm
//
//  Returns:    
//
void HMT_DeletePresentFollowingList(HMXCOM *pstComm)
{
   HMTPF *pstPf, *pstTmp;

   if(pstComm->pstHmtCat->pstEvt)
   {
      pstPf = pstComm->pstHmtCat->pstEvt->pstPf;
      //
      do
      {
         if(pstPf)
         {
            if(pstPf->pclEventDescr) delete(pstPf->pclEventDescr);
            if(pstPf->pclEventName)  delete(pstPf->pclEventName);
            pstTmp = pstPf;
            pstPf  = pstPf->pstNext;
            CVB_SafeFree(pstTmp);
         }
      }
      while(pstPf);
      //
      pstComm->pstHmtCat->pstEvt->pstPf = NULL;
   }
}

//
//  Function:   HMT_Init
//  Purpose:    Clean up HMT memory
//  Parms:      pstComm
//
//  Returns:    
//
void HMT_Init(HMXCOM *pstComm)
{
   hmt_Init(pstComm);
}

//
//  Function:   HMT_Cleanup
//  Purpose:    Clean up HMT memory
//  Parms:      pstComm
//
//  Returns:    
//
void HMT_Cleanup(HMXCOM *pstComm)
{
   HMTPF *pstPf = NULL;
   HMTPF *pstNn;
   
   if(pstComm == NULL) return;
   //
   if(pstComm->pstHmtCat) 
   {
      if(pstComm->pstHmtCat->pstEvt)
      {
         pstPf = pstComm->pstHmtCat->pstEvt->pstPf;
         pstComm->pstHmtCat->pstEvt = (HMTEVT *) CVB_SafeFree(pstComm->pstHmtCat->pstEvt);
      }
   }
   //
   while(pstPf)
   {
      if(pstPf->pclEventName)  delete(pstPf->pclEventName);
      if(pstPf->pclEventDescr) delete(pstPf->pclEventDescr);
      //
      pstNn = pstPf->pstNext;
      CVB_SafeFree(pstPf);
      pstPf = pstNn;
   }
   //
   if(pstComm->pubHmtRaw) pstComm->pubHmtRaw = (u_int8 *) CVB_SafeFree(pstComm->pubHmtRaw);
   if(pstComm->pubHmtCat) pstComm->pubHmtCat = (u_int8 *) CVB_SafeFree(pstComm->pubHmtCat);
   if(pstComm->pstHmtCat) pstComm->pstHmtCat = (HMTREC *) CVB_SafeFree(pstComm->pstHmtCat);
   //
   pstComm->iDurationStart = 0;
   pstComm->iDurationEnd   = 0;
}

//
//  Function:   HMT_CloseHmtFile
//  Purpose:    Close the HMT Catalog file
//  Parms:      pstComm
//
//  Returns:    void
//
void HMT_CloseHmtFile(HMXCOM *pstComm)
{
   if( pstComm->pclFileHmt && pstComm->fFileHmt)  pstComm->pclFileHmt->Close();
   pstComm->fFileHmt = 0;
}

//
//  Function:   HMT_FindCurrentEvent
//  Purpose:    Find the currentn event from the P/F list
//  Parms:      pstComm
//
//  Returns:    p/f struct
//
HMTPF *HMT_FindCurrentEvent(HMXCOM *pstComm)
{
   HMTPF   *pstPf;
   CString *pclEvt;
   CString *pclStr;
   
   pclStr = pstComm->pstHmtCat->pclEvent;
   pstPf  = pstComm->pstHmtCat->pstEvt->pstPf;
   
   while(pstPf)
   {
      pclEvt = pstPf->pclEventName;
      if(pclStr == pclEvt)
      {
         break;
      }
      pstPf = pstPf->pstNext;
   }
   //
   // If no match was found, return the 1st one as closest match
   //
   if(pstPf == NULL) pstPf = pstComm->pstHmtCat->pstEvt->pstPf;
   return(pstPf);
}

//
//  Function:   HMT_GenerateHmtFile
//  Purpose:    Generate hte Hmt (catalog) file
//  Parms:      pstComm
//
//  Returns:    Size of the HMT file
//
u_int32 HMT_GenerateHmtFile(HMXCOM *pstComm)
{
   u_int32 ulSize = HMT_CatalogTranslate(pstComm, HMT_CAT_TO_STREAM);
   return(ulSize);
}

//
//  Function:   HMT_OpenHmtFile
//  Purpose:    Open a Hmt (catalog) file for read or write
//  Parms:      pstComm, mode R(ead), W(rite), C(reate)
//
//  Returns:    0=Error
//
int HMT_OpenHmtFile(HMXCOM *pstComm, char cMode)
{
   pstComm->fFileHmt = HMX_OpenFile(pstComm->pclFileHmt, pstComm->pclNvm, NVM_HMX_FILEEXT_HMT, cMode);
   return(pstComm->fFileHmt);
}

//
//  Function:   HMT_ParseHmtFile
//  Purpose:    Read the catalog HMT file
//  Parms:      Comm struct
//
//  Returns:    SPTSCC completion code
//
SPTSCC HMT_ParseHmtFile(HMXCOM *pstComm)
{
   SPTSCC   tCc=SPTS_CC_BAD_FILE;
   long     lRead, lHmtSize;
   CFile   *pclFile;
   u_int8  *pubHmtRaw;
   u_int8  *pubHmtCat;
   
   if(pstComm == NULL) return(tCc);
   pclFile = pstComm->pclFileHmt;
   if(pclFile == NULL) return(tCc);

   //
   // The HMT file can be parsed now:
   // 1. Check the pstComm->pstHmtCat and allocate necessary storage room
   // 2. HMT file exists   : Read the file and update the storage
   //        doesnt/is zero: Enter default data  
   // 3. Remove the existing p/f linked list
   // 4. Start a new p/f linked list
   // 5. Read all p/f events plus descriptors into the linked list
   //
   hmt_AllocateHmtStorage(pstComm);
   //
   if(pstComm->fFileHmt)
   {
      tCc = SPTS_CC_OKEE;
      pubHmtRaw = pstComm->pubHmtRaw;
      pubHmtCat = pstComm->pubHmtCat;
      lHmtSize  = (int) pclFile->GetLength();
      //
      pclFile->SeekToBegin();
      //
      // The file buffer is HMT_FILE_END long. If the HMT file does not fit, report and exit
      //
      lRead = HMT_ReadHmtFile(pstComm, (char *)pubHmtRaw, pstComm->lHmtSize);
      if(lRead == lHmtSize)
      {
         // We have read the whole HMT catalog file
         // Retrieve raw info from it and store it
         //
         memcpy(pubHmtCat, pubHmtRaw, lHmtSize);
         
#if defined(FEATURE_REMOVE_ISO_DESCR)
         //
         // Skip all ISO language descriptors in the clean version
         // Event name, Service name and short event descriptor
         //
         hmt_DiscardIsoDescriptors(&pubHmtCat[HMT_EVENTNAME],   HMT_EVENTNAME_MAX);
         hmt_DiscardIsoDescriptors(&pubHmtCat[HMT_SERVICENAME], HMT_SERVICENAME_MAX);
#endif   // defined(FEATURE_REMOVE_ISO_DESCR)

         //
         // Fill the HMT record struct with the stream data
         //
         HMT_CatalogTranslate(pstComm, HMT_STREAM_TO_CAT);
      }
      else
      {
         AfxMessageBox("Problems with the hmt-file size !");
      }
   }
   return(tCc);   
}

//
//  Function:   HMT_ReadHmtFile
//  Purpose:    Read data from the HMT (Catalog) file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number read
//
int HMT_ReadHmtFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileHmt) return( HMX_ReadFile(pstComm->pclFileHmt, pcBuffer, iSize));
   else                       return(0);
}

//
//  Function:   HMT_GetNumEvents
//  Purpose:    Return the number of p/f events in the HMT buffer
//  Parms:      Comm struct
//
//  Returns:    ulNr
//
u_int32 HMT_GetNumEvents(HMXCOM *pstComm)
{
   u_int32  ulNumEvents=0;
   
   if(pstComm->pstHmtCat)
   {
      if(pstComm->pstHmtCat->pstEvt)
      {
         ulNumEvents = pstComm->pstHmtCat->pstEvt->ulNumEvents;
      }
   }
   return(ulNumEvents);
}

//
//  Function:   HMT_GetEvent
//  Purpose:    Read one p/f event from the linked list
//  Parms:      Comm struct
//
//  Returns:    pstPf
//
HMTPF* HMT_GetEvent(HMXCOM *pstComm, int iEvent)
{
   HMTREC  *pstHmtCat;
   HMTPF   *pstPf = NULL;
   
   pstHmtCat = pstComm->pstHmtCat;
   //
   if(pstHmtCat)
   {
      pstPf = pstHmtCat->pstEvt->pstPf;
      while(pstPf)
      {
         if(--iEvent <= 0) break;
         else              pstPf = pstPf->pstNext;
      }
   }
   return(pstPf);
}

//
//  Function:   HMT_ReadEvents
//  Purpose:    Read the p/f events from the HMT buffer
//  Parms:      Comm struct
//
//  Returns:    TRUE = OKee
//
BOOL HMT_ReadEvents(HMXCOM *pstComm)
{
   u_int32  ulNumEvents, ulSize;
   u_int8  *pubHmtCat, *pubEvent;
   HMTREC  *pstHmtCat;
   HMTPF  **ppstPf, *pstPf;
   
   pubHmtCat = pstComm->pubHmtCat;

   //--------------------------------------------------------------------
   // The p/f events are chain into 2 different loops:
   //
   // n = Num_events
   // while(n)
   // {
   //    event-data()
   //    event-name()
   //    i = textblocksize
   //    while(i)
   //    {
   //       eventtext+=text
   //    }
   // }
   //--------------------------------------------------------------------
   pstHmtCat   = pstComm->pstHmtCat;
   pubEvent    = &pubHmtCat[HMT_EVENTS];
   ulNumEvents = CVB_ulong_get(pubEvent, 0xffffffff, 0);
   pubEvent  += 4;
   pstHmtCat->pstEvt->ulNumEvents = ulNumEvents;
   ppstPf = &pstHmtCat->pstEvt->pstPf;

   //
   while(ulNumEvents)
   {
      pstPf = *ppstPf;
      if(pstPf == NULL)
      {
         pstPf   = HMT_CreatePresentFollowingList(pstComm);
         *ppstPf = pstPf;
         ppstPf  = &pstPf->pstNext;
      }
      pstPf->usEventId = CVB_ushort_get(&pubEvent[HMT_PF_EVT_ID], 0xffff, 0);
      pstPf->pclEventName->Format(_T("%s"), &pubEvent[HMT_PF_EVT_NAME]);
      pstPf->ulFlags10 = CVB_ulong_get(&pubEvent[HMT_PF_EVT_FLAGS10], 0xffffffff, 0);
      pstPf->ulFlags11 = CVB_ulong_get(&pubEvent[HMT_PF_EVT_FLAGS11], 0xffffffff, 0);
      pstPf->iAddr     = (int) (pubEvent - pubHmtCat);
      //
      // Unchain a P/F Event descriptor
      //
      ulSize = CVB_ulong_get(&pubEvent[HMT_PF_EVT_SIZE1], 0xffffffff, 0);
      hmt_UnchainEvent(pstComm, pstPf->pclEventDescr, &pubEvent[HMT_PF_EVT_ISO], &pubEvent[HMT_PF_EVT_SIZE2+ulSize]);
      pubEvent += ulSize + HMT_PF_EVT_SIZE2;
      //
      HMX_LogEventHexData(pstComm, 1, "HMT:Event-", pstPf->usEventId, TRUE);
      HMX_LogEventText(pstComm, 1, "HMT:  Name=",   pstPf->pclEventName->GetBuffer(), TRUE);
      HMX_LogEventText(pstComm, 1, "HMT:  Dscr=",   pstPf->pclEventDescr->GetBuffer(), TRUE);

      ulNumEvents--;
   }
   return(TRUE);   
}

//
//  Function:   HMT_WriteHmtFile
//  Purpose:    Write data to the HMT (Catalog) file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number written
//
int HMT_WriteHmtFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileHmt) return( HMX_WriteFile(pstComm->pclFileHmt, pcBuffer, iSize));
   else                       return(0);
}




/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/

//
//  Function:  hmt_AddLinebreaks
//  Purpose:   Add newlines to wrap the text
//
//  Parms:     buffer, buffer size, char/lines count, max new buffer size
//       
//  Returns:   Nr of lines
//
static int hmt_AddLinebreaks(u_int8 *pubData, int iSize, int iChars, int iMax)
{
   int   iLines=0, iNr;

   while( (iNr = (int) strlen((char *)pubData)) > iChars)
   {
      pubData += iChars;
      iSize   -= iChars;
      iMax    -= iChars;
      // Find word separator
      while (*pubData)
      {
         if(*pubData == ' ') 
         {
            // insert CRLF
            hmt_InsertIntoBuffer(pubData, (u_int8 *)pubCrLf, iMax);
            iNr      = (int) strlen((char *)pubCrLf);
            pubData += iNr;
            iSize   -= iNr;
            iMax    -= iNr;
            iLines++;
            break;
         }
         pubData++;
         iSize--;
         iMax--;
      }
   }
   return(iLines);
}

//
//  Function:   hmt_AllocateHmtStorage
//  Purpose:    Allocate the HMT data stucture 
//  Parms:      Comm struct
//
//  Returns:    
//
static BOOL hmt_AllocateHmtStorage(HMXCOM *pstComm)
{
   BOOL     fCc = FALSE;
   long     lHmtSize;
   CFile   *pclFile = pstComm->pclFileHmt;

   //
   // Allocate the max size of the HMT file 
   //
   pstComm->lHmtSize = HMT_FILE_END;
   hmt_Init(pstComm);
   //
   if(pstComm->fFileHmt)
   {
      // Hmt file is open: allocate file space
      //   
      lHmtSize = (int) pclFile->GetLength();
      if(lHmtSize > pstComm->lHmtSize)
      {
         HMX_LogEvent(pstComm, -1, "HMT:File too big !", TRUE);
         //pwjh hmt_Init(pstComm);
         hmt_CatalogDefaults(pstComm);
      }
      if(lHmtSize == 0) 
      {
         HMX_LogEvent(pstComm, -1, "HMT:File is empty !", TRUE);
         //pwjh hmt_Init(pstComm);
         hmt_CatalogDefaults(pstComm);
      }
      else
      {
         //pwjh hmt_Init(pstComm);
         fCc = TRUE;
      }
   }
   else
   {
      // No HMT file: use max space for now
      //
      HMX_LogEvent(pstComm, 0, "HMT:Missing catalog file", TRUE);
      //pwjh hmt_Init(pstComm);
      hmt_CatalogDefaults(pstComm);
   }
   return(fCc);   
}

//
//  Function:  hmt_CatalogDefaults
//  Purpose:   Fill catalog with default values if there is NO HMT file available
//
//  Parms:     pstComm
//       
//  Returns:   
//
static void hmt_CatalogDefaults(HMXCOM *pstComm)
{
   CString  clEvent, clDate, clTime;
   HMTPF   *pstPf;
   HMTREC  *pstHmtCat = pstComm->pstHmtCat;

   //
   // Make a record structure
   // Fill it with some sensible data if the HMT file does not exist
   //   ulStartTime;                           // HMT_TIME_START:   00.49.df.29
   //   ulEndTime;                             // HMT_TIME_END:     a4.49.df.29
   //   ubRecordingOk;                         // HMT_RECORDING_OK
   //   ubIconFlags1;                          // HMT_ICONS1
   //   ubIconFlags2;                          // HMT_ICONS2
   //   usServiceId;                           // HMT_SRVSID
   //   usVideoPid;                            // HMT_VIDPID
   //   usAudioPid;                            // HMT_AUDPID
   //   usPcrPid;                              // HMT_PCRPID
   //   usTtxPid;                              // HMT_TTXPID
   //   usStartPlay;                           // HMT_TIME_RESTART
   //   pclFilePath;                           // HMT_PATHNAME:     /media/sda1/Peter/Event_20090415_1100
   //   pclEvent;                              // HMT_EVENTNAME:    Event
   //   pclService;                            // HMT_SERVICENAME:  Nederland 3
   //
   if(pstHmtCat)
   {
      //
      pstHmtCat->ulStart1      = 0x00000001;
      pstHmtCat->ulStart2      = 0x01003800;
      pstHmtCat->ulStart3      = 0x00000000;
      pstHmtCat->ulStart4      = 0x00003800;
      //
      pstHmtCat->ubRecordingOk = HMT_REC_IS_OKEE;
      pstHmtCat->ubIconFlags1  = HMT_ICON_NEW;
      if( HMX_CheckHdtv(pstComm) )
      {
         pstHmtCat->ubIconFlags2 = HMT_ICON_HD;
      }
      pstHmtCat->usChannelNr   = HMT_DEFAULT_CH;
      //
      HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_EVENTNAME, &clEvent);
      HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DATE,      &clDate);
      HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_TIME,      &clTime);
      //
      pstHmtCat->pclFilePath->Format(_T("%s_%s_%s_%s"),  "/media/sda1/Video/", clEvent.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer());
      pstHmtCat->pclEvent->Format(_T("%s"),              clEvent.GetBuffer());
      pstHmtCat->pclService->Format(_T("%s"),            "Nederland 1");
      //
      // Clear some settings 
      //
      pstHmtCat->ulStartTime = 0;
      pstHmtCat->ulEndTime   = 0;
      pstHmtCat->usServiceId = 0;
      pstHmtCat->usVideoPid  = 0;
      pstHmtCat->usAudioPid  = 0;
      pstHmtCat->usPcrPid    = 0;
      pstHmtCat->usTtxPid    = 0;
      pstHmtCat->usStartPlay = 0;
      //
      // Create the dummy P/F event list
      //
      pstHmtCat->pstEvt->ulNumEvents = 1;
      //
      HMT_DeletePresentFollowingList(pstComm);                             // Delete p/f linked list
      pstPf = HMT_CreatePresentFollowingList(pstComm);                     // Create p/f linked list
      pstPf->pclEventName->Format(_T("%s"), clEvent.GetBuffer());          //   
      pstPf->pclEventDescr->Format(_T("%s"), pcDefaultDescr);              // 
      pstHmtCat->pstEvt->pstPf = pstPf;                                    //
   }
}


//
//  Function:  hmt_UnchainEvent
//  Purpose:   Unchain the text from a P/F event 
//
//  Parms:     pstComm, TextBufferStart, DescrBufferEnd
//       
//  Returns:   text size
//
static int hmt_UnchainEvent(HMXCOM *pstComm, CString *pclStr, u_int8 *pubData, u_int8 *pubEnd)
{
   u_int32  ulTextLen = 0;
   u_int16  usSize;
   
   //
   // Get all partial text blocks from the buffer and unchain them directly into the CString buffer
   //
   *pclStr = "";   // truncate CSting buffer
   //
   while(pubData < pubEnd)
   {
      //
      // pubData->  usEvent-ID
      //            usBeatsMe[9]
      //            cEventName[?]
      //            ulEventSize
      //            ulEventSize
      //            ulEventIdx 
      //            while(ulEventSize)
      //            {
      //                ulEventIsoLanguageDescriptor
      //                ulBeatsMe[2]
      //                usTextSize
      //                cEventDescr[usTextSize]
      //            }
      // 
      pubData += 12;  // skip ISO+ulBeatsMe's
      usSize   = CVB_ushort_get(pubData, 0xffff, 0);
      pubData += 2;
      hmt_RemoveChar((char *)pubData, 0x05, 0);
      *pclStr   += (char *) pubData;
      ulTextLen += usSize;
      pubData   += usSize;
   }
   return(ulTextLen);
}

//
//  Function:  hmt_CountIsoDescriptors
//  Purpose:   Count all ISO language descriptors in bytes
//
//  Parms:     Src buffer, Src buffer size
//       
//  Returns:   Nr of bytes ISO descriptors
//
static int hmt_CountIsoDescriptors(u_int8 *pubData, int iSize)
{
   int   iCount = 0;

   while(iSize > 0)
   {
      switch(*pubData)
      {
         case 0x05:
            // directive for 8859-9: skip
            iCount++;
            // Fall through
         default:
            pubData++;
            iSize--;
      }
   }
   return(iCount);
}

//
//  Function:  hmt_DiscardIsoDescriptors
//  Purpose:   Discard all ISO language descriptors
//
//  Parms:     Src buffer, Src buffer size
//       
//  Returns:   new size
//
static int hmt_DiscardIsoDescriptors(u_int8 *pubData, int iSize)
{
   int   iNewSize = iSize;

   while(iSize > 0)
   {
      switch(*pubData)
      {
         case 0x05:
            // directive for 8859-9: skip
            hmt_SkipData(pubData, iSize, 1);
            iNewSize -= 1;
            iSize    -= 1;
            break;

         default:
            pubData++;
            iSize--;
      }
   }
   return(iNewSize);
}

//
//  Function:  hmt_InsertIntoBuffer
//  Purpose:   Insert a substring into a buffer
//
//  Parms:     Dest, Src, src max new size
//       
//  Returns:   TRUE if OKee insertetd
//
static BOOL hmt_InsertIntoBuffer(u_int8 *pubDst, u_int8 *pubSrc, int iMaxSize)
{
   BOOL     fCc=FALSE;
   int      x, iSrcLen, iDstLen;
   u_int8  *pubTmp;

   iSrcLen = (int)strlen((char *)pubSrc);
   iDstLen = (int)strlen((char *)pubDst)+1;
   x       = iSrcLen+iDstLen;
   if(x<iMaxSize)
   {
      pubTmp = &pubDst[x];
      pubDst = &pubDst[iDstLen];
      //
      fCc = TRUE;
      //
      while(iDstLen--)
      {
         *pubTmp-- = *pubDst--;
      }
      pubDst++;
      // copy the dest in place
      strncpy((char *)pubDst, (char *)pubSrc, iSrcLen);
   }
   return(fCc);
}

//
//  Function:   hmt_Init
//  Purpose:    Initialize the HMT data structures
//  Parms:      pstComm
//
//  Returns:    
//
static void hmt_Init(HMXCOM *pstComm)
{
   long     lHmtSize, lCatSize;
   u_int8  *pubHmtRaw;
   u_int8  *pubHmtCat;
   HMTREC  *pstHmtCat;
   
   //
   // Make a record structure
   // Fill it with some sensible data if the HMT file does not exist
   //
   pstHmtCat = pstComm->pstHmtCat;
   if(pstHmtCat == NULL)
   {
      pstHmtCat = (HMTREC *) CVB_SafeMalloc(sizeof(HMTREC));
      // Allocate the name spaces
      pstHmtCat->pclFilePath  = (CString *) new(CString);
      pstHmtCat->pclEvent     = (CString *) new(CString);
      pstHmtCat->pclService   = (CString *) new(CString);
      pstComm->pstHmtCat      = pstHmtCat;
   }
   else
   {
      // Empty the buffers
      *pstHmtCat->pclFilePath  = "";
      *pstHmtCat->pclEvent     = "";
      *pstHmtCat->pclService   = "";
   }
   //
   // Get us a buffer to hold the catalog
   pubHmtRaw = pstComm->pubHmtRaw;
   lHmtSize  = pstComm->lHmtSize;
   //
   if(pubHmtRaw == NULL)
   {
      pubHmtRaw = (u_int8 *) CVB_SafeMalloc(lHmtSize);
      pstComm->pubHmtRaw = pubHmtRaw;
   }
   //
   // Make a version to edit too. The additional size might have to change later on.
   pubHmtCat = pstComm->pubHmtCat;
   if(pubHmtCat == NULL)
   {
      lCatSize = lHmtSize;
      pubHmtCat = (u_int8 *) CVB_SafeMalloc(lCatSize);
      pstComm->pubHmtCat = pubHmtCat;
      pstComm->lCatSize  = lCatSize;
   }
   //
   // Build the linked list of p/f events
   if(pstHmtCat->pstEvt == NULL)
   {
      pstHmtCat->pstEvt = (HMTEVT *) CVB_SafeMalloc(sizeof(HMTEVT));
   }
   //
   // Delete p/f linked list
   //
   HMT_DeletePresentFollowingList(pstComm);
}

//
//  Function:   hmt_RemoveChar
//  Purpose:    Remove a particular char from the buffer
//
//  Parms:      buffer ptr, char, new char
//  Returns:    number removed
//
static hmt_RemoveChar(char *pcBuffer, char cRemove, char cNew)
{
    int     iNr = 0;

    while(*pcBuffer)
    {
        if(*pcBuffer == cRemove)
        {
            if(cNew)
            {
                *pcBuffer = cNew;
            }
            else
            {
                *pcBuffer = '\0'; // in case last char !!
                strcpy(pcBuffer, pcBuffer+1);
            }
            iNr++;
        }
        else
        {
            pcBuffer++;
        }
    }
    return(iNr);
}

//
//  Function:  hmt_RemoveLinebreaks
//  Purpose:   Remove newlines that were used to visually wrap the text
//
//  Parms:     buffer, buffer size
//       
//  Returns:   Nr of linebreaks
//
static int hmt_RemoveLinebreaks(u_int8 *pubData, int iSize)
{
   int  iNr=0;

   while(iSize)
   {
      if(*pubData == 0x0d)
      {
         // Remove the CR
         hmt_RemoveChar((char *)pubData, 0x0d, 0x00);
         iSize--;
         iNr++;
      }
      if(*pubData == 0x0a)
      {
         // Replace the LF by SPACE
         hmt_RemoveChar((char *)pubData, 0x0a, ' ');
      }
      pubData++;
      iSize--;
   }
   return(iNr);
}

//
//  Function:  hmt_SkipData
//  Purpose:   Skip some data from the buffer
//
//  Parms:     Src buffer, buffer size, number to skip
//       
//  Returns:   u_int8 *
//
static u_int8 *hmt_SkipData(u_int8 *pubData, u_int32 iTotal, u_int32 iSize)
{
   u_int8  *pubSrc;

   // No separate destination: delete the data from this buffer. 
   pubSrc = &pubData[iSize];
   if(iSize > iTotal) iSize = iTotal;
   memcpy(pubData, pubSrc, iTotal-iSize);
   
   return(pubData);
}


