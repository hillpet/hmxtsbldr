/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxFiles.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Misc helper functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       11Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"

#define  DEFINE_GLOBALS
#include "HmxControl.h"
#undef   DEFINE_GLOBALS
#include "ClockApi.h"


static int        hmx_DeterminePacketSize (HMXCOM *, u_int8);
static void       hmx_DumpMapTable        (HMXCOM *, u_int8 *, u_int16);
static void       hmx_DumpPids            (HMXCOM *, PIDLST *);
static PIDLST    *hmx_HandlePids          (HMXCOM *, TS_MPEG2 *, PIDLST *);
static u_int16    hmx_FindSidInMapTable   (HMXCOM *, PIDLST *, u_int8 *, u_int16);
static BOOL       hmx_ExtractItem         (CNvmStorage *, char *, NVMDEFS);
static char      *hmx_FindChar            (char, char *, int);
static PIDLST    *hmx_FindPid             (HMXCOM *, PIDLST *, u_int16);
static u_int64    hmx_FindSyncByte        (HMXCOM *, u_int8, int, int);
static PIDLST    *hmx_FreePidList         (PIDLST *);
static void       hmx_HandleTsProgressBar (HMXCOM *, PRBAR);
static TS_MPEG2  *hmx_HandlePacketPointer (HMXCOM *, TS_MPEG2 *);
static int        hmx_ReadPacket          (HMXCOM *, TS_MPEG2 *, int);

/* ====== Functions separator ===========================================
void ____spts_functions____(){}
=========================================================================*/

//
//  Function:   HMX_Init
//  Purpose:    Init the SPTS list
//  Parms:      pstComm
//
//  Returns:    void
//
void HMX_Init(HMXCOM *pstComm)
{
   TS_Init(pstComm);
}

//
//  Function:   HMX_CloseAllFiles
//  Purpose:    Close all files
//  Parms:      pstComm
//
//  Returns:    void
//
void HMX_CloseAllFiles(HMXCOM *pstComm)
{
   HMT_CloseHmtFile(pstComm);
   HMX_CloseLogFile(pstComm);
   NTS_CloseNtsFile(pstComm);
   HMX_CloseTsFile(pstComm);
}

//
//  Function:   HMX_CloseLogFile
//  Purpose:    Close the log file
//  Parms:      pstComm
//
//  Returns:    void
//
void HMX_CloseLogFile(HMXCOM *pstComm)
{
   if( pstComm->pclFileLog && pstComm->fFileLog)  pstComm->pclFileLog->Close();
   pstComm->fFileLog = 0;
}

//
//  Function:   HMX_CloseMetaFiles
//  Purpose:    Close the files in the meta file structure
//  Parms:      pstComm
//
//  Returns:    void
//
void HMX_CloseMetaFiles(HMXCOM *pstComm)
{
   HMT_CloseHmtFile(pstComm);
   NTS_CloseNtsFile(pstComm);
   HMX_CloseTsFile(pstComm);
}

//
//  Function:   HMX_CloseTsFile
//  Purpose:    Close the SPTS file
//  Parms:      pstComm
//
//  Returns:    void
//
void HMX_CloseTsFile(HMXCOM *pstComm)
{
   if( pstComm->pclFileTs  && pstComm->fFileTs )  pstComm->pclFileTs->Close();
   pstComm->fFileTs = 0;
}

//
//  Function:   HMX_OpenFile
//  Purpose:    Open a file for read or write
//  Parms:      File^, NVM^, tExt, mode R(ead), W(rite), C(reate), A(ppend)
//
//  Returns:    0=Error
//
int HMX_OpenFile(CFile *pclFile, CNvmStorage *pclNvm, NVMDEFS tExt, char cMode)
{
   int      iCc;
   CString  clPathname;
   CString  clDir;
   CString  clFile;
   CString  clExt;
   
   if(pclFile == NULL) return(0);

   HMX_GetFromStorage(pclNvm, NVM_HMX_DIRNAME,  &clDir);
   HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME, &clFile);
   HMX_GetFromStorage(pclNvm, tExt,             &clExt);
   
   clPathname.Format(_T("%s\\%s.%s"), clDir.GetBuffer(), clFile.GetBuffer(), clExt.GetBuffer());
   

   //
   // Open the file
   //
   switch(cMode)
   {
      case 'R':
      case 'r':
         iCc = pclFile->Open(clPathname.GetBuffer(), CFile::modeRead, NULL);
         break;

      case 'W':
      case 'w':
         iCc = pclFile->Open(clPathname.GetBuffer(), CFile::modeReadWrite, NULL);
         break;

      case 'C':
      case 'c':
         iCc = pclFile->Open(clPathname.GetBuffer(), CFile::modeReadWrite|CFile::modeCreate, NULL);
         break;

      case 'A':
      case 'a':
         iCc = pclFile->Open(clPathname.GetBuffer(), CFile::modeNoTruncate|CFile::modeWrite|CFile::modeCreate, NULL);
         pclFile->SeekToEnd();
         break;

      default:
         iCc = 0;
         break;
   }
   return(iCc);
}

//
//  Function:   HMX_OpenLogFile
//  Purpose:    Open a log file for read or write
//  Parms:      pstComm, mode R(ead), W(rite), C(reate)
//
//  Returns:    0=Error
//
const char *pcBars = "-------------------------------------------------------------------------------";
//
int HMX_OpenLogFile(HMXCOM *pstComm, char cMode)
{
   HMX_CloseLogFile(pstComm);
   //
   if(pstComm->fDoLog)
   {
      pstComm->fFileLog = HMX_OpenFile(pstComm->pclFileLog, pstComm->pclNvm, NVM_HMX_FILEEXT_LOG, cMode);
      HMX_LogEvent(pstComm, 0, (char *)pcBars, TRUE);
      HMX_LogEventData(pstComm, 0, "      Log file active, Priority=", pstComm->iLogPriority, TRUE);
      HMX_LogEvent(pstComm, 0, (char *)pcBars, TRUE);

   }
   return(pstComm->fFileLog);
}

//
//  Function:   HMX_OpenMetaFiles
//  Purpose:    Open a Hmt (catalog) file for read or write
//  Parms:      pstComm, mode R(ead), W(rite), C(reate)
//
//  Returns:    0=Error
//
int HMX_OpenMetaFiles(HMXCOM *pstComm, char cMode)
{
   int   iCc = 1;

   iCc &= HMX_OpenTsFile(pstComm,  cMode);
   iCc &= NTS_OpenNtsFile(pstComm, cMode);
   iCc &= HMT_OpenHmtFile(pstComm, cMode);

   return( iCc);
}

//
//  Function:   HMX_OpenTsFile
//  Purpose:    Open a SPTS file for read or write
//  Parms:      pstComm, mode R(ead), W(rite), C(reate)
//
//  Returns:    0=Error
//
int HMX_OpenTsFile(HMXCOM *pstComm, char cMode)
{
   u_int64  llFp=0;

   pstComm->fFileTs = HMX_OpenFile(pstComm->pclFileTs, pstComm->pclNvm, NVM_HMX_FILEEXT_TS, cMode);
   if(pstComm->fFileTs)
   {
      //
      // Obtain the packet size of the TS, if any
      //
      pstComm->llCurrentFp = llFp;
      pstComm->iPacketSize = hmx_DeterminePacketSize(pstComm, 0x47);
      if(pstComm->iPacketSize == 0)
      {
         //
         // Verify we have a valid packet size. If not we might need to sync first
         //
         llFp = hmx_FindSyncByte(pstComm, 0x47, 192, TS_SYNC_WINDOW_SMALL);
         if(llFp == -1)
         {
            // 
            // No, this is not the right packet size
            //
            llFp = hmx_FindSyncByte(pstComm, 0x47, 188, TS_SYNC_WINDOW_SMALL);
            if(llFp == -1)
            {
               //
               // Hmmmmm. Neither packetsize can sync to the 0x47
               //
               HMX_LogEvent(pstComm, -1, "TS:Cannot synchronize to SPTS 0x47 byte !", TRUE);
            }
            else
            {
               //
               // SPTS in sync: store FP and packet size
               //
               pstComm->llCurrentFp = llFp;
               pstComm->iPacketSize = 188;
               HMX_LogEventData(pstComm, 0, "TS:Packet size=", pstComm->iPacketSize, TRUE);
               HMX_LogEventLongHexData(pstComm, 0, " Sync at FP=", llFp, FALSE);
            }
         }
         else
         {
            //
            // SPTS in sync: store FP and packet size
            //
            pstComm->llCurrentFp = llFp;
            pstComm->iPacketSize = 192;
            HMX_LogEventData(pstComm, 0, "TS:Packet size=", pstComm->iPacketSize, TRUE);
            HMX_LogEventLongHexData(pstComm, 0, " Sync at FP=", llFp, FALSE);
         }
      }
      else
      {
         HMX_LogEventData(pstComm, 0, "TS:Packet size=", pstComm->iPacketSize, TRUE);
         HMX_LogEventLongHexData(pstComm, 0, " Sync at FP=", llFp, FALSE);
      }
   }
   //
   return(pstComm->fFileTs);
}

//
//  Function:   HMX_ParseTsFile
//  Purpose:    Parse a SPTS file
//  Parms:      Comm struct
//
//  Returns:    SPTSCC completion code
//
//  struct TS_MPEG2
//     u_int8   ubTimecode[4];
//     u_int8   ubSync;
//     u_int8   ubFlag1Pid[2];
//     u_int8   ubFlag2Cc;
//     u_int8   ubPayload[TS_MAX_PAYLOAD_SIZE];
//  
//  struct TS_PES
//     char        cEsName[HMX_NAME_LEN];
//     int         iPid;
//     TS_PES     *pstNext;
//
SPTSCC HMX_ParseTsFile(HMXCOM *pstComm)
{
   SPTSCC   tCc=SPTS_CC_BAD_FILE;
   int      iNr, iNoResync;
   u_int64  llFp;
   int      iSession;
   CFile    *pclFile;
   TS_MPEG2 *pstPacket;
   
   if(pstComm == NULL) return(tCc);
   pclFile = pstComm->pclFileTs;
   if(pclFile == NULL) return(tCc);
   
   iSession = pstComm->pclComm->iSession;
   //
   HMX_GetFromStorage(pstComm->pclNvm, NVM_TS_NORESYNC, &iNoResync);
   //
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   TS_Init(pstComm);
   pstPacket = hmx_HandlePacketPointer(pstComm, NULL);
   //
   // We know the packet size: parse the whole file now
   // Update the App on every 1% of the file
   //
   tCc = SPTS_CC_OKEE;
   pclFile->SeekToBegin();
   pstComm->llCurrentFp = 0;
   do
   {
      if(iSession != pstComm->pclComm->iSession)
      {
         tCc = SPTS_CC_INTERRUPTED;
         break;
      }
      iNr = hmx_ReadPacket(pstComm, pstPacket, pstComm->iPacketSize);
      //
      if(iNr)
      {
         if(pstPacket->ubSync == 0x47)
         {
            hmx_HandleTsProgressBar(pstComm, BAR_STEP);
            TS_HandlePacket(pstComm, pstPacket);
            pstComm->llCurrentFp      += iNr;
            pstComm->pstNts->ulFrSize += iNr;
         }
         else
         {
            // This is not a valid MPEG-2 TS packet !!
            if(iNoResync)
            {
               AfxMessageBox("SPTS is invalid (missing Sync byte) !");
               iNr = 0;
            }
            else
            {
               HMX_LogEventLongHexData(pstComm, -1, "TS: Parser lost sync at FP=", pstComm->llCurrentFp, TRUE);
               llFp = hmx_FindSyncByte(pstComm, 0x47, pstComm->iPacketSize, TS_SYNC_WINDOW_BIG);
               if(llFp == -1)
               {
                  //
                  // Cannot resync to the 0x47
                  //
                  AfxMessageBox("SPTS is invalid (missing Sync byte) ! Cannot resync !");
                  iNr = 0;
                  HMX_LogEvent(pstComm, -1, "TS:Parser cannot re-synchronize to SPTS !", TRUE);
               }
               else
               {
                  //
                  // SPTS back in sync: store FP and packet size
                  //
                  pstComm->llCurrentFp = llFp;
                  HMX_LogEventLongHexData(pstComm, -1, "TS: Parser resync at FP=", llFp, TRUE);
               }
            }
         }
      }
   }
   while(iNr == pstComm->iPacketSize);
   //
   // Plug in SID.
   //
   pstComm->pstHmtCat->usServiceId = TS_GetServiceId(pstComm);

   TS_DumpPidList(pstComm, pstComm->pstPidList);
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   //
   // Update the Hmx-Comm structure
   //
   pstComm->tCc = tCc;
   hmx_HandlePacketPointer(pstComm, pstPacket);
   return(tCc);   
}

//
//  Function:   HMX_ReadFile
//  Purpose:    Read data from the file
//  Parms:      Buffer, size
//
//  Returns:    Number read
//
int HMX_ReadFile(CFile *pclFile, char *pcBuffer, int iSize)
{
   int    iNr;
   
   if(pclFile == NULL) return(0);
   
   iNr = pclFile->Read(pcBuffer, iSize);
   
   return(iNr);
}

//
//  Function:   HMX_ReadTsFile
//  Purpose:    Read data from the SPTS file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number read
//
int HMX_ReadTsFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileTs)  return( HMX_ReadFile(pstComm->pclFileTs, pcBuffer, iSize));
   else                       return(0);
}

//
//  Function:   HMX_WriteFile
//  Purpose:    Write data to the SPTS file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number written (faked)
//
int HMX_WriteFile(CFile *pclFile, char *pcBuffer, int iSize)
{
   if(pclFile == NULL) return(0);
   
   pclFile->Write(pcBuffer, iSize);
   
   return(iSize);
}

//
//  Function:   HMX_WriteLogFile
//  Purpose:    Write data to the LOG file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number written
//
int HMX_WriteLogFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileLog) return( HMX_WriteFile(pstComm->pclFileLog, pcBuffer, iSize));
   else                       return(0);
}

//
//  Function:   HMX_WriteTsFile
//  Purpose:    Write data to the SPTS file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number written
//
int HMX_WriteTsFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileTs)  return( HMX_WriteFile(pstComm->pclFileTs, pcBuffer, iSize));
   else                       return(0);
}

/* ====== Functions separator ===========================================
void ____NVM_functions____(){}
=========================================================================*/

//
//  Function:   HMX_NewFile
//  Purpose:    Create a new data file
//
//  Parms:      CNvmStorage *
//  Returns:
//
void HMX_NewFile(CNvmStorage *pclNvm)
{
   pclNvm->NvmPut(NVM_VERSION,                 HMX_VERSION);
   pclNvm->NvmPut(NVM_NAME_HUMAX,              "Humax 5050c HD-DVR");
   pclNvm->NvmPut(NVM_HMX_PATHNAME,            "");
   pclNvm->NvmPut(NVM_HMX_DEFDIR,              "");
   pclNvm->NvmPut(NVM_HMX_DIRNAME,             "D:\\Humax\\*.*");
   pclNvm->NvmPut(NVM_HMX_FILENAME,            "---");
   pclNvm->NvmPut(NVM_HMX_FILEEXT_TS,          "ts");
   pclNvm->NvmPut(NVM_HMX_FILEEXT_NTS,         "nts");
   pclNvm->NvmPut(NVM_HMX_FILEEXT_HMT,         "hmt");
   pclNvm->NvmPut(NVM_HMX_FILEEXT_LOG,         "log");
   pclNvm->NvmPut(NVM_HMX_EVENTNAME,           "---");
   pclNvm->NvmPut(NVM_HMX_TIME,                "----");
   pclNvm->NvmPut(NVM_HMX_DATE,                "--------");
   pclNvm->NvmPut(NVM_TS_PACKETSIZE,            TS_DEF_PACKET_SIZE);
   pclNvm->NvmPut(NVM_HMX_BATCHMODE,            FALSE);
   pclNvm->NvmPut(NVM_HMX_OVERWRITE,            FALSE);
   pclNvm->NvmPut(NVM_TS_TIMECODE,              FALSE);
   pclNvm->NvmPut(NVM_TS_NORESYNC,              FALSE);
}

//
//  Function:   HMX_GetFromStorage
//  Purpose:    Retrieve NVM data from storage
//  Parms:      NVM, NVM_xxx Storage ID, int
//  Returns:    TRUE if OKee
//
BOOL HMX_GetFromStorage(CNvmStorage *pclNvm, int iId, int *piData)
{
   if(pclNvm == NULL) return(FALSE);
   else               return( pclNvm->NvmGet(iId, piData) );
}

//
//  Function:   HMX_GetFromStorage
//  Purpose:    Retrieve NVM data from storage
//  Parms:      NVM, NVM_xxx Storage ID, CString buffer
//  Returns:    TRUE if OKee
//
BOOL HMX_GetFromStorage(CNvmStorage *pclNvm, int iId, CString *pclStr)
{
   if(pclNvm == NULL) return(FALSE);
   else               return( pclNvm->NvmGet(iId, pclStr) );
}

//
//  Function:   HMX_GetFromStorage
//  Purpose:    Retrieve NVM data from storage
//  Parms:      NVM, NVM_xxx Storage ID, ascii buffer, max buffer size
//  Returns:    TRUE if OKee
//
//  Note:       This function only returns that part of the storage that fits
//              into the buffer.
//
BOOL HMX_GetFromStorage(CNvmStorage *pclNvm, int iId, char *pcBuffer, int iSize)
{
   if(pclNvm == NULL) return(FALSE);
   else
   {
      memset(pcBuffer, ' ', iSize);
      pcBuffer[iSize-1] = '\0';
      return( pclNvm->NvmGet(iId, pcBuffer) );
   }
}

//
//  Function:   HMX_PutIntoStorage
//  Purpose:    Store NVM data to storage
//  Parms:      NVM, NVM_xxx Storage ID, int
//  Returns:    TRUE if OKee
//
BOOL HMX_PutIntoStorage(CNvmStorage *pclNvm, int iId, int iData)
{
   if(pclNvm == NULL) return(FALSE);
   else
   {
      pclNvm->NvmPut(iId, iData);
      return(TRUE);
   }
}

//
//  Function:   HMX_PutIntoStorage
//  Purpose:    Store NVM data to storage
//  Parms:      NVM, NVM_xxx Storage ID, CString buffer
//  Returns:    TRUE if OKee
//
BOOL HMX_PutIntoStorage(CNvmStorage *pclNvm, int iId, CString *pclStr)
{
   if(pclNvm == NULL) return(FALSE);
   else
   {
      pclNvm->NvmPut(iId, pclStr);
      return(TRUE);
   }
}

//
//  Function:   HMX_PutIntoStorage
//  Purpose:    Store NVM data to storage
//  Parms:      NVM, NVM_xxx Storage ID, ascii buffer, max buffer size
//  Returns:    TRUE if OKee
//
//  Note:       This function only writes that part of the storage that fits
//              into the buffer.
//
BOOL HMX_PutIntoStorage(CNvmStorage *pclNvm, int iId, char *pcBuffer, int iSize)
{
   if(pclNvm == NULL) return(FALSE);
   else
   {
      pcBuffer[iSize-1] = '\0';
      pclNvm->NvmPut(iId, pcBuffer); 
      return(TRUE);
   }
}

//
//  Function:   HMX_ReadHmxFile
//  Purpose:    Read the data file
//
//  Parms:      CNvmStorage *, filename
//  Returns:
//
void HMX_ReadHmxFile(CNvmStorage *pclNvm, char *pcFileName)
{
    //
    //  Load the default config file
    //
    CFile   clFile;

    if(clFile.Open(pcFileName, CFile::modeReadWrite, NULL) )
    {
        //
        // File is OKee: read it
        // 
        CArchive ar(&clFile, CArchive::load);

        pclNvm->Serialize(ar);
    }
}

//
//  Function:   HMX_WriteHmxFile
//  Purpose:    write the data file
//
//  Parms:      CNvmStorage *, filename
//  Returns:
//
void HMX_WriteHmxFile(CNvmStorage *pclNvm, char *pcFileName)
{
    //
    //  Load the default config file
    //
    CFile   clFile;

    //
    // if File does not exist: make a new one
    //
    int iLog = clFile.Open(pcFileName, CFile::modeReadWrite | 
                            CFile::modeCreate, NULL);
    VERIFY(iLog);

    CArchive ar(&clFile, CArchive::store);
    pclNvm->Serialize(ar);
}

/* ====== Functions separator ===========================================
void ____misc_functions____(){}
=========================================================================*/

//
//  Function:  HMX_BuildTimeDateStamp
//  Purpose:   Build a HMX compliant T&D stamp
//
//  Parms:     pclPath, pclDate, pclTime
//  Returns:   TRUE if name is OKee
//
BOOL HMX_BuildTimeDateStamp(CString *pclPath, CString *pclDate, CString *pclTime)
{
   BOOL        fCc = FALSE;
   u_int32     ulCreate;
   CFileStatus clSts;
   CTime       clTime;

   //
   // convert the file date&time stamp
   //
   if(CFile::GetStatus(pclPath->GetBuffer(), clSts))
   {
      // 
      // We have file D&T stamp: build date and time stamps
      //
      clTime   = clSts.m_ctime;
      ulCreate = (u_int32) clTime.GetTime();
      //
      HMX_SecsToDate(pclDate, ulCreate);
      HMX_SecsToTime(pclTime, ulCreate);
      fCc = TRUE;
   }
   return(fCc);
}

//
//  Function:  HMX_CheckHdtv
//  Purpose:   Check if the current stream type is HD
//
//  Parms:     pstComm
//  Returns:   TRUE if HD stream
//
//  Outputs:
//
//
BOOL HMX_CheckHdtv(HMXCOM *pstComm)
{
   switch(pstComm->tContentType)
   {
      case NTS_DEF_AUTO:
      case NTS_DEF_SD:
      case NTS_DEF_AUTO_SD:
         return(FALSE);
   
      case NTS_DEF_HD:
      case NTS_DEF_AUTO_HD:
         return(TRUE);
   }
   return(FALSE);
}

//
//  Function:  HMX_UpdateStreamType
//  Purpose:   Update the current stream type to HD or SD
//
//  Parms:     pstComm, streamtype
//  Returns:   
//
//  Outputs:
//
//
void HMX_UpdateStreamType(HMXCOM *pstComm, int iType)
{
   if(pstComm)
   {
      switch(iType)
      {
         case NTS_DEF_AUTO:
            switch(pstComm->tContentType)
            {
               default:
                  pstComm->tContentType = (NTSDEF)iType;
                  break;
                  
               case NTS_DEF_AUTO_SD:
               case NTS_DEF_AUTO_HD:
                  // Auto mode has already found a content type: leave as-is
                  break;
            }
            break;
         
         default:
         case NTS_DEF_SD:
         case NTS_DEF_HD:
            pstComm->tContentType = (NTSDEF)iType;
            break;
      }
   }
}

//
//  Function:  HMX_CopyWithTimecode
//  Purpose:   Copy the ts file, inserting timecodes
//
//  Parms:     Comm(with src file), Dest file, packet size
//  Returns:
//
//  Outputs:
//
//
int HMX_CopyWithTimecode(HMXCOM *pstComm, char *pcPathname, int iPacketSize)
{
   TS_MPEG2 *pstPacket;
   u_int64  llFp;
   CFile    clFile;
   int      iCc, iNr, iNoResync;
   u_int32  ulTimecode = 0xcd000000;  //dummy timecode


   HMX_GetFromStorage(pstComm->pclNvm, NVM_TS_NORESYNC, &iNoResync);
   
   pstComm->iPacketSize = iPacketSize;
   pstPacket = hmx_HandlePacketPointer(pstComm, NULL);
   //
   // We know the packet size: copy the file, inserting timecodes
   // Update the App on every 1% of the file
   //
   HMX_OpenTsFile(pstComm, 'r');
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   iCc = clFile.Open(pcPathname, CFile::modeReadWrite | CFile::modeCreate, NULL);
   if(iCc)
   {
      do
      {
         CVB_ulong_put((u_int8 *) pstPacket->ubTimecode, ulTimecode);
         iNr = hmx_ReadPacket(pstComm, pstPacket, pstComm->iPacketSize);
         if(iNr)
         {
            if(pstPacket->ubSync == 0x47)
            {
               hmx_HandleTsProgressBar(pstComm, BAR_STEP);
               HMX_WriteFile(&clFile, (char *)pstPacket, pstComm->iPacketSize+4);
               ulTimecode++;
               pstComm->llCurrentFp += iNr;
            }
            else
            {
               // This is not a valid MPEG-2 TS packet !!
               if(iNoResync)
               {
                  AfxMessageBox("SPTS is invalid (missing Sync byte) !");
                  iNr = 0;
               }
               else
               {
                  HMX_LogEventLongHexData(pstComm, -1, "TS: AddTimecode lost sync at FP=", pstComm->llCurrentFp, TRUE);
                  llFp = hmx_FindSyncByte(pstComm, 0x47, pstComm->iPacketSize, TS_SYNC_WINDOW_BIG);
                  if(llFp == -1)
                  {
                     //
                     // Cannot resync to the 0x47
                     //
                     AfxMessageBox("SPTS is invalid (missing Sync byte) ! Cannot resync !");
                     HMX_LogEvent(pstComm, -1, "TS:AddTimecode: cannot re-synchronize to SPTS !", TRUE);
                     iNr = 0;
                  }
                  else
                  {
                     //
                     // SPTS back in sync: store FP and packet size
                     //
                     pstComm->llCurrentFp = llFp;
                     HMX_LogEventLongHexData(pstComm, -1, "TS:AddTimecode: Resync at FP=", llFp, TRUE);
                  }
               }
            }
         }
      }
      while(iNr == pstComm->iPacketSize);
   }
   //
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   hmx_HandlePacketPointer(pstComm, pstPacket);
   //
   HMX_CloseTsFile(pstComm);
   clFile.Close();
   //
   return(1);
}

//
//  Function:  HMX_CopyWithoutTimecode
//  Purpose:   Copy the ts file, deleting timecodes
//
//  Parms:     Comm(with src file), Dest file, packet size
//  Returns:
//
//  Outputs:
//
//
int HMX_CopyWithoutTimecode(HMXCOM *pstComm, char *pcPathname, int iPacketSize)
{
   TS_MPEG2 *pstPacket;
   u_int64  llFp;
   CFile    clFile;
   int      iCc, iNr, iNoResync;

   HMX_GetFromStorage(pstComm->pclNvm, NVM_TS_NORESYNC, &iNoResync);

   pstComm->iPacketSize = iPacketSize;
   pstPacket = hmx_HandlePacketPointer(pstComm, NULL);
   //
   // We know the packet size: copy the file, deleting timecodes
   // Update the App on every 1% of the file
   //
   HMX_OpenTsFile(pstComm, 'r');
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   iCc = clFile.Open(pcPathname, CFile::modeReadWrite | CFile::modeCreate, NULL);
   if(iCc)
   {
      do
      {
         iNr = hmx_ReadPacket(pstComm, pstPacket, pstComm->iPacketSize);
         if(iNr)
         {
            if(pstPacket->ubSync == 0x47)
            {
               hmx_HandleTsProgressBar(pstComm, BAR_STEP);
               HMX_WriteFile(&clFile, (char *)&pstPacket->ubSync, pstComm->iPacketSize-4);
               pstComm->llCurrentFp += iNr;
            }
            else
            {
               // This is not a valid MPEG-2 TS packet !!
               if(iNoResync)
               {
                  AfxMessageBox("SPTS is invalid (missing Sync byte) !");
                  iNr = 0;
               }
               else
               {
                  HMX_LogEventLongHexData(pstComm, -1, "TS: RemoveTimecode lost sync at FP=", pstComm->llCurrentFp, TRUE);
                  llFp = hmx_FindSyncByte(pstComm, 0x47, pstComm->iPacketSize, TS_SYNC_WINDOW_BIG);
                  if(llFp == -1)
                  {
                     //
                     // Cannot resync to the 0x47
                     //
                     AfxMessageBox("SPTS is invalid (missing Sync byte) ! Cannot resync !");
                     iNr = 0;
                     HMX_LogEvent(pstComm, -1, "TS:Cannot re-synchronize to SPTS !", TRUE);
                  }
                  else
                  {
                     //
                     // SPTS back in sync: store FP and packet size
                     //
                     pstComm->llCurrentFp = llFp;
                     HMX_LogEventLongHexData(pstComm, -1, "TS:RemoveTimecode:Resync at FP=", llFp, TRUE);
                  }
               }
            }
         }
      }
      while(iNr == pstComm->iPacketSize);
   }
   //
   hmx_HandleTsProgressBar(pstComm, BAR_INIT);
   hmx_HandlePacketPointer(pstComm, pstPacket);
   //
   HMX_CloseTsFile(pstComm);
   clFile.Close();
   //
   return(1);
}

//
//  Function:  HMX_LogEvent
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, text, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-<description>"
//
//
void HMX_LogEvent(HMXCOM *pstComm, int iPrio, char *pcText, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;

         if(fNewLine) clStr.Format(_T(CRLF"%s-%s"), CVB_GetTimeDateStamp(), pcText);
         else         clStr.Format(_T("%s"), pcText);

         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
      }
      if(iPrio < 0)
      {
         // Error msgs go to thr DLG status window
         clStr.Format(_T("%s"), pcText);
         SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
      }
   }
}

//
//  Function:  HMX_LogEventText
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, static text, variable text, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-<description>"
//
//
void HMX_LogEventText(HMXCOM *pstComm, int iPrio, char *pcText, char *pcDynText, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;

         if(fNewLine) clStr.Format(_T(CRLF"%s-%s%s"), CVB_GetTimeDateStamp(), pcText, pcDynText);
         else         clStr.Format(_T("%s%s"), pcText, pcDynText);

         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
         if(iPrio < 0)
         {
            // Error msgs go to thr DLG status window as well
            SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
         }
      }
      if(iPrio < 0)
      {
         // Error msgs go to thr DLG status window
         clStr.Format(_T("%s%s"), pcText, pcDynText);
         SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
      }
   }
}

//
//  Function:  HMX_LogEventData
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, text, data, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-<description>"
//
//
void HMX_LogEventData(HMXCOM *pstComm, int iPrio, char *pcText, int iData, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;

         if(fNewLine) clStr.Format(_T(CRLF"%s-%s%d"), CVB_GetTimeDateStamp(), pcText, iData);
         else         clStr.Format(_T("%s%d"), pcText, iData);

         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
      }
      if(iPrio < 0)
      {
         // Error msgs go to thr DLG status window as well
         clStr.Format(_T("%s%d"), pcText, iData);
         SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
      }
   }
}

//
//  Function:  HMX_LogEventHexBytes
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, text, hexbyte^, count, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-Comment: 0xaa, 0xbb,...0x??"
//
//
void HMX_LogEventHexBytes(HMXCOM *pstComm, int iPrio, char *pcText, u_int8 *pubData, int iSize, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;
         if(fNewLine) clStr.Format(_T(CRLF"%s-%s"), CVB_GetTimeDateStamp(), pcText);
         else         clStr.Format(_T("%s"), pcText);
         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
         //
         while(iSize--)
         {
            clStr.Format(_T("%02X "), *pubData);
            pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
            pubData++;
         }
      }
   }
}

//
//  Function:  HMX_LogEventHexData
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, text, hexdata, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-<description>"
//
//
void HMX_LogEventHexData(HMXCOM *pstComm, int iPrio, char *pcText, int iData, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;

         if(fNewLine) clStr.Format(_T(CRLF"%s-%s0x%08X"), CVB_GetTimeDateStamp(), pcText, iData);
         else         clStr.Format(_T("%s0x%08X"), pcText, iData);

         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
      }
      if(iPrio < 0)
      {
         // Error msgs go to thr DLG status window as well
         clStr.Format(_T("%s0x%08X"), pcText, iData);
         SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
      }
   }
}

//
//  Function:  HMX_LogEventLongHexData
//  Purpose:   Write to the log file
//
//  Parms:     Comm, Priority, text, hexdata, Start on newline YesNo
//  Returns:
//
//  Outputs:
//  "24/12/2007 09:12:25-<description>"
//
//
void HMX_LogEventLongHexData(HMXCOM *pstComm, int iPrio, char *pcText, u_int64 llData, BOOL fNewLine)
{
   CString  clStr;
   CFile   *pclFile;

   if(pstComm)
   {
      if( (pstComm->fFileLog) && (iPrio <= pstComm->iLogPriority) )
      {
         // File is open and therefor in use
         pclFile = pstComm->pclFileLog;

         if(fNewLine) clStr.Format(_T(CRLF"%s-%s0x%016X"), CVB_GetTimeDateStamp(), pcText, llData);
         else         clStr.Format(_T("%s0x%016X"), pcText, llData);

         pclFile->Write(clStr.GetBuffer(), clStr.GetLength());
      }
      if(iPrio < 0)
      {
         // Error msgs go to thr DLG status window as well
         clStr.Format(_T("%s0x%016X"), pcText, llData);
         SolverSendUpdateToApp(pstComm, HMX_MSGID_SCAN_ERROR, clStr.GetBuffer(), 1, iPrio, 0, 0);
      }
   }
}

//
//  Function:   HMX_CheckCompatibility
//  Purpose:    Check if the filename is HMX compliant
//  Parms:      pstComm
//
//  Returns:    int with compliance bits
//
int HMX_CheckCompatibility(HMXCOM *pstComm)
{
   int         x, iCompat = pstComm->iHmxCompat & HMX_COMPAT_SET_MASK;
   CString     clDate, clTime, clExt;
   CString     clPathname, clDir, clFile;
   CFileStatus clFstat;

   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DATE,       &clDate);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_TIME,       &clTime);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_FILEEXT_TS, &clExt);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_DIRNAME,    &clDir);
   HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_FILENAME,   &clFile);
   //
   if(pstComm->fEventOk)
   {
      iCompat |= HMX_COMPAT_TS_EXIST;
      //
      if(strcmpi(clExt.GetBuffer(),"ts")==0)  
      {
         iCompat |= HMX_COMPAT_EXT_OK;
      }
      if(clDate.GetLength() == 8)
      {
         for(x=0; x<8; x++) 
         {
            if(!isdigit(clDate[x])) break;
         }
         if(x==8) iCompat |= HMX_COMPAT_DATE_OK;
      }
      if(clTime.GetLength() == 4)
      {
         for(x=0; x<4; x++) 
         {
            if(!isdigit(clTime[x])) break;
         }
         if(x==4) iCompat |= HMX_COMPAT_TIME_OK;
      }
      if( (iCompat & HMX_COMPAT_NAME_OK) == HMX_COMPAT_NAME_OK)
      {
         iCompat |= HMX_COMPAT_COMPLIANT;
      }
      //
      HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_FILEEXT_HMT, &clExt);
      clPathname.Format(_T("%s\\%s.%s"), clDir.GetBuffer(), clFile.GetBuffer(), clExt.GetBuffer());
      if(CFile::GetStatus(clPathname.GetBuffer(), clFstat)) iCompat |=  HMX_COMPAT_HMT_EXIST;
      if(strcmpi(clExt.GetBuffer(),"hmt")==0) iCompat |=  HMX_COMPAT_HMT_OK;
      //
      HMX_GetFromStorage(pstComm->pclNvm, NVM_HMX_FILEEXT_NTS, &clExt);
      clPathname.Format(_T("%s\\%s.%s"), clDir.GetBuffer(), clFile.GetBuffer(), clExt.GetBuffer());
      if(CFile::GetStatus(clPathname.GetBuffer(), clFstat)) iCompat |=  HMX_COMPAT_NTS_EXIST;
      if(strcmpi(clExt.GetBuffer(),"nts")==0) iCompat |=  HMX_COMPAT_NTS_OK;
   }
   return(iCompat);
}

//
//  Function:   HMX_MakeFilename
//  Purpose:    Build the filename from its components
//  Parms:      NVM, dest^
//
//  Returns:    Eventname_date_time.ext
//
void HMX_MakeFilename(CNvmStorage *pclNvm, CString *pclDest)
{
   CString  clEvent, clDate, clTime, clExt;
   
   HMX_GetFromStorage(pclNvm, NVM_HMX_EVENTNAME,  &clEvent);
   HMX_GetFromStorage(pclNvm, NVM_HMX_DATE,       &clDate);
   HMX_GetFromStorage(pclNvm, NVM_HMX_TIME,       &clTime);
   //
   pclDest->Format(_T("%s_%s_%s"), clEvent.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer());
}

//
//  Function:   HMX_MakePathname
//  Purpose:    Build the full pathname from its components
//  Parms:      NVM, dest^
//
//  Returns:    dirname\Eventname_date_time.ext
//
void HMX_MakePathname(CNvmStorage *pclNvm, CString *pclDest)
{
   CString  clDir, clEvent, clDate, clTime, clExt;
   
   HMX_GetFromStorage(pclNvm, NVM_HMX_DIRNAME,    &clDir);
   HMX_GetFromStorage(pclNvm, NVM_HMX_EVENTNAME,  &clEvent);
   HMX_GetFromStorage(pclNvm, NVM_HMX_DATE,       &clDate);
   HMX_GetFromStorage(pclNvm, NVM_HMX_TIME,       &clTime);
   HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_TS, &clExt);
   //
   pclDest->Format(_T("%s\\%s_%s_%s.%s"), clDir.GetBuffer(), clEvent.GetBuffer(), clDate.GetBuffer(), clTime.GetBuffer(), clExt.GetBuffer());
}

//
//  Function:  HMX_SplitPathname
//  Purpose:   Split the HMX TS pathname into its components
//
//  Parms:     NVM 
//  Returns:   HMXST
//                HMX_STATUS_COMPLIANT,
//                HMX_STATUS_NOT_COMPLIANT,
//
HMXST HMX_SplitPathname(CNvmStorage *pclNvm)
{
   HMXST    tCc       = HMX_STATUS_NOT_COMPLIANT;
   HMXCOMP  tComp     = HMX_COMP_INIT;
   BOOL     fChecking = TRUE;
   BOOL     fSyntax   = TRUE;
   CString  clStr;
   char    *pcTemp;
   int      iSize;

   HMX_GetFromStorage(pclNvm, NVM_HMX_PATHNAME, &clStr);
   
   //
   // A Correct Hmx pathname for a DVR recording would be:
   //
   //       "D:\Dir1\Dir2\Event Name_20090410_1200.ts"
   //
   //       This would be split up in :
   //                Pathname    : "D:\Dir1\Dir2\Event Name_20090410_1200.ts"
   //              1 Extension   : "ts"
   //              2 Filename    : "Event Name_20090410_1200"
   //              3 Timestamp   : "1200"
   //              4 Datestamp   : "20090410"
   //              5 Eventname   : "Event Name"
   //              6 Dirname     : "D:\Dir1\Dir2"
   //             
   //
   iSize  = clStr.GetLength()+1;
   pcTemp = (char *) CVB_SafeMalloc(iSize);
   strcpy(pcTemp, clStr.GetBuffer());
   //
   // Parse through the list in he order below
   //
   while(fChecking)
   {
      switch(tComp)
      {  
         case HMX_COMP_INIT:
            tComp = HMX_COMP_EXT;
            break;

         case HMX_COMP_EXT:
            fSyntax &= hmx_ExtractItem(pclNvm, pcTemp, NVM_HMX_FILEEXT_TS);
            tComp    = HMX_COMP_FILE;
            break;

         case HMX_COMP_TIME:
            fSyntax &= hmx_ExtractItem(pclNvm, pcTemp, NVM_HMX_TIME);
            tComp    = HMX_COMP_DATE;
            break;

         case HMX_COMP_DATE:
            fSyntax &= hmx_ExtractItem(pclNvm, pcTemp, NVM_HMX_DATE);
            tComp    = HMX_COMP_EVENT;
            break;

         case HMX_COMP_EVENT:
            fSyntax &= hmx_ExtractItem(pclNvm, pcTemp, NVM_HMX_EVENTNAME);
            tComp    = HMX_COMP_DIR;
            break;

         case HMX_COMP_FILE:
            fSyntax &= hmx_ExtractItem(pclNvm, pcTemp, NVM_HMX_FILENAME);
            tComp    = HMX_COMP_TIME;
            break;

         case HMX_COMP_DIR:
            iSize = (int) strlen(pcTemp)+1;
            HMX_PutIntoStorage(pclNvm, NVM_HMX_DIRNAME, pcTemp, iSize);
            tComp = HMX_COMP_OKEE;
            break;

         case HMX_COMP_OKEE:
            if(fSyntax) tCc = HMX_STATUS_COMPLIANT;
            fChecking = FALSE;
            break;

         default:
            tCc       = HMX_STATUS_NOT_COMPLIANT;
            fChecking = FALSE;
            break;
      } 
   }
   CVB_SafeFree(pcTemp);
   
   return(tCc);
}

//
//  Function:  HMX_DateTimeToSecs
//  Purpose:   Convert DateTime to Secs
//             Not used: needs sscanf
//  Parms:     buffer
//  Returns:   Secs
//
u_int32 HMX_DateTimeToSecs(CString *pclStr)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stDateTime.ubDay    = 10;
   stDateTime.ubMonth  =  4;
   stDateTime.usYear   = 2009-1900;
   stDateTime.ubHour   = 11;
   stDateTime.ubMin    = 12;
   stDateTime.ubSec    = 0;
   //
   CLOCK_UtilDateTimeToCount(&stDateTime, &stCount);
   //
   return(stCount.ulSeconds);
}

//
//  Function:  HMX_SecsToDateTime
//  Purpose:   Convert Secs to DateTime
//
//  Parms:     buffer, secs
//  Returns:   
//
void HMX_SecsToDateTime(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%02d-%02d-%04d  %02d:%02d:%02d"), 
                        stDateTime.ubDay,
                        stDateTime.ubMonth,
                        stDateTime.usYear+1900,
                        stDateTime.ubHour,
                        stDateTime.ubMin,
                        stDateTime.ubSec);
}

//
//  Function:  HMX_SecsToDate
//  Purpose:   Convert Secs to Date format "YYYYMMDD"
//
//  Parms:     buffer, secs
//  Returns:   
//
void HMX_SecsToDate(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%04d%02d%02d"), 
                        stDateTime.usYear+1900,
                        stDateTime.ubMonth,
                        stDateTime.ubDay);
}

//
//  Function:  HMX_SecsToTime
//  Purpose:   Convert Secs to Time format "HHMM"
//
//  Parms:     buffer, secs
//  Returns:   
//
void HMX_SecsToTime(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%02d%02d"), 
                        stDateTime.ubHour,
                        stDateTime.ubMin);
}


/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/


//
//  Function:  hmx_HandlePacketPointer
//  Purpose:   Set/Free the correct packet pointer
//
//  Parms:     pstComm, packet^
//  Returns:   Packet pointer
//
static TS_MPEG2 *hmx_HandlePacketPointer(HMXCOM *pstComm, TS_MPEG2 *pstPacket)
{
   if(pstPacket) CVB_SafeFree(pstPacket);
   else          pstPacket = (TS_MPEG2 *) CVB_SafeMalloc(sizeof(TS_MPEG2));
   //
   return(pstPacket);
}

//
//  Function:  hmx_ReadPacket
//  Purpose:   Read a MPEG2 packet
//
//  Parms:     pstComm, pstPacket, Size
//  Returns:   number read
//
static int hmx_ReadPacket(HMXCOM *pstComm, TS_MPEG2 *pstPacket, int iSize)
{
   int   iNr;
   char *pcPacket;

   if(iSize == 188) pcPacket = (char *) &(pstPacket->ubSync);
   else             pcPacket = (char *) pstPacket;
   //
   iNr = HMX_ReadTsFile(pstComm, pcPacket, iSize);
   return(iNr);
}


//
//  Function:  hmx_DeterminePacketSize
//  Purpose:   Determine the SPTS Packetsize
//
//  Parms:     pstComm, Sync byte
//  Returns:   Packet size 188, 192 (for now)
//  NOTE:      0 = no sync found
//
static int hmx_DeterminePacketSize(HMXCOM *pstComm, u_int8 ubSync)
{
   int      iNr, iSize = 0, iState = 1;
   BOOL     fChecking = TRUE;
   TS_MPEG2 *pstPacket;

   pstPacket = hmx_HandlePacketPointer(pstComm, NULL);
   //
   while(fChecking)
   {
      switch(iState)
      {
         case 0:
            // Found !
            fChecking = FALSE;
            break;
            
         case 1:
            // Read a 192 bytes record and verify the sync byte
            iSize = 192;
            pstComm->pclFileTs->Seek(pstComm->llCurrentFp, CFile::begin);
            iNr = hmx_ReadPacket(pstComm, pstPacket, iSize);
            if(iNr != iSize) return(0);
            if(pstPacket->ubSync == ubSync) iState = 0;
            else                            iState++;
            break;

         case 2:
            // Read a 188 bytes record and verify the sync byte
            iSize = 188;
            pstComm->pclFileTs->Seek(pstComm->llCurrentFp, CFile::begin);
            iNr = hmx_ReadPacket(pstComm, pstPacket, iSize);
            if(iNr != iSize) return(0);
            if(pstPacket->ubSync == ubSync) iState = 0;
            else                            iState++;
            break;

         default:
            iSize     = 0;
            fChecking = FALSE;
            break;
      }
   }
   pstComm->pclFileTs->Seek(pstComm->llCurrentFp, CFile::begin);
   hmx_HandlePacketPointer(pstComm, pstPacket);
   return(iSize);
}

//
//  Function:   hmx_DumpPids
//  Purpose:    Write the pid list to the log
//
//  Parms:      pstComm, pidlist
//  Returns:
//
static void hmx_DumpPids(HMXCOM *pstComm, PIDLST *pstPid)
{
   static int iLoop = 0;
   int iStart=pstPid->iIndex;

   HMX_LogEventData(pstComm, 5, " Loop=", ++iLoop, TRUE);
   do
   {
      HMX_LogEventData(pstComm, 5, "      i=", pstPid->iIndex, TRUE);
      HMX_LogEventData(pstComm, 5, " pid=", pstPid->usPid, FALSE);
      pstPid = pstPid->pstNext;
   }
   while(pstPid->iIndex != iStart);
}

//
//  Function:   hmx_ExtractItem
//  Purpose:    Extract components from the TS input pathname
//                Procedure:
//                1) Find/store ext, strip off ext
//                2) Find/store filename, store filename
//                3) Find/store time, strip off time
//                4) Find/stre date, strip off date
//                5) Find/store event, strip off event
//                6) Find/store  dir
//
//  Parms:      pcPathname, Component
//  Returns:
//
static BOOL hmx_ExtractItem(CNvmStorage *pclNvm, char *pcPath, NVMDEFS tComp)
{         
   BOOL  fCc = FALSE;      
   BOOL  fTruncate;      
   int   iSize;
   char *pcComp, cDelim;
   char  cZero[2];

   switch(tComp)
   {  
      case NVM_HMX_FILEEXT_TS:
         fTruncate = TRUE;      
         cDelim    = '.';
         break;

      case NVM_HMX_FILENAME:
         fTruncate = FALSE;
         cDelim    = '\\';
         break;

      case NVM_HMX_DATE:
      case NVM_HMX_TIME:
         fTruncate = TRUE;      
         cDelim    = '_';
         break;

      case NVM_HMX_EVENTNAME:
         fTruncate = TRUE;      
         cDelim    = '\\';
         break;

      default:
         break;
   }   

   iSize  = (int) strlen(pcPath);
   pcComp = hmx_FindChar(cDelim, pcPath, iSize);
   if(pcComp) 
   {
      if(fTruncate) 
      {
         *pcComp = '\0';
      }
      pcComp++;
      iSize = (int) strlen(pcComp) + 1;            // Add terminator
      HMX_PutIntoStorage(pclNvm, tComp, pcComp, iSize);
      fCc = TRUE;
   }
   else
   {
      cZero[0] = 0;
      cZero[1] = 0;
      HMX_PutIntoStorage(pclNvm, tComp, cZero, 2);         // Write NULL byte
   }
   return(fCc);
}

//
//  Function:   hmx_FindChar
//  Purpose:    Find the delimiter in the path, starting from the back
//
//  Parms:      Delimiter, Path, size
//  Returns:
//
static char *hmx_FindChar(char cDelim, char *pcPath, int iSize)
{         
   char *pcResult = NULL;

   while(iSize--)
   {
      if(pcPath[iSize] == cDelim)
      {
         pcResult = &pcPath[iSize];
         break;
      }
   }
   return(pcResult);
}

//
//  Function:  hmx_FindSyncByte
//  Purpose:   Find the SYNC byte
//
//  Parms:     Comm, sync byte, packetsize, window
//  Returns:   File pointer of the new sync position
//
//  Note:      Window=-1 means keep searching till 0x47 found
//
//
static u_int64 hmx_FindSyncByte(HMXCOM *pstComm, u_int8 ubSync, int iPacketSize, int iSearchWindow)
{
   TS_MPEG2   *pstPacket;
   u_int64     llFpSync, llFp, llFpOkee, llAct;
   BOOL        fSynchronizing = TRUE;
   int         iNr, iMaxCount = TS_MAX_SYNC_COUNT;
   int         iPacketSteps, iSmallSteps;
   //pwjh int         iMaxSearch;

   pstPacket = hmx_HandlePacketPointer(pstComm, NULL);
   //
   // We assume the packet size: find the sync byte
   // Update the App on every 1% of the file
   //
   llFp       = pstComm->llCurrentFp;
   llFpSync   = -1;
   llFpOkee   = -1;
   //pwjh iMaxSearch = iSearchWindow;
   if(iSearchWindow == -1)
   {
      iPacketSteps = 0;
      iSmallSteps  = 0;
   }
   else
   {
      iPacketSteps = iPacketSize;
      iSmallSteps  = 1;
   }
   //
   while(fSynchronizing)
   {
      //
      // Read complete MPEG-2 TS packet and verify the SYNC byte is at the right spot
      //
      llAct = pstComm->pclFileTs->Seek(llFp, CFile::begin);
      if(llAct != llFp)
      {
         HMX_LogEventLongHexData(pstComm, -1, "TS:Parser cannot position SPTS !", llFp, TRUE);
      }
      iNr = hmx_ReadPacket(pstComm, pstPacket, iPacketSize);
      if(iNr == iPacketSize)
      {
         if(pstPacket->ubSync == ubSync)
         {
            //
            // This packet seems to be OKee: read <iMaxCount> next packets which should all be OKee too
            //
            if(llFpOkee == -1)
            {
               //
               // We did not have sync before: keep this one 
               //
               llFpOkee = llFp;
            }
            //
            if(iMaxCount == 0)
            {
               //
               // We have resync !
               //
               pstComm->pclFileTs->Seek(llFpSync, CFile::begin);
               fSynchronizing = FALSE;
            }
            else
            {
               if(iMaxCount == TS_MAX_SYNC_COUNT) llFpSync = llFp;
               llFp += iPacketSize;
               iMaxCount--;
               //pwjh iMaxSearch -= iPacketSize;
               iSearchWindow -= iPacketSteps;
            }
         }
         else
         {
            //
            // No SYNC byte found yet: keep parsing byte for byte until we find one
            //
            if(llFpOkee != -1)
            {
               //
               // We had a sync before: continue here if next check fails
               //
               llFp     = llFpOkee;
               llFpOkee = -1;
            }
            llFp++;
            llFpSync  = -1;
            iMaxCount = TS_MAX_SYNC_COUNT;
            //pwjh if(--iMaxSearch <= 0) fSynchronizing = FALSE;
            iSearchWindow -= iSmallSteps;
            if(iSearchWindow == 0) 
            {
               fSynchronizing = FALSE;
            }
         }
      }
      else
      {
         //
         // EOF: Abort search: no SYNC found
         //
         fSynchronizing = FALSE;
         llFpSync       = -1;
      }
   }  // end while(fSynchronizing)
   //
   hmx_HandlePacketPointer(pstComm, pstPacket);
   return(llFpSync);
}

//
//  Function:   hmx_FreePidList
//  Purpose:    Free the PID list
//
//  Parms:      PIDlist
//  Returns:    void
//
static PIDLST *hmx_FreePidList(PIDLST *pstPid)
{
   PIDLST  *pstTmp;
   PIDLST  *pstStr;

   if(pstPid)
   {
      pstStr = pstPid;
      pstPid = pstPid->pstNext;
      while(pstPid != pstStr)
      {
         pstTmp = pstPid;
         pstPid = pstPid->pstNext;
         CVB_SafeFree(pstTmp);
      }
      CVB_SafeFree(pstStr);
  }
   return(NULL);
}

//
//  Function:   hmx_HandleTsProgressBar
//  Purpose:    Handle the TS progress bar
//  Parms:      Comm struct, state
//
//  Returns:    
//
static void hmx_HandleTsProgressBar(HMXCOM *pstComm, PRBAR tBarState)
{
   static int iProgress       = 0;
   static int iNextProgress   = 0;
   static int iPacketCount    = 0;
   static int iProgBar        = 0;
   //
   int64    llTsPackets=0;
   CFile   *pclFile;

   pclFile = pstComm->pclFileTs;
   if(pclFile == NULL) return;
   //
   switch(tBarState)
   {
      case BAR_INIT:
         llTsPackets   = pclFile->GetLength()/pstComm->iPacketSize;  // Number of packets
         iPacketCount  = 0;                                          // 
         iProgBar      = 0;                                          //
         iProgress     = (int) (llTsPackets/HMX_PROGRESS_RES);       // Resolution of the packets progress bar
         iNextProgress = iProgress;                                  //
         SolverMessageToApp(pstComm, WM_SOLVER_PROGBAR, NULL, 0);    // Reset the bar
         break;
      
      case BAR_STEP:
         iPacketCount++;
         if(iPacketCount == iNextProgress)
         {
            // Update the App
            iNextProgress += iProgress;
            iProgBar++;
            SolverMessageToApp(pstComm, WM_SOLVER_PROGBAR, NULL, iProgBar);
         }
         break;
   }
}



