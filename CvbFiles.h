/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          CvbFiles.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Define the Cvb helper functions
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(_CVBFILES_H_)
#define _CVBFILES_H_

typedef ULONGLONG       u_int64;
typedef LONGLONG        int64;
typedef int             int32;
typedef unsigned int    u_int32;
typedef unsigned short  u_int16;
typedef unsigned char   u_int8;
typedef          char   int8;

//
// Global function
//
CString     CVB_GetTimeDateStamp (void);
CString     CVB_GetTimeStamp     (void);
CString     CVB_GetDateStamp     (void);
//
void       *CVB_SafeMalloc       (size_t);
void       *CVB_SafeFree         (void *);
u_int32     CVB_SafeGetMallocSize(void *);

u_int8      CVB_ubyte_get        (u_int8 *, u_int8, u_int8);
u_int8     *CVB_ubyte_put        (u_int8 *, u_int8);
u_int8     *CVB_ubytes_get       (u_int8 *, u_int8 *, int);
u_int8     *CVB_ubytes_put       (u_int8 *, u_int8 *, int);
u_int16     CVB_ushort_get       (u_int8 *, u_int16, u_int8);
u_int32     CVB_ulong_get        (u_int8 *, u_int32, u_int8);
u_int8     *CVB_ulong_put        (u_int8 *, u_int32);
u_int8     *CVB_ushort_put       (u_int8 *, u_int16);
char       *CVB_ascii_to_ulong   (char *, u_int32 *);
char       *CVB_ascii_to_ushort  (char *, u_int16 *);
u_int32     CVB_BCD_to_ulong     (u_int32);
u_int16     CVB_BCD_to_ushort    (u_int16);
u_int32     CVB_ulong_to_BCD     (u_int32);
u_int16     CVB_ushort_to_BCD    (u_int16);
void        CVB_ushort_update    (u_int8 *, u_int16);
int64       CVB_llong_get        (u_int8 *, int64, u_int8);
u_int8     *CVB_llong_put        (u_int8 *, int64);


#endif // !defined(_CVBFILES_H_)
