/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageNts.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for TS Index file 
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(_AFX_HMXPAGENTS_H_)
#define _AFX_HMXPAGENTS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AutoFont.h"
#include "ReadOnlyEdit.h"
#include "afxwin.h"
#include "afxcmn.h"


class CHmxPageNts : public CPropertyPage
{
	DECLARE_DYNCREATE(CHmxPageNts)

// Construction
public:
	CHmxPageNts();
	~CHmxPageNts();

   void           LoadDialog        (void);
   void           StoreDialog       (void);
   void           UpdateDialog      (void);
   void           UpdateRecord      (HMXCOM *, NTSUPD, u_int8 *);
   void           UpdateDump        (void);
   CReadOnlyEdit *GetAddrCel        (int);
   CReadOnlyEdit *GetDataCel        (int);
   //
   static void    RequestWndHandle  (HWND);

// Dialog Data
	//{{AFX_DATA(CHmxPageNts)
	enum { IDD = IDD_PROP_HMX_NTS };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CHmxPageNts)
	public:
	virtual void OnOK                (void);
	virtual void OnCancel            (void);
	protected:
	virtual void DoDataExchange      (CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
   CNvmStorage      *pclNvm;
   CAutoFont        *pclFontMap;                // Font handler for Maps
   HMXCOM           *pstHmxComm;                // Communication srtucture
   int               iNtsRecord;                // Current record

public:
   CReadOnlyEdit     clNtsEditInfile;           // Event name
   int               iNtsBtnApply;              // Apply button
   int               iNtsBtnAutoHd;             // SD/HD button
   // Address fields
   CReadOnlyEdit     clNtsEditBase;
   CReadOnlyEdit     clNtsEditAddr01, clNtsEditAddr02, clNtsEditAddr03, clNtsEditAddr04;
   CReadOnlyEdit     clNtsEditAddr05, clNtsEditAddr06, clNtsEditAddr07, clNtsEditAddr08;
   // Data fields
   CReadOnlyEdit     clNtsEditData01, clNtsEditData02, clNtsEditData03, clNtsEditData04;
   CReadOnlyEdit     clNtsEditData05, clNtsEditData06, clNtsEditData07, clNtsEditData08;

protected:
	// Generated message map functions
	//{{AFX_MSG(CHmxPageNts)
	virtual BOOL      OnInitDialog            ();
	//}}AFX_MSG
   afx_msg void      OnBnClickedUp           ();
   afx_msg void      OnBnClickedDown         ();
   afx_msg void      OnBnClickedAnd          ();
   afx_msg void      OnBnClickedOr           ();
   afx_msg void      OnBnClickedAuto         ();
	DECLARE_MESSAGE_MAP()

public:
   afx_msg LRESULT   OnRcvWindowData         (WPARAM, LPARAM);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AFX_HMXPAGENTS_H_)
