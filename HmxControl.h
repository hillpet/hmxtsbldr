/*  (c) Copyright:  2009  CVB, Confidential Data
**
**  $Workfile:          HmxControl.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Main header file for the HMX TS metafile application
**
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       11Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_AFX_HMXCONTROL_H_)
#define _AFX_HMXCONTROL_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
    #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "HmxEnums.h"
#include "CvbFiles.h"
#include "NvmStorage.h"
#include "Solver.h"
#include "HmxFiles.h"
#include "HmtXlate.h"
#include "SolverThread.h"


//
// Global data
// Handle external declarations
//
#if defined(DEFINE_GLOBALS)
 #define EXTERN
EXTERN   BOOL  bGlobalHmxCompliant     = FALSE;

#else
 #define EXTERN extern

EXTERN   BOOL  bGlobalHmxCompliant;
#endif   //DEFINE_GLOBALS





#endif // !defined(_AFX_HMXCONTROL_H_)
