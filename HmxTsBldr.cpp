/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxTsBldr.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Defines the class behaviors for the application.
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxTsBldr.h"
#include "TsBldrDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMy5050CApp

BEGIN_MESSAGE_MAP(CMyHmxTsApp, CWinApp)
   ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()



/* ====== Functions separator ===========================================
void ____Command_Line____(){}
=========================================================================*/

//
//  Function:  CMyCommandLineInfo
//  Purpose:   Constructor of the Commandline info class
//
//  Parms:     
//  Returns:
//
CMyCommandLineInfo::CMyCommandLineInfo(void)
{
   CCommandLineInfo();
   //
   fCmdLineB = FALSE;   // No Batchmode
   fCmdLineH = FALSE;   // No Help required
   fCmdLineO = FALSE;   // No file overwrites
   fCmdLineR = FALSE;   // No TS Auto resync suppression
   fCmdLineP = FALSE;   // No Default pathname
   fSuccess  = TRUE;
}

//
//  Function:  ParseParam
//  Purpose:   Parser for the command line info
//
//  Parms:     
//  Returns:
//
void CMyCommandLineInfo::ParseParam(LPCTSTR pcParms, BOOL fSwitch, BOOL /*bLast*/)
{
   BOOL fContinue = TRUE;
   
   if (fSwitch)
   {
      if (0 == lstrcmpi(_T("help"), pcParms))
      {
         fCmdLineH = TRUE;
      }
      else
      {
         // the for loop enables 'compound' switches like "/XYZ"
         for(int i = 0; (i < lstrlen(pcParms)) && fSuccess && fContinue; i++)
         {
            switch(pcParms[i])
            {
               case _T('?'):
               case _T('h'):
               case _T('H'):
                  // Cmdline Help
                  fCmdLineH = TRUE;
                  break;

               case _T('o'):
               case _T('O'):
                  // Cmdline Overwrite
                  fCmdLineO = TRUE;
                  break;

               case _T('p'):
               case _T('P'):
                  // Cmdline Path
                  fCmdLineP = TRUE;
                  clStrCmdLineP.Format(_T("%s"), &pcParms[i+2]);
                  break;

               case _T('r'):
               case _T('R'):
                  // Cmdline Auto resync TS suppression
                  fCmdLineR = TRUE;
                  break;

               case _T('b'):
               case _T('B'):
                  // Cmdline Batchmode
                  fCmdLineB = TRUE;
                  break;

               default:
                  fSuccess = fContinue = FALSE;
                  break;
            }
         }
      }
   }
   clStrCmdLineAll = pcParms;
}


/* ====== Functions separator ===========================================
void ____The_App____(){}
=========================================================================*/

// CMy5050CApp construction

CMyHmxTsApp::CMyHmxTsApp()
{
   // TODO: add construction code here,
   // Place all significant initialization in InitInstance
}


// The one and only CMy5050CApp object

CMyHmxTsApp theApp;


// CMy5050CApp initialization

BOOL CMyHmxTsApp::InitInstance()
{
   CTsBuilderDlg   clDlg;
   CString        *pclStr;
   
   //
   // InitCommonControls() is required on Windows XP if an application
   // manifest specifies use of ComCtl32.dll version 6 or later to enable
   // visual styles.  Otherwise, any window creation will fail.
   //
   InitCommonControls();
   CWinApp::InitInstance();
   AfxEnableControlContainer();
   //
   // Create Storage 
   //
   HMX_NewFile(pclNvm);
   //
   // Handle the commandline parameters
   //
   ParseCommandLine(clCmdInfo);
   //
   if( clCmdInfo.IsBatchMode() )       pclNvm->NvmPut(NVM_HMX_BATCHMODE, TRUE);
   if( clCmdInfo.IsOverwriteMode() )   pclNvm->NvmPut(NVM_HMX_OVERWRITE, TRUE);
   if( clCmdInfo.IsAutoResyncMode() )  pclNvm->NvmPut(NVM_TS_NORESYNC,   TRUE);
   if( clCmdInfo.HasDefaultDir() )     
   {
      pclStr = clCmdInfo.GetDefaultDir();
      pclNvm->NvmPut(NVM_HMX_DEFDIR, pclStr);
   }

   m_pMainWnd = &clDlg;
   INT_PTR nResponse = clDlg.DoModal();
   if (nResponse == IDOK)
   {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with OK
   }
   else if (nResponse == IDCANCEL)
   {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with Cancel
   }

   // Since the dialog has been closed, return FALSE so that we exit the
   //  application, rather than start the application's message pump.
   return FALSE;
}
