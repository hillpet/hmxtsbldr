/*  (c) Copyright:  2009  CVB, Confidential Data
**
**  $Workfile:          HmxPageHmt.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for HMT Files
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxPageHmt.h"
#include "ClockApi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CHmxPageHmt, CPropertyPage)

//
// Poor mans solution to keep the main dialog window handle here
//
static HWND    hDialogWnd;

//
// Text translation lookup
//
const XLAT stHmtTextXlate[] =
{  // In       Out
   {  0x05,    '.'    },
   {  0,       '.'    }
};

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageHmt::CHmxPageHmt() : CPropertyPage(CHmxPageHmt::IDD)
{
    ulBaseAddr = 0;
    ulBaseStep = 4;
    pstHmxComm = NULL;
    pclNvm     = (CNvmStorage *) new(CNvmStorage);
    VERIFY(pclNvm);

    pclFontMap = (CAutoFont *)  new(CAutoFont)("Courier New");
    pclFontMap->SetHeight(HMX_MAP_FONT_SIZE);

   //{{AFX_DATA_INIT(CHmxPageHmt)
   //}}AFX_DATA_INIT
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxPageHmt::~CHmxPageHmt()
{
    //
    //  Get rid of the  NVM Storage Map
    //
    if(pclNvm)
    {
        delete pclNvm;
    }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_HMT_EDIT_INFILE,     clHmtEditInfile);
   //
   DDX_Control(pDX, IDC_HMT_EDIT_REC_DURATION, clHmtEditRecDuration);
   DDX_Control(pDX, IDC_HMT_EDIT_PATH,       clHmtEditPathname);
   DDX_Control(pDX, IDC_HMT_EDIT_EVENT,      clHmtEditEventname);
   DDX_Control(pDX, IDC_HMT_EDIT_SERVICE,    clHmtEditServicename);
   DDX_Control(pDX, IDC_HMT_EDIT_SHORT,      clHmtEditShortDescr);
   DDX_Control(pDX, IDC_HMT_EDIT_LONG,       clHmtEditLongDescr);
   //
   DDX_Control(pDX, IDC_HMT_EDIT_BASE,       clHmtEditBase);      // Base address
   //
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR01,     clHmtEditAddr01);    // Address/data fields
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR02,     clHmtEditAddr02);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR03,     clHmtEditAddr03);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR04,     clHmtEditAddr04);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR05,     clHmtEditAddr05);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR06,     clHmtEditAddr06);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR07,     clHmtEditAddr07);
   DDX_Control(pDX, IDC_HMT_EDIT_ADDR08,     clHmtEditAddr08);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA01,     clHmtEditData01);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA02,     clHmtEditData02);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA03,     clHmtEditData03);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA04,     clHmtEditData04);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA05,     clHmtEditData05);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA06,     clHmtEditData06);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA07,     clHmtEditData07);
   DDX_Control(pDX, IDC_HMT_EDIT_DATA08,     clHmtEditData08);
   //{{AFX_DATA_MAP(CHmxPageHmt)
   //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHmxPageHmt, CPropertyPage)
   //{{AFX_MSG_MAP(CHmxPageHmt)
   //}}AFX_MSG_MAP
   ON_BN_CLICKED(IDC_HMT_BTN_UP,             OnBnClickedUp)
   ON_BN_CLICKED(IDC_HMT_BTN_DOWN,           OnBnClickedDown)
   ON_BN_CLICKED(IDC_HMT_BTN_SAVE,           OnBnClickedSave)
   ON_MESSAGE(WM_SOLVER_DATA,                OnRcvWindowData)
END_MESSAGE_MAP()




//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CHmxPageHmt::OnInitDialog()
{
   CPropertyPage::OnInitDialog();

   LoadDialog();
   //
   // Send back the window handle for this dialog, so update messages arrive here
   //
   HWND hThisWnd = this->GetSafeHwnd();
   if(hDialogWnd)
   {
      ::PostMessage(hDialogWnd, WM_SOLVER_HANDLE, (WPARAM) hThisWnd, (LPARAM) HMX_WND_HMT);
   }
   UpdateData(FALSE);      // data -> dialog window
   return TRUE;            // return TRUE unless you set the focus to a control
                           // EXCEPTION: OCX Property Pages should return FALSE
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::OnOK()
{
   UpdateData(TRUE);       // data <-- dialog window

   StoreDialog();

   CPropertyPage::OnOK();
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::OnCancel()
{
   CPropertyPage::OnCancel();
}

//
//  Function:  OnBnClickedDown
//  Purpose:   Button down 
//
//  Parms:     
//  Returns:   
//
void CHmxPageHmt::OnBnClickedDown()
{
   if(ulBaseAddr >= (HMX_HMT_NUM_ADDR*ulBaseStep)) ulBaseAddr -= (HMX_HMT_NUM_ADDR*ulBaseStep);
   UpdateDialog();
}

//
//  Function:  OnBnClickedUp
//  Purpose:   Button up
//
//  Parms:     
//  Returns:   
//
void CHmxPageHmt::OnBnClickedUp()
{
   if(pstHmxComm)
   {
      if(ulBaseAddr < pstHmxComm->lHmtSize - (HMX_HMT_NUM_ADDR*ulBaseStep)) ulBaseAddr += (HMX_HMT_NUM_ADDR*ulBaseStep);
      UpdateDialog();
   }
}

//
//  Function:  OnBnClickedSave
//  Purpose:   Button down 
//
//  Parms:     
//  Returns:   
//
void CHmxPageHmt::OnBnClickedSave()
{
   u_int8  *pubHmtCat;
   int      iDuration;
   CString  clStr;

   // UpdateData(TRUE);       // data <-- dialog window
   //
   // Store all fields back into the catalog stuctures
   //
   if(pstHmxComm && pclNvm)
   {
      pubHmtCat = pstHmxComm->pubHmtCat;
      if(pubHmtCat)
      {
         // HMT_START            Start HMT buffer
         // HMT_TIME_START       DateTime stamp start
         // HMT_TIME_END         DateTime stamp end
         //
         clHmtEditRecDuration.GetWindowText(clStr);
         if ( sscanf(clStr.GetBuffer(), "%d", &iDuration ) == 1 )
         {
            pstHmxComm->pstHmtCat->ulEndTime = pstHmxComm->pstHmtCat->ulStartTime + iDuration;
         }
         
         // HMT_PATHNAME         Linux pathname on /media/sda1/*
         clHmtEditPathname.GetWindowText(*pstHmxComm->pstHmtCat->pclFilePath);
         //
         // HMT_EVENTNAME        Event name
         clHmtEditEventname.GetWindowText(*pstHmxComm->pstHmtCat->pclEvent);
         //
         // HMT_SERVICENAME      Service name
         clHmtEditServicename.GetWindowText(*pstHmxComm->pstHmtCat->pclService);
         //
         // HMT_PATPID           Start of the PID list
         //
         // HMT_SHORTDESCR       Short event descriptor start
         //
         // HMT_LONGDESCR_SIZE   Long event descriptor size
         // HMT_LONGDESCR_HDR    Long event descriptor Header
         // HMT_LONGDESCR        Long event descriptor start
      }
   }
}

//
//  Function:  OnRcvWindowData
//  Purpose:   Message from the SOLVER Thread: new data is in
//
//  Parms:     WPARAM = 0
//             TPARAM = 0
//  Returns:   0
//
LRESULT CHmxPageHmt::OnRcvWindowData(WPARAM tWp, LPARAM tLp)
{
   pstHmxComm = (HMXCOM *) tWp;
   UpdateDialog();
   return(0);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::LoadDialog()
{
    //
    // Load the PAGE data from storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::StoreDialog()
{
    //
    // Save the PAGE data to storage
    //
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CHmxPageHmt::UpdateDialog()
{
   HMTREC  *pstHmtCat;
   HMTPF   *pstPf;
   int      iDuration;
   CString  clFile, clStr, clExt;

   if(pstHmxComm && pclNvm)
   {
      pstHmtCat = pstHmxComm->pstHmtCat;
      if(pstHmtCat)
      {
         //
         // Event name field
         HMX_GetFromStorage(pclNvm, NVM_HMX_FILENAME,    &clStr);
         HMX_GetFromStorage(pclNvm, NVM_HMX_FILEEXT_HMT, &clExt);
         clFile.Format(_T("%s.%s"), clStr.GetBuffer(), clExt.GetBuffer());
         clHmtEditInfile.SetWindowText(clFile.GetBuffer());
         //
         // Recording duration
         iDuration = pstHmxComm->pstHmtCat->ulEndTime - pstHmxComm->pstHmtCat->ulStartTime;
         clStr.Format(_T("%d"), iDuration);
         clHmtEditRecDuration.SetWindowText(clStr.GetBuffer());

         // Catalog pathname
         clHmtEditPathname.SetWindowText(pstHmtCat->pclFilePath->GetBuffer());
         //
         // Catalog Eventname
         clHmtEditEventname.SetWindowText(pstHmtCat->pclEvent->GetBuffer());
         //
         // Catalog Service name
         clHmtEditServicename.SetWindowText(pstHmtCat->pclService->GetBuffer());
         //
         // Find the correct p/f event in the list
         // Catalog Short Event descriptor
         pstPf = HMT_FindCurrentEvent(pstHmxComm);
         if(pstPf)
         {
            clHmtEditShortDescr.SetWindowText(pstPf->pclEventName->GetBuffer());
            //
            // Catalog Long Event descriptor
            clHmtEditLongDescr.SetWindowText(pstPf->pclEventDescr->GetBuffer());
         }
         //
         // Catalog Address/Data fields
         UpdateDump(pstHmxComm->pubHmtRaw);
      }
   }
   UpdateData(FALSE);      // data -> dialog window

}

//
//  Function:   UpdateDump
//  Purpose:    Update the inspect and change fields
//  Parms:      Buffer start
//  Returns:    
//
void CHmxPageHmt::UpdateDump(u_int8 *pubBuffer)
{
   int            iIdx;
   CString        clStr;
   u_int32        ulAddr, ulValue;
   CReadOnlyEdit *pclAddr;
   CReadOnlyEdit *pclData;

   ulAddr = ulBaseAddr;
   //
   clStr.Format(_T("%d"), ulAddr);
   clHmtEditBase.SetWindowText(clStr);
   //
   for(iIdx=0; iIdx<HMX_HMT_NUM_ADDR; iIdx++)
   {
      pclAddr = GetAddrCel(iIdx);
      pclData = GetDataCel(iIdx);
      pclAddr->SetFont(pclFontMap);
      pclData->SetFont(pclFontMap);
      //
      ulValue = CVB_ulong_get(&pubBuffer[ulAddr], 0xffffffffL, 0);
      clStr.Format(_T("%08X"), ulAddr);
      pclAddr->SetWindowText(clStr);
      clStr.Format(_T("%08X"), ulValue);
      pclData->SetWindowText(clStr);
      //
      // Dump some timestamps to the logfile
      ulAddr += 4;
   }
}

//
//  Function:  GetAddrCel
//  Purpose:   Retrieve a Address cell pointer
//
//  Parms:     Index 0..?
//  Returns:   Pointer to field
//
CReadOnlyEdit *CHmxPageHmt::GetAddrCel(int iIdx)
{
   const CReadOnlyEdit *pclHmtAddr[HMX_HMT_NUM_ADDR] =
   {
      &clHmtEditAddr01, &clHmtEditAddr02, &clHmtEditAddr03, &clHmtEditAddr04,
      &clHmtEditAddr05, &clHmtEditAddr06, &clHmtEditAddr07, &clHmtEditAddr08
   };
   return((CReadOnlyEdit*)pclHmtAddr[iIdx]);
};

//
//  Function:  GetDataCel
//  Purpose:   Retrieve a Data cell pointer
//
//  Parms:     Index 0..?
//  Returns:   Pointer to field
//
CReadOnlyEdit *CHmxPageHmt::GetDataCel(int iIdx)
{
   const CReadOnlyEdit *pclHmtData[HMX_HMT_NUM_ADDR] =
   {
      &clHmtEditData01, &clHmtEditData02, &clHmtEditData03, &clHmtEditData04,
      &clHmtEditData05, &clHmtEditData06, &clHmtEditData07, &clHmtEditData08
   };
   //
   return((CReadOnlyEdit*)pclHmtData[iIdx]);
};

//
//  Function:  HandleNonPrintable
//  Purpose:   Handle all non printable chars for now
//
//  Parms:     Translate table, Buffer, buffer size
//  Returns:   void
//
void CHmxPageHmt::HandleNonPrintable(const XLAT *pstXlate, char *pcBuffer, int iNr)
{
   int   iIdx;
   
   while(iNr--)
   {
      iIdx = 0;
      while( pstXlate[iIdx].cIn )
      {
         if(pstXlate[iIdx].cIn == *pcBuffer) 
         {
            *pcBuffer = pstXlate[iIdx].cOut;
            break;
         }
         iIdx++;
      }
      pcBuffer++;
   }
}


//
//  Function:  RequestWndHandle
//  Purpose:   Called from the Main dialog window to save its window handle
//
//  Parms:     void
//  Returns:   void
//
void CHmxPageHmt::RequestWndHandle(HWND hWnd)
{
   //
   // Get the window handle and send it to the solver thread for communicating
   // Accept dropped files
   //
   hDialogWnd = hWnd;
}

