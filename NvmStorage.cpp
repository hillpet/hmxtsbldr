/*  (c) Copyright:  2007 CVB  
**
**  $Workfile:          NvmStorage.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Implementation of the NVM permanent storage class
**
**  Entries:       
**                      
**                      
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       22Feb2003
**
 *  Revisions:
 *    $Log:   $
 *    12Apr2009: PwjH-Removed BOOL, added u_int16 and int
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "CvbFiles.h"
#include "NvmStorage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



static CString m_WatchMessage[NUM_WNVMS];

IMPLEMENT_SERIAL(CNvmStorage, CObject, 0)

//
// This is the storage class for Non volatile data in any form. All data is stored
// into a CString array in ASCII format. 
//
//  To store/retrieve :
//
//  Cstring data:       NvmPut(ID, clString)
//                      NvmGet(ID, &clString)
//  u_int8:             NvmPut(ID, iNr)
//                      NvmGet(ID, &iNr)
//  u_int16:            NvmPut(ID, iNr)
//                      NvmGet(ID, &iNr)
//  int:                NvmPut(ID, iNr)
//                      NvmGet(ID, &iNr)
//  ASCIIz              NvmPut(ID, pzChar)
//                      NvmGet(ID, pzChar) NOTE: pzChar MUST be preformatted to hold the data (with non-zero data).
//  Array of u_int8:    NvmPut(ID, pubData, size)
//                      NvmGet(ID, pubData, size)
//


//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CNvmStorage::CNvmStorage()
{
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CNvmStorage::~CNvmStorage()
{

}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , CString *pclStr)
{
   if(iNvmNr < NUM_WNVMS)
   {
      m_WatchMessage[iNvmNr] = *pclStr;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , int iNr)
{
   if(iNvmNr < NUM_WNVMS)
   {
      CString clStr;

      clStr.Format(_T("%x"), iNr);

      m_WatchMessage[iNvmNr] = clStr;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , u_int8 ubNr)
{

   if(iNvmNr < NUM_WNVMS)
   {
      CString s;

      s.Format(_T("%x"), ubNr);

      m_WatchMessage[iNvmNr] = s;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , u_int16 usNr)
{

   if(iNvmNr < NUM_WNVMS)
   {
      CString s;

      s.Format(_T("%x"), usNr);

      m_WatchMessage[iNvmNr] = s;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , char *pcData)
{
   if(iNvmNr < NUM_WNVMS)
   {
      m_WatchMessage[iNvmNr] = pcData;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::NvmPut(int iNvmNr , u_int8 *pubNr, int iSize)
{

   if(iNvmNr < NUM_WNVMS)
   {
      CString s, stotal;
        int     iNr=0;

        while(iSize--)
        {
          s.Format(_T("%02x,"), (u_int8) pubNr[iNr++]);
            stotal += s;
        }
        //
        // s = "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
        //
         m_WatchMessage[iNvmNr] = stotal;
   }
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, int *piDest)
{
   if(piDest && (iNvmNr < NUM_WNVMS) )
   {
      int iNr, iResult;

      *piDest = 0;

      iNr = sscanf(m_WatchMessage[iNvmNr].GetBuffer(10), "%x", &iResult);
      if(iNr == 1)
      {
         *piDest = iResult;
         return(TRUE);
      }
   }
   return(FALSE);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, u_int8 *pubDest)
{
   if(pubDest && (iNvmNr < NUM_WNVMS) )
   {
      int iNr, iResult;

      *pubDest = 0;

      iNr = sscanf(m_WatchMessage[iNvmNr].GetBuffer(10), "%x", &iResult);
      if(iNr == 1)
      {
         *pubDest = (u_int8) iResult;
         return(TRUE);
      }
   }
   return(FALSE);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, u_int16 *pusDest)
{
   if(pusDest && (iNvmNr < NUM_WNVMS) )
   {
      int iNr, iResult;

      *pusDest = 0;

      iNr = sscanf(m_WatchMessage[iNvmNr].GetBuffer(10), "%x", &iResult);
      if(iNr == 1)
      {
         *pusDest = (u_int16) iResult;
         return(TRUE);
      }
   }
   return(FALSE);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, char *pcDest)
{
   BOOL     fCC = FALSE;
   char    *pcBuffer;
   size_t   iSize = 0;

   //
   // Get NVM data in the user buffer. The buffer MUST be pre-formatted with 0x20 to take the data.
   //
   if(pcDest && (iNvmNr < NUM_WNVMS) )
   {
      // Count the destination size
      pcBuffer = pcDest;
      while(*pcBuffer++ == 0x20) iSize++;
      //copy the storage into the buffer
      strncpy(pcDest, m_WatchMessage[iNvmNr], iSize);
      fCC = TRUE;
   }
   return(fCC);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, u_int8 *pcDest, int iSize)
{
    BOOL fCC = TRUE;

   if(pcDest && (iNvmNr < NUM_WNVMS) )
   {
        int     iNr;
        int     iResult;
        char   *pcBuffer;

        pcBuffer = m_WatchMessage[iNvmNr].GetBuffer(0);
        //
        // pcBuffer -> "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
        //
        while(fCC && iSize--)
        {
          iNr = sscanf(pcBuffer, "%x", &iResult);
          if(iNr == 1)
          {
             *pcDest++ = (char) (iResult & 0xff);
                pcBuffer +=3;
          }
            else
            {
                fCC = FALSE;
            }
        }
   }
   return(fCC);
}


//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CNvmStorage::NvmGet(int iNvmNr, CString *pclStr)
{
   if(pclStr && (iNvmNr < NUM_WNVMS) )
   {
      *pclStr = "";

      *pclStr = m_WatchMessage[iNvmNr];
      return(TRUE);
   }
   return(FALSE);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
void CNvmStorage::Serialize(CArchive &ar)
{
   int      iNvmNr=NVM_VERSION; 

   CObject::Serialize(ar);

   if( ar.IsStoring() )
   {
      while( iNvmNr<NUM_WNVMS )
      {
         ar << m_WatchMessage[iNvmNr++];
      }
   }
   else
   {
      while( iNvmNr<NUM_WNVMS )
      {
         ar >> m_WatchMessage[iNvmNr++];
      }
   }
}

//
//  Function:   NvmGetSize
//  Purpose:    Returs the size of the object stored into NVM (including the \0 !!)
//  Parms:      
//  Returns:    Size
//
int CNvmStorage::NvmGetSize(int iNvmNr)
{
    int iSize = 0;

   if(iNvmNr < NUM_WNVMS)
   {
        iSize = m_WatchMessage[iNvmNr].GetLength()+1;
   }
   return(iSize);
}
