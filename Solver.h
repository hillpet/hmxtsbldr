/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:  Solver.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the Solver class
**
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       12Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/
#if !defined(_Solver_h_)
#define _Solver_h_


class CSolver : public CWnd
{
// Construction
public:
   CSolver();
   virtual ~CSolver();

// Operations
public:
   BOOL        InitSolver();
   UINT_PTR    iSolverTimer;
   HMXCMD      tCmd;
   int         iSession;
   int         iCounter;
   volatile BOOL fDoExit;

// Generated message map functions
protected:
   //{{AFX_MSG(CSolver)
   afx_msg void      OnTimer        (UINT nIDEvent);
   //}}AFX_MSG

   // ===================
   // Private WM messages
   // ===================
   afx_msg LRESULT   OnSolverCmd    (WPARAM, LPARAM);
   afx_msg LRESULT   OnSolverLog    (WPARAM, LPARAM);
   afx_msg LRESULT   OnSolverExit   (WPARAM, LPARAM);

   DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_Solver_h_)
