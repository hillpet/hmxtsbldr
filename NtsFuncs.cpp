/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          NtsFiles.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            NTS index file functions
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "ClockApi.h"

//
// Local prototypes
//
static void nts_Init            (HMXCOM *);


/* ====== Functions separator ===========================================
void ___api_functions____(){}
=========================================================================*/
//
//  Function:   NTS_Cleanup
//  Purpose:    Cleanup the NTS record structure
//  Parms:      pstComm
//
//  Returns:    
//
void NTS_Cleanup(HMXCOM *pstComm)
{
   if(pstComm == NULL) return;

   if(pstComm->pubNts) pstComm->pubNts = (u_int8 *) CVB_SafeFree(pstComm->pubNts);
   if(pstComm->pstNts) pstComm->pstNts = (NTSREC *) CVB_SafeFree(pstComm->pstNts);
}


//
//  Function:   NTS_AddIndexRecord
//  Purpose:    Add a record to the NTS index file
//  Parms:      pstComm
//
//  Returns:    int 0=error
//
BOOL NTS_AddIndexRecord(HMXCOM *pstComm)
{
   BOOL     fCc = TRUE;
   NTSREC  *pstNts;
   u_int8   pubRecord[NTS_IDX_SIZE];
   u_int8  *pubData;

   //
   // Write the current assembled NTS index record to file
   //
   pubData = pubRecord;
   //
   memset(pubData, 0x00, NTS_IDX_SIZE);
   pstNts  = pstComm->pstNts;
   //
   pubData = CVB_ulong_put(pubData,  pstNts->ulField1b);
             CVB_ubyte_put(pubRecord,pstNts->ubField1a);  // Overlay frame type
   pubData = CVB_ushort_put(pubData, pstNts->usField2a);
   pubData = CVB_ubyte_put(pubData,  pstNts->ubField2b);
   pubData = CVB_ubyte_put(pubData,  pstNts->ubField2c);
   pubData = CVB_llong_put(pubData,  pstNts->llField34);
   pubData = CVB_ulong_put(pubData,  pstNts->ulField5);
   pubData = CVB_ulong_put(pubData,  pstNts->ulField6);
   pubData = CVB_ulong_put(pubData,  pstNts->ulField7);
   pubData = CVB_ulong_put(pubData,  pstNts->ulField8);
   //
   // Write struct to stream
   //
   NTS_WriteNtsFile(pstComm, (char *)pubRecord, NTS_IDX_SIZE);
   //
   // For HD we need an additional record
   //
   // HMX_GetFromStorage(pstComm->pclNvm, NVM_NTS_AUTOHD, &iProfile);
   // switch(iProfile)
   // { };
   //
   if( HMX_CheckHdtv(pstComm) )
   {
      pubData = pubRecord;
      memset(pubData, 0x00, NTS_IDX_SIZE);
      //
      switch(pstNts->ubFrType)
      {
         case FRAME_TYPE_I:
            pubData = CVB_ulong_put(pubData, pstNts->ulField9);
            pubData = CVB_ulong_put(pubData, pstNts->ulField10);
            break;

         default:
            pubData = CVB_ulong_put(pubData, pstNts->ulField9);
            pubData = CVB_ulong_put(pubData, pstNts->ulField10);
            break;
      }
      //
      // Write 2nd record
      //
      NTS_WriteNtsFile(pstComm, (char *)pubRecord, NTS_IDX_SIZE);
   }
   //pwjh switch(pstComm->tContentType)
   //pwjh {
   //pwjh    case NTS_DEF_AUTO:
   //pwjh    case NTS_DEF_SD:
   //pwjh    case NTS_DEF_AUTO_SD:
   //pwjh       break;
   //pwjh       
   //pwjh    case NTS_DEF_HD:
   //pwjh    case NTS_DEF_AUTO_HD:
   //pwjh       pubData = pubRecord;
   //pwjh       memset(pubData, 0x00, NTS_IDX_SIZE);
   //pwjh       //
   //pwjh       switch(pstNts->ubFrType)
   //pwjh       {
   //pwjh          case FRAME_TYPE_I:
   //pwjh             pubData = CVB_ulong_put(pubData, pstNts->ulField9);
   //pwjh             pubData = CVB_ulong_put(pubData, pstNts->ulField10);
   //pwjh             break;
   //pwjh 
   //pwjh          default:
   //pwjh             pubData = CVB_ulong_put(pubData, pstNts->ulField9);
   //pwjh             pubData = CVB_ulong_put(pubData, pstNts->ulField10);
   //pwjh             break;
   //pwjh       }
   //pwjh       //
   //pwjh       // Write 2nd record
   //pwjh       //
   //pwjh       NTS_WriteNtsFile(pstComm, (char *)pubRecord, NTS_IDX_SIZE);
   //pwjh       break;
   //pwjh }
   
   pstComm->ulNumFrames++;
   return(fCc);
}

//
//  Function:   NTS_CloseNtsFile
//  Purpose:    Close the NTS (index) file
//  Parms:      pstComm
//
//  Returns:    void
//
void NTS_CloseNtsFile(HMXCOM *pstComm)
{
   if( pstComm->pclFileNts && pstComm->fFileNts)  pstComm->pclFileNts->Close();
   pstComm->fFileNts = 0;
}

//
//  Function:   NTS_OpenNtsFile
//  Purpose:    Open a NTS (Index) file for read or write
//  Parms:      pstComm, mode R(ead), W(rite), C(reate)
//
//  Returns:    0=Error
//
int NTS_OpenNtsFile(HMXCOM *pstComm, char cMode)
{
   pstComm->fFileNts = HMX_OpenFile(pstComm->pclFileNts, pstComm->pclNvm, NVM_HMX_FILEEXT_NTS, cMode);
   return(pstComm->fFileNts);
}

//
//  Function:   NTS_ParseNtsFile
//  Purpose:    Parse the index NTS-file
//  Parms:      Comm struct
//
//  Returns:    SPTSCC completion code
//
SPTSCC NTS_ParseNtsFile(HMXCOM *pstComm)
{
   SPTSCC   tCc=SPTS_CC_BAD_FILE;
   CFile   *pclFile;

   if(pstComm == NULL) return(tCc);
   pclFile = pstComm->pclFileNts;
   if(pclFile == NULL) return(tCc);

    if(pstComm->fFileNts)
    {
        pstComm->iNtsNrRecords = (int) pclFile->GetLength()/NTS_IDX_SIZE;
    }
    else
    {
        HMX_LogEvent(pstComm, 0, "NTS:Missing *.nts Index file", TRUE);
        tCc = SPTS_CC_OKEE;
    }
    //
    // Get us a buffer to hold a record: Stream and Structure
    //
    nts_Init(pstComm);
    return(tCc);   
}

//
//  Function:   NTS_ReadNtsFile
//  Purpose:    Read data from the NTS (Index) file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number read
//
int NTS_ReadNtsFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileNts) return( HMX_ReadFile(pstComm->pclFileNts, pcBuffer, iSize));
   else                       return(0);
}

//
//  Function:   NTS_ReadNtsRecord
//  Purpose:    Read a record in the NTS (Index) file
//  Parms:      pstComm, Buffer, record 0..iNtsNrRecords
//
//  Returns:    TRUE if OKee
//
BOOL NTS_ReadNtsRecord(HMXCOM *pstComm, u_int8 *pubBuffer, int iRecord)
{
   int   iRead; 
   long  lSeek, lAct=-1;

   if(pstComm == NULL) return(0);
   else if(pstComm->fFileNts)
   {
      lSeek = (long) iRecord * NTS_IDX_SIZE;
      if(pstComm->fFileNts)
      {
         lAct  = (long) pstComm->pclFileNts->Seek(lSeek, CFile::begin);

         if(lSeek == lAct)
         {
            // OKee positioned: read the record
            iRead = HMX_ReadFile(pstComm->pclFileNts, (char *)pubBuffer, NTS_IDX_SIZE);
            return(iRead == NTS_IDX_SIZE);
         }
      }
   }
   return(FALSE);
}

//
//  Function:   NTS_WriteNtsRecord
//  Purpose:    Write a record in the NTS (Index) file
//  Parms:      pstComm, Buffer, record 0..iNtsNrRecords
//
//  Returns:    TRUE if OKee
//
BOOL NTS_WriteNtsRecord(HMXCOM *pstComm, u_int8 *pubBuffer, int iRecord)
{
   int   iWritten; 
   long  lSeek, lAct=-1;

   if(pstComm == NULL) return(0);
   else if(pstComm->fFileNts)
   {
      lSeek = (long) iRecord * NTS_IDX_SIZE;
      if(pstComm->fFileNts)
      {
         lAct  = (long) pstComm->pclFileNts->Seek(lSeek, CFile::begin);

         if(lSeek == lAct)
         {
            // OKee positioned: write the record
            iWritten = HMX_WriteFile(pstComm->pclFileNts, (char *)pubBuffer, NTS_IDX_SIZE);
            return(iWritten == NTS_IDX_SIZE);
         }
      }
   }
   return(FALSE);
}

//
//  Function:   NTS_WriteNtsFile
//  Purpose:    Write data to the NTS (Index) file
//  Parms:      pstComm, Buffer, size
//
//  Returns:    Number written
//
int NTS_WriteNtsFile(HMXCOM *pstComm, char *pcBuffer, int iSize)
{
   if(pstComm == NULL)        return(0);
   else if(pstComm->fFileNts) return( HMX_WriteFile(pstComm->pclFileNts, pcBuffer, iSize));
   else                       return(0);
}


/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/

//
//  Function:   nts_Init
//  Purpose:    Initialize the NTS data structures
//  Parms:      pstComm
//
//  Returns:    
//
static void nts_Init(HMXCOM *pstComm)
{
   u_int8  *pubNts;
   NTSREC  *pstNts;

   // Stream data
   pubNts = pstComm->pubNts;
   if(pubNts == NULL)
   {
      pubNts = (u_int8 *) CVB_SafeMalloc(NTS_IDX_SIZE);
      pstComm->pubNts = pubNts;
   }
   //
   // Structure data
   //
   pstNts = pstComm->pstNts;
   if(pstNts == NULL)
   {
      pstNts = (NTSREC *) CVB_SafeMalloc(sizeof(NTSREC));
      pstComm->pstNts = pstNts;
   }
}


