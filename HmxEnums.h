/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxEnums.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Define the Hmx enums
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       11Apr2009
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_HMXENUMS_H_)
#define _HMXENUMS_H_


//#define FEATURE_REMOVE_ISO_DESCR  // HMT: Remove ISO language desrciptors
                                    //      ISO-8859-9 (0x05)
                                    //      ISO-639
//#define FEATURE_REPLACE_CRLF      // HMT: Replace all <'.'><' '> combinations by CRLF and visa versa
//#define FEATURE_ADD_LINEBREAKS    // HMT: Add CRLF to wrap text in short/long descriptors

//#define FEATURE_DURATION_FROM_GOP // Take the duration from the GOP
#define FEATURE_DURATION_FROM_PTS   // Take the duration from the Video PTS
//
//#define FEATURE_WRITE_DEBUG       // Enable debug output
//#define  FEATURE_SKIP_PES_LENGTH  // Skip all data within the PES packet length

#ifdef  FEATURE_WRITE_DEBUG
   #define WRITEDEBUG(a,b,c,d)   HMX_WriteDebug(a,b,c,d)
#else
   #define WRITEDEBUG(a,b,c,d)
#endif

#define HMX_VERSION              "v2.21-beta"
#define CRLF                     "\r\n"

#define HMX_NAME_LEN             256
#define HMX_LOGFILE_LEN          512
//
#define HMX_STS_FONT_SIZE        14
#define HMX_MAP_FONT_SIZE        14
#define HMX_BTN_FONT_SIZE        14
#define HMX_IDX_FONT_SIZE        20
//
#define HMX_PROGRESS_RES         100               // Number of steps for progress bar
#define HMX_HMT_NUM_ADDR         8                 // Number of addr/data fields in the HMT property page
#define HMX_NTS_NUM_ADDR         8                 // Number of addr/data fields in the NTS property page
//
#define HMX_COMPAT_TS_EXIST      0x00000001        // TS exists
#define HMX_COMPAT_NTS_EXIST     0x00000002        // NTS index file already exists
#define HMX_COMPAT_HMT_EXIST     0x00000004        // HMT Catalog file already exists
#define HMX_COMPAT_NTS_OK        0x00000008        // NTS index is correct 
#define HMX_COMPAT_HMT_OK        0x00000010        // HMT index is correct 
#define HMX_COMPAT_EXT_OK        0x00000020        // SPTS extension is OKee
#define HMX_COMPAT_TIME_OK       0x00000040        // Time stamp is OKee
#define HMX_COMPAT_DATE_OK       0x00000080        // Date stamp is OKee
#define HMX_COMPAT_GET_MASK      0x0000ffff        // Get mask
//
#define HMX_COMPAT_NAME_OK       (HMX_COMPAT_DATE_OK|HMX_COMPAT_TIME_OK|HMX_COMPAT_EXT_OK)
//
#define HMX_COMPAT_COMPLIANT     0x00008000        // Filename is HMX compliant
//
#define HMX_COMPAT_TS_RENAME     0x00010000        // DO: Rename extension into <*.ts>
#define HMX_COMPAT_NTS_OVERWRITE 0x00020000        // DO: Overwrite *.nts file
#define HMX_COMPAT_HMT_OVERWRITE 0x00040000        // DO: Overwrite *.hmt file
#define HMX_COMPAT_SET_MASK      0xffff0000        // Set mask


const COLORREF CLOUDBLUE         = RGB(128, 184, 223);
const COLORREF WHITE             = RGB(255, 255, 255);
const COLORREF BLACK             = RGB(  1,   1,   1);
const COLORREF DKGRAY            = RGB(128, 128, 128);
const COLORREF LTGRAY            = RGB(192, 192, 192);
const COLORREF YELLOW            = RGB(255, 255,   0);
const COLORREF DKYELLOW          = RGB(128, 128,   0);
const COLORREF LTRED             = RGB(255, 128, 128);
const COLORREF RED               = RGB(255,   0,   0);
const COLORREF DKRED             = RGB(128,   0,   0);
const COLORREF BLUE              = RGB(  0,   0, 255);
const COLORREF DKBLUE            = RGB(  0,   0, 128);
const COLORREF CYAN              = RGB(  0, 255, 255);
const COLORREF DKCYAN            = RGB(  0, 128, 128);
const COLORREF LTGREEN           = RGB(128, 255, 128);
const COLORREF GREEN             = RGB(128, 255, 128);
const COLORREF DKGREEN           = RGB(  0, 128,   0);
const COLORREF MAGENTA           = RGB(255,   0, 255);
const COLORREF DKMAGENTA         = RGB(128,   0, 128);

typedef enum WMESG
{  //                                     // Direction            Comment
   WM_SOLVER_NONE = WM_USER + 100,        // Dlg    -> Solver    Pass Solver win handle
   WM_SOLVER_HANDLE,                      // Dlg    -> Solver    Pass Solver win handle
   WM_SOLVER_CMD,                         // Dlg    -> Solver    Send CMD
   WM_SOLVER_LOG,                         // Dlg    -> Solver    Start/Stop logfile
   WM_SOLVER_UPDATE,                      // Solver -> Dlg       Update counts
   WM_SOLVER_READY,                       // Solver -> Dlg       Solver has finished
   WM_SOLVER_PROGBAR,                     // Solver -> Dlg       Solver has progressbar update
   WM_SOLVER_COMM,                        // Solver -> Dlg       Solver has HMXCOM structure ptr
   WM_SOLVER_DATA,                        // Solver -> Dlg       Solver has event data
   WM_SOLVER_EXIT,                        // Dlg    -> Solver    Send exit

   NUM_WM_SOLVER_MESSAGES
}  WMESG;

//
// TS STH states
//
typedef enum STHTS
{                                         // Statehandler SPTS packet parser
   STH_TS_INIT = 0,                       // Init
   STH_TS_PAT,                            // Wait for PAT
   STH_TS_PES,                            // Parse PES
   STH_TS_PSI,                            //       PSI
                                          // 
   NUM_STHTS                              // 
}  STHTS;

//
// HMX commands
//
typedef enum HMXCMD
{
   HMX_CMD_NONE = 0,
   HMX_CMD_PARSE,
   HMX_CMD_CHECK,
   HMX_CMD_BUILD,
   HMX_CMD_BUILD_BATCH,
   HMX_CMD_TIMECODES,
   HMX_CMD_NO_TIMECODES,
   HMX_CMD_STOP,
   HMX_CMD_EXIT,

   NUM_HMX_CMDS
}  HMXCMD;

//
// Status for HMX compliance checks
//
typedef enum HMXST
{
   HMX_STATUS_INIT = 0,
   HMX_STATUS_COMPLIANT,
   HMX_STATUS_MISSING_TS,
   HMX_STATUS_MISSING_HMT,
   HMX_STATUS_MISSING_NTS,
   HMX_STATUS_NOT_COMPLIANT,

   NUM_HMX_STATS
}  HMXST;

//
// HMX compliancy steps
//
typedef enum HMXCOMP
{
   HMX_COMP_INIT = 0,
   HMX_COMP_EXT,
   HMX_COMP_TIME,
   HMX_COMP_DATE,
   HMX_COMP_EVENT,
   HMX_COMP_FILE,
   HMX_COMP_DIR,
   HMX_COMP_OKEE,

   NUM_HMX_COMPS
}  HMXCOMP;

//
// HMX dialog window handles
//
typedef enum HMXWND
{
   HMX_WND_TERM = 0,             // No messages
   HMX_WND_DLG,                  // Main dialog window
   HMX_WND_SLV,                  // Solver      window
   HMX_WND_GEN,                  // Generic     window
   HMX_WND_TS,                   // SPTS        window
   HMX_WND_NTS,                  // NDX         window
   HMX_WND_HMT,                  // HMT         window
                                 // 
   NUM_HMX_WNDS                  // Number of dialog windows
}  HMXWND;

//
// HMX dialog status messages. 
// Keep in sync with the list in TsBldrDlg.cpp: pcHmxMessages[]
//
typedef enum HMXMSGID
{
   HMX_MSGID_NONE = 0,           // No messages
   HMX_MSGID_SCAN_ERROR,         // Error during scan
   HMX_MSGID_SCAN_STARTED,       // 
   HMX_MSGID_SCAN_PACKETS,       //
   HMX_MSGID_SCAN_STOPPED,       // 
                                 // 
   NUM_HMX_MSGID                 // Number of messages
}  HMXMSGID;

//
// SPTS parsing Completion codes
//
typedef enum SPTSCC
{
   SPTS_CC_OKEE= 0,              // OK completion
   SPTS_CC_NO_SYNC,              // No sync detected
   SPTS_CC_BAD_FILE,             // No such SPTS file
   SPTS_CC_BAD_PACKET,           // Wrong SPTS packet size
   SPTS_CC_BAD_FORMAT,           // Bad SPTS format
   SPTS_CC_INTERRUPTED,          // Scan was interrupted
                                 // 
   NUM_SPTS_CC                   // Number of CCs
}  SPTSCC;

//
// HMT transfer direction (Stream->Cat and vv.
//
typedef enum HMTDIR
{
   HMT_STREAM_TO_CAT  = 0,       // Stream to catalog
   HMT_CAT_TO_STREAM,            // Catalog to stream
   NUM_HMT_DIRS                  // Number
}  HMTDIR;

//
// MPEG2 Frame types
//
typedef enum FRTYPE
{
   FRAME_TYPE_NONE = 0,          // Frame types from Picture Header
   FRAME_TYPE_I,                 // I-Frame
   FRAME_TYPE_P,                 // P-Frame
   FRAME_TYPE_B,                 // B-Frame
   FRAME_TYPE_D,                 // D-Frame

   NUM_FRAME_TYPES               // Number of MPEG-2 frame types
}  FRTYPE;

//
// TS Progress bar
//
typedef enum PRBAR
{
   BAR_INIT = 0,                 // Init the progress bar
   BAR_STEP,

   NUM_PRBARS
}  PRBAR;

//
// TS Button status
//
typedef enum STBTN
{
   BTN_INIT     = 0,             // Init the status buttons
   BTN_UPDATE,                   // Update the buttons

   NUM_STBTNS
}  STBTN;

//
// NTS Update types
//
typedef enum NTSUPD
{
   NTS_UPDATE_AND = 0,
   NTS_UPDATE_OR,

   NUM_NTSUPD
}  NTSUPD;

//
// NTS SD/HD switch
//
typedef enum NTSDEF
{
   NTS_DEF_AUTO = 0,             // Auto SD/HD
   NTS_DEF_SD,                   // SDTV
   NTS_DEF_HD,                   // HDTV
   NTS_DEF_AUTO_SD,              // Auto-detected-SD
   NTS_DEF_AUTO_HD,              // Auto-detected-HD

   NUM_NTSDEFS
}  NTSDEF;


#define  HMT_MIN_RECORDING_DURATION    60
#define  HMT_NO_PID                    0x2000
//
// Default settings for the catalog
//
#define  HMT_REC_IS_OKEE               0x50
#define  HMT_ICON_NEW                  0x20
#define  HMT_ICON_HD                   0x80
#define  HMT_DEFAULT_CH                777

#define  TS_SYNC_WINDOW_SMALL          1000
#define  TS_SYNC_WINDOW_BIG            0x7FFFFFFF
#define  TS_MAX_SYNC_COUNT             5

#define  TS_MAX_PAYLOAD_SIZE           1024
#define  TS_DEF_PACKET_SIZE            192

#define  TS_FLAG_TEI                   0x8000
#define  TS_FLAG_PUSI                  0x4000
#define  TS_FLAG_TP                    0x2000
#define  TS_MASK_PID                   0x1fff

#define  TS_NALU_MASK                  0x1f
#define  TS_NALU_SYSTEM_CODES          0xB0

#define  TS_FLAG_SCRAMBLED             0x80
#define  TS_FLAG_CONTROL               0x40
#define  TS_FLAG_ADAPTFLD              0x20
#define  TS_FLAG_PAYLOAD               0x10
#define  TS_MASK_CC                    0x0f

#endif // !defined(_HMXENUMS_H_)
