/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxPageGeneric.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for generic issues
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#if !defined(_AFX_HMXPAGEGENERIC_H_)
#define _AFX_HMXPAGEGENERIC_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AutoFont.h"
#include "ReadOnlyEdit.h"
#include "afxwin.h"
#include "afxcmn.h"


class CHmxPageGeneric : public CPropertyPage
{
	DECLARE_DYNCREATE(CHmxPageGeneric)

// Construction
public:
	CHmxPageGeneric                     ();
  ~CHmxPageGeneric                     ();

   void           LoadDialog           ();
   void           StoreDialog          ();
   void           UpdateDialog         ();
   void           CreateIndexSpin      ();
   //
   static void    RequestWndHandle     (HWND);

// Dialog Data
	//{{AFX_DATA(CHmxPageGeneric)
	enum { IDD = IDD_PROP_HMX_GENERIC };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CHmxPageGeneric)
	public:
	virtual void OnOK();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
   CNvmStorage      *pclNvm;              // Storage structure
   HMXCOM           *pstHmxComm;          // Communication srtucture
   int               iNumEs;              // Nr ES streams 1..?
   int               iIdxEs;              // Es selector index 1..?
   
public:
   CReadOnlyEdit     clGenEditInfile;     // Event name
   CReadOnlyEdit     clGenEditPid;        // PID field
   CReadOnlyEdit     clGenEditPsiPackets; // PSI Packets
   CReadOnlyEdit     clGenEditPesPackets; // PES Packets
   CReadOnlyEdit     clGenEditPesName;    // PES Name
   CReadOnlyEdit     clGenEditRecStart;   // Event recording start time
   CReadOnlyEdit     clGenEditRecEnd;     // Event recording end   time
   //
   CEdit             clGenEditIndex;      // TS PES Index 
   CSpinButtonCtrl   clGenSpinButton;     // TS PES Spin buttons up/dn
   //


protected:
	// Generated message map functions
	//{{AFX_MSG(CHmxPageGeneric)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
   afx_msg void      OnBnSpinEsIndex         (NMHDR *, LRESULT *);
   afx_msg LRESULT   OnRcvWindowData         (WPARAM, LPARAM);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_AFX_HMXPAGEGENERIC_H_)
