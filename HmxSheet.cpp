/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          HmxSheet.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Setup the property pages for the dialogs
**
**                      
**                      
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:       
**
 *  Author:             Peter Hillen
 *  Date Created:       10Apr2009
**
 *  Revisions:
 *    $Log:   $
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
**/

#include "stdafx.h"
#include "HmxControl.h"
#include "HmxSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(CHmxSheet, CPropertySheet)

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxSheet::CHmxSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
    AddPage(&m_HmxPageGeneric);
    AddPage(&m_HmxPageTs);
    AddPage(&m_HmxPageHmt);
    AddPage(&m_HmxPageNts);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxSheet::CHmxSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
    AddPage(&m_HmxPageGeneric);
    AddPage(&m_HmxPageTs);
    AddPage(&m_HmxPageHmt);
    AddPage(&m_HmxPageNts);
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
CHmxSheet::~CHmxSheet()
{
}


BEGIN_MESSAGE_MAP(CHmxSheet, CPropertySheet)
	//{{AFX_MSG_MAP(CHmxSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CHmxSheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	
	return bResult;
}

//
//  Function:   
//  Purpose:    
//  Parms:      
//  Returns:    
//
BOOL CHmxSheet::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	return CPropertySheet::OnNotify(wParam, lParam, pResult);
}

